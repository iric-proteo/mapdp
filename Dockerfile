# Dockerfile for MAPDP Django application

FROM gitlab.iric.ca:4567/proteo-public/images/mapdp:2022.03.07

# FROM python:3.8-buster

# # File Author / Maintainer
# MAINTAINER Mathieu Courcelles

# RUN apt-get update -qq && \
#     apt-get install -y \
#             default-jre \
#             graphviz \
#             libblas-dev \
#             liblapack-dev \
#             libatlas-base-dev \
#             gfortran \
#     && rm -rf /var/lib/apt/lists/*

# # Ensure that Python outputs everything that's printed inside
# # the application rather than buffering it.
# ENV PYTHONUNBUFFERED 1

# RUN mkdir -p /usr/src/app  && \
#     chown www-data:www-data /usr/src/app && \
#     mkdir -p /var/www && chown www-data:www-data /var/www

# # Install Nextflow
# USER www-data
# RUN mkdir -p /usr/src/app/bin
# WORKDIR /usr/src/app/bin
# RUN curl -s https://get.nextflow.io | bash
# USER root

# # Prepare Python environment
# WORKDIR /usr/src/app
# RUN pip install --no-cache-dir pipenv
# COPY Pipfile* /usr/src/app/
# RUN /usr/local/bin/pipenv install --system --deploy

# COPY services/gunicorn /usr/src/app/services/gunicorn

COPY src /usr/src/app/src

RUN chown www-data:www-data /usr/src/app \
    && chown www-data:www-data /usr/src/app/src/*.sh \
    && chmod 700 /usr/src/app/src/*.sh

ARG CI_COMMIT_SHA
ENV GIT_SHA $CI_COMMIT_SHA

RUN export TODAY=$(date +"%Y/%m/%d") && \
    sed -i "s|DEV|$TODAY|g" /usr/src/app/src/base_site/templates/footer.html


USER www-data

WORKDIR /usr/src/app/src

CMD ["./init.sh"]
