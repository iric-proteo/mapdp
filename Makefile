TEST=mhc_pipeline

unittest:
	cd src && \
	coverage run manage.py test --failfast --settings=base_site.settings.test $(TEST) && \
	coverage html -i --omit="*virtualenv*,*migrations*" && \
	cd ..
