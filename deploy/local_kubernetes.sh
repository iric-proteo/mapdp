#!/bin/bash

# Deployment parameters

# Services password
MINIO_PASSWORD='changeme'
POSTGRES_PASSWORD='changeme'
PPS_TOKEN='change_me_' # Can be changed only using the PPS web UI after first deployment
SECRET_KEY='changeme'

# Docker images
MIXMHCP_CONTAINER='mixmhcp:build'
MODEC_CONTAINER='modec:build'
NETMHC_CONTAINER='netmhc-services:latest'


# Deployment script - DO NOT MODIFY

if [ ! -d "$HOME/mapdp-local" ] 
then
    mkdir -p $HOME/mapdp-local
    mkdir -p $HOME/mapdp-local/media_root
    touch $HOME/mapdp-local/media_root/fake
    chmod 777 $HOME/mapdp-local/media_root
    chmod 777 $HOME/mapdp-local/media_root/fake

    mkdir -p $HOME/mapdp-local/ms
    chmod 777 $HOME/mapdp-local/ms
    touch $HOME/mapdp-local/ms/fake
    chmod 777 $HOME/mapdp-local/ms/fake

    mkdir -p $HOME/mapdp-local/nextflow
    chmod 777 $HOME/mapdp-local/nextflow

    mkdir -p $HOME/mapdp-local/shared_data
    touch $HOME/mapdp-local/shared_data/fake
    chmod 777 $HOME/mapdp-local/shared_data
    chmod 777 $HOME/mapdp-local/shared_data/fake

    mkdir -p $HOME/mapdp-local/pps/media_root
    chmod 777 $HOME/mapdp-local/pps/media_root

    mkdir -p $HOME/mapdp-local/pps/www
    chmod 777 $HOME/mapdp-local/pps/www

fi

mkdir -p customized

k3d cluster create mapdp-local --volume $HOME/mapdp-local:/data --port "8081:80@loadbalancer"
k3d image import $MIXMHCP_CONTAINER -c mapdp-local
k3d image import $MODEC_CONTAINER -c mapdp-local
k3d image import $NETMHC_CONTAINER -c mapdp-local

kubectl label nodes k3d-mapdp-local-server-0 avx=true


kubectl apply -f ./templates/mapdp-namespace.yaml
kubectl apply -f ./templates/nextflow-namespace.yaml

# Postgres
sed -r "s|POSTGRES_PASSWORD_R|$POSTGRES_PASSWORD|g" ./templates/postgres-deployment.yaml > ./customized/postgres-deployment.yaml
kubectl apply -f ./customized/postgres-deployment.yaml
kubectl apply -f ./templates/postgres-service.yaml

# Redis
kubectl apply -f ./templates/redis-deployment.yaml
kubectl apply -f ./templates/redis-service.yaml

# Nextflow
kubectl apply -f ./templates/nextflow-serviceaccount.yaml
kubectl apply -f ./templates/nextflow-role.yaml
kubectl apply -f ./templates/nextflow-rolebinding.yaml

kubectl apply -f ./templates/nextflow-pv.yaml
kubectl apply -f ./templates/nextflow-pvc.yaml

# Minio
sed -r "s|MINIO_PASSWORD_R|$MINIO_PASSWORD|g" ./templates/minio-deployment.yaml > ./customized/minio-deployment.yaml
kubectl apply -f ./customized/minio-deployment.yaml
kubectl apply -f ./templates/minio-service.yaml

# NetMHC

sed -r "s|NETMHC_CONTAINER_R|$NETMHC_CONTAINER|g" ./templates/netmhc-deployment.yaml > ./customized/netmhc-deployment.yaml
kubectl apply -f ./customized/netmhc-deployment.yaml
kubectl apply -f ./templates/netmhc-service.yaml

# Beat
sed -r "s|MINIO_PASSWORD_R|$MINIO_PASSWORD|g" ./templates/beat-deployment.yaml > ./customized/beat-deployment.yaml
sed -i -r "s|MIXMHCP_CONTAINER_R|$MIXMHCP_CONTAINER|g" ./customized/beat-deployment.yaml
sed -i -r "s|MODEC_CONTAINER_R|$MODEC_CONTAINER|g" ./customized/beat-deployment.yaml
sed -i -r "s|POSTGRES_PASSWORD_R|$POSTGRES_PASSWORD|g" ./customized/beat-deployment.yaml
sed -i -r "s|PPS_TOKEN_R|$PPS_TOKEN|g" ./customized/beat-deployment.yaml
sed -i -r "s|SECRET_KEY_R|$SECRET_KEY|g" ./customized/beat-deployment.yaml
kubectl apply -f ./customized/beat-deployment.yaml

# PPS
sed -r "s|POSTGRES_PASSWORD_R|$POSTGRES_PASSWORD|g" ./templates/pps-app-nginx-deployment.yaml > ./customized/pps-app-nginx-deployment.yaml
sed -i -r "s|SECRET_KEY_R|$SECRET_KEY|g" ./customized/pps-app-nginx-deployment.yaml
kubectl apply -f ./templates/pps-nginx-configmap.yaml
kubectl apply -f ./customized/pps-app-nginx-deployment.yaml
kubectl apply -f ./templates/pps-app-nginx-service.yaml

sed -r "s|POSTGRES_PASSWORD_R|$POSTGRES_PASSWORD|g" ./templates/pps-worker-deployment.yaml > ./customized/pps-worker-deployment.yaml
sed -i -r "s|SECRET_KEY_R|$SECRET_KEY|g" ./customized/pps-worker-deployment.yaml
kubectl apply -f ./customized/pps-worker-deployment.yaml

# Worker
sed -r "s|MINIO_PASSWORD_R|$MINIO_PASSWORD|g" ./templates/worker-deployment.yaml > ./customized/worker-deployment.yaml
sed -i -r "s|MIXMHCP_CONTAINER_R|$MIXMHCP_CONTAINER|g" ./customized/worker-deployment.yaml
sed -i -r "s|MODEC_CONTAINER_R|$MODEC_CONTAINER|g" ./customized/worker-deployment.yaml
sed -i -r "s|POSTGRES_PASSWORD_R|$POSTGRES_PASSWORD|g" ./customized/worker-deployment.yaml
sed -i -r "s|PPS_TOKEN_R|$PPS_TOKEN|g" ./customized/worker-deployment.yaml
sed -i -r "s|SECRET_KEY_R|$SECRET_KEY|g" ./customized/worker-deployment.yaml
kubectl apply -f ./templates/worker-serviceaccount.yaml
kubectl apply -f ./customized/worker-deployment.yaml

# App
sed -r "s|MINIO_PASSWORD_R|$MINIO_PASSWORD|g" ./templates/app-nginx-deployment.yaml > ./customized/app-nginx-deployment.yaml
sed -i -r "s|MIXMHCP_CONTAINER_R|$MIXMHCP_CONTAINER|g" ./customized/app-nginx-deployment.yaml
sed -i -r "s|MODEC_CONTAINER_R|$MODEC_CONTAINER|g" ./customized/app-nginx-deployment.yaml
sed -i -r "s|POSTGRES_PASSWORD_R|$POSTGRES_PASSWORD|g" ./customized/app-nginx-deployment.yaml
sed -i -r "s|PPS_TOKEN_R|$PPS_TOKEN|g" ./customized/app-nginx-deployment.yaml
sed -i -r "s|SECRET_KEY_R|$SECRET_KEY|g" ./customized/app-nginx-deployment.yaml
kubectl apply -f ./templates/nginx-configmap.yaml
kubectl apply -f ./customized/app-nginx-deployment.yaml
kubectl apply -f ./templates/app-nginx-service.yaml

# Ingress
kubectl apply -f ./templates/ingress.yaml













