# MHC-associated peptide discovery platform (MAPDP)

The MHC-associated peptide discovery platform (**MAP**DP) was conceived to process routine data sets from mass spectrometry 
immopeptidomics experiments as well as bigger and long term projects. It serves 
as an in house repository to store, process and query multiple immopeptidomics 
data sets. 

\
&nbsp;

# Try MAPDP on our demo site

&nbsp;

**MAP**DP can be tried here: https://demo-mapdp.proteo.iric.ca

**user name:** demo

**password:** demo-mapdp

&nbsp;

**Notes**:
* Only pre-loaded data sets are available. It is not possible to upload any data set to the demo server. 
* MixMHCp, Modec and NetMHC predictors are not available on the demo site because of licensing restriction.

\
&nbsp;


# Deployment of MAPDP on a local Kubernetes cluster

&nbsp;

This part provides instructions to deploy a local Kubernetes cluster on a Linux 
workstation with MADPP.

&nbsp;

**Notes**: 
- This MAPDP deployment was tested on Ubuntu Linux 21.04 with Docker 20.10.8, kubectl 1.22 and K3D 4.4.8. The following instructions might depend on it. 
- This deployment is made for simplicity. **It should not be considered secure**.
\
&nbsp;


## Step 1 - Install Docker

&nbsp;

MAPDP and other required bioinformatics applications are packaged in Docker containers for a simple and reproducible deployment.

&nbsp;

a. Follow the Docker installation instructions for your Linux distribution (see https://docs.docker.com/engine/install/). 

b. Allow your Linux user to access the Docker deamon (see https://docs.docker.com/engine/install/linux-postinstall/)

&nbsp;



## Step 2 - Install K3D

&nbsp;

K3D (https://k3d.io/) is used to deploy a local light weight Kubernetes cluster.

&nbsp;


    $ wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash


 
&nbsp;

## Step 3 - Install kubectl

&nbsp;

kubectl controls the Kubernetes cluster manager.

&nbsp;

Follow kubectl installation instruction for your Linux distribution (see https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

 
&nbsp;

## Step 4 - Build optional softwares Docker images

&nbsp;

Because of licensing restriction, Docker images for the following softwares must be manually built.

Follow instructions on these links to build these optional Docker images:

* [MixMHCp](https://gitlab.com/iric-proteo/mixmhcp)
* [MoDec](https://gitlab.com/iric-proteo/modec)
* [NetMHC services](https://gitlab.com/iric-proteo/netmhc-services)

&nbsp;

## Step 5 - Retrieve MAPDP source code

&nbsp;

Deployment code is available within the MAPDP repository:


    $ git clone https://gitlab.com/iric-proteo/mapdp
    $ cd mapdp/deploy

&nbsp;

## Step 6 - Customize deployment parameters

&nbsp;

* Open **local_kubernetes.sh** with you favorite text editor
* Customize deployment parameters
  * Change default passwords
  * Change Docker image names for MixMHCp, MoDec and NetMHC
* Save the file.

&nbsp;

## Step 7 - Deploy MAPDP

&nbsp;

Now lets create the local Kubernetes cluster with K3D and then deploy MAPDP to it.

&nbsp;


Run the deployment script: 

    $ bash ./local_kubernetes.sh

&nbsp;

Wait for the all service pods to come online. You can check pods status with this command:

    $ kubectl get pods --all-namespaces

Status should be **Running** for all pods in the **mapdp** namespace.

&nbsp;

## Step 8 - Access MAPDP

&nbsp;

To access MAPDP, open you browser and go to http://127.0.0.1:8081/

&nbsp;

**user name:** mapdp-admin

**password:** change_me_

&nbsp;

**Note**: It is recommended to change the admin password after the first login.

&nbsp;

Consult the user manual to learn how to use **MAP**DP.

&nbsp;

MAPDP data are stored in **$HOME/mapdp-local**. To load MS data (raw files, PEAKS results) into MAPDP, you must place these files in sub-folders in **$HOME/mapdp-local/ms**.

&nbsp;

## Step 9 - Stop/restart/delete MAPDP

&nbsp;


To shutdown MADP: 

    $ k3d cluster stop mapdp-local


&nbsp;


To restart MADP:

    $ k3d cluster start mapdp-local

&nbsp;



To delete the cluster and data:

    $ k3d cluster delete mapdp-local
    $ sudo rm -rf $HOME/mapdp-local


&nbsp;

# Windows 10 deployment

&nbsp;

To use MAPDP in Windows 10, you must install Docker Desktop WSL 2 backend (See https://docs.docker.com/desktop/windows/wsl/) and then follow the above procedure.

&nbsp;

# License
&nbsp;

AGPL-3.0-or-later

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

&nbsp;

Copyright 2015-2021 Mathieu Courcelles

CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses

Pierre Thibault's lab

IRIC - Universite de Montreal
