"""
Copyright 2015-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries
from django.shortcuts import render

# Third party libraries

# Import project libraries
from .exceptions import BaseSiteException


class BaseSiteExceptionMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def process_exception(self, request, exception):
        """
        Looks for BaseSiteException and renders message page.

        :Parameters:
           - `request`: request that caused the exception
           - `exception`: actual exception being raised
        """

        if isinstance(exception, BaseSiteException):
            return render(request, f'{exception.level}.html',
                          {exception.level: exception.message})
