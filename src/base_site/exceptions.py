"""
Copyright 2015-2016 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries

# Third party libraries

# Import project libraries


class BaseSiteException(Exception):

    def __init__(self, level, message):
        self.level = level
        self.message = message
