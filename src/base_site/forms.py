"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from registration.users import UserModel, UsernameField
from zxcvbn_password.fields import PasswordField, PasswordConfirmationField

# Import project libraries


User = UserModel()


class PasswordChangeFormCustom(PasswordChangeForm):
    """
    Custom form to change password with Python django-zxcvbn-password==2.0.0
    """

    new_password1 = PasswordField()
    new_password2 = PasswordConfirmationField(confirm_with='new_password1')


class RegistrationFormCustom(UserCreationForm):
    """
    Form for registering a new user account.

    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.

    Subclasses should feel free to add any additional validation they
    need, but should avoid defining a ``save()`` method -- the actual
    saving of collected user data is delegated to the active
    registration backend.

    """
    required_css_class = 'required'

    email = forms.EmailField(label='E-mail')

    first_name = forms.CharField(max_length=30)

    last_name = forms.CharField(max_length=30)

    password1 = PasswordField()
    password2 = PasswordConfirmationField(confirm_with='password1')

    class Meta:
        model = User
        fields = (UsernameField(), 'email', 'first_name', 'last_name')

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.

        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(
                'This email address is already in use. Please supply a different email address.')
        return self.cleaned_data['email']
