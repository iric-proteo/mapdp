"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os

# Import Django related libraries

# Import project libraries


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'This should be changed or overwritten'


DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'OPTIONS': {
            'options': '-c search_path=mapdp'
        },
        'HOST': os.environ.get('POSTGRES_HOST', ''),
        'NAME': os.environ.get('POSTGRES_DB', ''),
        'USER': os.environ.get('POSTGRES_USER', ''),
        'PASSWORD': '',
        'CONN_MAX_AGE': 300,
    },

}

# Application definition
INSTALLED_APPS = [

    # Visual enhancement for admin
    'grappelli',

    # Django
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third party apps
    'chunked_upload',
    'crispy_forms',
    'defender',
    'ajax_select',
    'django_celery_results',
    'guardian',
    'health_check',
    'health_check.cache',
    'health_check.contrib.celery',
    'health_check.db',
    'health_check.storage',
    'registration',
    'reversion',
    'session_security',
    'zxcvbn_password',

    # My apps
    'library',
    'base_site',
    'mhc_pipeline',
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',  # default
    'guardian.backends.ObjectPermissionBackend',
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 10,
            # 14 characters recommended by the CIS password policy guide
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
    {
        'NAME': 'zxcvbn_password.ZXCVBNValidator',
        'OPTIONS': {
            'min_score': 3,
            'user_attributes': ('username', 'email', 'first_name', 'last_name')
        }
    }
]

MIDDLEWARE = [
    'base_site.exception_middleware.BaseSiteExceptionMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'session_security.middleware.SessionSecurityMiddleware',
    'reversion.middleware.RevisionMiddleware',
]

ROOT_URLCONF = 'base_site.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'base_site', 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'base_site.context_processor.site_processor',
                'django_settings_export.settings_export',
            ],
        },
    },
]

WSGI_APPLICATION = 'base_site.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'America/Montreal'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static_root')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)
# Project static are sent to STATIC_ROOT to be served
# python manage.py collectstatic

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'media_root')

DATA_UPLOAD_MAX_MEMORY_SIZE = 15728640
FILE_UPLOAD_MAX_MEMORY_SIZE = 15728640

SITE_ID = 1

# Third party apps settings

# Django guardian
ANONYMOUS_USER_ID = -1

# Django registration redux settings
ACCOUNT_ACTIVATION_DAYS = 7
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/accounts/login/'
REGISTRATION_AUTO_LOGIN = False
REGISTRATION_EMAIL_HTML = False
REGISTRATION_OPEN = True
SEND_ACTIVATION_EMAIL = False

# Crispy form tags settings
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# Session security
SESSION_SECURITY_WARN_AFTER = 1800 - 60 * 5  # seconds
SESSION_SECURITY_EXPIRE_AFTER = 1800  # seconds
# Higher than the 15 minutes recommended by the CIS password policy guide

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# Data path
MS_DIR = ''


MATOMO_SITEID = os.getenv('MATOMO_SITEID')
MATOMO_URL = os.getenv('MATOMO_URL')

SETTINGS_EXPORT = [
    'MATOMO_SITEID',
    'MATOMO_URL',
]

MINIO_EXPIRATION_DAYS = -1