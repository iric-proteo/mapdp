"""
Copyright 2015-2018 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

import os

# Django settings #############################################################

SECRET_KEY = 'None'
TIME_ZONE = os.environ.get('TIME_ZONE', '')

# Celery

CELERY_BROKER_URL = 'redis://redis:6379/2'
CELERY_TIMEZONE = TIME_ZONE
