"""
Copyright 2015-2021 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from socket import gethostname, gethostbyname

# Import Django related libraries
from celery.schedules import crontab

# Import third party libraries

# Import project libraries
from .base import *
from ..secrets import get_secret

# Django settings #############################################################

ADMINS = (
    (os.environ.get('ADMIN_NAME', ''), os.environ.get('ADMIN_EMAIL', '')),
)

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '').split()
ALLOWED_HOSTS += ['0.0.0.0', gethostname(), gethostbyname(gethostname())]

REDIS = f'redis://{os.environ.get("REDIS_HOST")}'

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': REDIS + '/1',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'IGNORE_EXCEPTIONS': True,
        }
    },
    # 'ident': {
    #     'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
    #     'LOCATION': os.path.join(os.path.dirname(BASE_DIR), 'tmp',
    #                              'ident_cache'),
    # }
}

if os.environ.get('COOKIE_SECURE', False):
    CSRF_COOKIE_SECURE = True
    SESSION_COOKIE_SECURE = True
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': os.environ.get('POSTGRES_HOST', ''),
        'NAME': os.environ.get('POSTGRES_DB', ''),
        'USER': os.environ.get('POSTGRES_USER', ''),
        'PASSWORD': get_secret('POSTGRES_PASSWORD'),
        'CONN_MAX_AGE': 300,
    },

}

if os.environ.get('POSTGRES_SCHEMA', False):
    DATABASES['default']['OPTIONS'] = {
            'options': '-c search_path=' + os.environ.get('POSTGRES_SCHEMA')
        }

# Mail server configuration
EMAIL_HOST = os.environ.get('EMAIL_HOST', '')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = get_secret('EMAIL_HOST_PASSWORD')
EMAIL_PORT = 587
EMAIL_SUBJECT_PREFIX = os.environ.get('HOSTNAME', 'MAPDP')
EMAIL_USE_TLS = True


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'json',
        },

    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
        },
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
        },
        'elasticapm': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
        },            
    },
    'formatters': {
        'json': {
            '()': 'pythonjsonlogger.jsonlogger.JsonFormatter',
            'format': '%(asctime) %(name) %(processName) %(filename) %(funcName)'
                      ' %(levelname) %(lineno) %(module) %(threadName) %(message)',
        },
    },
}


MANAGERS = (
    (os.environ.get('ADMIN_NAME', ''), os.environ.get('ADMIN_EMAIL', '')),
)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_secret('SECRET_KEY')

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

# Cache templates
TEMPLATES[0]['APP_DIRS'] = False
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    ]),
]

TIME_ZONE = os.environ.get('TIME_ZONE', '')


# Third party settings ########################################################

# Cacheops
INSTALLED_APPS += ['cacheops']
CACHEOPS_REDIS = REDIS + '/4'

CACHEOPS_DEFAULTS = {
    'timeout': 60*60
}

CACHEOPS = {
    'auth.user': {'ops': ('fetch', 'get'), 'timeout': 60*15},
    'auth.*': {'ops': ('fetch', 'get')},
    'auth.permission': {'ops': 'all'},
    'contenttypes.contenttype': {'local_get': True, 'ops': 'all', },
    'guardian.groupobjectpermission': {'ops': 'all'},
    'mhc_pipeline.biologicalsample': {'ops': ('get',)},
    'mhc_pipeline.biologicalsamplemhcallele': {'ops': ('get',)},
    'mhc_pipeline.filter': {'ops': ('get',)},
    'mhc_pipeline.immunopeptidome': {'ops': ('get',)},
    'mhc_pipeline.mhcallele': {'ops': ('fetch', 'get')},
    'mhc_pipeline.mhcsample': {'ops': ('fetch', 'get')},
    'sites.site': {'local_get': True, 'ops': 'all', },
    '*.*': {},
}
CACHEOPS_DEGRADE_ON_FAILURE = True

# Celery
CELERY_BROKER_URL = f'amqp://{get_secret("RABBITMQ_DEFAULT_USER")}:' \
                    f'{get_secret("RABBITMQ_DEFAULT_PASS")}@{os.environ.get("RABBITMQ_HOST")}'
CELERY_RESULT_BACKEND = 'django-cache'
CELERY_TIMEZONE = TIME_ZONE
CELERY_BEAT_SCHEDULE = {
    'peptide_counts': {
        'task': 'mhc_pipeline.tasks.plots.peptide_counts',
        'schedule': crontab(day_of_week="*", hour=1, minute=0),
    },
}
CELERY_TASK_ROUTES = {
    'mhc_pipeline.tasks.genotyping.genotype': {'queue': 'long'},
    'mhc_pipeline.tasks.import_identification': {'queue': 'mem'}
}


# Django Defender
DEFENDER_BEHIND_REVERSE_PROXY = True
DEFENDER_LOGIN_FAILURE_LIMIT = 5  # CIS password policy guide
DEFENDER_COOLOFF_TIME = 900  # CIS password policy guide
DEFENDER_REDIS_URL = REDIS + '/3'
DEFENDER_USE_CELERY = True
MIDDLEWARE.append('defender.middleware.FailedLoginMiddleware')

# Django-health-check
HEALTHCHECK_CELERY_TIMEOUT = 30

# Django-xforwardedfor-middleware
MIDDLEWARE.insert(0, 'x_forwarded_for.middleware.XForwardedForMiddleware')

DEMO = os.environ.get('DEMO')

# Elasticsearch APM
if os.environ.get('ELASTIC_APM_HOST', False):
    import urllib3
    urllib3.disable_warnings()
    ELASTIC_APM = {
        'CLOUD_PROVIDER': 'none',
        'SERVICE_NAME': 'mapdp',
        'SERVER_URL': os.environ.get('ELASTIC_APM_HOST'),
        'SECRET_TOKEN': get_secret('ELASTIC_APM_TOKEN'),
        'VERIFY_SERVER_CERT': False,
    }
    INSTALLED_APPS += ['elasticapm.contrib.django']

    MIDDLEWARE.insert(1, 'elasticapm.contrib.django.middleware.Catch404Middleware')
    MIDDLEWARE.insert(1, 'elasticapm.contrib.django.middleware.TracingMiddleware')

# MHC pipeline
MS_DIR = '/mnt/ms'


# Minio storage
AWS_S3_ENDPOINT_URL = os.environ.get('MINIO_HOST')
AWS_ACCESS_KEY_ID = get_secret('MINIO_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_secret('MINIO_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = 'identifications'
AWS_AUTO_CREATE_BUCKET = True
AWS_BUCKET_ACL = None
AWS_DEFAULT_ACL = None
MINIO_EXPIRATION_DAYS = 7

# Nextflow
NEXTFLOW_FS = '/mnt/nextflow'

# Personalized proteome service
PPS_HOST = os.environ.get('PPS_HOST', '')
PPS_TOKEN = get_secret('PPS_TOKEN')

SHARED_DATA = '/mnt/shared_data'

