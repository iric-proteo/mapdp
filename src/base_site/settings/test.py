"""
Copyright 2015-2020 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

print('Loading capaMHC test settings...\n')

# Import standard libraries
from pathlib import Path

# Import Django related libraries

# Import project libraries
from .base import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'bg4ez744v#ukitt@k6=+4y%q$j_n1*12*1+zbf3mg&6-@8=)o='

DEBUG = True
LOCAL = True

p = Path(__file__).resolve().parent / 'local.py'
LOCAL_BROWSER = p.exists()

ADMINS = (
    ('Test', 'fake221213243@localhost'),
)

########## IN-MEMORY TEST DATABASE

DATABASES = {

    'default': {

        'ENGINE': 'django.db.backends.sqlite3',
        'OPTIONS': {
            'timeout': 30,
        },
        'NAME': os.path.join(BASE_DIR, '..', 'db', 'db.sqlite3.test'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',

    },

}

os.environ['REUSE_DB'] = "1"

MEDIA_ROOT = '/tmp/media'
Path(MEDIA_ROOT).mkdir(exist_ok=True)

# Data path
MS_DIR = r'test_files'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            # 'formatter': 'json',
            'level': 'DEBUG',
        },
        # Log to a text file that can be rotated by logrotate
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '../django_test.log'
        },
    },
    'loggers': {
        #Might as well log any errors anywhere else in Django
        # '': {
        #     'handlers': ['console', 'logfile'],
        #     'level': 'DEBUG',
        #     'propagate': False
        # },
        # 'django': {
        #     'handlers': ['logfile', 'console'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },
        # 'selenium': {
        #     'handlers': ['logfile', 'console'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },
    },
}

# Jenkins
PROJECT_APPS = ['library',
                'base_site',
                'mhc_pipeline']


CELERY_TASK_ALWAYS_EAGER=True

os.environ['MHCFLURRY_URL'] = 'http://localhost'

# NetMHC
os.environ['NETMHC_URL'] = 'http://localhost'

CACHES = {
    # 'default': {
    #     'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    # },
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': os.path.join(os.path.dirname(BASE_DIR), 'tmp', 'cache_test'),
    },
    'mem': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache'
    }
}


PPS_HOST = ''
PPS_TOKEN = ''

SHARED_DATA = r'test_files'

os.environ['GIT_SHA'] = 'local'

NEXTFLOW_FS = MEDIA_ROOT + '/nextflow'
DEMO = False