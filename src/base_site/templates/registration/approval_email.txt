{% load i18n %}
Hello there!

Click the link below to set permissions to the new account on {{ site.name }}.

http://{{ site.domain }}/admin/auth/user/{{ user.pk }}/


Click here to transmit the activation email to the user:

http://{{ site.domain }}{% url 'registration_email' user.pk activation_key %}

-Team {{ site.name }}
