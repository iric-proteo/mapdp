from django import template
register = template.Library()


@register.filter
def get_attribute(obj, key):

    return obj.__getattribute__(key)
