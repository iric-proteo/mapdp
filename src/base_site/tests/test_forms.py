"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..forms import RegistrationFormCustom


class FormTest(BaseTest):

    def test_RegistrationFormCustom_empty(self):

        form = RegistrationFormCustom()
        self.assertFalse(form.is_valid())

    def test_RegistrationFormCustom_clean_email(self):

        form_data = {'username': 'test',
                     'email': 'm@c.com',
                     'first_name': 'Test',
                     'last_name': 'Toto',
                     'password1': 'test12345test',
                     'password2': 'test12345test',
                     }

        form = RegistrationFormCustom(data=form_data)
        form.save()

        form_data = {'username': 'test2',
                     'email': 'm@c.com',
                     'first_name': 'Test',
                     'last_name': 'Toto',
                     'password1': 'test12345test',
                     'password2': 'test12345test',
                     }

        form2 = RegistrationFormCustom(data=form_data)
        form2.is_valid()

        self.assertNotIn('email', form2.cleaned_data)

