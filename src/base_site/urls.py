"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.contrib.auth.views import (PasswordChangeView,
                                       PasswordChangeDoneView)
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.template.response import TemplateResponse
from django.views.generic.base import RedirectView

from base_site.views.registration_custom import RegistrationView

# Third party libraries
from ajax_select import urls as ajax_select_urls

# Import project libraries
from .forms import PasswordChangeFormCustom, RegistrationFormCustom
import mhc_pipeline.views.base_site
from .views.base_site import alive, server_secure_media


def handler500(request):
    """500 error handler which includes ``request`` in the context.

    Templates: `500.html`
    Context: None
    """

    context = {'request': request}

    template_name = '500.html'  # You need to create a 500.html template.
    return TemplateResponse(request, template_name, context, status=500)


urlpatterns = [

    # My apps
    url(r'mhc_pipeline/', include('mhc_pipeline.urls')),

    url(r'session_security/', include('session_security.urls')),

    # Dashboard
    url(r'^$', mhc_pipeline.views.base_site.home, name='home'),

    url(r'^accounts/password/change/$',  # hijack password_change's url
        PasswordChangeView.as_view(form_class=PasswordChangeFormCustom),
        name="password_change"),

    url(r'^password/change/done/$',
        PasswordChangeDoneView.as_view(),
        name='password_change_done'),

    url(r'^accounts/', include('registration.backends.default.urls')),

    url(r'^favicon.ico$',
        RedirectView.as_view(url=staticfiles_storage.url('img/favicon.ico'),
                             permanent=False),
        name="favicon"),

    # Admin
    url(r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
    url(r'^admin/', admin.site.urls),
    url(r'^admin/defender/', include('defender.urls')),  # defender admin


    # Authentication
    url(r'^accounts/register/$',
        RedirectView.as_view(url='/accounts/register/closed/', permanent=False),
        name='registration_closed'),

    url(r'^accounts/register_mapdp/$',
        RegistrationView.as_view(form_class=RegistrationFormCustom),
        name='registration_register'),

    url(
        r'^accounts/register_iric/email/(?P<user_pk>\d+)/(?P<activation_key>\w+)/$',
        RegistrationView.transmit_activation_mail,
        name='registration_email'),

    url(r'^alive/', alive),
    url(r'^hc/', include('health_check.urls')),

    url(r'^media/(?P<url>.+)$', server_secure_media),

    url(r'^ajax_select/', include(ajax_select_urls)),
]


# Local only
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)

    import debug_toolbar

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]