"""
Copyright 2015-2018 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.views import static


# Third party libraries

# Import project libraries


def alive(request):
    """
    Returns HttpResponse with status 204
    :param request: HttpRequest
    :return: HttpResponse
    """

    return HttpResponse(status=204)


@login_required()
def server_secure_media(request, url: str, staff_only: bool=True):
    """
    Serves media file securely. User must be logged and optionally be a staff
    member (default for security).
    :param request: Http request
    :param url: File location
    :param staff_only: Control if only staff users can see media files. If
    disabled, a view is responsible for checking permission.
    :return: Http response
    """

    if staff_only:
        if not request.user.is_staff:
            raise PermissionDenied

    if settings.DEBUG:

        response = static.serve(request, url, document_root=settings.MEDIA_ROOT)

    else:

        response = HttpResponse()
        response['X-Accel-Redirect'] = '/protected_media/' + url

    file_name = url.split('/')[-1]
    response["Content-Disposition"] = f'attachment; filename={file_name}'

    return response
