"""
Copyright 2015 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal

This is a custom registration backend that change the registration process so
admin must approve all registration request and assign permissions.

"""


# Import standard libraries

# Import Django related libraries
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.sites.requests import RequestSite
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import render_to_string

# Third party libraries
from registration import signals
from registration.models import RegistrationProfile
from registration.views import ActivationView as BaseActivationView
from registration.views import RegistrationView as BaseRegistrationView
from registration.users import UserModel


# Import project libraries


# Create your views here.




class RegistrationView(BaseRegistrationView):
    """
    A registration backend which follows a simple workflow:

    1. User signs up, inactive account is created.

    2. Email is sent to user with activation link.

    3. User clicks activation link, account is now active.

    Using this backend requires that

    * ``registration`` be listed in the ``INSTALLED_APPS`` setting
      (since this backend makes use of models defined in this
      application).

    * The setting ``ACCOUNT_ACTIVATION_DAYS`` be supplied, specifying
      (as an integer) the number of days from registration during
      which a user may activate their account (after that period
      expires, activation will be disallowed).

    * The creation of the templates
      ``registration/activation_email_subject.txt`` and
      ``registration/activation_email.txt``, which will be used for
      the activation email. See the notes for this backends
      ``register`` method for details regarding these templates.

    When subclassing this view, you can set the ``SEND_ACTIVATION_EMAIL``
    class variable to False to skip sending the new user a confirmation
    email or set ``SEND_ACTIVATION_EMAIL`` to ``False``. Doing so implies
    that you will have to activate the user manually from the admin site or
    send an activation by some other method. For example, by listening for
    the ``user_registered`` signal.

    Additionally, registration can be temporarily closed by adding the
    setting ``REGISTRATION_OPEN`` and setting it to
    ``False``. Omitting this setting, or setting it to ``True``, will
    be interpreted as meaning that registration is currently open and
    permitted.

    Internally, this is accomplished via storing an activation key in
    an instance of ``registration.models.RegistrationProfile``. See
    that model and its custom manager for full documentation of its
    fields and supported operations.

    """
    SEND_ACTIVATION_EMAIL = getattr(settings, 'SEND_ACTIVATION_EMAIL', True)
    success_url = 'registration_complete'

    def register(self, form):
        """
        Given a username, email address and password, register a new
        user account, which will initially be inactive.

        Along with the new ``User`` object, a new
        ``registration.models.RegistrationProfile`` will be created,
        tied to that ``User``, containing the activation key which
        will be used for this account.

        An email will be sent to the supplied email address; this
        email should contain an activation link. The email will be
        rendered using two templates. See the documentation for
        ``RegistrationProfile.send_activation_email()`` for
        information about these templates and the contexts provided to
        them.

        After the ``User`` and ``RegistrationProfile`` are created and
        the activation email is sent, the signal
        ``registration.signals.user_registered`` will be sent, with
        the new ``User`` as the keyword argument ``user`` and the
        class of this backend as the sender.

        """

        request = None

        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        if hasattr(form, 'save'):
            new_user_instance = form.save()
        else:
            new_user_instance = UserModel().objects.create_user(
                **form.cleaned_data)

        new_user = RegistrationProfile.objects.create_inactive_user(
            new_user=new_user_instance,
            site=site,
            send_email=self.SEND_ACTIVATION_EMAIL,
            request=request,
        )

        # Send approval email to admin
        profile = RegistrationProfile.objects.get(user_id=new_user.pk)

        self.user = new_user
        self.activation_key = profile.activation_key

        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=request)

        self.send_approval_email(site, request=request)

        return new_user

    def send_approval_email(self, site, request=None):
        """
        Send an approval email to the admin.
        """

        ctx_dict = {}
        if request is not None:
            ctx_dict = RequestContext(request, ctx_dict)
        # update ctx_dict after RequestContext is created
        # because template context processors
        # can overwrite some of the values like user
        # if django.contrib.auth.context_processors.auth is used
        ctx_dict.update({
            'user': self.user,
            'activation_key': self.activation_key,
            'site': site,
        })
        subject = getattr(settings, 'REGISTRATION_EMAIL_SUBJECT_PREFIX', '') + \
                  render_to_string('registration/approval_email_subject.txt',
                                   ctx_dict)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        from_email = getattr(settings, 'REGISTRATION_DEFAULT_FROM_EMAIL',
                             settings.DEFAULT_FROM_EMAIL)
        message_txt = render_to_string('registration/approval_email.txt',
                                       ctx_dict)

        admin_email = getattr(settings, 'ADMINS')[0][1]
        email_message = EmailMultiAlternatives(subject, message_txt, from_email,
                                               [admin_email])

        email_message.send()

    @staticmethod
    def transmit_activation_mail(request, user_pk, activation_key):
        """
        Transmit activation email to the user. Admin will access this view
        from an email link.
        """

        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        self = lambda: None
        self.user = User.objects.get(pk=user_pk)
        self.activation_key = activation_key

        RegistrationProfile.send_activation_email(self, site, request=request)

        return render(request, 'registration/registration_approval_complete.html', {})


    def registration_allowed(self):
        """
        Indicate whether account registration is currently permitted,
        based on the value of the setting ``REGISTRATION_OPEN``. This
        is determined as follows:

        * If ``REGISTRATION_OPEN`` is not specified in settings, or is
          set to ``True``, registration is permitted.

        * If ``REGISTRATION_OPEN`` is both specified and set to
          ``False``, registration is not permitted.

        """
        return getattr(settings, 'REGISTRATION_OPEN', True)


class ActivationView(BaseActivationView):
    def activate(self, request, activation_key):
        """
        Given an an activation key, look up and activate the user
        account corresponding to that key (if possible).

        After successful activation, the signal
        ``registration.signals.user_activated`` will be sent, with the
        newly activated ``User`` as the keyword argument ``user`` and
        the class of this backend as the sender.

        """
        activated_user = RegistrationProfile.objects.activate_user(
            activation_key)
        if activated_user:
            signals.user_activated.send(sender=self.__class__,
                                        user=activated_user,
                                        request=request)
        return activated_user

    def get_success_url(self, request, user):
        return ('registration_activation_complete', (), {})
