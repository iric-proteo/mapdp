"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import shutil
import socket
import tempfile
import time

# Import Django related libraries
from django.conf import settings
from django.contrib.auth.models import (Group,
                                        User)
from django.contrib.sites.models import Site
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core.cache import caches


# Third party libraries
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Import project libraries
from mhc_pipeline.tests.helper import TestHelper


class FunctionalTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):

        cls.host = socket.gethostbyname(socket.gethostname())
        super(FunctionalTest, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):

        # From https://code.djangoproject.com/ticket/21227
        if hasattr(cls, 'server_thread'):
            # test if server_thread attribute is available (as there may have
            # been an exception in setUpClass)
            # setting ignore_errors flag on WSGI server thread to avoid
            # unwanted 10054
            cls.server_thread.httpd.ignore_errors = True

    def setUp(self):

        #print(self._testMethodName)

        # Prepare site
        s = Site.objects.all()[0]
        s.name = 'MAPDP'
        s.save()

        self.dirpath = tempfile.mkdtemp()

        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")

        if settings.LOCAL_BROWSER:

            self.browser = webdriver.Chrome(chrome_options=options)

        else:
            self.browser = webdriver.Remote(
                command_executor='http://selenium-selenium-hub.selenium:4444/wd/hub',
                desired_capabilities=options.to_capabilities(),
            )

        # self.browser.maximize_window()
        self.browser.implicitly_wait(10)
        self.browser.set_page_load_timeout(20)

        # Create some users
        self.username1 = 'bob'
        self.password1 = 'fasdfk9890dfjlkj'
        self.email1 = 'user@fake.com'

        user = self.user1 = User.objects.create_user(
            username=self.username1, email=self.email1,
            password=self.password1)

        # user.is_staff = True
        # user.is_admin = True
        # user.is_superuser = True
        # user.save()

        self.username2 = 'test_user'
        self.password2 = 'fasdfk9890dfjlkjT'

        User.objects.create_user(
            username=self.username2, email='user2@fake.com',
            password=self.password2)

        # Create group
        Group.objects.create(name='mhc')
        g = Group.objects.create(name='ms')
        g.user_set.add(self.user1)
        g = Group.objects.create(name='validation')
        g.user_set.add(self.user1)

        # Helper methods
        self.uhelpers = TestHelper(self.user1)

    def _test_has_failed(self):

        # From http://stackoverflow.com/questions/4414234/getting-pythons-unittest-results-in-a-teardown-method

        for method, error in self._outcome.errors:
            if error:
                return True
        return False

    def tearDown(self):


        # Screenshot
        # Commented because it is stalling in some test
        # if self._test_has_failed():
        #     test_method_name = self._testMethodName
        #     self.browser.save_screenshot("%s.png" % test_method_name)

        # Wait to avoid ConnectionResetError
        # time.sleep(5)
        self.browser.close()
        self.browser.quit()
        shutil.rmtree(self.dirpath)

        caches['default'].clear()

    # Helper methods below

    def login(self):
        """
        This log the user and also test login.
        :return: None
        """

        # Someone just found out about MAPDP and go to check it out.
        self.browser.get(self.live_server_url)

        # He notices the page title
        self.assertIn('MAPDP', self.browser.title)

        # He must login to go further in
        username_box = self.browser.find_element_by_id('id_username')
        username_box.send_keys(self.username1)

        password_box = self.browser.find_element_by_id('id_password')
        password_box.send_keys(self.password1)
        password_box.send_keys(Keys.ENTER)

        # element = WebDriverWait(self.browser, 60).until(
        #     EC.title_contains('Dashboard')
        # )
        time.sleep(0.2)
        # He notices the page title changed to Home
        self.assertIn('Dashboard', self.browser.title)
