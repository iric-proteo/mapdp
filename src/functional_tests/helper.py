"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import time

# Import Django related libraries

# Third party libraries
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


# Import project libraries


def create_biological_sample(self):
    """
    Creates a biological for functional test.
    User must be logged in.
    :return: None
    """

    b = self.browser

    # He notices the biological samples and click on it
    element = b.find_element_by_link_text('Biological samples')
    scroll_click(self, element)

    # He creates a new biological sample
    element = b.find_element_by_link_text('Create')
    scroll_click(self, element)

    body = b.find_element_by_tag_name('body')
    self.assertIn('Create', body.text)

    field = b.find_element_by_id('id_identifier')
    field.send_keys('Test')

    field = b.find_element_by_id('id_short_description')
    field.send_keys('Automated Test description')
    field.send_keys(Keys.ENTER)

    # He notices that the biological sample was created
    # and verifies identifier
    body = b.find_element_by_tag_name('body')
    self.assertIn('Test', body.text)


def create_maxquant_fileset(self):
    """
    Create a biological sample file set for MaxQuant import.
    User must be log in.
    :return: None
    """

    b = self.browser

    # He add a new file set for PEAKS results
    element = b.find_element_by_partial_link_text('Identification')
    scroll_click(self, element)

    element = b.find_element_by_id('id_add_fileset')
    scroll_click(self, element)

    body = b.find_element_by_tag_name('body')
    self.assertIn('FileSet', body.text)

    element = b.find_element_by_id('id_name')
    scroll_click(self, element)
    element.send_keys('Maxquant results')

    field = b.find_element_by_id('id_set_class')

    for option in field.find_elements_by_tag_name('option'):
        if option.text == 'MaxQuant':
            option.click()
            break

    element = b.find_element_by_id('id_submit_button')
    scroll_click(self, element)

    body = b.find_element_by_tag_name('body')
    self.assertIn('File browser', body.text)

    # He select MaxQuant results file
    element = b.find_element_by_partial_link_text('MaxQuant')
    scroll_click(self, element)
    time.sleep(0.5)
    element = b.find_element_by_partial_link_text('txt')
    scroll_click(self, element)

    time.sleep(0.5)
    element = b.find_element_by_id('id_box_0')
    scroll_click(self, element)
    element = b.find_element_by_id('id_box_2')
    scroll_click(self, element)
    element = b.find_element_by_id('id_box_3')
    scroll_click(self, element)
    element = b.find_element_by_id('id_submit')
    scroll_click(self, element)

    # He confirms that the file has been added to file set
    element = b.find_element_by_partial_link_text('Identification')
    scroll_click(self, element)

    element = b.find_element_by_class_name('show_files')
    scroll_click(self, element)

    list_file = b.find_elements_by_xpath('//*[contains(text(), "evidence.txt")]')
    self.assertEqual(len(list_file), 1)


def create_peaks_fileset(self):
    """
    Create a biological sample file set for peaks import.
    User must be log in.
    :return: None
    """

    b = self.browser

    # He add a new file set for PEAKS results
    element = b.find_element_by_partial_link_text('Identification')
    scroll_click(self, element)

    element = b.find_element_by_id('id_add_fileset')
    scroll_click(self, element)

    body = b.find_element_by_tag_name('body')
    self.assertIn('FileSet', body.text)

    element = b.find_element_by_id('id_name')
    scroll_click(self, element)
    element.send_keys('PEAKS results')

    field = b.find_element_by_id('id_set_class')

    for option in field.find_elements_by_tag_name('option'):
        if option.text == 'PEAKS - mzIdentML 1.1':
            option.click()
            break

    element = b.find_element_by_id('id_submit_button')
    scroll_click(self, element)

    body = b.find_element_by_tag_name('body')
    self.assertIn('File browser', body.text)

    # He select a single mzIdentML file
    element = b.find_element_by_partial_link_text('mzid')
    scroll_click(self, element)

    element = b.find_element_by_id('id_box_2')
    scroll_click(self, element)
    element = b.find_element_by_id('id_box_4')
    scroll_click(self, element)
    element = b.find_element_by_id('id_submit')
    scroll_click(self, element)

    # He confirms that the file has been added to file set

    element = b.find_element_by_partial_link_text('Identification')
    scroll_click(self, element)

    element = b.find_element_by_class_name('show_files')
    scroll_click(self, element)

    list_file = b.find_elements_by_xpath('//*[contains(text(), ".mzid")]')
    self.assertEqual(len(list_file), 1)
    list_file = b.find_elements_by_xpath('//*[contains(text(), ".mgf")]')
    self.assertEqual(len(list_file), 1)


def create_raw_fileset(self):
    """
    Create a biological sample file set for peaks import.
    User must be log in.
    :return: None
    """

    b = self.browser

    # He add a new file set for PEAKS results
    element = b.find_element_by_partial_link_text('MS raw')
    scroll_click(self, element)

    element = b.find_element_by_id('id_add_fileset')
    scroll_click(self, element)

    body = b.find_element_by_tag_name('body')
    self.assertIn('FileSet', body.text)

    element = b.find_element_by_id('id_name')
    scroll_click(self, element)
    element.send_keys('RAW data')

    element = b.find_element_by_id('id_submit_button')
    scroll_click(self, element)

    body = b.find_element_by_tag_name('body')
    self.assertIn('File browser', body.text)

    # He select a single mzIdentML file
    element = b.find_element_by_partial_link_text('fileset')
    scroll_click(self, element)

    element = b.find_element_by_id('id_box_2')
    scroll_click(self, element)
    element = b.find_element_by_id('id_submit')
    scroll_click(self, element)

    # He confirms that the file has been added to file set
    element = b.find_element_by_partial_link_text('MS raw')
    scroll_click(self, element)

    list_file = b.find_elements_by_xpath('//*[contains(text(), ".raw")]')
    self.assertEqual(len(list_file), 1)


def import_maxquant_peptides(self):
    """
    Import MaxQuant peptides from a file set that is already created
    User must be log in.
    :return: None
    """

    b = self.browser

    # He expand file set and choose to import results
    element = b.find_element_by_partial_link_text('Maxquant')
    scroll_click(self, element)
    element = b.find_element_by_partial_link_text('Import peptides')
    scroll_click(self, element)

    # He confirms the new identifications have been linked to the sample
    element = b.find_element_by_partial_link_text('Identification')
    scroll_click(self, element)

    xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
    element = b.find_element_by_xpath(xp)
    scroll(self, element)
    self.assertTrue(element.is_displayed())


def import_peaks_peptides(self):
    """
    Import peptides from a file set that is already created
    User must be log in.
    :return: None
    """

    b = self.browser

    # He expand file set and choose to import results
    element = b.find_element_by_partial_link_text('PEAKS results')
    scroll_click(self, element)
    element = b.find_element_by_partial_link_text('Import peptides')
    scroll_click(self, element)

    element = b.find_element_by_partial_link_text('Identification')
    scroll_click(self, element)

    element = b.find_element_by_class_name('show_files')
    scroll_click(self, element)

    # He confirms the new identifications have been linked to the sample
    xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
    element = b.find_element_by_xpath(xp)
    scroll(self, element)
    self.assertTrue(element.is_displayed())


def scroll(self, element):
    """
    Scroll to element and click.
    :param element:
    :return: None
    """

    # From http://blog.likewise.org/2015/04/scrolling-to-an-element-with-the-python-bindings-for-selenium-webdriver/

    b = self.browser

    b.execute_script("return arguments[0].scrollIntoView();", element)
    time.sleep(0.1)


def scroll_click(self, element):
    """
    Scroll to element and click.
    :param element:
    :return: None
    """

    # From http://blog.likewise.org/2015/04/scrolling-to-an-element-with-the-python-bindings-for-selenium-webdriver/

    b = self.browser

    b.execute_script("return arguments[0].scrollIntoView();", element)
    ActionChains(b).click(element).perform()
    time.sleep(0.4)
