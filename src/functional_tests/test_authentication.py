"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import time

# Import Django related libraries

# Third party libraries
from selenium.webdriver.common.keys import Keys

# Import project libraries
from .base import FunctionalTest
from .helper import scroll_click


class AuthenticationTest(FunctionalTest):
    # Test methods
    def test_log_out(self):
        self.login()

        b = self.browser

        # He notices is name in the top bar
        body = b.find_element_by_tag_name('body')
        self.assertIn(self.username1, body.text)

        # He opens the user panel
        b.find_element_by_id('id_user_panel_link').click()

        # He chooses to log out
        b.find_element_by_id('id_auth_logout_link').click()

        # He sees the login confirmation
        body = b.find_element_by_tag_name('body')
        self.assertIn('Sign Out', body.text)

    def test_change_password(self):
        self.login()

        b = self.browser

        # He opens the user panel
        b.find_element_by_id('id_user_panel_link').click()

        # He chooses to change his password
        b.find_element_by_id('id_auth_password_change_link').click()

        body = b.find_element_by_tag_name('body')
        self.assertIn('Password change', body.text)

        field = b.find_element_by_id('id_old_password')
        field.send_keys(self.password1)

        field = b.find_element_by_id('id_new_password1')
        field.send_keys(self.password2)

        field = b.find_element_by_id('id_new_password2')
        field.send_keys(self.password2)
        field.send_keys(Keys.ENTER)

        body = b.find_element_by_tag_name('body')
        self.assertIn('Password changed', body.text)

    def test_user_registration_form(self):
        b = self.browser

        # Someone just found out about capaMHC and go to check it out.
        b.get(self.live_server_url + '/accounts/register_mapdp/')

        # He notices the page title
        self.assertIn('Register', self.browser.title)

        # He fills the form with already used email.
        e = b.find_element_by_id('id_username')
        e.send_keys('test')

        e = b.find_element_by_id('id_email')
        e.send_keys('user@fake.com')

        e = b.find_element_by_id('id_first_name')
        e.send_keys('test')

        e = b.find_element_by_id('id_last_name')
        e.send_keys('test')

        e = b.find_element_by_id('id_password1')
        e.send_keys(self.password1)

        e = b.find_element_by_id('id_password2')
        e.send_keys(self.password1)

        e = b.find_element_by_id('id_submit')
        scroll_click(self, e)

        # He notice validation error
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('This email address is already in use', body.text)
        time.sleep(0.2)

        # He fills the form with correct email
        e = b.find_element_by_id('id_email')
        time.sleep(0.2)
        e.clear()
        time.sleep(0.2)
        e.send_keys('fake@fake112312321321321.com')

        e = b.find_element_by_id('id_password1')
        e.send_keys(self.password1)

        e = b.find_element_by_id('id_password2')
        e.send_keys(self.password1)

        e = b.find_element_by_id('id_submit')
        scroll_click(self, e)

        time.sleep(1)
        # He notices the registration msg
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('You are now registered', body.text)

    def test_password_reset(self):
        b = self.browser
        b.get(self.live_server_url)

        # He clicks on the reset link
        b.find_element_by_partial_link_text('Reset it').click()

        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Password Reset', body.text)

        # He fills the form with its email
        field = b.find_element_by_id('id_email')
        field.send_keys(self.email1)
        field.send_keys(Keys.ENTER)

        # He receives confirmation
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('reset instructions', body.text)
