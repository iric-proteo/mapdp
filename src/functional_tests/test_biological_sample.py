"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import time

# Import Django related libraries

# Third party libraries
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

# Import project libraries
from .base import FunctionalTest
from .helper import (create_biological_sample,
                     scroll_click)


class BiologicalSampleTest(FunctionalTest):
    def setUp(self):
        super().setUp()
        self.login()

    def test_edit(self):
        b = self.browser
        create_biological_sample(self)

        # He chooses to edit the sample
        element = b.find_element_by_id('id_link_biological_sample_edit')
        scroll_click(self, element)

        time.sleep(0.4)
        field = b.find_element_by_id('id_identifier')
        field.send_keys('Edited')
        field.send_keys(Keys.ENTER)
        time.sleep(0.4)
        # He notices that the biological sample was changed
        # and verifies identifier
        body = b.find_element_by_tag_name('body')
        self.assertIn('TestEdited', body.text)

    def test_experiment_details(self):
        b = self.browser
        create_biological_sample(self)

        # He chooses to add experiment details
        element = b.find_element_by_partial_link_text('Experiment details')
        scroll_click(self, element)

        b.find_element_by_id('id_experiment_details_add_link').click()

        # He fills name field
        field = b.find_element_by_id('id_name')
        name = 'TestExperimentDetails'
        time.sleep(0.5)
        field.send_keys(name)
        field.send_keys(Keys.ENTER)

        element = b.find_element_by_partial_link_text('Experiment details')
        scroll_click(self, element)
        time.sleep(0.2)
        body = b.find_element_by_tag_name('body')
        self.assertIn(name, body.text)

        # He expands experiment details and add cell count
        b.find_element_by_partial_link_text(name).click()
        field = b.find_element_by_id('id_cell_count')
        cell_count = 10
        field.send_keys(cell_count)
        time.sleep(0.2)
        element = b.find_element_by_id('id_submit_button')
        scroll_click(self, element)
        time.sleep(0.2)
        # He confirms that cell count was saved
        element = b.find_element_by_partial_link_text('Experiment details')
        scroll_click(self, element)

        element = b.find_element_by_partial_link_text(name)
        scroll_click(self, element)
        field = b.find_element_by_id('id_cell_count')

        self.assertEqual(field.get_attribute('value'), str(cell_count))

    def test_list(self):
        b = self.browser
        create_biological_sample(self)

        # He now checks that its sample is listed
        b.find_element_by_link_text('Biological samples').click()

        b.find_element_by_link_text('List').click()

        body = b.find_element_by_tag_name('body')
        self.assertIn('Test', body.text)

        # He now decides to look at a sample from the list
        link = b.find_element_by_xpath(
            '//table[@id="list_table"]/tbody/tr[1]/td[1]/a')
        link.click()

        body = b.find_element_by_tag_name('body')
        self.assertIn('Test', body.text)

    def test_mhc_allele(self):
        b = self.browser
        create_biological_sample(self)

        allele_1 = 'HLA-A*02:02'
        allele_2 = 'HLA-A*02:03'

        self.uhelpers.create_mhc_allele(name=allele_1)
        self.uhelpers.create_mhc_allele(name=allele_2)

        # He decides to add MHC alleles
        b.find_element_by_id('id_mhc_allele_link').click()
        body = b.find_element_by_tag_name('body')
        self.assertIn('Modify', body.text)

        # He clicks on the mch allele box
        element = b.find_element_by_id('id_mhc_allele_text')
        scroll_click(self, element)

        element.send_keys(allele_1)
        time.sleep(0.5)  # Wait for suggestion to pop up

        (ActionChains(b)
         .send_keys(Keys.ARROW_DOWN)
         .send_keys(Keys.ENTER)
         .perform())

        time.sleep(0.5)  # Wait for Javascript

        element = b.find_element_by_id('id_submit_button')
        scroll_click(self, element)

        # He confirms that allele was added
        body = b.find_element_by_tag_name('body')
        self.assertIn(allele_1, body.text)

    def test_mhc_details(self):
        b = self.browser
        create_biological_sample(self)

        # He chooses to edit the MHC sample details
        element = b.find_element_by_id('id_mhc_sample_link')
        scroll_click(self, element)

        time.sleep(0.4)
        field = b.find_element_by_id('id_group')
        field.send_keys('ControlGroup')
        field.send_keys(Keys.ENTER)
        time.sleep(0.3)
        # He notices that the biological sample was changed
        body = b.find_element_by_tag_name('body')
        self.assertIn('ControlGroup', body.text)

    def test_trash(self):
        b = self.browser
        create_biological_sample(self)

        # He decides to trash the sample
        element = b.find_element_by_id('id_biological_sample_trash')
        scroll_click(self, element)
        time.sleep(0.3)
        element = b.find_element_by_partial_link_text('Trash')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')
        self.assertIn('The selected item was trashed', body.text)
