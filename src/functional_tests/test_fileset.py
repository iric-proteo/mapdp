"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import time

# Import Django related libraries

# Third party libraries

# Import project libraries
from .base import FunctionalTest
from .helper import (create_biological_sample,
                     create_peaks_fileset,
                     create_raw_fileset,
                     scroll_click)


class Test(FunctionalTest):
    def setUp(self):
        super().setUp()
        self.login()

    def test_edit(self):
        b = self.browser
        create_biological_sample(self)
        create_raw_fileset(self)

        # He expand file set and choose to add more files
        element = b.find_element_by_partial_link_text('RAW data')
        scroll_click(self, element)
        element = b.find_element_by_partial_link_text('Add files')
        scroll_click(self, element)

        # He select a new and an existing files
        element = b.find_element_by_partial_link_text('fileset')
        scroll_click(self, element)
        element = b.find_element_by_id('id_box_0')
        scroll_click(self, element)
        element = b.find_element_by_id('id_submit')
        scroll_click(self, element)

        # He confirms that the file has been added to file set
        element = b.find_element_by_partial_link_text('MS raw')
        scroll_click(self, element)

        list_file = b.find_elements_by_xpath('//*[contains(text(), ".raw")]')
        self.assertEqual(len(list_file), 2)

    def test_refresh(self):
        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        # He notices the refresh button
        element = b.find_element_by_id('id_fileset_refresh')
        scroll_click(self, element)

        element = b.find_element_by_partial_link_text('Identification')
        scroll_click(self, element)

        element = b.find_element_by_class_name('show_files')
        scroll_click(self, element)

        list_file = b.find_elements_by_xpath(
            '//i[@class="fa fa-check text-green"]')
        self.assertEqual(len(list_file), 2)

    def test_remove_file(self):
        # This test can fail in CI if windows is not wide enough
        # Xvfb screen was set to 1600x1200x24

        b = self.browser
        create_biological_sample(self)
        create_raw_fileset(self)

        # He expand file set
        element = b.find_element_by_partial_link_text('MS raw')
        scroll_click(self, element)

        element = b.find_element_by_partial_link_text('RAW data')
        scroll_click(self, element)

        # He notices the trash icon next to a file
        element = b.find_element_by_id('id_remove_file_link')
        scroll_click(self, element)

        # He expand file set
        element = b.find_element_by_partial_link_text('MS raw')
        scroll_click(self, element)

        element = b.find_element_by_partial_link_text('RAW data')
        scroll_click(self, element)

        # He confirms that it has been removed
        list_file = b.find_elements_by_xpath('//*[contains(text(), ".raw")]')
        self.assertEqual(len(list_file), 0)

    def test_send_file(self):
        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        # He expand file set
        element = b.find_element_by_class_name('show_files')
        scroll_click(self, element)

        # He notices the download icon next to a file
        elements = b.find_elements_by_id('id_send_file_link')
        scroll_click(self, elements[1])

    def test_send_files(self):
        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        # He expand file set
        element = b.find_element_by_class_name('show_files')
        scroll_click(self, element)

        # He notices the download icon
        element = b.find_element_by_id('id_send_files_link')
        scroll_click(self, element)

    def test_trash(self):
        b = self.browser
        create_biological_sample(self)
        create_raw_fileset(self)

        element = b.find_element_by_partial_link_text('RAW data')
        scroll_click(self, element)

        # He notices the fileset trash icon
        element = b.find_element_by_id('id_fileset_trash_link')
        scroll_click(self, element)

        # He confirms that the file set is removed
        list_file = b.find_elements_by_xpath('//*[contains(text(), ".mzid")]')

        self.assertEqual(len(list_file), 0)
