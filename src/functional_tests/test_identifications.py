""""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from pathlib import Path
import shutil
import time

# Import Django related libraries
from django.conf import settings

# Third party libraries
import requests_mock
from selenium.webdriver.common.keys import Keys

# Import project libraries
from .base import FunctionalTest
from .helper import (create_biological_sample,
                     create_maxquant_fileset,
                     create_peaks_fileset,
                     scroll_click,
                     )
from mhc_pipeline.models import Peptide


class IdentificationTest(FunctionalTest):
    def setUp(self):

        # Clean media storage
        path = Path(settings.MEDIA_ROOT)

        if path.exists():
            shutil.rmtree(path)
        path.mkdir()

        super().setUp()
        self.login()

    def test_view(self):

        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        # He click to see identifications and sees the filter form
        xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
        element = b.find_element_by_xpath(xp)
        scroll_click(self, element)
        element = b.find_element_by_id('id_submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Scoring', body.text)

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 125 entries', body.text)

        # Now he enters 2 ppm in the filter and add name to save filter
        field = b.find_element_by_id('id_error_ppm__lte')
        field.send_keys(2)
        field = b.find_element_by_id('id_filter_name')
        field.send_keys('FilterTest')
        field.send_keys(Keys.ENTER)

        body = b.find_element_by_tag_name('body')
        self.assertIn('Showing 1 to 91 of 91 entries', body.text)

        # Now he selects the unique sequence
        element = b.find_element_by_id('id_unique_sequence')
        scroll_click(self, element)
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')
        self.assertIn('Showing 1 to 82 of 82 entries', body.text)

        # He loads saved filter
        field = b.find_element_by_id('id_selected_filter')
        for option in field.find_elements_by_tag_name('option'):
            if option.text == 'FilterTest':
                option.click()
                break

        b.find_element_by_id('submit-id-submit').click()
        body = b.find_element_by_tag_name('body')
        self.assertIn('Showing 1 to 91 of 91 entries', body.text)

    def test_view_all(self):

        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        # He notices the biological samples and click on it
        element = b.find_element_by_link_text('Biological samples')
        scroll_click(self, element)

        # He navigates to raw peptide identifications
        element = b.find_element_by_link_text('View identifications')
        scroll_click(self, element)

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 0 to 0 of 0 entries', body.text)

        # He selects one identification
        element = b.find_element_by_id('id_identification_text')
        element.send_keys('PEAKS')
        time.sleep(0.5)
        element.send_keys(Keys.ARROW_DOWN)
        element.send_keys(Keys.ENTER)

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 125 entries', body.text)

    def test_view_download(self):

        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        # He notices the biological samples and click on it
        element = b.find_element_by_link_text('Biological samples')
        scroll_click(self, element)

        # He navigates to raw peptide identifications
        element = b.find_element_by_link_text('View identifications')
        scroll_click(self, element)

        element = b.find_element_by_id('submit-id-download')
        scroll_click(self, element)

    def test_view_filter(self):

        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)
        #import_peaks_peptides(self)

        # He click to see identifications and sees the filter form
        xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
        element = b.find_element_by_xpath(xp)
        scroll_click(self, element)
        element = b.find_element_by_id('id_submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Scoring', body.text)

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 125 entries', body.text)

        # Now he enters 2 ppm in the filter and add name to save filter
        field = b.find_element_by_id('id_error_ppm__lte')
        field.send_keys(2)
        field = b.find_element_by_id('id_filter_name')
        field.send_keys('FilterTest')
        field.send_keys(Keys.ENTER)

        time.sleep(2)
        body = b.find_element_by_tag_name('body')
        self.assertIn('Showing 1 to 91 of 91 entries', body.text)

        # He edits filter name
        element = b.find_element_by_id('id_filters_link')
        scroll_click(self, element)

        time.sleep(0.5)
        element = b.find_element_by_id('id_rename_link')
        scroll_click(self, element)

        time.sleep(0.5)
        field = b.find_element_by_id('id_name')
        field.clear()
        field.send_keys('RenamedFilterTest')
        field.send_keys(Keys.ENTER)

        time.sleep(1)

        body = b.find_element_by_tag_name('body')

        self.assertIn('RenamedFilterTest', body.text)

        # He shares the filter
        element = b.find_element_by_id('id_share_link')
        scroll_click(self, element)

        element_id = 'bootstrap-duallistbox-nonselected-list_users'
        element = b.find_element_by_id(element_id)

        for option in element.find_elements_by_tag_name('option'):
            if option.text == 'test_user':
                option.click()
                break

        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        # He un-shares the filter
        time.sleep(0.5)
        element = b.find_element_by_id('id_share_link')
        scroll_click(self, element)

        time.sleep(0.5)
        element_id = 'bootstrap-duallistbox-selected-list_users'
        element = b.find_element_by_id(element_id)

        for option in element.find_elements_by_tag_name('option'):
            if option.text == 'test_user':
                option.click()
                break

        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        # He hides the filter
        element = b.find_element_by_class_name('hide_box')
        scroll_click(self, element)
        time.sleep(0.5)

        element = b.find_element_by_link_text('Biological samples')
        scroll_click(self, element)
        time.sleep(0.5)
        element = b.find_element_by_link_text('View identifications')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertNotIn('RenamedFilterTest', body.text)

        # He un-hides the filter
        element = b.find_element_by_id('id_filters_link')
        scroll_click(self, element)
        time.sleep(0.5)
        element = b.find_element_by_class_name('hide_box')
        scroll_click(self, element)
        time.sleep(0.5)

        element = b.find_element_by_link_text('Biological samples')
        scroll_click(self, element)
        time.sleep(0.5)
        element = b.find_element_by_link_text('View identifications')
        scroll_click(self, element)

        time.sleep(1)
        body = b.find_element_by_tag_name('body')

        self.assertIn('RenamedFilterTest', body.text)

        # He trashes the filter
        element = b.find_element_by_id('id_filters_link')
        scroll_click(self, element)
        time.sleep(0.5)
        element = b.find_element_by_id('id_trash_link')
        scroll_click(self, element)
        element = b.find_element_by_partial_link_text('Trash')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')
        self.assertIn('The selected item was trashed', body.text)

    def test_view_mhc_predictor(self):

        netmhc_data = b'Friday December  1 2017 14:14\n\n\nNetMHC version 3.4. 9mer predictions using Artificial Neural Networks - Direct. Allele HLA-A02:02. \nStrong binder threshold  50 nM. Weak binder threshold score 500 nM\n\n\n\n----------------------------------------------------------------------------------------------------\n pos    peptide      logscore affinity(nM) Bind Level    Protein Name     Allele\n----------------------------------------------------------------------------------------------------\n   0  LKVPPTIAQ         0.053        28224                   Peptides HLA-A02:02\n   1  KYKQRPIDK         0.021        39735                   Peptides HLA-A02:02\n   2  LGPISNLVK         0.027        37333                   PeptidesHLA-A02:02\n   3  ESTLHLVLR         0.045        30580                   Peptides HLA-A02:02\n   4  APVVAIVKK         0.029        36722                   Peptides HLA-A02:02\n   5  ANELLINVK         0.037        33336                   Peptides HLA-A02:02\n   6  LAKLNQLLR         0.036        33803                   Peptides HLA-A02:02\n   7  FDKLNELGK         0.029        36559                   Peptides HLA-A02:02\n   8  FHLGNLGVR         0.044        30985                   Peptides HLA-A02:02\n   9  QVTALAQQK         0.046        30432                   Peptides HLA-A02:02\n  10  IVAALPTIK         0.097        17508                   Peptides HLA-A02:02\n  11  VISNPLLAR         0.067        24247                   Peptides HLA-A02:02\n  12  SKPVALTSF         0.050        29089                   Peptides HLA-A02:02\n  13  SLKPHSVFK         0.235         3929                   Peptides HLA-A02:02\n  14  AGLTFPVGR         0.039        32718                   Peptides HLA-A02:02\n  15  WTLIPEDKR         0.031        35883                   Peptides HLA-A02:02\n  16  TSRFAGLLT         0.041        32151                   Peptides HLA-A02:02\n  17  MGVPYAIVK         0.047        30075 Peptides HLA-A02:02\n  18  YVKWPEYVR         0.059        26365                   Peptides HLA-A02:02\n  19  ALLPHLTNA         0.691           28         SB        Peptides HLA-A02:02\n  20  VDVTDPGIF         0.025        37965   Peptides HLA-A02:02\n'

        with requests_mock.Mocker() as m:
            m.post('http://localhost/predict', content=netmhc_data)

            b = self.browser
            create_biological_sample(self)
            create_peaks_fileset(self)

            allele_1 = 'HLA-A*02:02'
            allele_2 = 'HLA-A*02:03'

            self.uhelpers.create_mhc_allele(name=allele_1)
            self.uhelpers.create_mhc_allele(name=allele_2)

            # He decides to add MHC alleles
            element = b.find_element_by_id('id_mhc_allele_link')
            scroll_click(self, element)

            body = b.find_element_by_tag_name('body')
            self.assertIn('Modify', body.text)

            # He clicks on the mhc allele box
            field = b.find_element_by_id('id_mhc_allele_text')
            field.send_keys(allele_1)
            time.sleep(0.5)
            field.send_keys(Keys.ARROW_DOWN)
            field.send_keys(Keys.ENTER)
            b.find_element_by_id('id_submit_button').send_keys(Keys.ENTER)

            # He click to see identifications and sees the filter form
            element = b.find_element_by_partial_link_text('Identification')
            scroll_click(self, element)

            xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
            element = b.find_element_by_xpath(xp)
            scroll_click(self, element)
            element = b.find_element_by_id('id_submit')
            scroll_click(self, element)

            # He selects NetMHC predictions
            field = b.find_element_by_id('id_mhc_predictor')
            scroll_click(self, field)
            field.send_keys('n')
            field.send_keys(Keys.ENTER)
            element = b.find_element_by_id('submit-id-submit')
            scroll_click(self, element)
            body = b.find_element_by_tag_name('body')
            self.assertIn('NetMHC-3.4a affinity: HLA-A*02:02', body.text)

            # He filters by NetMHC affinity
            field = b.find_element_by_id('id_mhc_allele_text')
            field.send_keys(allele_1)
            time.sleep(0.5)
            field.send_keys(Keys.ARROW_DOWN)
            field.send_keys(Keys.ENTER)
            field = b.find_element_by_id('id_affinity_threshold')
            field.send_keys('1000')
            element = b.find_element_by_id('submit-id-submit')
            scroll_click(self, element)

            body = b.find_element_by_tag_name('body')
            self.assertIn('Showing 1 to 1 of 1 entries', body.text)

    def test_view_pep_score_fdr(self):

        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        # He click to see identifications and sees the filter form
        xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
        element = b.find_element_by_xpath(xp)
        scroll_click(self, element)
        element = b.find_element_by_id('id_submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')
        self.assertIn('Scoring', body.text)

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 125 entries', body.text)

        # He enter peptide score
        element = b.find_element_by_id('id_score_fdr')
        element.send_keys('0.05')

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 115 entries', body.text)

        # He decides to remove decoys
        element = b.find_element_by_id('id_remove_decoy')
        scroll_click(self, element)

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 110 entries', body.text)

    def test_view_pep_score_mhc_fdr(self):

        netmhc_data = b'Friday December  1 2017 14:14\n\n\nNetMHC version 3.4. 9mer predictions using Artificial Neural Networks - Direct. Allele HLA-A02:02. \nStrong binder threshold  50 nM. Weak binder threshold score 500 nM\n\n\n\n----------------------------------------------------------------------------------------------------\n pos    peptide      logscore affinity(nM) Bind Level    Protein Name     Allele\n----------------------------------------------------------------------------------------------------\n   0  LKVPPTIAQ         0.053        28224                   Peptides HLA-A02:02\n   1  KYKQRPIDK         0.021        39735                   Peptides HLA-A02:02\n   2  LGPISNLVK         0.027        37333                   PeptidesHLA-A02:02\n   3  ESTLHLVLR         0.045        30580                   Peptides HLA-A02:02\n   4  APVVAIVKK         0.029        36722                   Peptides HLA-A02:02\n   5  ANELLINVK         0.037        33336                   Peptides HLA-A02:02\n   6  LAKLNQLLR         0.036        33803                   Peptides HLA-A02:02\n   7  FDKLNELGK         0.029        36559                   Peptides HLA-A02:02\n   8  FHLGNLGVR         0.044        30985                   Peptides HLA-A02:02\n   9  QVTALAQQK         0.046        30432                   Peptides HLA-A02:02\n  10  IVAALPTIK         0.097        17508                   Peptides HLA-A02:02\n  11  VISNPLLAR         0.067        24247                   Peptides HLA-A02:02\n  12  SKPVALTSF         0.050        29089                   Peptides HLA-A02:02\n  13  SLKPHSVFK         0.235         3929                   Peptides HLA-A02:02\n  14  AGLTFPVGR         0.039        32718                   Peptides HLA-A02:02\n  15  WTLIPEDKR         0.031        35883                   Peptides HLA-A02:02\n  16  TSRFAGLLT         0.041        32151                   Peptides HLA-A02:02\n  17  MGVPYAIVK         0.047        30075 Peptides HLA-A02:02\n  18  YVKWPEYVR         0.059        26365                   Peptides HLA-A02:02\n  19  ALLPHLTNA         0.691           28         SB        Peptides HLA-A02:02\n  20  VDVTDPGIF         0.025        37965   Peptides HLA-A02:02\n'

        with requests_mock.Mocker() as m:
            m.post('http://localhost/predict', content=netmhc_data)

            b = self.browser
            create_biological_sample(self)
            create_peaks_fileset(self)

            allele_1 = 'HLA-A*02:02'
            allele_2 = 'HLA-A*02:03'

            self.uhelpers.create_mhc_allele(name=allele_1)
            self.uhelpers.create_mhc_allele(name=allele_2)

            # He decides to add MHC alleles
            element = b.find_element_by_id('id_mhc_allele_link')
            scroll_click(self, element)

            body = b.find_element_by_tag_name('body')
            self.assertIn('Modify', body.text)

            # He clicks on the mch allele box
            field = b.find_element_by_id('id_mhc_allele_text')
            field.send_keys(allele_1)
            time.sleep(0.5)
            field.send_keys(Keys.ARROW_DOWN)
            field.send_keys(Keys.ENTER)
            b.find_element_by_id('id_submit_button').click()

            # He click to see identifications and sees the filter form
            element = b.find_element_by_partial_link_text('Identification')
            scroll_click(self, element)

            xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
            element = b.find_element_by_xpath(xp)
            scroll_click(self, element)
            element = b.find_element_by_id('id_submit')
            scroll_click(self, element)

            body = b.find_element_by_tag_name('body')

            self.assertIn('Scoring', body.text)

            # He clicks on filter and notice identifications
            element = b.find_element_by_id('submit-id-submit')
            scroll_click(self, element)

            body = b.find_element_by_tag_name('body')

            self.assertIn('Showing 1 to 100 of 125 entries', body.text)

            # He enter peptide score/netmhc FDR
            element = b.find_element_by_id('id_score_mhc_fdr')
            element.send_keys('0.1')

            # He selects NetMHC predictions
            field = b.find_element_by_id('id_mhc_predictor')
            field.click()
            field.send_keys('n')
            field.send_keys(Keys.ENTER)
            element = b.find_element_by_id('submit-id-submit')
            scroll_click(self, element)

            # He clicks on filter and notice identifications
            element = b.find_element_by_id('submit-id-submit')
            scroll_click(self, element)
            time.sleep(5)
            body = b.find_element_by_tag_name('body')

            self.assertIn('Showing 1 to 22 of 22 entries', body.text)

            # He decides to remove decoys and select MHC allele
            element = b.find_element_by_id('id_remove_decoy')
            scroll_click(self, element)

            field = b.find_element_by_id('id_mhc_allele_text')
            field.send_keys(allele_1)
            time.sleep(0.5)
            field.send_keys(Keys.ARROW_DOWN)
            field.send_keys(Keys.ENTER)

            # He clicks on filter and notice identifications
            element = b.find_element_by_id('submit-id-submit')
            scroll_click(self, element)

            body = b.find_element_by_tag_name('body')

            self.assertIn('Showing 1 to 20 of 20 entries', body.text)

    def test_view_trash(self):

        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        time.sleep(0.2)
        # He click to trash the identification
        element = b.find_element_by_class_name('identification_trash_link')
        scroll_click(self, element)
        time.sleep(0.2)
        
        element = b.find_element_by_partial_link_text('Trash')
        scroll_click(self, element)
        body = b.find_element_by_tag_name('body')

        self.assertIn('The selected item was trashed', body.text)

    def test_peptide_msms(self):

        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)

        peptide = Peptide.objects.get(pk=6)
        peptide.scan_file_location = 'test_files/mzid/Yeast_2015-05-26_R9.mgf'
        peptide.save()

        # He click to see identifications and sees the filter form
        xp = '//table[@id="id_identifications_table"]/tbody/tr[1]/td[1]'
        element = b.find_element_by_xpath(xp)
        scroll_click(self, element)
        element = b.find_element_by_id('id_submit')
        scroll_click(self, element)
        body = b.find_element_by_tag_name('body')

        self.assertIn('Scoring', body.text)

        # He clicks on filter and notice identifications
        element = b.find_element_by_id('submit-id-submit')
        scroll_click(self, element)
        time.sleep(5)
        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 125 entries', body.text)

        # He notices links for MSMS spectrum
        for i, element in enumerate(
                b.find_elements_by_class_name('id_msms_link')):
            if i == 5:
                element.click()

        # Wait for windows to pop-up
        time.sleep(5)
        window_after = b.window_handles[1]
        b.switch_to_window(window_after)
        body = b.find_element_by_tag_name('body')
        self.assertIn('Spectrum Viewer', body.text)

        # He validate the spectrum
        b.find_element_by_id('validation-green').click()
        element = b.find_element_by_id('validation-green')
        element = element.find_element_by_tag_name('span')
        self.assertNotEqual(element.text, '&nbps;')

    def test_maxquant(self):

        create_biological_sample(self)
        create_maxquant_fileset(self)
