"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import time

# Import Django related libraries

# Third party libraries
from selenium.webdriver.common.keys import Keys

# Import project libraries
from .base import FunctionalTest
from .helper import (create_biological_sample,
                     create_peaks_fileset,
                     import_peaks_peptides,
                     scroll_click,
                     )


class Test(FunctionalTest):
    def setUp(self):
        super().setUp()
        self.login()

    def test_create_view_edit_trash_list(self):
        b = self.browser
        create_biological_sample(self)
        create_peaks_fileset(self)
        # import_peaks_peptides(self)

        # He decides to create an immunopeptidome
        element = b.find_element_by_id('id_immunopeptidome_menu')
        scroll_click(self, element)
        element = b.find_element_by_id('id_immunopeptidome_create')
        scroll_click(self, element)

        # He selects one identification set and add name to create
        # immunopeptidome
        element = b.find_element_by_id('id_identification_text')
        element.send_keys('PEAKS')
        time.sleep(0.5)
        element.send_keys(Keys.ARROW_DOWN)
        element.send_keys(Keys.ENTER)
        element = b.find_element_by_id('id_immunopeptidome_name')
        name = 'Children 1'
        element.send_keys(name)
        element.send_keys(Keys.ENTER)

        # He views the new immunopeptidome instance
        body = b.find_element_by_tag_name('body')

        self.assertIn(name, body.text)

        # He adds a comment
        element = b.find_element_by_partial_link_text('Edit')
        scroll_click(self, element)
        element = b.find_element_by_id('id_comments')
        comment = 'Hello!'
        element.send_keys(comment)
        element = b.find_element_by_id('id_submit_button')
        scroll_click(self, element)
        body = b.find_element_by_tag_name('body')

        self.assertIn(comment, body.text)

        # He looks at associated peptides
        b.find_element_by_id('id_submit_button').click()
        time.sleep(1)
        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 100 of 125 entries', body.text)

        # He creates a new immunopeptidome
        element = b.find_element_by_id('id_peptide_score__gte')
        element.send_keys('40')
        element = b.find_element_by_id('id_immunopeptidome_name')
        name = 'Parent 1'
        element.send_keys(name)
        element.send_keys(Keys.ENTER)

        # He views the new immunopeptidome instance
        body = b.find_element_by_tag_name('body')

        self.assertIn(name, body.text)
        self.assertIn('46', body.text)

        # He trashes the instance
        element = b.find_element_by_partial_link_text('Trash')
        scroll_click(self, element)
        element = b.find_element_by_partial_link_text('Trash')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')

        self.assertIn('The selected item was trashed', body.text)

        # He checks list of immunopeptidome

        b.find_element_by_partial_link_text('Immunopeptidomes').click()
        b.find_element_by_partial_link_text('List').click()

        body = b.find_element_by_tag_name('body')

        self.assertIn('Showing 1 to 1 of 1 entries', body.text)
