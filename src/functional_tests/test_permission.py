"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries

# Third party libraries
from selenium.webdriver.common.keys import Keys

# Import project libraries
from .base import FunctionalTest
from .helper import create_biological_sample, scroll_click


class BiologicalSampleTest(FunctionalTest):
    def setUp(self):

        super().setUp()
        self.login()

    def test_permission(self):

        b = self.browser
        create_biological_sample(self)

        # He changes user permissions
        b.find_element_by_partial_link_text('Permissions').click()

        b.find_element_by_id('id_link_permission_add_user').click()

        body = b.find_element_by_tag_name('body')
        self.assertIn('Modify permission', body.text)

        input_field = b.find_element_by_id('id_user')

        for option in input_field.find_elements_by_tag_name('option'):
            if option.text == self.username2:
                option.click()
                break

        element = b.find_element_by_id('id_change')
        scroll_click(self, element)
        element.send_keys(Keys.ENTER)

        # He confirms permission change
        element = b.find_element_by_partial_link_text('Permissions')
        scroll_click(self, element)

        td = b.find_element_by_xpath(
            '//table[@id="id_permission_panel"]/tbody/tr[2]/td[2]')
        self.assertEqual(td.text, 'False')
        td = b.find_element_by_xpath(
            '//table[@id="id_permission_panel"]/tbody/tr[2]/td[3]')
        self.assertEqual(td.text, 'True')
        td = b.find_element_by_xpath(
            '//table[@id="id_permission_panel"]/tbody/tr[2]/td[4]')
        self.assertEqual(td.text, 'False')

        # He changes group permissions
        element = b.find_element_by_partial_link_text('Permissions')
        scroll_click(self, element)

        element = b.find_element_by_id('id_link_permission_add_group')
        scroll_click(self, element)

        body = b.find_element_by_tag_name('body')
        self.assertIn('Modify permission', body.text)

        input_field = b.find_element_by_id('id_group')

        for option in input_field.find_elements_by_tag_name('option'):
            if option.text == 'mhc':
                option.click()
                break

        input_field = b.find_element_by_id('id_change')
        input_field.click()
        input_field.send_keys(Keys.ENTER)

        element = b.find_element_by_partial_link_text('Permissions')
        scroll_click(self, element)
        element = b.find_element_by_id('id_link_permission_group')
        scroll_click(self, element)

        input_field = b.find_element_by_id('id_change')
        self.assertTrue(input_field.get_attribute('checked'))

        element = b.find_element_by_id('id_view')
        self.assertFalse(element.get_attribute('checked'))
        scroll_click(self, element)

        element.send_keys(Keys.ENTER)
        # He confirms permission change
        element = b.find_element_by_partial_link_text('Permissions')
        scroll_click(self, element)

        td = b.find_element_by_xpath(
            '//table[@id="id_permission_panel"]/tbody[2]/tr[1]/td[2]')
        self.assertEqual(td.text, 'True')
        td = b.find_element_by_xpath(
            '//table[@id="id_permission_panel"]/tbody[2]/tr[1]/td[3]')
        self.assertEqual(td.text, 'True')
        td = b.find_element_by_xpath(
            '//table[@id="id_permission_panel"]/tbody[2]/tr[1]/td[4]')
        self.assertEqual(td.text, 'False')
