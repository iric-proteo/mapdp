#!/bin/bash

_term() {
  echo "Caught SIGTERM signal!"
  kill -TERM "$child" 2>/dev/null
}

trap _term SIGTERM

python manage.py collectstatic --noinput --settings=base_site.settings.base
python manage.py migrate

while [ $? -ne 0 ]; do
    sleep 15
    python manage.py migrate
done

python manage.py mhc_pipeline_setup

gunicorn -c ../services/gunicorn/gunicorn_conf.py --forwarded-allow-ips="*" base_site.wsgi:application &

child=$!
wait "$child"