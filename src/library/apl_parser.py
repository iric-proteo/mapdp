"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from collections import defaultdict
import re

# Import project libraries

# Create your views here.


class APLParser(object):
    def __init__(self, file_name):

        self.file_name = file_name
        self.fh = None

    @staticmethod
    def scan_total_ion_current(peak_list):
        """
        Sums the intensity of all peaks
        :param peak_list: List of [mz,intensity]
        :return: Float
        """

        tic = 0

        for mz, intensity in peak_list:
            tic += intensity

        return tic

    def parse_next_scan(self):
        """
        Extract values from one scan.
        :return: Dictionary
        """

        scan = dict()
        scan['peak_list'] = list()

        # Search beginning of the scan
        scan['scan_file_index'] = self.fh.tell()
        line = self.fh.readline()

        while not line.startswith('peaklist start'):
            scan['scan_file_index'] = self.fh.tell()
            line = self.fh.readline()

            if line == '':
                return None

        # Read fields
        while line:
            line = self.fh.readline().strip()

            if '=' in line:
                part1, part2 = (line.split('=', 1))
                scan[part1] = part2

            else:

                if line.startswith('peaklist end'):
                    line = False

                else:

                    fields = (line.split('\t', 1))
                    fields = [float(x) for x in fields]
                    scan['peak_list'].append(fields)

        scan['peak_count'] = len(scan['peak_list'])
        scan['total_ion_current'] = self.scan_total_ion_current(
            scan['peak_list'])

        pattern = 'RawFile: (.+) Index: (\d+)'
        m = re.match(pattern, scan['header'])

        scan['file'] = m.group(1)
        scan['scan_number'] = m.group(2)

        return scan

    def read_scans(self):
        """
        Reads and parses all scan in the APL file.
        :return: Generator that yield a scan dictionary
        """

        with open(self.file_name, 'r') as self.fh:

            scan = ''

            while scan is not None:

                scan = self.parse_next_scan()

                if scan is not None:
                    yield scan

    def get_scan_at_index(self, index):
        """
        Extracts a single scan from the APL by its file index.
        :param index: Integer that point to a single scan in the APL file.
        :return: Scan dictionary
        """

        with open(self.file_name, 'r') as self.fh:
            self.fh.seek(index)

            return self.parse_next_scan()

    def get_scans_dict(self, remove_fields=list(), apl_scans=None):
        """
        Returns dictionary with all scans.
        :param remove_fields: String of the field to remove.
        :param apl_scans: default dict(dict)
        :return: Dictionary
        """

        for scan in self.read_scans():

            for field in remove_fields:
                scan.pop(field, None)

            file = scan.pop('file')

            scan['apl_file'] = self.file_name

            if 'secpep' in str(scan['apl_file']):
                file += '_secpep'

            apl_scans[file][scan['scan_number']] = scan

        return apl_scans
