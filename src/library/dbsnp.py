"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import gzip
import math
from pathlib import Path

# Import Django related libraries

# Third party libraries
from Bio.Seq import Seq
import numpy as np
import pandas as pd

# Import project libraries
from .parquet import convert_csv_to_parquet
import pyarrow.parquet as pq

bases = ('A', 'T', 'C', 'G', ';')

# Genetic code
# https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi

table_r = dict()

# 1. The Standard Code (transl_table=1)
table_r[1] = {
    'F': ('TTT', 'TTC'),
    'L': ('TTA', 'TTG', 'CTT', 'CTC', 'CTA', 'CTG'),
    'S': ('TCT', 'TCC', 'TCA', 'TCG', 'AGT', 'AGC'),
    'Y': ('TAT', 'TAC'),
    'C': ('TGT', 'TGC'),
    'W': ('TGG',),
    'P': ('CCT', 'CCC', 'CCA', 'CCG'),
    'H': ('CAT', 'CAC'),
    'Q': ('CAA', 'CAG'),
    'R': ('CGT', 'CGC', 'CGA', 'CGG', 'AGA', 'AGG'),
    'I': ('ATT', 'ATC', 'ATA'),
    'M': ('ATG',),
    'T': ('ACT', 'ACC', 'ACA', 'ACG'),
    'N': ('AAT', 'AAC'),
    'K': ('AAA', 'AAG'),
    'V': ('GTT', 'GTC', 'GTA', 'GTG'),
    'A': ('GCT', 'GCC', 'GCA', 'GCG'),
    'D': ('GAT', 'GAC'),
    'E': ('GAA', 'GAG'),
    'G': ('GGT', 'GGC', 'GGA', 'GGG'),
}

# 2. The Vertebrate Mitochondrial Code (transl_table=2)
table_r[2] = {
    'F': ('TTT', 'TTC'),
    'L': ('TTA', 'TTG', 'CTT', 'CTC', 'CTA', 'CTG'),
    'S': ('TCT', 'TCC', 'TCA', 'TCG', 'AGT', 'AGC'),
    'Y': ('TAT', 'TAC'),
    'C': ('TGT', 'TGC'),
    'W': ('TGG', 'TGA'),
    'P': ('CCT', 'CCC', 'CCA', 'CCG'),
    'H': ('CAT', 'CAC'),
    'Q': ('CAA', 'CAG'),
    'R': ('CGT', 'CGC', 'CGA', 'CGG'),
    'I': ('ATT', 'ATC'),
    'M': ('ATG', 'ATA'),
    'T': ('ACT', 'ACC', 'ACA', 'ACG'),
    'N': ('AAT', 'AAC'),
    'K': ('AAA', 'AAG'),
    'V': ('GTT', 'GTC', 'GTA', 'GTG'),
    'A': ('GCT', 'GCC', 'GCA', 'GCG'),
    'D': ('GAT', 'GAC'),
    'E': ('GAA', 'GAG'),
    'G': ('GGT', 'GGC', 'GGA', 'GGG'),
}

# IUPAC nucleotide code
# https://www.bioinformatics.org/sms/iupac.html
nucleotides_match = {
    'R': ('A', 'G'),
    'Y': ('C', 'T'),
    'S': ('G', 'C'),
    'W': ('A', 'T'),
    'K': ('G', 'T'),
    'M': ('A', 'C'),
    'B': ('C', 'G', 'T'),
    'D': ('A', 'G', 'T'),
    'H': ('A', 'C', 'T'),
    'V': ('A', 'C', 'G'),
    'N': ('A', 'T', 'C', 'G')
}

translate_index = ('dbsnp_identified_peptide_sequence_variants',
                   'dbsnp_identified_peptide_sequence_variants_compact',
                   'dbsnp_identified_peptide_sequence_variants_counts',
                   'subject_nucleotide_is_ref',
                   'subject_nucleotide',
                   )
translate_empty = pd.Series(['', '', '', '', ''], index=translate_index)


def annotate_csv(csv_file: Path, dbsnp_file: Path) -> None:
    """
    Annotates peptides csv file with dbSNP
    :param csv_file:  CSV file with peptides.
    :param dbsnp_file: dbSNP vcf or vcf.gz
    :return: None
    """

    # Read peptide file with exon positions
    df = pd.read_csv(csv_file, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})
    df['chromosome'] = df['chromosome'].replace('M', 'MT')

    expanded_df = expand_chromosome_positions(df)

    # Parse and extract entries from dbSNP VCF file
    annotation_parquet = str(dbsnp_file) + '.parquet'

    if not Path(annotation_parquet).exists():
        parse_vcf(dbsnp_file)

    pf = pq.ParquetFile(annotation_parquet)

    chunks = list()

    for i in range(0, pf.num_row_groups):

        chunk = pf.read_row_group(i).to_pandas()
        chunk = expanded_df.merge(chunk, how='inner', on=['chromosome',
                                                          'dbsnp_position'])

        if len(chunk.index):
            chunks.append(chunk)

    if len(chunks):
        snp_df = pd.concat(chunks)
        merge_df = df.merge(snp_df, how='left', on=['chromosome',
                                                    'exon_positions'])

        # Translate variants
        translate_df = merge_df.apply(translate, axis=1)
        merge_df = pd.concat([merge_df, translate_df], axis=1)

        # Overwrite CSV file
        with csv_file.open(mode='r+') as f:
            f.readline()
            f.readline()
            f.seek(f.tell())

            merge_df.to_csv(f, index=False)


def expand_chromosome_positions(df: pd.DataFrame) -> pd.DataFrame:
    """
    Expands chromosome positions.
    :param df: Peptides dataframe with genomic locations
    :return: Peptides dataframe with expanded genomic locations
    """

    rows = list()

    tmp_df = df[df['mapped'] == True]
    tmp_df = tmp_df[['chromosome', 'exon_positions']].drop_duplicates()

    for row in tmp_df.itertuples():

        for exon_position in row.exon_positions.split(';'):

            start, stop = exon_position.split('-')

            for position in range(int(start), int(stop) + 1, 1):

                rows.append((row.chromosome, position, row.exon_positions))

    expanded_df = pd.DataFrame(rows, columns=['chromosome', 'dbsnp_position',
                                              'exon_positions'])

    return expanded_df


def fix_codon(peptide, dna_seq, table):
    """
    Fixes uncertain nucleotide in codon based on the peptide sequence.
    :param peptide: String
    :param dna_seq: String
    :param table: Genetic code table
    :return: List
    """

    nucleotides = list()

    base_positions = (0, 1, 2)

    for i, residue in enumerate(list(peptide)):

        codon = dna_seq[i * 3:i * 3 + 3]

        bad = [base for base in codon if base not in bases]

        if len(bad):

            codons = table_r[table][residue]

            max_matches = 0
            max_nucleotides = list()

            for c in codons:

                matches = 0
                codon_nucleotides = list()

                for j in base_positions:

                    if codon[j] == c[j]:

                        matches += 1

                    else:
                        nucleotide_codon = codon[j]
                        nucleotide_c = c[j]

                        if nucleotide_codon in nucleotides_match:
                            if nucleotide_c in nucleotides_match[nucleotide_codon]:
                                matches += 1.0 / len(nucleotides_match[nucleotide_codon])
                                codon_nucleotides.append(nucleotide_c)

                if matches > max_matches:
                    max_nucleotides = codon_nucleotides
                    max_matches = matches

            nucleotides.extend(max_nucleotides)

    return nucleotides


def parse_vcf(dbsnp_file: Path) -> None:
    """
    Parses and extracts entries from dbSNP VCF file and stores it into
    Parquet format

    :param dbsnp_file: dbSNP VCF or VCF.gz file
    :return: None
    """

    columns = ('chromosome', 'dbsnp_position', 'dbsnp_rs',
               'dbsnp_ref', 'dbsnp_alt', 'dbsnp_caf', 'dbsnp_maf',
               'dbsnp_major_allele', 'dbsnp_validated', 'dbsnp_nsm')
    info_fields = ['CAF', 'TOPMED']

    csv_file = str(dbsnp_file) + '.csv'
    parquet_file = str(dbsnp_file) + '.parquet'

    if dbsnp_file.suffix == '.gz':
        file_object = gzip.open(dbsnp_file, 'rt')

    else:
        file_object = dbsnp_file.open()

    with file_object as f, open(csv_file, 'w') as out:

        out.write(','.join(columns))
        out.write('\n')

        line = f.readline()

        while not line.startswith('#CHROM'):
            line = f.readline()

        for line in f:

            line = line.rstrip()
            chrom, pos, id_, ref, alt, qual, filter_, info = line.split('\t')

            # Skip MNV
            if len(ref) > 1:
                continue

            annotations = {
                'CAF': '',
                'MAF': '',
                'NSM': False,
                'TOPMED': '',
                'VLD': False,
            }

            for field in info.split(';'):

                if '=' in field:
                    field_name, value = field.split('=')

                    if field_name in info_fields:
                        annotations[field_name] = value

                if field == 'NSM':
                    annotations['NSM'] = True

                if field == 'VLD':
                    annotations['VLD'] = True

            if annotations['NSM']:

                major_allele = 'REF'

                if annotations['TOPMED'] != '' and annotations['CAF'] == '':
                    annotations['CAF'] = annotations['TOPMED']

                if annotations['CAF'] != '':
                    caf = annotations['CAF']

                    caf_list_tmp = caf.split(',')
                    caf_list_tmp = [float(x) for x in caf_list_tmp if x not in ['.', '']]

                    caf_list = sorted(caf_list_tmp, reverse=True)

                    if len(caf_list) == 1:
                        caf_list.insert(0, 1 - caf_list[0])

                    if len(caf_list) >= 2:
                        annotations['MAF'] = str(caf_list[1])

                        if caf_list[0] != float(caf_list_tmp[0]):
                            major_allele = 'ALT'

                values = (chrom, pos, id_, ref, alt.replace(',', ';'),
                          annotations['CAF'].replace(',', ';'),
                          annotations['MAF'],
                          major_allele, annotations['VLD'],
                          annotations['NSM'])

                out.write(','.join([str(x) for x in values]))
                out.write('\n')

    # Convert CSV to Parquet format
    dtype = {
        'chromosome': object,
    }
    convert_csv_to_parquet(csv_file, parquet_file, dtype=dtype)

    Path(csv_file).unlink()


def translate(row):
    """
    Translates sequence variants
    :param row: Data frame row
    :return: Pandas series
    """

    exon_sequences = row.exon_sequences

    if row.dbsnp_position is not np.NAN and math.isnan(row.dbsnp_position) is False and exon_sequences is not np.NAN:

        table = 1

        if row.chromosome == 'MT':
            table = 2

        nucleotides = fix_codon(row['Peptide sequence'],
                                exon_sequences.replace(';', ''),
                                table)

        if nucleotides:

            exon_sequences = ''

            for nucleotide in list(row.exon_sequences):

                if nucleotide not in bases:
                    nucleotide = nucleotides.pop(0)

                exon_sequences += nucleotide

        exon_num = 0
        diff = None

        for i, position in enumerate(row.exon_positions.split(';')):

            start, stop = position.split('-')
            start = int(start)
            stop = int(stop)

            diff = int(row.dbsnp_position) - start

            if 0 <= diff <= stop - start:
                exon_num = i
                break

        nucleotides = [row.dbsnp_ref] + row.dbsnp_alt.split(';')

        if row.strand == '-':
            diff = int(diff / -1) - 1

            nucleotides = [str(Seq(nucleotide).complement())
                           for nucleotide in nucleotides]

        peptides = list()

        identified_nucleotide = None

        for nucleotide in nucleotides:

            dna_seq = ''

            for i, exon in enumerate(exon_sequences.split(';')):

                if i == exon_num:
                    exon = list(exon)
                    identified_nucleotide = exon[diff]
                    exon[diff] = nucleotide
                    exon = ''.join(exon)

                dna_seq += exon

            coding_dna = Seq(dna_seq)

            peptide = str(coding_dna.translate(table=table))

            if peptide not in peptides:
                peptides.append(peptide)

        residues = None

        variant_counts = 1

        for peptide in peptides:

            if residues is None:

                residues = list(peptide)

            else:

                for i, residue in enumerate(list(peptide)):

                    if residue != residues[i]:

                        if residue not in residues[i]:
                            residues[i] += '/' + residue
                            variant_counts += 1

        peptides_alt = ';'.join(peptides)
        variant = ''.join(residues)

        nucleotide_is_ref = identified_nucleotide == nucleotides[0]

        if row.strand == '-':
            identified_nucleotide = str(Seq(identified_nucleotide).complement())

        return pd.Series([peptides_alt, variant, variant_counts,
                          nucleotide_is_ref,
                          identified_nucleotide
                          ],
                         index=translate_index)

    else:
        return translate_empty
