"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Third party
import numpy as np
from scipy import stats


# Import project libraries


class FDR:
    @staticmethod
    def bin_count_cumsum(x, x_bin):
        """
        1D count binning of x values, then it reverse array rows and
        finally compute cumulative sum for rows and columns.
        :param x: List of values
        :param x_bin: List of x bin values
        :return: Numpy array
        """

        array = stats.binned_statistic(x, list(), 'count',
                                       bins=x_bin)

        reversed_array = array.statistic[::-1]
        np.cumsum(reversed_array, axis=0, out=reversed_array)

        return reversed_array

    @staticmethod
    def bin2d_count_cumsum(x, y, x_bin, y_bin):
        """
        2D count binning of x, y pair value, then it reverse array rows and
        finally compute cumulative sum for rows and columns.
        :param x: List of values
        :param y: List of values where length is equal to x
        :param x_bin: List of x bin values
        :param y_bin: List of y bin values
        :return: Numpy array
        """
        array = stats.binned_statistic_2d(x, y, list(), 'count',
                                          bins=[x_bin, y_bin])

        row_reversed_array = array.statistic[::-1]
        np.cumsum(row_reversed_array, axis=0, out=row_reversed_array)
        np.cumsum(row_reversed_array, axis=1, out=row_reversed_array)

        return row_reversed_array

    @staticmethod
    def fdr_score(tp_data, fp_data, fdr_threshold):
        """
        Compute FDR based on peptide score.
        :param tp_data: List of peptide score for true positives.
        :param fp_data: List of peptide score (key: 'peptide_score') for false positives.
        :param fdr_threshold: FDR threshold float value between 0 and 1.
        :return: nb_target, score_threshold
        """

        np.seterr(divide='ignore', invalid='ignore')

        score_bins = range(0, 410, 1)

        if len(fp_data) == 0:
            return [None, None]

        tp_count = FDR.bin_count_cumsum(tp_data, score_bins)
        fp_count = FDR.bin_count_cumsum(fp_data, score_bins)

        fdr = fp_count / tp_count

        fdr_mask = fdr <= fdr_threshold

        if len(tp_count[fdr_mask]):

            nb_target = tp_count[fdr_mask].max()
            indices = np.where(np.logical_and(fdr_mask, tp_count == nb_target))

            score_threshold = list(reversed(score_bins))[indices[0][0] + 1]

        else:
            # FDR is higher than threshold
            nb_target, score_threshold = [None, 'Min FDR ' + str(np.nanmin(fdr))]

        return nb_target, score_threshold

    @staticmethod
    def fdr_score_mhc(tp_data, fp_data, fdr_threshold):
        """
        Compute 2D FDR base on peptide score and mhc binding score.
        :param tp_data: Dict that contains list of peptide score (key: 'peptide_score') and list of netmhc binding affinity (key: 'netmhc_binding') for true positives.
        :param fp_data: Dict that contains list of peptide score (key: 'peptide_score') and list of netmhc binding affinity (key: 'netmhc_binding') for false positives.
        :param fdr_threshold: FDR threshold float value between 0 and 1.
        :return: nb_target, score_threshold, mhc_threshold
        """

        np.seterr(divide='ignore', invalid='ignore')

        score_bins = range(0, 410, 1)
        mhc_bins = range(0, 50250, 250)

        if len(fp_data['peptide_score']) == 0:
            return [None, None, None]  # Should replace with Named tuple

        tp_count = FDR.bin2d_count_cumsum(tp_data['peptide_score'],
                                          tp_data['mhc_binding'], score_bins,
                                          mhc_bins)
        fp_count = FDR.bin2d_count_cumsum(fp_data['peptide_score'],
                                          fp_data['mhc_binding'], score_bins,
                                          mhc_bins)

        fdr = fp_count / tp_count

        fdr_mask = fdr <= fdr_threshold

        if len(tp_count[fdr_mask]):

            nb_target = tp_count[fdr_mask].max()
            indices = np.where(np.logical_and(fdr_mask, tp_count == nb_target))

            mhc_threshold = mhc_bins[indices[1][0] + 1]
            score_threshold = list(reversed(score_bins))[indices[0][0] + 1]

        else:
            nb_target, score_threshold, mhc_threshold = [None, None, None]

        return nb_target, score_threshold, mhc_threshold
