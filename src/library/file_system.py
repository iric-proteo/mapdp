"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""




# Import standard libraries
import os


def get_files(directory, extensions):
    """
    This function inspects the folder supplied,
    searches for files and return a list of files.
    :param directory: Directory to search for sub-folder.
    :param extensions: Filter files with an extension list.
    :return: List of file names.
    """

    if extensions != '':
        return [name for name in os.listdir(directory)
                if name.endswith(extensions)]

    else:
        return [name for name in os.listdir(directory)]


def get_subdirectories(directory):
    """
    This function inspects the folder supplied,
    searches for sub folders and return a list.
    :param directory: Directory to search for sub-folder.
    :return: List of directories.
    """

    return [name for name in os.listdir(directory)
            if os.path.isdir(os.path.join(directory, name))]
