"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
from typing import (List, Union)

# Import Django related libraries

# Third party libraries

# Import project libraries


def maf_from_frequencies(frequencies: List) -> Union[float, str]:
    """
    Extracts MAF from a list of allele frequencies
    :param frequencies: List of allele frequencies
    :return: MAF
    """

    maf = ''

    frequencies = [float(x) for x in frequencies if x not in ['', '.']]

    if len(frequencies):
        frequencies.append(1 - sum(frequencies))
        frequencies = sorted(frequencies, reverse=True)
        maf = frequencies[1]

    return maf
