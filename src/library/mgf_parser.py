"""
Copyright 2015-2018 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal
"""


# Import standard libraries

# Import project libraries

# Create your views here.


class MGFParser(object):
    def __init__(self, file_name):

        self.file_name = file_name
        self.fh = None

    @staticmethod
    def scan_total_ion_current(peak_list):
        """
        Sums the intensity of all peaks
        :param peak_list: List of [mz,intensity]
        :return: Float
        """

        tic = 0

        for mz, intensity in peak_list:
            tic += intensity

        return tic

    def parse_next_scan(self):
        """
        Extract values from one scan.
        :return: Dictionary
        """

        scan = dict()
        scan['peak_list'] = list()

        # Search beginning of the scan
        scan['scan_file_index'] = self.fh.tell()
        line = self.fh.readline()

        while not line.startswith('BEGIN IONS'):
            scan['scan_file_index'] = self.fh.tell()
            line = self.fh.readline()

            if line == '':
                return None

        # Read fields
        while line:
            line = self.fh.readline().strip()

            if '=' in line:
                part1, part2 = (line.split('=', 1))
                scan[part1] = part2

            else:

                if line.startswith('END'):
                    line = False

                else:

                    fields = (line.split(' ', 1))
                    fields = [float(x) for x in fields]
                    scan['peak_list'].append(fields)

        scan['peak_count'] = len(scan['peak_list'])
        scan['total_ion_current'] = self.scan_total_ion_current(
            scan['peak_list'])

        return scan

    def read_raw_scans(self):
        """
        Reads raw scan in the MGF file.
        :return: Generator that yield a raw scan rows
        """

        with open(self.file_name, 'r') as self.fh:

            scan_lines = []

            for line in self.fh:

                scan_lines.append(line)

                if line.startswith('END'):
                    yield scan_lines
                    scan_lines = []

    def read_scans(self):
        """
        Reads and parses all scan in the MGF file.
        :return: Generator that yield a scan dictionary
        """

        with open(self.file_name, 'r') as self.fh:

            scan = ''

            while scan is not None:

                scan = self.parse_next_scan()

                if scan is not None:
                    yield scan

    def get_scan_at_index(self, index):
        """
        Extracts a single scan from the MGF by its file index.
        :param index: Integer that point to a single scan in the MGF file.
        :return: Scan dictionary
        """

        with open(self.file_name, 'r') as self.fh:
            self.fh.seek(index)

            return self.parse_next_scan()

    def get_scans_dict(self, remove_fields=[]):
        """
        Returns dictionary with all scans.
        :param remove_fields: String of the field to remove.
        :return: Dictionary
        """

        mgf_scans = dict()

        for scan in self.read_scans():

            mgf_scans[scan['TITLE']] = scan

            for field in remove_fields:
                scan.pop(field, None)

        return mgf_scans
