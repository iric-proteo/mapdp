"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
from collections import OrderedDict
import requests

# Third party libraries


# Import project libraries

URL = 'https://raw.githubusercontent.com/ANHIG/IMGTHLA/Latest/wmda/hla_nom_p.txt'


def retrieve_hla_alleles():
    """
    Retrieves hla alleles from alleles.org
    :return: List of allele name
    """

    alleles = OrderedDict()

    for line in requests.get(URL).iter_lines():

        line = line.decode('utf-8').strip('\n')

        if not line.startswith('#'):
            # Format following nomenclature up to level 2
            # http://hla.alleles.org/announcement.html

            gene, fields = line.split(';', 1)

            for subfield in fields.split('/'):
                name = 'HLA-%s%s' % (gene, subfield[0:5])

                alleles[name] = True

    return list(alleles.keys())
