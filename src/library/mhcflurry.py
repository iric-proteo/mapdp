"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from pathlib import Path
import random
import string
from typing import Callable, Generator, Iterable, List, Set

# Third party libraries
from mhc_pipeline.library.nextflow import Nextflow


def batch_peptides(peptides: Iterable, batch_size: int = 50000) \
        -> Generator[List, None, None]:
    """
    Creates a generator that yield list of peptides

    :param peptides: Iterable of unique peptides.
    :param batch_size: Number of peptides per batch.
    :return: Generator of peptide batch
    """

    short_list = list()

    for peptide in peptides:

        short_list.append(peptide)

        if len(short_list) == batch_size:
            yield short_list

            # Reset peptide batch
            short_list = list()

    yield short_list


def get_supported_alleles() -> Set[str]:
    """
    Reads MHCflurry supported alleles
    :return: Supported alleles
    """

    alleles = set()

    with (Path(__file__).parent / 'mhc_flurry_2.0.1_alleles').open() \
            as fh_in:
        for line in fh_in:
            alleles.add(line.rstrip())

    return alleles


def queue_jobs(parameters: Iterable[dict],
               format_predictions: Callable) -> None:
    """
    Queues MHCflurry jobs via Nextflow
    :param parameters: Parameters for each jobs
    :param format_predictions: Callback function that receives predictions
    :return: None
    """

    characters = string.ascii_lowercase + string.ascii_uppercase
    key = ''.join([random.choice(characters) for _ in range(10)])

    with Nextflow(workflow='mhcflurry',
                  identifier=key) as nf:
        light_parameters = write_files(parameters, nf.work_dir)
        nf.run(list())

        for predictions in parse_predictions(light_parameters,
                                             nf.work_dir):
            format_predictions(predictions)


def parse_predictions(light_parameters: List[dict],
                      work_dir: Path) -> Generator[tuple, None, None]:
    """
    Reads predictions files, parse values and calls formatting
    :param light_parameters: Parameters for each jobs without peptides
    :param work_dir: Nextflow working directory
    :return: Generator that output tuple of predictions and parameters
    """

    for i, params in enumerate(light_parameters):

        prediction_file = work_dir / 'results' / f'pred_peptides_{i}.csv'

        if prediction_file.exists():

            def _generator():

                with prediction_file.open() as fh_in:

                    for line in fh_in:

                        if line.startswith('allele,peptide'):
                            break

                    for line in fh_in:
                        values = line.rstrip().split(',')

                        yield {
                            'peptide': values[1],
                            'affinity': '%.2f' % float(values[2]),
                            'rank': '%.1f' % float(values[3]),
                            'processing_score': '%.2f' % float(values[4]),
                            'presentation_score': '%.2f' % float(values[5]),
                            'presentation_rank': '%.1f' % float(values[6])
                        }

            yield _generator(), params


def write_files(parameters: Iterable[dict], work_dir: Path) -> List[dict]:
    """
    Writes peptide files for each allele
    :param parameters: Parameters for each jobs
    :param work_dir: Nextflow working directory
    :return: Parameters without peptide batches
    """

    light_parameters = list()
    supported_alleles = get_supported_alleles()

    for i, params in enumerate(parameters):

        # Discard allele not supported by MHCflurry to avoid Nextflow
        # workflow stop
        if params['allele'] in supported_alleles:

            with (work_dir / f'peptides_{i}.csv').open('w') as fh_out:

                fh_out.write('allele,peptide\n')

                for peptide in params['peptide_batch']:
                    fh_out.write(f'{params["allele"]},{peptide}\n')

        del (params['peptide_batch'])

        light_parameters.append(params)

    return light_parameters
