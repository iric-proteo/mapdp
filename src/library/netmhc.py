"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from collections import defaultdict
from multiprocessing.dummy import Pool
import os
import re
import requests
import time

# Third party libraries


CONFIG = {
    'NETMHC34': {
        'allele_replace': ('*',),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+(?P<peptide>\w+)\s+(?P<logscore>\d.\d+)\s+(?P<affinity>\d+)\s+'
    },
    'NETMHC4': {
        'allele_replace': (':', '*'),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+[\w-]+\s+(?P<peptide>\w+)\s+(?P<core_offset>[-\w]+).+PEPLIST\s+(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+(?P<rank>\d+.\d+)'
    },
    'NETMHCII22': {
        'allele_replace': (':', '*'),
        'parsing_regexp': r'^.+?\s(?P<pos>\d+)\s+(?P<peptide>\w+).+?(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+'
    },
    'NETMHCII23': {
        'allele_replace': (('HLA-DR', 'DR'), ('*', '_'), ':'),
        'parsing_regexp': r'^.+?\s(?P<pos>\d+)\s+(?P<peptide>\w+).+?(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+'
    },
    'NETMHCpan30': {
        'allele_replace': ('*',),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+\w+-.+?\s+(?P<peptide>\w+).+PEPLIST\s+(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+(?P<rank>\d+\.\d+)'
    },
    'NETMHCpan40': {
        'allele_replace': ('*',),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+\w+-.+?\s+(?P<peptide>\w+).+PEPLIST\s+(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+(?P<rank>\d+\.\d+)'
    },
    'NETMHCpan41': {
        'allele_replace': ('*',),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+\w+-.+?\s+(?P<peptide>\w+).+PEPLIST\s+(?P<score_el>\d+.\d+)\s+(?P<rank_el>\d+\.\d+)\s+(?P<score_ba>\d.\d+)\s+(?P<rank>\d+\.\d+)\s+(?P<affinity>\d+\.\d+)'
    },
    'NETMHCIIpan31': {
        'allele_replace': (('HLA-DR', 'DR'), ('*', '_'), ':'),
        'parsing_regexp': r'^.+?\s(?P<pos>\d+)\s+.+?\s+(?P<peptide>\w+).+?\d.\d+\s+(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+(?P<rank>\d+\.\d+)'
    },
    'NETMHCIIpan32': {
        'allele_replace': (('HLA-DR', 'DR'), ('*', '_'), ':'),
        'parsing_regexp': r'^.+?\s(?P<pos>\d+)\s+.+?\s+(?P<peptide>\w+).+?\d.\d+\s+(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+(?P<rank>\d+\.\d+)'
    },
    'NETMHCIIpan40': {
        'allele_replace': (('HLA-DR', 'DR'), ('*', '_'), ':'),
        'parsing_regexp': r'^.+?\s?(?P<pos>\d+)\s+.+?\s+(?P<peptide>\w+).+?Sequence.+?\s+(?P<score_el>\d.\d+)\s+(?P<rank_el>\d+\.\d+)\s+(?P<exp>[\d\w\.]+)\s+(?P<score_ba>\d\.\d+)\s+(?P<affinity>\d+\.\d+)\s+(?P<rank>\d+\.\d+)'
    },
    'NETMHCCONS11': {
        'allele_replace': ('*',),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+(?P<allele>[-:\w]+)\s+(?P<peptide>\w+)\s+(?P<peplist>\w+)\s+(?P<logscore>\d.\d+)\s+(?P<affinity>\d+\.\d+)\s+(?P<rank>\d+\.\d+)'
    },
    'NETMHCSTABPAN1a': {
        'allele_replace': (),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+[\*-:\w]+\s+(?P<peptide>\w+)\s+\w+\s+(?P<Pred>\d.\d+)\s+(?P<Thalf_h>\d+\.\d+)\s+(?P<Rank_Stab>\d+\.\d+)'
    },
    'NETCTLPAN11b': {
        'allele_replace': (),
        'parsing_regexp': r'^\s*(?P<pos>\d+)\s+.+\s+(?P<peptide>\w+)\s+(?P<mhc>\d+\.\d+)\s+(?P<tap>-*\d+\.\d+)\s+(?P<cle>-*\d+\.\d+)\s+(?P<comb>-*\d+\.\d+)\s+(?P<rank>-*\d+\.\d+)'
    },
}


class NetMHC:
    def __init__(self, predictor):
        """
        Initializes NetMHC predictor instance
        :param predictor: String (see CONFIG for valid one)
        """

        self.config = CONFIG[predictor]
        self.predictor = predictor
        self.url = os.environ['NETMHC_URL']

    def alive_check(self):
        """
        Checks if the NetMHC service is accessible.
        :return: Boolean
        """

        try:
            response = requests.get(self.url + '/alive/')
        except (requests.exceptions.ConnectTimeout,
                requests.exceptions.ConnectionError):
            return False

        return response.status_code == 204

    @staticmethod
    def batch_peptides_by_length(peptides, batch_size=50000):
        """
        Creates a generator that yield list of peptides

        :param peptides: Iterable of unique peptides.
        :param batch_size: Number of peptides per batch.
        :return: Generator
        """

        peptide_lists = defaultdict(list)

        for peptide in peptides:

            peptide_length = len(peptide)
            peptide_lists[peptide_length].append(peptide)

            if len(peptide_lists[peptide_length]) == batch_size:
                yield peptide_lists[peptide_length]

                # Reset peptide batch
                peptide_lists[peptide_length] = list()

        for peptide_length, peptide_list in peptide_lists.items():

            if len(peptide_list):
                yield peptide_list

    def format_allele_name(self, allele):
        """
        Formats MHC allele name in the format expected by the NetMHC predictor.
        :param allele: Allele name string
        :return: String
        """

        for query in self.config['allele_replace']:

            if type(query) == tuple:
                allele = allele.replace(query[0], query[1])
            else:
                allele = allele.replace(query, '')

        return allele

    @staticmethod
    def protein_peptides(sequence, length):
        """
        Generates all peptides of specified length from a sequence.
        :param sequence: String
        :param length: Integer
        :return: Generator
        """

        return (sequence[i:i + length] for i in range(0, len(sequence) - length + 1))

    @staticmethod
    def queue_jobs(parameters, callback, processes=20):
        """
        Queues NetMHC jobs
        :param parameters: Iterable with parameters for each jobs with func
        :param callback: Callback function that receives predictions
        :param processes: Number of concurrent processes
        :return: None
        """

        pool = Pool(processes=processes)

        for values in pool.imap_unordered(NetMHC.run_job, parameters):
            callback(values)

    def run_netmhc(self, mhc_peptides, allele, trial=8):
        """
        Runs NetMHC and parses prediction.

        Expect that mhc_peptides are pre-batched by length.

        :param mhc_peptides: List of MHC peptides.
        :param allele: MHC allele string
        :param trial: Integer
        :return: List of peptides with a prediction dictionary.
        """

        args = {'allele': self.format_allele_name(allele),
                'peptides': '\n'.join(mhc_peptides),
                'predictor': self.predictor}

        response = None
        sleep_time = 0.1

        # TODO=MC - Requests has a retry api more elegant
        # https://hodovi.ch/blog/advanced-usage-python-requests-timeouts-retries-hooks/
        while trial > 0:

            try:
                response = requests.post(self.url + '/predict', data=args)

                if response.status_code == 200:
                    break

            except requests.exceptions.ConnectionError:
                pass

            trial -= 1
            time.sleep(sleep_time)
            sleep_time *= 2

        if not hasattr(response, 'status_code'):
            raise requests.exceptions.ConnectionError()

        if response.status_code != 200:
            if response.status_code == 504:
                raise requests.exceptions.ConnectTimeout(response.status_code)
            else:
                raise requests.exceptions.ConnectionError(response.status_code)

        # Read NetMHC output

        def _generator():

            regexp = re.compile(self.config['parsing_regexp'])

            for line in response.iter_lines():

                line = line.decode()
                #print(line)

                if 'Error' in line or 'ERROR' in line:

                    raise RuntimeError(response.text)

                m = regexp.match(line)

                if m is not None:
                    values = m.groupdict()

                    yield values

        return _generator()

    @staticmethod
    def run_job(params):
        """
        Runs a NetMHC job

        This function unpacks the single parameter provided by pool.imap_unordered
        to all the parameters required for the prediction function.
        :param params: Tuple with params from job_params.
        :return: Tuple with results and parameters
        """

        predictor = NetMHC(params['predictor'])
        predictions = predictor.run_netmhc(params['peptide_batch'],
                                           params['allele'])

        return predictions, params
