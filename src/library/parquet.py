"""
Copyright 2015-2018 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries

# Third party libraries
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

# Import project libraries


def convert_csv_to_parquet(csv_file: str, parquet_file: str,
                           chunk_size: int=100000, dtype: dict=None) -> None:
    """
    Converts MAF csv file to parquet format
    :param csv_file: Path to csv file
    :param parquet_file: Patht to parquet file
    :param chunk_size: Number of rows per group
    :param dtype: Data type for data or columns
    :return: None
    """

    chunks = pd.read_csv(csv_file, chunksize=chunk_size, dtype=dtype)

    for i, chunk in enumerate(chunks):

        if i == 0:
            # Infer schema and open parquet file on first chunk
            parquet_schema = pa.Table.from_pandas(df=chunk).schema
            parquet_writer = pq.ParquetWriter(parquet_file, parquet_schema,
                                              compression='snappy')

        table = pa.Table.from_pandas(chunk, schema=parquet_schema)
        parquet_writer.write_table(table)

    parquet_writer.close()
