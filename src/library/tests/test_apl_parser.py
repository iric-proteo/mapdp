"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""




# Import standard libraries
from collections import defaultdict
import os
import unittest

# Import project libraries
from ..apl_parser import APLParser


# Create your views here.


class APLParserTest(unittest.TestCase):
    def setUp(self):
        file_name = os.path.join('library', 'tests', 'MQ.apl')
        self.parser = APLParser(file_name)

    def test_get_scan_at_index(self):
        scan = self.parser.get_scan_at_index(4581)

        self.assertEqual(scan['scan_number'], '6124')

    def test_read_scans(self):
        self.assertEqual(sum(1 for _ in self.parser.read_scans()), 5)

    def test_scan_total_ion_current(self):
        peak_list = [[1, 1], [2, 2], [3, 3]]

        self.assertEqual(APLParser.scan_total_ion_current(peak_list), 6)

    def test_get_scans_dict(self):
        self.assertIsInstance(
            self.parser.get_scans_dict(remove_fields=['peak_list'],
                                       apl_scans=defaultdict(dict)), dict)

        file_name = os.path.join('library', 'tests', 'MQ_secpep.apl')
        self.parser = APLParser(file_name)

        self.assertIsInstance(
            self.parser.get_scans_dict(remove_fields=['peak_list'],
                                       apl_scans=defaultdict(dict)), dict)
