"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
from collections import defaultdict
import os
from pathlib import Path
import shutil
from unittest import TestCase

# Third party libraries
import numpy as np
import pandas as pd

# Import project libraries
from .. import dbsnp


class Test(TestCase):

    def test_annotate_csv(self):
        dbsnp_file = Path(os.getcwd() + '/test_files/dbsnp/dbsnp.vcf')
        csv_file_ori = Path(os.getcwd() + '/test_files/dbsnp/annotate.csv')
        csv_file_tmp = Path(os.getcwd() + '/test_files/dbsnp/annotate_tmp.csv')
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        out = dbsnp.annotate_csv(csv_file_tmp, dbsnp_file)

    def test_fix_codon(self):
        out = dbsnp.fix_codon('PDPAKSAPAPKKGSKK',
                              'CCNGATCCAGCTAAGTCCGCTCCCGCCCCGAAGAAGGGCTCCAAGAAG',
                              1)
        self.assertIsInstance(out, list)
        self.assertIn('T', out)

        # Test two unknown nucleotides in codon
        out = dbsnp.fix_codon('EDIVADHVASYGVN',
                              'GAAGACATTGTGGCTGACCACGTYGCCTCTTRYGGTGTAAAC',
                              1)

        self.assertEqual(['T', 'A', 'T'], out)

        out = dbsnp.fix_codon('DTQFVRFDSDAASQR',
                              'GACACGCAGTTCGTGCGGTTCGACAGCGACGCCGCGAGCCAGARG',
                              1)

        self.assertEqual(['G'], out)

    def test_parse_vcf(self):
        chromosome_positions = {
            ('1', '69899'): [''],
            ('1', '69995'): [''],
            ('1', '241319'): [''],
        }

        dbsnp_file = Path(os.getcwd() + '/test_files/dbsnp/dbsnp.vcf')
        out = dbsnp.parse_vcf(dbsnp_file)

        Path(os.getcwd() + '/test_files/dbsnp/dbsnp.vcf.parquet').unlink()

        dbsnp_file = Path(os.getcwd() + '/test_files/dbsnp/dbsnp.vcf.gz')
        out = dbsnp.parse_vcf(dbsnp_file)

        Path(os.getcwd() + '/test_files/dbsnp/dbsnp.vcf.gz.parquet').unlink()

    def test_translate(self):
        data = {
            'mapped': [True],
            'chromosome': ['1'],
            'Peptide sequence': ['PDPAKSAPAPKKGSKK'],
            'strand': ['-'],
            'exon_sequences': ['CCNGATCCAGCGAAATCCGCTCCTGCTCCCAAGAAGGGCTCCAAAAAG'],
            'exon_positions': ['149783828-149783875'],
            'dbsnp_position': 149783828,
            'dbsnp_ref': ['C'],
            'dbsnp_alt': ['A']
        }

        df = pd.DataFrame(data=data)

        row = df.iloc[0]

        out = dbsnp.translate(row)
        self.assertIsInstance(out, pd.Series)

        data = {
            'mapped': [True],
            'chromosome': ['MT'],
            'Peptide sequence': ['PDPAKSAPAPKKGSKI'],
            'strand': ['-'],
            'exon_sequences': ['CCNGATCCAGCGAAATCCGCTCCTGCTCCCAAGAAGGGCTCCAAAAAG'],
            'exon_positions': ['149783828-149783875'],
            'dbsnp_position': 149783828,
            'dbsnp_ref': ['C'],
            'dbsnp_alt': ['A']
        }

        df = pd.DataFrame(data=data)
        row = df.iloc[0]
        out = dbsnp.translate(row)

        df.loc[0, 'dbsnp_position'] = np.NAN
        row = df.iloc[0]

        out = dbsnp.translate(row)
