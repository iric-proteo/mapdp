"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
from unittest import TestCase

# Third party libraries

# Import project libraries
from ..fdr import FDR


class FDRTest(TestCase):
    def test_bin_count_cumsum(self):
        x = [1, 2, 13]

        x_bin = [0, 10, 20]

        array = FDR.bin_count_cumsum(x, x_bin)

        self.assertEqual(array.shape, (len(x_bin) - 1,))
        self.assertEqual(array.max(), len(x))

    def test_bin2d_count_cumsum(self):
        x = [1, 2, 13]
        y = [5, 5, 100]

        x_bin = [0, 10, 20]
        y_bin = [0, 100, 200]

        array = FDR.bin2d_count_cumsum(x, y, x_bin, y_bin)

        self.assertEqual(array.shape, (len(x_bin) - 1, len(y_bin) - 1))
        self.assertEqual(array.max(), len(x))

    def test_fdr_score(self):
        tp_data = [40, 30, 30]

        fp_data = [1, 2, 3]

        fdr_threshold = 0.01

        nb_target, score_threshold = FDR.fdr_score(
            tp_data, fp_data, fdr_threshold)

        self.assertEqual(nb_target, 3)

        tp_data = [1, 2, 3]

        fp_data = [40, 30, 30]

        nb_target, score_threshold = FDR.fdr_score(
            tp_data, fp_data, 0)

        self.assertEqual(nb_target, None)

        nb_target, score_threshold = FDR.fdr_score(
            tp_data, [], fdr_threshold)

        self.assertEqual(nb_target, None)

    def test_fdr_score_mhc(self):
        tp_data = {
            'peptide_score': [40, 30, 30],
            'mhc_binding': [100, 20, 1000],
        }

        fp_data = {
            'peptide_score': [1, 2, 3],
            'mhc_binding': [1000, 2000, 5000],
        }

        fdr_threshold = 0.01

        nb_target, score_threshold, netmhc_threshold = FDR.fdr_score_mhc(
            tp_data, fp_data, fdr_threshold)

        self.assertEqual(nb_target, 3)

    def test_fdr_score_mhc_nothing_below_fdr_threshold(self):
        tp_data = {
            'peptide_score': [1, 2, 2],
            'mhc_binding': [100, 20, 1000],
        }

        fp_data = {
            'peptide_score': [5, 6, 7],
            'mhc_binding': [10, 20, 50],
        }

        fdr_threshold = 0.01

        nb_target, score_threshold, mhc_threshold = FDR.fdr_score_mhc(
            tp_data, fp_data, fdr_threshold)

        self.assertEqual(nb_target, None)

    def test_fdr_score_mhc_no_fp(self):
        tp_data = {
            'peptide_score': [1, 2, 2],
            'mhc_binding': [100, 20, 1000],
        }

        fp_data = {
            'peptide_score': [],
            'mhc_binding': [],
        }

        fdr_threshold = 0.01

        nb_target, score_threshold, mhc_threshold = FDR.fdr_score_mhc(
            tp_data, fp_data, fdr_threshold)

        self.assertEqual(nb_target, None)
