"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""




# Import standard libraries
from unittest import (skipIf,
                      TestCase)

# Third party libraries

# Import project libraries
from ..file_system import (get_files,
                           get_subdirectories)


class FileSystemTest(TestCase):

    def test_get_sub_directories(self):
        subdirectories = get_subdirectories('.')

        self.assertGreater(len(subdirectories), 0)



    def test_get_files(self):
        files = get_files('.', '')

        self.assertGreater(len(files), 0)

        files = get_files('.', '.do_not_exits')

        self.assertEqual(len(files), 0)
