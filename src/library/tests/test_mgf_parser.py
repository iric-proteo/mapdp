"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""




# Import standard libraries
import os
import unittest

# Import project libraries
from ..mgf_parser import MGFParser


# Create your views here.


class MGFParserTest(unittest.TestCase):
    def setUp(self):
        file_name = os.path.join('library', 'tests', 'PEAKS.mgf')
        self.parser = MGFParser(file_name)

    def test_get_scan_at_index(self):
        scan = self.parser.get_scan_at_index(421748)

        self.assertEqual(scan['SCANS'], '3417')

    def test_read_scans(self):
        self.assertEqual(sum(1 for _ in self.parser.read_scans()), 153)

    def test_scan_total_ion_current(self):
        peak_list = [[1, 1], [2, 2], [3, 3]]

        self.assertEqual(MGFParser.scan_total_ion_current(peak_list), 6)

    def test_get_scans_dict(self):
        self.assertIsInstance(
            self.parser.get_scans_dict(remove_fields=['peak_list']), dict)
