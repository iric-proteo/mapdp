"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from unittest import TestCase

# Third party libraries
import requests_mock

# Import project libraries
from ..mhc_allele import retrieve_hla_alleles, URL


# Create your tests here.


class MHCAlleleTest(TestCase):

    def test_retrieve_hla_allele(self):
        with requests_mock.Mocker() as m:
            data = '# file: hla_nom_p.txt\nA*;01:02;'

            m.get(URL, text=data)

            self.assertIsInstance(retrieve_hla_alleles(), list)

    def test_retrieve_hla_allele_long(self):
        with requests_mock.Mocker() as m:
            data = '# file: hla_nom_p.txt\nB*;47:01:01:02/47:01:01:03/47:01:01:04/47:01:01:05/47:01:02/47:01:03/47:01:04/47:01:05;47:01P\nDQA1*;03:01:01:01/03:01:03/03:01:04/03:01:05/03:02:01:01/03:02:01:02/03:03:01:01/03:03:01:02/03:03:01:03/03:03:01:04/03:03:01:05/03:03:01:06/03:03:01:07/03:03:01:08/03:03:01:09/03:03:01:10/03:03:01:11/03:03:01:12/03:03:02/03:03:03/03:04/03:06/03:07/03:08/03:09/03:11/03:12/03:13;03:01P'

            m.get(URL, text=data)

            alleles = retrieve_hla_alleles()
            self.assertIsInstance(alleles, list)
            self.assertEquals(len(alleles), 12)
