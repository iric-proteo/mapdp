"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import os
import random
from unittest import TestCase

# Third party libraries
from pyteomics import mass
import requests
import requests_mock

# Import project libraries
from library.mhcflurry import MHCFlurry


class Test(TestCase):
    def setUp(self):

        self.data = """2019-06-14 14:28:05.565515: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 3392315000 Hz
2019-06-14 14:28:05.579544: I tensorflow/compiler/xla/service/service.cc:150] XLA service 0x55b87c231d40 executing computations on platform Host. Devices:
2019-06-14 14:28:05.579586: I tensorflow/compiler/xla/service/service.cc:158]   StreamExecutor device (0): <undefined>, <undefined>
Using TensorFlow backend.
WARNING:tensorflow:From /usr/local/lib/python3.6/site-packages/tensorflow/python/framework/op_def_library.py:263: colocate_with (from tensorflow.python.framework.ops) is deprecated and will be removed in a future version.
Instructions for updating:
Colocations handled automatically by placer.
Forcing tensorflow backend.
Read input CSV with 3 rows, columns are: allele, peptide
allele,peptide,mhcflurry_prediction,mhcflurry_prediction_low,mhcflurry_prediction_high,mhcflurry_prediction_percentile
HLA-A0201,SIINFEKL,4571.983273187334,2093.5416199928345,11331.82765707368,6.381625000000002
HLA-A0201,SIINFEKD,20649.720227493523,14353.611034127305,32423.368823383964,55.223
HLA-A0201,SIINFEKQ,18592.228315865304,12515.67110707229,29905.257002058413,41.16749999999999"""

        self.url = 'http://localhost'
        os.environ['MHCFLURRY_URL'] = self.url

        self.predictor = MHCFlurry()

    # Helper methods
    @staticmethod
    def random_peptides(peptide_count, peptide_length):

        aa = list(mass.std_aa_mass.keys())
        aa_len = len(aa) - 1

        peptides = list()

        for i in range(peptide_count):

            peptide_aa = list()

            for j in range(peptide_length):
                peptide_aa.append(aa[random.randint(0, aa_len)])

            peptides.append(''.join(peptide_aa))

        return peptides

    @staticmethod
    def variable_length_peptides():

        mhc_peptides = Test.random_peptides(1, 7)
        mhc_peptides.extend(Test.random_peptides(1, 8))
        mhc_peptides.extend(Test.random_peptides(1, 9))
        mhc_peptides.extend(Test.random_peptides(1, 10))
        mhc_peptides.extend(Test.random_peptides(1, 11))
        mhc_peptides.extend(Test.random_peptides(1, 12))
        mhc_peptides.extend(Test.random_peptides(1, 13))
        mhc_peptides.extend(Test.random_peptides(1, 14))
        mhc_peptides.extend(Test.random_peptides(1, 15))
        mhc_peptides.extend(Test.random_peptides(1, 16))

        return mhc_peptides

    # Test methods
    def test_alive(self):

        with requests_mock.Mocker() as m:
            m.get(self.url + '/alive', text='resp')

            self.assertTrue(self.predictor.alive_check())

            m.get(self.url + '/alive', status_code=500, text='resp')

            self.assertFalse(self.predictor.alive_check())

    def test_alive_exceptions(self):

        with requests_mock.Mocker() as m:
            m.get(self.url + '/alive',
                  exc=requests.exceptions.ConnectTimeout)

            self.assertFalse(self.predictor.alive_check())

            m.get(self.url + '/alive',
                  exc=requests.exceptions.ConnectionError)

            self.assertFalse(self.predictor.alive_check())

    def test(self):

        mhc_peptides = self.random_peptides(5, 9)
        allele = 'HLA-A01:01'

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', text=self.data)

            results = list(self.predictor.run(mhc_peptides, allele))

        self.assertEqual(len(results), 3)
        self.assertIsInstance(results, list)
        self.assertIsInstance(results[0], dict)

    def test_batch_peptides(self):

        batches = MHCFlurry.batch_peptides([1, 2])
        self.assertEqual((len(list(batches))), 1)

        batches = MHCFlurry.batch_peptides([1, 2], batch_size=1)
        self.assertEqual((len(list(batches))), 3)

        batches = MHCFlurry.batch_peptides([1, 2, 3], batch_size=2)
        self.assertEqual((len(list(batches))), 2)

    def test_queue_jobs(self):
        value = 'hey'

        parameters = [value]

        def callback(x):
            self.assertEqual(x, value)

        MHCFlurry.queue_jobs(parameters, lambda x: x, callback)

    def test_run_error(self):

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', text='ValueError')

            with self.assertRaises(RuntimeError):
                list(self.predictor.run('', ''))

    def test_run_status_code(self):

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', status_code=500, text='')

            with self.assertRaises(requests.exceptions.ConnectionError):
                self.predictor.run('', '', trial=1)

            m.post(self.url + '/predict', status_code=504, text='')

            with self.assertRaises(requests.exceptions.ConnectTimeout):
                self.predictor.run('', '', trial=1)

        with self.assertRaises(requests.exceptions.ConnectionError):
            self.predictor.run('', '', trial=1)

    def test_run_jobs(self):

        params = {'allele': 'HLA-A*01:01',
                  'peptide_batch': self.random_peptides(5, 9),
                  }

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', text=self.data)
            predictions, params = MHCFlurry.run_job(params)

        self.assertEqual(len(list(predictions)), 3)
