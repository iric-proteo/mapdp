"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
from unittest import TestCase

# Third party libraries

# Import project libraries
from ..ms import (measurement_error_ppm,
                  mz_range_ppm)


class FDRTest(TestCase):
    def test_measurement_error_ppm(self):
        error = measurement_error_ppm(100, 100.01)

        self.assertEqual(int(error), -99)

    def test_mz_range(self):

        self.assertEqual(mz_range_ppm(100, 10), (99.999, 100.001))
