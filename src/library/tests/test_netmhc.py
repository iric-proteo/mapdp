"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import os
import random
from unittest import TestCase
from unittest.mock import patch

# Third party libraries
from pyteomics import mass
import requests
import requests_mock

# Import project libraries
from library.netmhc import NetMHC


class NetMHCTest(TestCase):
    def setUp(self):

        self.url = 'http://localhost'
        os.environ['NETMHC_URL'] = self.url
        # os.environ['NETMHC34_URL'] = 'http://132.204.81.148:9101'
        self.predictor = NetMHC('NETMHC34')

        self.data = 'Thursday November  9 2017 14:01\n\n\nNetMHC version 3.4. 9mer predictions using Artificial Neural Networks - Direct. Allele HLA-A01:01. \nStrong binder threshold  50 nM. Weak binder threshold score 500 nM\n\n\n\n----------------------------------------------------------------------------------------------------\n pos    peptide      logscore affinity(nM) Bind Level    Protein Name     Allele\n----------------------------------------------------------------------------------------------------\n   0  CEFREVTVH         0.077        21674                   Peptides HLA-A01:01\n   1  PYSAIFATV         0.075        22256                   Peptides HLA-A01:01\n   2  WPTNYHHVQ         0.079        21331                   Peptides HLA-A01:01\n   3  RWHRKVVKI         0.078        21539                   Peptides HLA-A01:01\n   4  DSWTRDWDF         0.086        19706                   Peptides HLA-A01:01\n--------------------------------------------------------------------------------------------------\n\n'

    # Helper methods
    @staticmethod
    def random_peptides(peptide_count, peptide_length):

        aa = list(mass.std_aa_mass.keys())
        aa_len = len(aa) - 1

        peptides = list()

        for i in range(peptide_count):

            peptide_aa = list()

            for j in range(peptide_length):
                peptide_aa.append(aa[random.randint(0, aa_len)])

            peptides.append(''.join(peptide_aa))

        return peptides

    @staticmethod
    def variable_length_peptides():

        mhc_peptides = NetMHCTest.random_peptides(1, 7)
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 8))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 9))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 10))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 11))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 12))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 13))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 14))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 15))
        mhc_peptides.extend(NetMHCTest.random_peptides(1, 16))

        return mhc_peptides

    # Test methods
    def test_alive(self):

        with requests_mock.Mocker() as m:
            m.get(self.url + '/alive/', text='resp')

            self.assertFalse(self.predictor.alive_check())

            m.get(self.url + '/alive/', status_code=204, text='resp')

            self.assertTrue(self.predictor.alive_check())

    def test_alive_exceptions(self):

        with requests_mock.Mocker() as m:
            m.get(self.url + '/alive/',
                  exc=requests.exceptions.ConnectTimeout)

            self.assertFalse(self.predictor.alive_check())

            m.get(self.url + '/alive/',
                  exc=requests.exceptions.ConnectionError)

            self.assertFalse(self.predictor.alive_check())

    def test_batch_peptides_by_length(self):
        mhc_peptides = self.variable_length_peptides()

        peptide_bins = NetMHC.batch_peptides_by_length(mhc_peptides)

        bins = list(peptide_bins)

        self.assertEqual(len(bins), 10)

        peptide_bins = NetMHC.batch_peptides_by_length(mhc_peptides,
                                                       batch_size=1)
        bins = list(peptide_bins)

        self.assertEqual(len(bins), 10)

    def test_format_allele_names(self):
        allele = 'HLA-A*01:01'

        formatted_alleles = self.predictor.format_allele_name(allele)

        self.assertNotIn(formatted_alleles[0], '*')

    def test_protein_peptides(self):
        peptides = NetMHC.protein_peptides('AAA', 2)

        self.assertEqual(len(list(peptides)), 2)

    def test_queue_jobs(self):
        value = 'hey'

        parameters = [{
            'predictor': 'NETMHC34',
            'peptide_batch': [],
            'allele': 'HLA-A01:01'
        }]

        def callback(x):
            pass

        with patch('library.netmhc.NetMHC.run_netmhc') as p:
            NetMHC.queue_jobs(parameters, callback)
            p.assert_called_once()

    def test_run_netmhc(self):

        mhc_peptides = self.random_peptides(5, 9)
        allele = 'HLA-A01:01'

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', text=self.data)

            results = list(self.predictor.run_netmhc(mhc_peptides, allele))

        self.assertEqual(len(results), 5)
        self.assertIsInstance(results, list)
        self.assertIsInstance(results[0], dict)

    def test_run_netmhc_error(self):

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', text='ERROR')

            with self.assertRaises(RuntimeError):
                list(self.predictor.run_netmhc('', ''))

    def test_run_netmhc_status_code(self):

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', status_code=500, text='')

            with self.assertRaises(requests.exceptions.ConnectionError):
                self.predictor.run_netmhc('', '', trial=1)

            m.post(self.url + '/predict', status_code=504, text='')

            with self.assertRaises(requests.exceptions.ConnectTimeout):
                self.predictor.run_netmhc('', '', trial=1)

        with self.assertRaises(requests.exceptions.ConnectionError):
            self.predictor.run_netmhc('', '', trial=1)

    def test_run_jobs(self):

        params = {'allele': 'HLA-A*01:01',
                  'predictor': 'NETMHC34',
                  'peptide_batch': self.random_peptides(5, 9),
                  }

        with requests_mock.Mocker() as m:
            m.post(self.url + '/predict', text=self.data)
            predictions, params = NetMHC.run_job(params)

        self.assertEqual(len(list(predictions)), 5)
