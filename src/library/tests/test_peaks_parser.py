"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import os
import unittest

# Third party libraries

# Import project libraries
from library.peaks_parser import PEAKSMzIdentML


# Create your tests here.


class Test(unittest.TestCase):
    def test_check_peaks_mzident_1_1(self):
        file_name = os.path.join('test_files',
                                 'mzid', 'peptides_1_1_0_scaffold.mzid')

        self.assertTrue(PEAKSMzIdentML.check_peaks_mzident_1_1(file_name))

        file_name = os.path.join('test_files',
                                 'mzid', 'peptides_1_0_0_header.mzid')

        self.assertFalse(PEAKSMzIdentML.check_peaks_mzident_1_1(file_name))

    def test_get_version(self):
        file_name = os.path.join('test_files',
                                 'mzid', 'peptides_1_1_0_scaffold.mzid')

        self.assertEqual(PEAKSMzIdentML.get_version(file_name), '7.5')

        self.assertEqual(PEAKSMzIdentML.get_version(file_name, full=bool),
                         '7.5 (2015-06-15)')

    def test_get_q_values_dict(self):
        values = [(5, True)]

        scan_q_value = PEAKSMzIdentML.get_q_values_dict(values)
        self.assertIsInstance(scan_q_value, dict)
        self.assertEqual(len(scan_q_value.values()), 1)

    def test_get_q_values_dict_no_decoy(self):
        values = [(5, False)]

        scan_q_value = PEAKSMzIdentML.get_q_values_dict(values)
        self.assertIsInstance(scan_q_value, dict)
        self.assertEqual(len(scan_q_value.values()), 0)

    def test_read_scans(self):
        file_name = os.path.join('test_files',
                                 'mzid', 'peptides_1_1_0_scaffold.mzid')

        scan_count = 0

        scan = None

        for scan in PEAKSMzIdentML.read_scans(file_name):
            scan_count += 1

        self.assertIsInstance(scan, dict)
        self.assertEqual(scan_count, 125)

    def test_read_scans_PEAKS_8(self):
        file_name = os.path.join('test_files', 'mzid', '8',
                                 'peptides_1_1_0.mzid')

        scan_count = 0

        scan = None

        for scan in PEAKSMzIdentML.read_scans(file_name):
            scan_count += 1

        self.assertIsInstance(scan, dict)
        self.assertEqual(scan_count, 101)
