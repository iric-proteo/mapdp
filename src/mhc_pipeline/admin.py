"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.contrib import admin
from django.core.paginator import Paginator
from django.db import connection
from django.db.utils import OperationalError
from django.utils.functional import cached_property
from guardian.admin import GuardedModelAdmin
from reversion.admin import VersionAdmin

# Import project libraries
import mhc_pipeline.models as models


class FasterAdminPaginator(Paginator):
    @cached_property
    def count(self):
        try:
            if not self.object_list.query.where:
                # estimates COUNT: https://djangosnippets.org/snippets/2593/
                cursor = connection.cursor()
                cursor.execute("SELECT reltuples FROM pg_class WHERE relname = %s",
                               [self.object_list.query.model._meta.db_table])
                ret = int(cursor.fetchone()[0])

                return ret

        except OperationalError:
            pass

        try:
            return self.object_list.count()
        except (AttributeError, TypeError):
            # AttributeError if object_list has no count() method.
            # TypeError if object_list.count() requires arguments
            # (i.e. is of type list).
            return len(self.object_list)


class AnnotationAdmin(VersionAdmin):
    """
    Admin panel for Annotation
    """

    list_display = ('pk', 'created_at', 'type', 'sub_path', 'file_name',
                    'display_name', 'source_url', 'hidden')


class BiologicalSampleAdmin(GuardedModelAdmin, VersionAdmin):
    """
    Admin panel for BiologicalSample
    """

    list_display = ('pk', 'identifier', 'short_description',
                    'created_by', 'created_at',
                    'modified_by', 'modified_at',
                    'trashed')


class BiologicalSampleFileSetAdmin(VersionAdmin):
    """
    Admin panel for BiologicalSampleFileSet
    """

    list_display = ('pk', 'biological_sample', 'name', 'set_class')


class BiologicalSampleMHCAlleleAdmin(VersionAdmin):
    """
    Admin panel for BiologicalSampleMHCAllele
    """

    list_display = ('pk', 'biological_sample')

    filter_horizontal = ('mhc_allele',)


class ExperimentDetailsAdmin(VersionAdmin):
    """
    Admin panel for ExperimentalDetails
    """

    list_display = ('biological_sample', 'name', 'elution_date',
                    'cell_count', 'pct_elution', 'pct_viability',
                    'amount_injected', 'comments')


class FileAdmin(VersionAdmin):
    """
    Admin panel for File
    """

    date_hierarchy = 'date_time'

    list_display = ('pk', 'file_set', 'path', 'name', 'extension', 'size',
                    'date_time', 'available')

    list_filter = ('available', 'extension',)

    search_fields = ('file_set__name', 'path', 'name')


class FileSetAdmin(GuardedModelAdmin, VersionAdmin):
    """
    Admin panel for FileSet
    """

    list_filter = ('set_class',)

    search_fields = ('name',)

    list_display = ('pk', 'name', 'set_class')


class FilterAdmin(GuardedModelAdmin, VersionAdmin):
    """
    Admin panel for Filter
    """

    list_display = ('pk', 'name', 'json_values',
                    'created_by', 'created_at',
                    'modified_by', 'modified_at',
                    'trashed')


class IdentificationAdmin(GuardedModelAdmin):
    """
    Admin panel for Identification
    """

    list_display = ('pk', 'biological_sample', 'file_set', 'software', 'state')

    list_filter = ('state',)


class ImmunopeptidomeAdmin(GuardedModelAdmin, VersionAdmin):
    """
    Admin panel for Immunopeptidome
    """

    fields = ('name', 'comments', 'filter', 'biological_sample',
              'identification', 'immunopeptidome')

    list_display = ('pk', 'name', 'comments', 'filter')


class LockModelAdmin(admin.ModelAdmin):
    """
    Admin panel for LockModel
    """

    list_display = ('pk', 'model', 'timestamp')


class MHCAlleleAdmin(admin.ModelAdmin):
    """
    Admin panel for MHCAllele
    """

    list_display = ('pk', 'mhc_class', 'gene', 'name')


class MHCSampleAdmin(VersionAdmin):
    """
    Admin panel for MHCSample
    """

    list_display = ('pk', 'biological_sample', 'group', 'gender')


class NewAdmin(GuardedModelAdmin, VersionAdmin):
    """
    Admin panel for New
    """

    list_display = ('pk', 'short_message',
                    'created_by', 'created_at',
                    'modified_by', 'modified_at',
                    'trashed')


class PeptideAdmin(VersionAdmin):
    list_display = ('pk', 'identification_id', 'scan_file', 'scan_number',
                    'experimental_mz', 'charge_state', 'peptide_score',
                    'peptide_sequence', 'modification', 'retention_time')

    paginator = FasterAdminPaginator

    raw_id_fields = ('identification', 'protein_matches', 'unique_peptide')

    search_fields = ('peptide_sequence', 'modification', 'scan_file')

    show_full_result_count = False


class PeptideCountsAdmin(admin.ModelAdmin):
    list_display = ('pk', 'identification_id', 'biological_sample_id',
                    'taxonomy_identifier', 'allele', 'individual_counts',
                    'cumulative_counts'
                    )

    list_filter = ('biological_sample_id',)


class PeptideBiologicalSampleCountAdmin(admin.ModelAdmin):
    list_display = ('pk', 'biological_sample_id', 'taxonomy_identifier',
                    'biological_sample_counts', 'peptide_counts',
                    )


class PeptideFileCountAdmin(admin.ModelAdmin):
    list_display = ('pk', 'biological_sample_id', 'taxonomy_identifier',
                    'file_counts', 'peptide_counts',
                    )


class PeptideGenomeAdmin(VersionAdmin):
    list_display = ('pk', 'unique_peptide_id', 'pp_id', 'mapped')

    raw_id_fields = ('unique_peptide',)


class PeptideBEDAdmin(VersionAdmin):
    raw_id_fields = ('unique_peptide',)


class PersonalizedProteomeAdmin(admin.ModelAdmin):
    list_display = ('pk', 'snps', 'pps_pp_id')


class ProteinDatabaseAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name', 'file_format', 'location', 'num_sequences')


class ProteinSequenceAdmin(admin.ModelAdmin):
    list_display = ('pk', 'protein_database', 'accession', 'short_description',
                    'length')


class SNPsAdmin(admin.ModelAdmin):
    list_display = ('pk', 'biological_sample', 'pps_snps_id')


class TagAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name')


class UniquePeptideAdmin(admin.ModelAdmin):
    list_display = ('pk', 'peptide_sequence', 'peptide_length')


# Register your models here.
admin.site.register(models.Annotation, AnnotationAdmin)
admin.site.register(models.BiologicalSample, BiologicalSampleAdmin)
admin.site.register(models.BiologicalSampleFileSet,
                    BiologicalSampleFileSetAdmin)
admin.site.register(models.BiologicalSampleMHCAllele,
                    BiologicalSampleMHCAlleleAdmin)
admin.site.register(models.Document),
admin.site.register(models.ExperimentDetails, ExperimentDetailsAdmin)
admin.site.register(models.File, FileAdmin)
admin.site.register(models.FileSet, FileSetAdmin)
admin.site.register(models.Filter, FilterAdmin)
admin.site.register(models.Genotyping)
admin.site.register(models.Identification, IdentificationAdmin)
admin.site.register(models.ImmunoPeptidome, ImmunopeptidomeAdmin)
admin.site.register(models.ImmunoPeptidomeDocument),
admin.site.register(models.LockModel, LockModelAdmin)
admin.site.register(models.MHCAllele, MHCAlleleAdmin)
admin.site.register(models.MHCSample, MHCSampleAdmin)
admin.site.register(models.New, NewAdmin)
admin.site.register(models.SNPs, SNPsAdmin)
admin.site.register(models.Peptide, PeptideAdmin)
admin.site.register(models.PeptideCounts, PeptideCountsAdmin)
admin.site.register(models.PeptideBiologicalSampleCount,
                    PeptideBiologicalSampleCountAdmin)
admin.site.register(models.PeptideFileCount, PeptideFileCountAdmin)
admin.site.register(models.PeptideGenome, PeptideGenomeAdmin)
admin.site.register(models.PersonalizedProteome, PersonalizedProteomeAdmin)
admin.site.register(models.ProteinDatabase, ProteinDatabaseAdmin)
admin.site.register(models.ProteinSequence, ProteinSequenceAdmin)
admin.site.register(models.Tag, TagAdmin)
admin.site.register(models.UniquePeptide, UniquePeptideAdmin)
admin.site.register(models.UploadedFile)
