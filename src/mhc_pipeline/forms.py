"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os

# Import Django related libraries
from django import forms
from django.conf import settings
from django.contrib.auth.models import (Group,
                                        User)
from django.core.validators import (MaxValueValidator,
                                    MinValueValidator)
from django.db.models.fields import BLANK_CHOICE_DASH
from django.forms.models import ModelMultipleChoiceField
from django.shortcuts import resolve_url

# Third party libraries
from ajax_select.fields import AutoCompleteSelectMultipleField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (Div,
                                 Field,
                                 Fieldset,
                                 Layout,
                                 Submit)

# Import project libraries
from .library.mhc_allele import MHC_PREDICTOR_NAMES
from .library.pps import (get_personalized_proteome_for_form,
                          get_reference_genome_for_form,
                          get_snps_for_form)
from .models import (Annotation,
                     BiologicalSample,
                     BiologicalSampleFileSet,
                     BiologicalSampleMHCAllele,
                     ExperimentDetails,
                     Filter,
                     Genotyping,
                     ImmunoPeptidome,
                     MHCSample,
                     UploadedFile)

"""
Custom Fields
"""

deconvolution_software = []

if settings.DEMO != "True":
    if os.getenv('MIXMHCP_CONTAINER'):
        deconvolution_software.append(('MixMHCp-2.1', 'MixMHCp-2.1 (MHC-I)'))

    if os.getenv('MODEC_CONTAINER'):
        deconvolution_software.append(('MoDec-1.1', 'MoDec-1.1 (MHC-II)'))


class AlternateLabelModelMultipleChoiceField(ModelMultipleChoiceField):
    """
    Field with alternate label.
    """

    def label_from_instance(self, obj):
        """
        Returns object alternate string label.
        :param obj: Object instance
        :return: String
        """
        return obj.alternate_label()


"""
Forms
"""

template_2_10 = 'mhc_pipeline/base_site/field_2_10.html'


class AnnotateForm(forms.Form):
    format = forms.ChoiceField(
        choices=(
            ('csv', 'csv'),
            ('bed', 'bed')),
        required=False
    )

    def __init__(self, *args, **kwargs):

        # Form layout
        self.helper = FormHelper()

        pp_ids = kwargs.pop('personalized_proteome')

        super().__init__(*args, **kwargs)
        fields = self.fields

        pp_choices = BLANK_CHOICE_DASH + get_personalized_proteome_for_form(
            pp_ids=pp_ids)

        fields['personalized_proteome'] = forms.ChoiceField(choices=pp_choices,
                                                            required=False)

        dbsnp_fields = list()
        maf_fields = list()
        rna_fields = list()
        other_fields = list()

        for annotation in Annotation.objects.filter(hidden=False):

            key = f'annotation_{annotation.type}_{annotation.pk}'

            fields[key] = forms.BooleanField(label=annotation.display_name,
                                             required=False)

            if annotation.type == 'dbsnp':
                dbsnp_fields.append(key)
            elif annotation.type in ('esp', '1000genomes', 'exac', 'gnomad'):
                maf_fields.append(key)
            elif annotation.type in ('hpa', 'tcga'):
                rna_fields.append(key)
            else:
                other_fields.append(key)

        mhc_predictor_fields = list()

        for predictor_key, predictor_name in MHC_PREDICTOR_NAMES.items():
            key = f'mhc_predictor_{predictor_key}'

            fields[key] = forms.BooleanField(label=predictor_name,
                                             required=False)
            mhc_predictor_fields.append(key)

        # Form layout
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4'
        self.helper.field_class = 'col-md-8'

        self.helper.layout = Layout(
            Div(
                Fieldset(
                    '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;Parameters',
                ),
                css_class='col-md-12',
            ),
            Div(
                Field('format',
                      ),
                css_class='col-md-6'
            ),

            Div(
                Field('personalized_proteome',
                      ),
                css_class='col-md-6'
            ),
            Div(css_class='col-md-12', style='height:25px;'
                ),
            Div(
                Fieldset('Annotations',
                         ),
                css_class='col-md-12'
            ),
            Div(
                Fieldset('dbSNP variants',
                         *dbsnp_fields,
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('MHC predictors',
                         *mhc_predictor_fields,
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('Minor allele frequencies',
                         *maf_fields,
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('RNA expression',
                         *rna_fields,
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('Other',
                         *other_fields,
                         ),
                css_class='col-md-6'
            ),
            Div(css_class='col-md-12', style='height:25px;'
                ),
            Div(css_class='col-md-8',
                ),
            Div(Submit('submit', 'Save', css_id='id_submit_button',
                       css_class='btn btn-block btn-skin btn-flat btn-lg',
                       ),
                css_class='col-md-4'
                ),
        )

    def clean(self):
        cleaned_data = self.cleaned_data

        # Count dbsnp selected
        dbsnp_selected = [field for field, value in cleaned_data.items()
                          if value and 'dbsnp' in field]
        dbsnp_count = len(dbsnp_selected)

        if dbsnp_count >= 2:
            for field in dbsnp_selected:
                msg = 'Please select only one'
                self._errors[field] = self.error_class([msg])

        maf_selected = [field for field, value in cleaned_data.items()
                        if value and ('esp' in field or
                                      '1000genomes' in field or
                                      'exac' in field or
                                      'gnomad' in field
                                      )]

        for field in maf_selected:
            if dbsnp_count == 0:
                msg = 'Please select one dbSNP entry'
                self._errors[field] = self.error_class([msg])

        # Check fields that required a personalized proteome
        pp_required_selected = [field for field, value in cleaned_data.items()
                                if value and ('dbsnp' in field or
                                              'hpa' in field or
                                              'tcga' in field
                                              )]

        if len(pp_required_selected) >= 1:

            if len(cleaned_data['personalized_proteome']) == 0:
                msg = 'Please select one'
                self._errors['personalized_proteome'] = self.error_class([msg])

        return cleaned_data


class DefaultModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # Form layout
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Save', css_id='id_submit_button',
                   css_class='btn btn-block btn-skin btn-flat btn-lg'))

        super(DefaultModelForm, self).__init__(*args, **kwargs)


class BiologicalSampleForm(DefaultModelForm):
    class Meta:
        fields = ['identifier', 'short_description', 'Species', 'cell_type',
                  'tissue', 'organ']

        model = BiologicalSample


class BiologicalSampleFileSetForm(DefaultModelForm):
    class Meta:
        fields = ['name', 'set_class', ]

        model = BiologicalSampleFileSet

    def __init__(self, *args, **kwargs):
        set_classes = kwargs.pop('set_classes', False)

        super().__init__(*args, **kwargs)

        # Restrict choices

        if set_classes:
            choices = [x for x in self.fields['set_class'].choices
                       if x[0] in set_classes]
            self.fields['set_class'].choices = choices


class CommentForm(forms.Form):
    comment = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        # Form layout
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Save', css_id='id_submit_button',
                   css_class='btn btn-block btn-skin btn-flat btn-lg'))

        super().__init__(*args, **kwargs)


class ExperimentDetailsForm(forms.ModelForm):
    class Meta:
        fields = ['name', 'elution_date', 'cell_count', 'pct_elution',
                  'pct_viability', 'facs_file', 'amount_injected',
                  'fractionnated', 'comments']

        model = ExperimentDetails

    def __init__(self, *args, **kwargs):
        submit = kwargs.pop('submit', False)

        super(ExperimentDetailsForm, self).__init__(*args, **kwargs)

        # Form layout
        self.helper = FormHelper()

        if self.instance.pk:
            self.helper.form_action = self.instance.get_absolute_url()

        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4'
        self.helper.field_class = 'col-md-8'

        self.helper.layout = Layout(
            Div(Field('name',
                      template=template_2_10),
                css_class='col-md-12', style='margin-top: 25px;'),

            Div(
                Fieldset('<br />Elution',
                         'cell_count',
                         'elution_date',
                         css_class='col-md-12')
                , css_class='col-md-6'),

            Div(
                Fieldset('<br />FACS',
                         'pct_elution',
                         'pct_viability',
                         'facs_file',
                         css_class='col-md-12')
                , css_class='col-md-6'),

            Div(
                Fieldset('<br />LC-MS',
                         'amount_injected',
                         'fractionnated',
                         css_class='col-md-12')
                , css_class='col-md-6'),

            Div(
                Fieldset('<br />Other',
                         Field('comments',
                               template=template_2_10)
                         ),
                style='margin-bottom: 25px;', css_class='col-md-12',
            ),

        )

        if submit:
            self.helper.layout.extend(
                Div(
                    Submit('submit', 'Save', css_id='id_submit_button',
                           css_class='btn btn-skin btn-flat pull-right'),
                    style='margin-bottom: 25px;', css_class='col-md-12'
                )
            )


class FilterIdentificationForm(forms.Form):
    identification = AutoCompleteSelectMultipleField('identifications',
                                                     help_text='Search by id, BS id or name',
                                                     required=False)

    immunopeptidome = AutoCompleteSelectMultipleField('immunopeptidomes',
                                                      help_text='Search by id or name',
                                                      required=False)

    unique_peptide__peptide_sequence__exact = forms.CharField(
        label='Sequence (exact)', required=False)

    unique_peptide__peptide_sequence__icontains = forms.CharField(
        label='Sequence (contains)', required=False)

    modification__icontains = forms.CharField(label='Modification (contains)',
                                              required=False)

    peptide_length__gte = forms.IntegerField(label='Length (&ge;)',
                                             required=False, )

    peptide_length__lte = forms.IntegerField(label='Length (&le;)',
                                             required=False, )

    peptide_score__gte = forms.FloatField(label='Peptide score (&ge;)',
                                          required=False)

    q_value__lte = forms.FloatField(label='q-value (&le;)',
                                    required=False,
                                    validators=[MinValueValidator(0),
                                                MaxValueValidator(1)])

    experimental_mz__lte = forms.FloatField(label='m/z (&le;)',
                                            required=False, )

    experimental_mz__gte = forms.FloatField(label='m/z (&ge;)',
                                            required=False)

    error_ppm__lte = forms.FloatField(label='Error (ppm)',
                                      required=False, )

    charge_state__gte = forms.IntegerField(label='z (&ge;)',
                                           required=False)

    charge_state__lte = forms.IntegerField(label='z (&le;)',
                                           required=False)

    remove_decoy = forms.BooleanField(label='Remove decoy hits',
                                      required=False)

    unique_sequence = forms.BooleanField(required=False)

    ignore_sample_alleles = forms.BooleanField(required=False)

    mhc_predictor = forms.ChoiceField(
        choices=BLANK_CHOICE_DASH + list(MHC_PREDICTOR_NAMES.items()),
        label='MHC predictor',
        required=False)

    mhc_allele = AutoCompleteSelectMultipleField('mhcalleles', help_text='',
                                                 required=False)

    affinity_threshold = forms.IntegerField(
        label='Affinity threshold &le; (nM)',
        required=False)

    rank_threshold = forms.FloatField(help_text='0-100',
                                      label='Rank threshold &le; (%)',
                                      required=False,
                                      validators=[MaxValueValidator(100),
                                                  MinValueValidator(0)]
                                      )

    deconvolution_software = forms.ChoiceField(
        choices=BLANK_CHOICE_DASH + deconvolution_software,
        label='Software',
        required=False)

    deconvolution_cluster_number = forms.IntegerField(
        initial=4,
        help_text='Inspect the motif logos to set the appropriate value.',
        label='Number of clusters/motifs',
        required=False,
        validators=[MinValueValidator(1),
                    MaxValueValidator(10)]
    )
    deconvolution_remove_trash = forms.BooleanField(
        label='Remove peptides from the trash cluster',
        required=False)

    deconvolution_before_fdr = forms.BooleanField(
        label='Filter peptides before FDR',
        required=False)

    deconvolution_max_trash_responsibility = forms.FloatField(
        label='Maximum trash responsibility (&le;)',
        required=False,
        validators=[MinValueValidator(0),
                    MaxValueValidator(1)]
    )

    protein_accessions_positions__icontains = forms.CharField(
        label='Accession (contains)',
        required=False)

    protein_description = forms.BooleanField(
        label='Output parsed description from FASTA',
        required=False)

    validated__in = forms.MultipleChoiceField(
        choices=(('R', 'Red'),
                 ('Y', 'Yellow'),
                 ('G', 'Green'),
                 (None, 'None')),
        initial='',
        label='Validated',
        required=False,
        widget=forms.CheckboxSelectMultiple)

    filter_name = forms.CharField(help_text='Name this filter to save it.',
                                  max_length=100,
                                  required=False)

    score_fdr = forms.FloatField(label='Peptide score FDR (&le;)',
                                 required=False,
                                 validators=[MinValueValidator(0),
                                             MaxValueValidator(1)])

    score_mhc_fdr = forms.FloatField(label='Peptide score/MHC FDR (&le;)',
                                     required=False,
                                     validators=[MinValueValidator(0),
                                                 MaxValueValidator(1)])

    immunopeptidome_name = forms.CharField(
        label='Name',
        max_length=500,
        required=False)

    ignore_cache = forms.BooleanField(required=False)

    files = forms.CharField(
        help_text='List of file names or keywords'
                  ' separated by comma',
        label='File names',
        required=False
    )

    files_action = forms.ChoiceField(
        choices=(
            ('', ''),
            ('exclude', 'exclude'),
            ('filter', 'filter')),
        label='Action',
        required=False
    )

    ref_url = forms.CharField(required=False)

    def clean_filter_name(self):

        filter_name = self.cleaned_data['filter_name']

        count = Filter.objects.filter(name=filter_name).exists()

        if count:
            raise forms.ValidationError(
                'Filter name already exists. Please rename it.')

        return filter_name

    def clean_immunopeptidome_name(self):

        immunopeptidome_name = self.cleaned_data['immunopeptidome_name']

        if self.data.get('submit') == 'Create' and immunopeptidome_name == '':
            raise forms.ValidationError('Required field')

        count = ImmunoPeptidome.objects.filter(
            name=immunopeptidome_name).exists()

        if count:
            raise forms.ValidationError(
                'Immunopeptidome name already exists. Please rename it.')

        return immunopeptidome_name

    def validate_mhc_predictor(self):

        if self.cleaned_data.get('mhc_predictor', '') == '':
            raise forms.ValidationError(
                'Please select MHC predictor algorithm.')

    def clean_files_action(self):

        if self.cleaned_data.get('files_action', '') == '' and \
                self.cleaned_data['files'] != '':
            raise forms.ValidationError('Please select')

        return self.cleaned_data['files_action']

    def clean_score_mhc_fdr(self):

        if self.cleaned_data['score_mhc_fdr'] is not None:
            self.validate_mhc_predictor()

        return self.cleaned_data['score_mhc_fdr']

    def clean_affinity_threshold(self):

        if self.cleaned_data['affinity_threshold'] is not None:
            self.validate_mhc_predictor()

        return self.cleaned_data['affinity_threshold']

    def clean_deconvolution_software(self):
        if (self.cleaned_data['deconvolution_software'] is not None and
                self.cleaned_data['deconvolution_software'] != ''):

            if self.data['deconvolution_cluster_number'] == '':
                raise forms.ValidationError(
                    'Please set the number of clusters/motifs.')

        return self.cleaned_data['deconvolution_software']

    def __init__(self, *args, **kwargs):

        filters = kwargs.pop('filters', Filter.objects.none())

        super(FilterIdentificationForm, self).__init__(*args, **kwargs)

        filter_url = resolve_url('filter:list')

        self.fields[
            'selected_filter'] = forms.ModelChoiceField(
            help_text='Note: any filter set above will be ignored if selected.',
            label='Filter &nbsp;&nbsp;<a href="' + filter_url + '" id="id_filters_link"><i class="fa fa-pencil" title="Manage"></i></a>',
            queryset=filters,
            required=False
        )

        # Form layout
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4'
        self.helper.field_class = 'col-md-8'

        self.helper.layout = Layout(
            Fieldset(
                '<br /><i class="fa fa-filter"></i>&nbsp;&nbsp;Datasets',
                css_class='col-md-12'),
            Div(
                Field('identification', ),
                css_class='col-md-6'
            ),
            Div(
                Field('immunopeptidome'),
                css_class='col-md-6'
            ),
            Fieldset(
                '<br /><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter',
                css_class='col-md-12'),
            Div(Fieldset('Scoring &amp; FDR',
                         'peptide_score__gte',
                         'q_value__lte',
                         'score_fdr',
                         'score_mhc_fdr',
                         'remove_decoy',
                         ),

                Fieldset('MHC motif deconvolution',
                         'deconvolution_software',
                         'deconvolution_cluster_number',
                         'deconvolution_max_trash_responsibility',
                         'deconvolution_remove_trash',
                         'deconvolution_before_fdr',
                         ),
                Fieldset('Mass &amp; charge',
                         'error_ppm__lte',
                         Div('experimental_mz__gte',
                             css_class='col-md-6')
                         ,
                         Div('experimental_mz__lte',
                             css_class='col-md-6')
                         ,
                         Div('charge_state__gte',
                             css_class='col-md-6')
                         ,
                         Div('charge_state__lte',
                             css_class='col-md-6')
                         ),

                Fieldset('Protein',
                         'protein_accessions_positions__icontains',
                         'protein_description'
                         ),

                css_class='col-md-6'
                ),

            Div(
                Fieldset('Peptide sequence &amp; modification',
                         Div('peptide_length__gte',
                             css_class='col-md-6'),
                         Div('peptide_length__lte',
                             css_class='col-md-6'),
                         'unique_peptide__peptide_sequence__icontains',
                         'unique_peptide__peptide_sequence__exact',
                         'modification__icontains',
                         'unique_sequence',
                         ),

                Fieldset('MHC predictions',
                         'mhc_predictor',
                         'mhc_allele',
                         'ignore_sample_alleles',
                         'affinity_threshold',
                         'rank_threshold'
                         ),
                Fieldset('Spectrum validation',
                         'validated__in',
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('Files',
                         'files',
                         'files_action',
                         css_class='col-md-12;'
                         ),

                css_class='col-md-12', style='margin-bottom: 25px;',
            ),
            Div(
                Fieldset('Load',
                         'selected_filter',
                         'ignore_cache'
                         ),
                css_class='col-md-6',
            ),
            Div(
                Fieldset('Save',
                         'filter_name'),
                css_class='col-md-6',
            ),
            Div(
                Field('ref_url', type='hidden'),
                Submit('submit', 'View',
                       css_class='pull-right'),
                Submit('peptides_plots', 'Exploratory plots',
                       css_class='pull-right',
                       style='margin-right: 15px; margin-bottom: 50px;'),
                Submit('download', 'Download',
                       css_class='pull-right',
                       style='margin-right: 15px; margin-bottom: 50px;'),
                css_class='col-md-12',
            ),
            Div(
                Fieldset('Create Immunopeptidome',
                         'immunopeptidome_name',
                         css_class='col-md-12;'),

                css_class='col-md-12', style='margin-bottom: 25px;',
            ),
            Div(
                Submit('submit', 'Create',
                       css_class='pull-right', style='margin-right: 15px;'),
                css_class='col-md-12',
            ),
        )

        # if identification is None:
        #     self.helper.layout.pop(0)


class FilterRenameForm(DefaultModelForm):
    class Meta:
        fields = ['name']

        model = Filter


class GenotypingForm(DefaultModelForm):

    def __init__(self, *args, **kwargs):
        filesets = kwargs.pop('filesets')

        super().__init__(*args, **kwargs)

        self.fields[
            'file_set'] = forms.ModelChoiceField(
            queryset=filesets,
        )

    class Meta:
        fields = ['file_set', 'update_alleles']

        model = Genotyping


class ImmunoPeptidomeForm(DefaultModelForm):
    class Meta:
        fields = ['name', 'comments', 'tags']

        model = ImmunoPeptidome


class ImmunoPeptidomeFileForm(ImmunoPeptidomeForm):
    file = forms.FileField(help_text='Select MAPDP identifications CSV file.')


class MHCAlleleForm(DefaultModelForm):
    class Meta:
        fields = ['mhc_allele']

        model = BiologicalSampleMHCAllele

    mhc_allele = AutoCompleteSelectMultipleField('mhcalleles',
                                                 required=False)


class MHCSampleForm(DefaultModelForm):
    class Meta:
        fields = ['group', 'gender']

        model = MHCSample


class MiHAFilterForm(forms.Form):
    # annotate_document

    maf = forms.FloatField(label='Minor allele frequency (&ge;)',
                           help_text='MAF from the genome Aggregation Database '
                                     '(gnomAD)',
                           initial=0.05)

    no_hla_igg = forms.BooleanField(label='Not derived from HLA or IgG genes',
                                    initial=True,
                                    required=False)

    unambiguous_genetic_origin = forms.BooleanField(initial=True,
                                                    required=False)

    msms_validation = forms.BooleanField(label='Green only',
                                         required=False)

    expression_not_ubiquitous = forms.BooleanField(
        label='Not ubiquitous in the human protein atlas tissue RNA expression',
        initial=True,
        required=False,
    )

    ratio_bone_marrow_skin = forms.FloatField(
        label='Ratio bone marrow/skin (&ge;)',
        help_text='The human protein atlas tissue RNA expression',
        required=False
    )

    expression_aml = forms.FloatField(
        label='Primary AML samples (RPKM &gt;)',
        help_text='The Cancer Genome Atlas Program (TCGA)',
        required=False
    )

    immunopeptidome_name = forms.CharField(
        label='Name',
        help_text='Enter a name to create a new immunopeptidome.',
        max_length=500,
        required=False)

    dbsnp_validated = forms.BooleanField(label='Validated',
                                         initial=True,
                                         required=False)

    def __init__(self, *args, **kwargs):
        documents_choices = kwargs.pop('documents')

        super().__init__(*args, **kwargs)

        fields = self.fields

        documents = BLANK_CHOICE_DASH + documents_choices

        fields['document'] = forms.ChoiceField(
            choices=documents,
            help_text='The selected document must contain expected annotations '
                      'for the selected filters below.')

        # Form layout
        self.helper = FormHelper()

        # Form layout
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4'
        self.helper.field_class = 'col-md-8'

        self.helper.layout = Layout(
            Div(
                Fieldset(
                    '<i class="fa fa-file"></i>&nbsp;&nbsp;Annotated document',
                    'document'
                ),
                css_class='col-md-12',
            ),
            Div(css_class='col-md-12', style='height:25px;'
                ),
            Div(
                Fieldset(
                    '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;Parameters',
                ),
                css_class='col-md-12',
            ),
            Div(
                Fieldset('Minor allele frequency',
                         'maf',
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('Genetic origin',
                         'no_hla_igg',
                         'unambiguous_genetic_origin'
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('Coding gene RNA expression',
                         'expression_not_ubiquitous',
                         'ratio_bone_marrow_skin',
                         'expression_aml'
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('dbSNP',
                         'dbsnp_validated',
                         ),
                css_class='col-md-6'
            ),
            Div(
                Fieldset('MS/MS spectrum validation',
                         'msms_validation',
                         ),
                css_class='col-md-6'
            ),
            Div(css_class='col-md-12', style='height:25px;'
                ),
            Div(
                Fieldset('Create immunopeptidome',
                         'immunopeptidome_name',
                         ),
                css_class='col-md-12'
            ),
            Div(css_class='col-md-12', style='height:25px;'
                ),
            Div(css_class='col-md-8',
                ),
            Div(Submit('submit', 'Save', css_id='id_submit_button',
                       css_class='btn btn-block btn-skin btn-flat btn-lg',
                       ),
                css_class='col-md-4'
                ),
        )

    def clean_immunopeptidome_name(self):
        immunopeptidome_name = self.cleaned_data['immunopeptidome_name']

        if ImmunoPeptidome.objects.filter(name=immunopeptidome_name).exists():
            raise forms.ValidationError(
                'Immunopeptidome name already exists. Please rename it.')

        return immunopeptidome_name


class PermissionUpdateForm(forms.Form):
    view = forms.BooleanField(required=False)

    change = forms.BooleanField(label='Edit',
                                required=False)

    delete = forms.BooleanField(label='Trash',
                                required=False)


class PermissionUserForm(PermissionUpdateForm):
    user = forms.ModelChoiceField(queryset=(User.objects
                                            .all()
                                            .filter(is_active=True)))


class PermissionGroupForm(PermissionUpdateForm):
    group = forms.ModelChoiceField(queryset=(Group.objects
                                             .all()
                                             .exclude(name__in=['ms',
                                                                'upload',
                                                                'validation'
                                                                ])))


class UploadedFileForm(DefaultModelForm):
    class Meta:
        fields = ['file']

        model = UploadedFile

    def __init__(self, *args, **kwargs):
        self.extensions = kwargs.pop('extensions')

        super().__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Field('file',
                  accept=','.join(self.extensions))
        )

    def clean_file(self):
        file = self.cleaned_data['file']
        ext = os.path.splitext(file.name)[1]

        if ext not in self.extensions:
            raise forms.ValidationError(
                u'File format is not supported. Valid choices are: %s' %
                ', '.join(self.extensions))

        return file


class UserGroupForm(forms.Form):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(),
                                           required=False)

    def __init__(self, *args, **kwargs):
        user_groups = kwargs.pop('user_groups')

        super().__init__(*args, **kwargs)

        self.fields[
            'groups'] = ModelMultipleChoiceField(
            queryset=user_groups,
            required=False,
        )

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4'
        self.helper.field_class = 'col-md-8'

        self.helper.layout = Layout(
            Div('users',
                css_class='col-md-6'),
            Div('groups',
                css_class='col-md-6'),
            Div(
                Submit('submit', 'Save',
                       css_class='pull-right btn btn-block btn-skin btn-flat btn-lg'),
                css_class='col-md-12', style='margin-top: 25px;'
            ),

        )


class RefGenomeUploadForm(forms.Form):
    file = forms.FileField(help_text='pyGeno reference genome data wrap file')

    def __init__(self, *args, **kwargs):
        # Form layout
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Save', css_id='id_submit_button',
                   css_class='btn btn-block btn-skin btn-flat btn-lg'))

        super().__init__(*args, **kwargs)


class SNPSUploadForm(forms.Form):
    file = forms.FileField(
        help_text='pyGeno SNPs data wrap file (tar.gz archive with manifest.ini and snps files).')

    def __init__(self, *args, **kwargs):
        # Form layout
        self.helper = FormHelper()
        self.helper.add_input(
            Submit('submit', 'Save', css_id='id_submit_button',
                   css_class='btn btn-block btn-skin btn-flat btn-lg'))

        super().__init__(*args, **kwargs)

        self.fields[
            'reference_genome'] = forms.ChoiceField(
            choices=get_reference_genome_for_form()
        )


class PPUploadForm(forms.Form):
    quality_threshold = forms.IntegerField(initial=20,
                                           help_text='CASAVA Q(max_gt) > threshold'
                                                     ' or Quality > threshold',
                                           validators=[
                                               MinValueValidator(0)
                                           ],
                                           )

    expression_file = forms.FileField(help_text='Kallisto abundance.tsv file',
                                      required=False)

    tpm_threshold = forms.FloatField(
        help_text='Transcript per million (tpm) > threshold',
        required=False,
        validators=[
            MinValueValidator(0)
        ])

    protein_length_threshold = forms.IntegerField(initial=10000,
                                                  help_text='<=',
                                                  label='Maximum protein length',
                                                  validators=[
                                                      MinValueValidator(0)
                                                  ])

    # remove_redundant = forms.BooleanField(label='Remove redundant protein sequences',
    #                                       required=False)

    def __init__(self, *args, **kwargs):
        # Form layout
        self.helper = FormHelper()

        snps_ids = kwargs.pop('snps_ids')

        super().__init__(*args, **kwargs)

        self.fields[
            'snps'] = forms.ChoiceField(
            choices=get_snps_for_form(snps_ids)
        )

        # Form layout
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-4'
        self.helper.field_class = 'col-md-8'

        self.helper.layout = Layout(
            Div(
                Fieldset(
                    '<i class="fa fa-list-alt"></i>&nbsp;&nbsp;SNPs datasets associated to the biological sample',
                    Field('snps')),
                css_class='col-md-12',
            ),
            Fieldset(
                '<br /><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter',
                css_class='col-md-12'),
            Div(Fieldset('SNPs quality',
                         'quality_threshold',
                         ),
                Fieldset('Transcript expression',
                         'expression_file',
                         'tpm_threshold',
                         ),
                css_class='col-md-6'
                ),
            Div(
                Fieldset('Other',
                         'protein_length_threshold',
                         # 'remove_redundant',
                         ),
                css_class='col-md-6'
            ),
            Div(css_class='col-md-8'
                ),
            Div(Submit('submit', 'Save', css_id='id_submit_button',
                       css_class='btn btn-block btn-skin btn-flat btn-lg',
                       ),
                css_class='col-md-4'
                ),
        )
