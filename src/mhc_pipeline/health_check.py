"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os


# Import Django related libraries
from django.conf import settings

# Third party libraries
from health_check.backends import BaseHealthCheckBackend, HealthCheckException
import requests

# Import project libraries
from library.netmhc import NetMHC
from mhc_pipeline.library import pps


class HealthCheckExceptionCustom(HealthCheckException):
    message_type = 'unknown error'

    def __init__(self, service, message):
        self.message = message
        self.service = service

    def __str__(self):

        hostname = os.getenv('HOSTNAME', '')
        hostname = hostname.split('-')[-1]

        return '%s %s: %s (%s)' % (self.service, self.message_type,
                                   self.message, hostname)


class ServiceUnavailable(HealthCheckExceptionCustom):
    message_type = 'unavailable'


class BackendMixIn:
    """
    This mixing allows a more declarative ways to interact with
    BaseHealthCheckBackend.
    """

    def check(self):
        """
        Backend should provides boolean value to indicate if service is ok.
        :return: Boolean or NotImplementedError
        """

        raise NotImplementedError

    def check_status(self):
        """
        Raises ServiceUnavailable and catches exceptions from check.
        :return: None or ServiceUnavailable
        """

        try:
            if not self.check():
                raise ServiceUnavailable(self.name, self.msg)

        except Exception as e:

            raise ServiceUnavailable(self.name, e.__class__.__name__)

    def identifier(self):
        """
        User friendly name of the backend (instead of class name).
        :return: String
        """

        return self.name


class Media(BackendMixIn, BaseHealthCheckBackend):
    msg = 'empty'
    name = 'Media data'

    def check(self):

        with open(os.path.join(settings.MEDIA_ROOT, 'fake'), 'w'):
            return os.listdir(settings.MEDIA_ROOT)


class Minio(BackendMixIn, BaseHealthCheckBackend):
    msg = 'not available'
    name = 'Minio service'

    def check(self):
        url = settings.AWS_S3_ENDPOINT_URL + '/minio/health/ready'
        response = requests.get(url)

        return response.status_code == 200


class MSData(BackendMixIn, BaseHealthCheckBackend):
    msg = 'empty'
    name = 'MS data'

    def check(self):
        return os.listdir(settings.MS_DIR)


class NetMHCBackend(BackendMixIn, BaseHealthCheckBackend):
    msg = 'NetMHC not alive'
    name = 'NetMHC'

    def check(self):

        return NetMHC('NETMHC34').alive_check()


class PPS(BackendMixIn, BaseHealthCheckBackend):
    msg = 'not available'
    name = 'Personalized proteome service'

    def check(self):
        return pps.alive()


class SharedData(BackendMixIn, BaseHealthCheckBackend):
    msg = 'empty'
    name = 'Shared data'

    def check(self):
        return os.listdir(settings.SHARED_DATA)
