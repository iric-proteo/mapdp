"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries

# Import project libraries


class BulkInsert(object):

    def __init__(self, model, unique_fields=None, filter_set=None):

        self.filter_set = filter_set
        self.last_pk = None
        self.model = model
        self.to_create_list = list()
        self.unique_entries_dict = dict()
        self.unique_fields = unique_fields
        self.unique_queryset = model.objects

    def entry_unique_key(self, fields: list) -> tuple:
        """
        Generates unique key for entry
        :param fields: Model fields
        :return: Key
        """

        key = list()

        for field in self.unique_fields:

            key.append(fields[field])

        return tuple(key)

    def get_create_pk(self, fields: list) -> int:
        """
        Gets or creates primary key
        :param fields: Model fields
        :return: Primary key
        """

        self.set_last_pk()

        self.last_pk += 1
        pk = self.last_pk

        obj = self.model(**fields)
        obj.id = pk
        self.to_create_list.append(obj)

        return pk

    def get_create_unique_pk(self, fields: list) -> int:
        """
        Gets or creates unique primary key.
        :param fields: Model fields
        :return: Primary key
        """

        self.set_last_pk()

        # Check database
        key = self.entry_unique_key(fields)
        pk = self.unique_entries_dict.get(key, None)

        if pk is None:
            # Create a new one
            self.last_pk += 1
            pk = self.last_pk

            obj = self.model(**fields)
            obj.id = pk

            self.to_create_list.append(obj)

            self.unique_entries_dict[key] = pk

        return pk

    def iterate_create_pk(self):
        """
        Iterates fields object and creates model objects
        :return: Primary key
        """

        self.set_last_pk()

        for index, fields in enumerate(self.to_create_list):
            self.last_pk += 1
            pk = self.last_pk

            obj = self.model(**fields)
            obj.id = pk

            self.to_create_list[index] = obj

            yield obj

    def set_last_pk(self):
        """
        Set last primary key
        :return: None
        """

        if self.last_pk is None:

            last = self.model.objects.last()

            if last is not None:
                self.last_pk = last.pk
            else:
                self.last_pk = 0

    def set_unique_entries_dict(self):
        """
        Retrieves all unique entries in model based on fields
        :return: Dictionary (key: tuple key, value: primary key int.
        """

        values = ['id'] + self.unique_fields

        for entry in self.unique_queryset.values_list(*values).iterator():

            self.unique_entries_dict[entry[1:]] = entry[0]

    def store(self):
        """
        Stores model instances in bulk to database.
        :return: None
        """

        self.model.objects.bulk_create(self.to_create_list)
        self.to_create_list = list()

