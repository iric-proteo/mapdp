"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import logging

# Import Django related libraries
from django.utils.safestring import mark_safe

# Import third party libraries

# Import project libraries
from base_site.exceptions import BaseSiteException

logger = logging.getLogger('django')

error_msg = {
    'database_integrity': lambda x: 'Unexpected issue while storing to database.',
    'file_missing': lambda x: f'{x} does not exists.',
    'fileset_missing': lambda x: f'Your File set should contain {x}.',
    'mzidentml_multi': lambda x: ('Please select a single mzIdentML file in the'
                                  ' file set'),
    'mzidentml_version': lambda x: (f'mzIdentML file {x} is not '
                                    'version 1.1.'),
    'parsing': lambda x: (f'Unexpected issue while parsing {x}. '
                          'Please check file format or if file is corrupted.'
                          ),
    'service_na': lambda x: f'{x} service is currently unavailable.',
    'unexpected': lambda x: mark_safe(f'{x["name"]} returned the following error: '
                                      f'<br /><pre>{x["error"]}</pre>')
}


def process(error_type: str, values: dict, log: bool=False, request=None):
    """

    :param error_type: Error key in error_msg dict.
    :param values: Dict with values required to display msg.
    :param log: Boolean to log the error.
    :param request: Django request object.
    :return: Raises BaseSiteException
    """

    msg = error_msg[error_type](values)

    if log:
        kwargs = {
            'exc_info': True,
        }

        if request is not None:
            kwargs['extra'] = {'request': request}

        logger.error(msg, **kwargs)

    raise BaseSiteException('error', msg)

