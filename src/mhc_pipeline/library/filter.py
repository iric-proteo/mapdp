"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import json

# Import Django related libraries

# Third party libraries

# Import project libraries
from ..library.permission import set_content_permission
from ..models import Filter


def create_filter(fc, user):
    """
    Creates a new Filter instance.
    :param fc: Form cleaned
    :param user: User instance
    :return: Filter instance
    """

    if fc.get('filter_name', '') != '':

        fc_copy = fc.copy()
        filter_name = fc_copy['filter_name']

        # Remove fields that should not be saved in the filter
        fields = ['filter_json', 'filter_name', 'identification',
                  'identification_key', 'immunopeptidome',
                  'immunopeptidome_name', 'selected_filter', 'url',
                  'validation_timestamp', 'ref_url'
                  ]

        for field in fields:
            fc_copy.pop(field, None)

        # Remove empty values
        exclude_list = [None, '', False, []]
        fc_copy = {key: value for key, value in fc_copy.items() if
                   value not in exclude_list}

        # Converts MHC allele
        if 'mhc_allele' in fc_copy:
            fc_copy['mhc_allele'] = '|' + '|'.join(fc_copy['mhc_allele']) + '|'

        if len(fc_copy) == 0:
            return None

        filter_obj = Filter.objects.create(created_by=user,
                                           modified_by=user,
                                           name=filter_name,
                                           json_values=json.dumps(fc_copy)
                                           )

        set_content_permission(filter_obj, user,
                               ['change', 'delete', 'view', 'visible'])

        return filter_obj

    else:
        return None


def filter_json(fc):
    """
    Generates filter form JSON representation
    :param fc: Form cleaned data
    :return: JSON String
    """

    fc_copy = fc.copy()

    if 'ref_url' in fc_copy:
        del (fc_copy['ref_url'])

    # Remove empty values
    exclude_list = [None, '', False, []]
    fc_copy = {key: value for key, value in fc_copy.items() if
               value not in exclude_list}

    json_str = json.dumps(fc_copy)

    return json_str
