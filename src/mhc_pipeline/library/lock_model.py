"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
from datetime import datetime, timedelta
from pytz import timezone
import time

# Import Django related libraries
from django.conf import settings

# Third party libraries

# Import project libraries
from base_site.exceptions import BaseSiteException
from ..models import LockModel


class ModelLocker(object):

    def __init__(self, models: list, lock_trial: int, lock_trial_delay: int):
        """
        Locks models for write
        :param models: Models
        :param lock_trial: Number of time to try to acquire db lock
        :param lock_trial_delay: Seconds between db lock trial
        """

        self.models = models
        self.locked = False
        self.locks = list()

        trial = 0

        while self.locked is False:

            self.acquire_locks()

            if trial == lock_trial:
                raise BaseSiteException('database_locked', 'Database locked')

            time.sleep(lock_trial_delay)
            trial += 1

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release_locks()

    def acquire_locks(self):
        """
        Creates locks in the database.
        :return:
        """

        self.clean_expired_locks()

        for model in self.models:

            obj, created = LockModel.objects.get_or_create(model=model)

            if created:

                self.locks.append(obj)
                self.locked = True

            else:
                self.release_locks()
                self.locked = False
                break

    @staticmethod
    def clean_expired_locks(minutes=10):
        """
        Removes locks older than x minutes from the database.
        :param minutes: Integer
        :return: None
        """

        tz = timezone(settings.TIME_ZONE)

        time_threshold = datetime.now(tz=tz) - timedelta(minutes=minutes)

        LockModel.objects.filter(timestamp__lt=time_threshold).delete()

    def release_locks(self):
        """
        Removes locks from the database.
        :return: None
        """

        for lock in self.locks:

            lock.delete()

