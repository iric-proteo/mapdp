"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from collections import OrderedDict
import functools
import os
import re
from typing import List

# Import Django related libraries
from django.core.exceptions import ObjectDoesNotExist
from django.utils.safestring import mark_safe

# Third party library

# Import project libraries
from base_site.exceptions import BaseSiteException
from library.mhc_allele import retrieve_hla_alleles
from mhc_pipeline import models

# Fix for pandas invalid warning
import warnings

warnings.simplefilter(action="ignore", category=RuntimeWarning)

MHC_PREDICTOR_NAMES = OrderedDict()

MHC_PREDICTOR_NAMES['mhcflurry_2.0.1'] = 'mhcflurry-2.0.1'

if os.environ.get('NETMHC_URL', ''):
    MHC_PREDICTOR_NAMES.update(
        OrderedDict([
        ('NETMHC34', 'NetMHC-3.4a'),
        ('NETMHC4', 'NetMHC-4.0'),
        ('NETMHCCONS11', 'NetMHCcons-1.1'),
        ('NETMHCpan30', 'NetMHCpan-3.0a'),
        ('NETMHCpan40', 'NetMHCpan-4.0a'),
        ('NETMHCpan41', 'NetMHCpan-4.1'),
        ('NETMHCII22', 'NetMHCII-2.2'),
        ('NETMHCII23', 'NetMHCII-2.3'),
        ('NETMHCIIpan31', 'NetMHCIIpan-3.1a'),
        ('NETMHCIIpan32', 'NetMHCIIpan-3.2'),
        ('NETMHCIIpan40', 'NetMHCIIpan-4.0'),
        ]))


@functools.lru_cache(maxsize=None)
def get_biological_sample_mhc_allele(biological_sample_id,
                                     selected_alleles):
    """
    Gets MHCAllele instances associated to a BiologicalSample
    :param biological_sample_id: BiologicalSample instance primary key
    :param selected_alleles: Queryset of alleles to keep
    :return: MHCAllele queryset
    """

    try:
        bs_mhc_allele = models.BiologicalSampleMHCAllele.objects.get(
        biological_sample_id=biological_sample_id)

    except ObjectDoesNotExist:

        url = models.BiologicalSample.objects.get(
            pk=biological_sample_id).get_absolute_url()

        msg = mark_safe('A biological sample is missing MHC allele. '
                        f'Please fix it <a href="{url}">here</a>.')

        raise BaseSiteException('warning', msg)

    mhc_allele = bs_mhc_allele.mhc_allele.all()

    if len(selected_alleles):
        mhc_allele = mhc_allele.filter(pk__in=selected_alleles)

    return mhc_allele


@functools.lru_cache(maxsize=None)
def get_biological_sample_mhc_allele_names(biological_sample_id,
                                           selected_alleles=models.MHCAllele.objects.none()):
    """
    Gets MHCAllele names associated to a BiologicalSample
    :param biological_sample_id: BiologicalSample instance primary key
    :param selected_alleles: Queryset of alleles to keep
    :return: List of string.
    """

    return get_biological_sample_mhc_allele(
        biological_sample_id, selected_alleles).values_list('name', flat=True)


def pair_alleles(alleles: List[str]) -> List[str]:
    """
    Pair HLA-II alleles DP and DQ into a single entry
    :param alleles: List of alleles
    :return: Modified list of alleles
    """

    for mhc_class in ('DP', 'DQ'):
        alpha_alleles = [allele for allele in alleles
                         if mhc_class + 'A' in allele]
        beta_alleles = [allele for allele in alleles
                        if mhc_class + 'B' in allele]

        for a in alpha_alleles:
            for b in beta_alleles:
                try:
                    alleles.remove(b)
                except ValueError:
                    pass
                b = b.replace('HLA', '')
                name = a + b
                name = name.replace('*', '')
                alleles.append(name)

            alleles.remove(a)

    return alleles


@functools.lru_cache(maxsize=None)
def get_biological_sample_mhc_allele_names_paired(biological_sample_id: int,
                                                  selected_alleles):
    """
    Gets MHCAllele names associated to a BiologicalSample
    HLA-II alleles DP and DQ are paired into a single entry
    :param biological_sample_id: BiologicalSample instance primary key
    :param selected_alleles: Queryset of alleles to keep
    :return: List of string.
    """

    names = get_biological_sample_mhc_allele_names(
        biological_sample_id, selected_alleles=selected_alleles)

    return pair_alleles(list(names))


def get_used_mhc_alleles():
    """
    Gets alleles used in all biological samples.
    :return: MHCAllele queryset
    """

    inner_qs = models.BiologicalSampleMHCAllele.mhc_allele.through.objects.all()
    inner_qs = inner_qs.values('mhcallele_id')

    return models.MHCAllele.objects.filter(pk__in=inner_qs)


def store_mhc_alleles(mhc_alleles=None, mhc_classes=None):
    """
    Stores MHC alleles in database
    :param mhc_alleles: List of MHC alleles.
    :param mhc_classes: List of MHC gene to keep.
    :return: None
    """

    stored_mhc_alleles = models.MHCAllele.objects.all().values_list('name',
                                                                    flat=True)
    stored_mhc_alleles = {allele for allele in stored_mhc_alleles}
    
    if mhc_alleles is None:
        mhc_alleles = retrieve_hla_alleles()    

    objs = list()

    for allele in mhc_alleles:
        mhc_class, gene, fields = re.split('[-\*]', allele)

        if mhc_classes is not None and gene not in mhc_classes:
            continue

        if allele in stored_mhc_alleles:
            continue

        objs.append(models.MHCAllele(mhc_class=mhc_class,
                                     gene=gene,
                                     name=allele))

    models.MHCAllele.objects.bulk_create(objs, batch_size=500)

