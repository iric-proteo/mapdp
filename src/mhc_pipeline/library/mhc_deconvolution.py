"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
from pathlib import Path
import shutil
import subprocess

# Import Django related libraries
from django.conf import settings

# Third party libraries
import pandas as pd

# Import project libraries
from ..library.minio import get_storage


def deconvolution(peptides, filter_):
    """
    Runs MHC motif deconvolution
    :param peptides: Peptide queryset
    :param filter_: Form cleaned data
    :return: Pandas data frame
    """

    software = filter_['deconvolution_software']

    work_dir = (Path(settings.NEXTFLOW_FS) / 'deconvolution' /
                filter_['identification_key'])
    work_dir.mkdir(exist_ok=True, parents=True)
    peptides_file = work_dir / 'peptides.txt'

    if type(peptides) == list:
        peptide_subset = peptides_selection(peptides, software)
    else:
        peptide_subset = peptides_queryset(peptides, software)

    with peptides_file.open('w') as fh_out:
        for peptide in peptide_subset:
            fh_out.write(peptide)
            fh_out.write('\n')

    if software.startswith('MoDec'):
        command = ['/usr/src/app/bin/nextflow',
                   '/usr/src/app/src/nf-workflows/modec/main.nf',
                   '--pepfiles=*.txt',
                   '-profile',
                   'k8s']
    else:
        command = ['/usr/src/app/bin/nextflow',
                   '/usr/src/app/src/nf-workflows/mixmhcp/main.nf',
                   '--pepfiles=*.txt',
                   '-profile',
                   'k8s']

    cwd = Path.cwd()
    os.chdir(work_dir)

    subprocess.run(command, check=True)

    os.chdir(cwd)

    if software.startswith('MoDec'):
        df = data_frame_modec(work_dir, filter_['deconvolution_cluster_number'])
        df = filters_modec(df, filter_)
        df['resp_1st'] = df['resp_1st'].apply(lambda x: f'{x:.4f}')
        df['resp_2nd'] = df['resp_2nd'].apply(lambda x: f'{x:.4f}')
    else:
        df = data_frame_mixmhcp(work_dir, filter_['deconvolution_cluster_number'])
        df = filters_mixmhcp(df, filter_)

        for i in range(1, filter_['deconvolution_cluster_number'] + 3):
            df.iloc[:, i] = df.iloc[:, i].apply(lambda x: f'{x:.4f}')

    df = df.set_index('Peptide')

    storage = get_storage()

    results_path = work_dir / 'results' / 'deconvolution'

    for full_path in (results_path).glob('**/*'):

        path = str(full_path).replace(str(results_path), '')

        if any(keyword in path for keyword in ['.html', '.png']):
            key = filter_['identification_key'] + str(path)
            key = key.replace('/', '_')

            if 'deconvolution_report' in key:
                key = key.replace('deconvolution_report', 'logos')

            with storage.open(key, 'wb') as cache_file, \
                    full_path.open('rb') as fh:
                cache_file.write(fh.read())

    shutil.rmtree(work_dir, ignore_errors=True)

    return df


def data_frame_mixmhcp(work_dir: Path, cluster_count: int) -> pd.DataFrame:
    """
    Shapes a Pandas data frame to annotate peptide
    :param work_dir: Directory with results files
    :param cluster_count: Number of clusters/motifs
    :return: MixMHCp responsibility dataframe
    """

    responsibility_file = (work_dir / 'results' / 'deconvolution'
                           / 'responsibility' / f'resp_{cluster_count}.txt')

    df = pd.read_csv(responsibility_file, sep='\t')
    df = df.iloc[:, 0:cluster_count + 2]

    df['max'] = df.iloc[:, 1:cluster_count + 2].max(axis=1)
    df['idxmax'] = df.iloc[:, 1:cluster_count + 2].idxmax(axis=1)

    return df


def data_frame_modec(work_dir: Path, cluster_count: int) -> pd.DataFrame:
    """
    Shapes a Pandas data frame to annotate peptide
    :param work_dir: Directory with results files
    :param cluster_count: Number of clusters/motifs
    :return: MoDec responsibility dataframe
    """

    responsibility_file = (work_dir / 'results' / 'deconvolution'
                           / 'Responsibilities' / f'bestPepResp_K{cluster_count}.txt')

    df = pd.read_csv(responsibility_file, sep='\t')

    columns = ['Peptide', 'resp_1st', 'Motif_1st', 'resp_2nd', 'Motif_2nd']

    df = df[columns]

    return df


def filters_mixmhcp(df: pd.DataFrame, filter_) -> pd.DataFrame:
    """
    Filters MHC deconvolution data frame
    :param df: Deconvolution data frame
    :param filter_: Form cleaned data
    :return: Filtered deconvolution data frame
    """

    if filter_.get('deconvolution_remove_trash'):
        df = df[df['idxmax'] != 'Trash']

    if filter_.get('deconvolution_max_trash_responsibility'):
        df = df[
            df['Trash'] <= filter_['deconvolution_max_trash_responsibility']]

    return df


def filters_modec(df: pd.DataFrame, filter_) -> pd.DataFrame:
    """
    Filters MHC deconvolution data frame
    :param df: Deconvolution data frame
    :param filter_: Form cleaned data
    :return: Filtered deconvolution data frame
    """

    if filter_.get('deconvolution_remove_trash'):
        df = df[df['Motif_1st'] != 0]

    if filter_.get('deconvolution_max_trash_responsibility'):
        df = df[(df['Motif_2nd'] != 0) |
                (df['resp_2nd'] <= filter_['deconvolution_max_trash_responsibility'])]

    return df


def peptides_queryset(peptides, software: str):
    """
    Modifies peptides queryset with peptide length base on the software

    This is called before FDR.

    :param peptides: Peptides queryset
    :param software: MHC deconvolution software
    :return: Queryset that returns dictionaries
    """

    if software.startswith('MoDec'):

        # MHC II peptides
        peptide_length_min = 12
        peptide_length_max = 25

    else:

        # MHC I peptides
        peptide_length_min = 8
        peptide_length_max = 15

    peptides = (peptides.filter(peptide_length__gte=peptide_length_min,
                                peptide_length__lte=peptide_length_max)
                .values_list('peptide_sequence', flat=True)
                .distinct())

    return peptides


def peptides_selection(peptides, software: str):
    """
    Modifies peptides list with peptide length base on the software

    This is called after FDR.

    :param peptides: Peptides list
    :param software: MHC deconvolution software
    :return: Queryset that returns dictionaries
    """

    if software.startswith('MoDec'):

        # MHC II peptides
        peptide_length_min = 9
        peptide_length_max = 25

    else:

        # MHC I peptides
        peptide_length_min = 8
        peptide_length_max = 15

    selected_peptides = dict()

    for peptide in peptides:

        if peptide.is_decoy:
            continue

        if peptide_length_min <= peptide.peptide_length <= peptide_length_max:
            selected_peptides[
                peptide.unique_peptide_id] = peptide.peptide_sequence

    return selected_peptides.values()
