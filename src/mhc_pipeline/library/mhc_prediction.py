"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from collections import defaultdict
import functools
import warnings

# Import Django related libraries

# Third party libraries
import pandas as pd
from requests.exceptions import (ConnectionError,
                                 ConnectTimeout)

# Import project libraries
from base_site.exceptions import BaseSiteException
from library import mhcflurry
from library.netmhc import NetMHC
from mhc_pipeline.library import (errors, mhc_allele)
from mhc_pipeline.library.mhc_allele import MHC_PREDICTOR_NAMES
from ..models import MHCAllele


def bin_peptides_per_alleles(peptides, selected_alleles,
                             ignore_sample_alleles):
    """
    Bins peptides per allele based on sample alleles and user selection.

    This code works for multi sample predictions.
    :param peptides: Peptide queryset
    :param selected_alleles: MHC allele queryset
    :param ignore_sample_alleles: Boolean
    :return: allele_peptides dictionary, sequences dictionary
    """

    allele_peptides = defaultdict(set)
    sequences = defaultdict(list)

    # Flush cache to have up to date values
    mhc_allele.get_biological_sample_mhc_allele.cache_clear()
    mhc_allele.get_biological_sample_mhc_allele_names.cache_clear()

    # Classify peptide per allele ()
    if ignore_sample_alleles:

        for peptide in peptides:
            sequences[peptide['peptide_sequence']].append(
                (peptide['pk'],
                 peptide['identification__biological_sample_id'])
            )

        allele_names = [allele.name for allele in selected_alleles]
        paired_alleles = mhc_allele.pair_alleles(allele_names)

        for allele in paired_alleles:
            allele_peptides[allele] = sequences.keys()

    else:

        for peptide in peptides:

            sequences[peptide['peptide_sequence']].append(
                (peptide['pk'],
                 peptide['identification__biological_sample_id'])
            )

            peptide_mhc_alleles = mhc_allele.get_biological_sample_mhc_allele_names_paired(
                peptide['identification__biological_sample_id'],
                selected_alleles)

            for allele in peptide_mhc_alleles:
                allele_peptides[allele].add(peptide['peptide_sequence'])

    return allele_peptides, sequences


def data_frame(predictions):
    """
    Shapes a Pandas data frame to query predictions
    :param predictions: List
    :return: Pandas data frame
    """

    df = pd.DataFrame(predictions)

    if df.shape[0] == 0:
        msg = 'MHC predictor returned no prediction.'

        raise BaseSiteException('warning', msg)

    df = pd.pivot_table(df, index=['peptide_id'], columns=['mhc_allele__name'])

    warnings.simplefilter(action="ignore", category=RuntimeWarning)

    columns = ['affinity']

    if 'rank' in df:
        columns.append('rank')

    if 'rank_el' in df:
        columns.append('rank_el')

    for column in columns:
        min_value = df[column].min(axis=1)
        min_allele = df[column].idxmin(axis=1)

        df[column, 'Minimum'] = min_value
        df[column, 'Minimum allele'] = min_allele

    return df


def filters(df, fc, mhc_predictor):
    """
    Filters MHC predictions data frame
    :param df: Predictions data frame
    :param fc: Form cleaned data
    :param mhc_predictor: String
    :return: Predictions data frame
    """

    if fc.get('affinity_threshold'):
        df = df[df['affinity', 'Minimum'] <= fc['affinity_threshold']]

    if fc.get('rank_threshold'):

        if 'rank' not in df:
            msg = f'{MHC_PREDICTOR_NAMES[mhc_predictor]} doesn\'t support rank' \
                  f' filter.'

            raise BaseSiteException('warning', msg)

        df = df[df['rank', 'Minimum'] <= fc['rank_threshold']]

    if fc.get('rank_el_threshold'):

        if 'rank_el' not in df:
            msg = f'{MHC_PREDICTOR_NAMES[mhc_predictor]} doesn\'t support rank_el' \
                  f' filter.'

            raise BaseSiteException('warning', msg)

        df = df[df['rank_el', 'Minimum'] <= fc['rank_el_threshold']]

    return df


def format_predictions(args, sequences=None, predictions_complete=None,
                       selected_alleles=None):
    """
    Format MHC predictions
    :param args: NetMHC predictions and job parameters
    :param sequences: Peptide sequence
    :param predictions_complete: List with of prediction dictionary
    :param selected_alleles: Queryset of alleles to keep
    :return: None
    """

    predictions = args[0]
    params = args[1]

    allele = params['allele']

    try:

        for prediction in predictions:

            for peptide in sequences[prediction['peptide']]:

                if not params['ignore_sample_alleles']:

                    peptide_mhc_alleles = \
                        mhc_allele.get_biological_sample_mhc_allele_names_paired(
                            peptide[1], selected_alleles)

                    if allele not in peptide_mhc_alleles:
                        continue

                data = {
                    'peptide_id': peptide[0],
                    'affinity': float(prediction['affinity']),
                    'mhc_allele__name': allele
                }

                if 'rank' in prediction:
                    data['rank'] = float(prediction['rank'])

                if 'rank_el' in prediction:
                    data['rank_el'] = float(prediction['rank_el'])


                for field in ['rank', 'rank_el', 'processing_score',
                              'presentation_score', 'presentation_rank']:
                    if field in prediction:
                        data[field] = float(prediction[field])

                predictions_complete.append(data)

    except RuntimeError as e:

        predictor_errors = [
            'cannot be found in hla_list',
            'NO PREDICTION METHODS AVAILABLE FOR ALLELE ',
            'wrong format or doesn\'t exist in allele list',
            'thr does not exist',
            'refer to the list of available molecules',
            'cannot be found in hla_pseudo list',
            '.thr_cmb does not exist.',
            'No single-allele models for allele(s)'
        ]

        exception_str = str(e)

        for error in predictor_errors:

            if error in exception_str:
                return

        errors.process('unexpected', {'name': 'MHC predictor',
                                      'error': exception_str}, log=True)


def get_affinity(df, peptide_pk, allele_name):
    """
    Gets MHC affinity from data frame
    :param df: Pandas data frame with predictions
    :param peptide_pk: Peptide instance primary key
    :param allele_name: String
    :return: Float or None
    """

    try:
        return df.loc[peptide_pk, 'affinity'].loc[allele_name]

    except (AttributeError, KeyError):
        return None


def get_predictions(peptides, filter_):
    """
    Gets MHC binding predictions
    :param peptides: Peptide queryset
    :param filter_: Form cleaned data
    :return: Pandas data frame
    """

    mhc_predictor = filter_['mhc_predictor']
    ignore_sample_alleles = filter_.get('ignore_sample_alleles', False)
    selected_alleles = MHCAllele.objects.filter(pk__in=filter_.get('mhc_allele',
                                                                   []))

    predictions = list()

    # Prepare peptide queryset and allele bins
    peptides = peptides_queryset(peptides, mhc_predictor)

    allele_peptides, sequences = bin_peptides_per_alleles(
        peptides, selected_alleles, ignore_sample_alleles)

    # Run and parse prediction
    params = jobs_params(allele_peptides, mhc_predictor,
                         ignore_sample_alleles)

    process_predictions_partial = functools.partial(
        format_predictions,
        sequences=sequences,
        predictions_complete=predictions,
        selected_alleles=selected_alleles
    )

    if mhc_predictor == 'mhcflurry_2.0.1':

        mhcflurry.queue_jobs(params, process_predictions_partial)

    else:

        try:
            NetMHC.queue_jobs(params, process_predictions_partial)

        except (ConnectionError, ConnectTimeout):

            errors.process('service_na', mhc_predictor, log=True)

    # # Prepare and format data frame
    df = data_frame(predictions)

    return filters(df, filter_, mhc_predictor)


def jobs_params(classified_peptides, mhc_predictor, ignore_sample_alleles):
    """
    Creates generator that prepare parameters to run multiple MHC jobs.
    :param classified_peptides: Dictionary key: allele value: peptide set
    :param mhc_predictor: MHC predictor integer
    :param ignore_sample_alleles: Ignore sample alleles check
    :return: Generator that yield dictionary of parameters
    """

    batch_function = NetMHC.batch_peptides_by_length

    if mhc_predictor == 'mhcflurry_2.0.1':
        batch_function = mhcflurry.batch_peptides

    for allele, peptides in classified_peptides.items():

        for peptide_batch in batch_function(peptides, batch_size=5000):

            yield {'allele': allele,
                   'predictor': mhc_predictor,
                   'peptide_batch': peptide_batch,
                   'ignore_sample_alleles': ignore_sample_alleles
                   }


def peptides_queryset(peptides, predictor):
    """
    Modifies peptides queryset with peptide length base on the predictor
    :param peptides: Peptides queryset
    :param predictor: String
    :return: Queryset that returns dictionaries
    """

    predictors = ['NETMHCII22',
                  'NETMHCII23',
                  'NETMHCIIpan31',
                  'NETMHCIIpan32',
                  'NETMHCIIpan40',
                  ]

    if predictor in predictors:

        # MHC II peptides
        peptide_length_min = 9
        peptide_length_max = 25

    else:

        # MHC I peptides
        peptide_length_min = 8
        peptide_length_max = 15

    peptides = peptides.filter(peptide_length__gte=peptide_length_min,
                               peptide_length__lte=peptide_length_max)
    peptides = peptides.values('pk', 'peptide_sequence',
                               'identification__biological_sample_id')

    return peptides
