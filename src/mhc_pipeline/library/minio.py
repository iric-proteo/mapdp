"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.conf import settings
from django.core.files.storage import (default_storage,
                                       get_storage_class)
from django.utils.functional import LazyObject


# Third party libraries
from botocore.exceptions import ClientError

# Import project libraries


class MinioStorage(LazyObject):
    def _setup(self):
        self._wrapped = get_storage_class(
            import_path='storages.backends.s3boto3.S3Boto3Storage')()


def get_storage():
    """
    Selects Minio storage if configured or default to disk storage (for test).
    :return: Storage class
    """

    if hasattr(settings, 'AWS_S3_ENDPOINT_URL'):
        storage = MinioStorage()

        # Setup bucket life cycle
        # This should not be here if Minio server has a permanent volume

        if settings.MINIO_EXPIRATION_DAYS != -1:
            s3 = storage.connection

            try:
                bucket_lifecycle_configuration = s3.BucketLifecycleConfiguration(
                    'identifications')
                bucket_lifecycle_configuration.put(
                    LifecycleConfiguration={
                        'Rules': [
                            {
                                "Expiration": {
                                    "Days": 7
                                },
                                "ID": "Delete temporary identifications",
                                "Status": "Enabled"
                            }
                        ]
                    }
                )
            except ClientError:
                pass

    else:
        storage = default_storage

    return storage
