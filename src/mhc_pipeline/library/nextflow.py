"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from pathlib import Path
import shutil
import subprocess
from typing import List, Union

# Import Django related libraries
from django.conf import settings

# Third party libraries

# Import project libraries


class Nextflow:
    """
    This class runs Nextflow and prepares/cleans working directory
    """

    def __init__(self, workflow: str, identifier: Union[str, int]) -> None:
        """
        Initializes working directory
        :param workflow: Workflow name
        :param identifier: Identifier for working directory
        """

        self.clean = True
        self.workflow = workflow

        # Prepare working directory
        self.work_dir = Path(settings.NEXTFLOW_FS) / workflow / str(identifier)
        self.work_dir.mkdir(exist_ok=True, parents=True)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Clean working directory
        if self.clean:
            shutil.rmtree(self.work_dir, ignore_errors=True)

    def run(self, parameters: List[str]) -> None:
        """
        Executes Nextflow
        :param parameters: Workflow parameters
        :return: None
        """

        command = ['/usr/src/app/bin/nextflow',
                   f'/usr/src/app/src/nf-workflows/{self.workflow}/main.nf',
                   '-profile',
                   'k8s'] + parameters

        try:
            subprocess.run(command, check=True, cwd=self.work_dir)

        except subprocess.CalledProcessError:
            self.clean = False

