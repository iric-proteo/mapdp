"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
from collections import defaultdict
from multiprocessing import Pool
import os
from pathlib import Path
import pickle
import time
from typing import Union

# Import Django related libraries
from django import db
from django.conf import settings

# Import third party library

# Import project libraries
from base_site.exceptions import BaseSiteException
from mhc_pipeline.models import (Identification,
                                 MHCAllele,
                                 PeptideCounts,
                                 PeptideBiologicalSampleCount,
                                 PeptideFileCount)
from mhc_pipeline.library.peptides_filter import PeptidesFilter


def process(parallel=0) -> None:
    """
    Gets peptide counts for identification
    :param parallel: Parallel processing (doesn't work with Celery)
    :return: None, stores results to MEDIA_ROOT/stats and database
    """

    # Retrieves last processed identification instance
    last_identification = PeptideCounts.objects.last()
    last_identification_id = 0

    if last_identification is not None:
        last_identification_id = last_identification.identification_id

    identifications = Identification.objects.filter(trashed=False)
    identifications = identifications.filter(pk__gt=last_identification_id)
    identifications = list(identifications)

    # Loads stats object
    stats_path = Path(settings.MEDIA_ROOT) / 'stats'
    stats_file = stats_path / 'data.pickle'

    if not stats_path.exists():
        stats_path.mkdir()

    if stats_file.exists():

        with stats_file.open('rb') as f:
            stats = pickle.load(f)
    else:
        stats = Stats()

    # Process identifications

    if parallel:

        # Required to avoid django.db.utils.OperationalError: lost
        # synchronization with server
        db.connections.close_all()

        with Pool(parallel) as pool:
            for values in pool.imap(get_peptides, identifications):
                stats.process_peptides(*values)
    else:
        for values in map(get_peptides, identifications):
            stats.process_peptides(*values)

    stats.peptide_file_counts()
    stats.peptide_biological_sample_counts()

    # Stores stats object
    with stats_file.open('wb') as f:
        pickle.dump(stats, f, pickle.HIGHEST_PROTOCOL)


def get_peptides(identification) -> tuple:
    """
    Filters peptides associated to the identification instance and returns them.
    :param identification: Identification instance
    :return: Peptides
    """

    print(identification, identification.state)

    data = dict()
    peptides = set()

    while identification.state in ['pending', 'started']:
        time.sleep(25)
        identification.refresh_from_db()

    if identification.state == 'failed':
        return identification.pk, peptides, data

    try:
        filter_options = {
            'identification': [identification.pk],
            'immunopeptidome': [],
            'peptide_length__gte': 8,
            'peptide_length__lte': 15,
            'score_fdr': 0.01,
            'unique_sequence': 'on'
        }

        data = dict()
        peptide_filters = PeptidesFilter(filter_options, data=data)
        peptide_filters.apply()

        score_threshold = peptide_filters.values.get('score_threshold')

        filter_options_2 = {
            'identification': [identification.pk],
            'immunopeptidome': [],
            'mhc_predictor': 'NETMHCpan40',
            'peptide_length__gte': 8,
            'peptide_length__lte': 15,
            'peptide_score__gte': score_threshold,
            'rank_threshold': 2,
            'remove_decoy': True,
            'unique_sequence': 'on'
        }

        # Fall back to mhcflurry if NetMHC predictors are not available
        if os.environ.get('NETMHC_URL', '') == '':
            filter_options_2['mhc_predictor'] = 'mhcflurry_1.2.4'


        peptide_filters = PeptidesFilter(filter_options_2, data=data)
        peptides = peptide_filters.apply()

    except BaseSiteException as e:

        print(e.message)

    return identification.pk, peptides, data


class Stats:
    """
    Computes peptide counts statistics for all identification
    """

    def __init__(self):

        self.unique_peptides = dict()
        self.biological_samples = set()
        self.biological_samples.add('All')
        self.biological_sample_counts = defaultdict(set)

    def process_peptides(self, identification_pk: int, peptides: list,
                         data: dict) -> None:
        """
        Processes the list of peptides to get various peptide counts
        :param identification_pk: Identification primary key
        :param peptides: Peptides
        :param data: Extra data
        :return: None
        """

        identification = Identification.objects. \
            select_related('biological_sample').get(pk=identification_pk)
        taxonomy_identifier = identification.biological_sample.Species

        biological_sample_id = identification.biological_sample_id
        self.biological_samples.add(identification.biological_sample)

        unique_peptides = dict()

        for peptide in peptides:
            file_set = set()
            file_set.add(peptide.scan_file)
            unique_peptides[peptide.unique_peptide_id] = file_set

            self.biological_sample_counts[peptide.unique_peptide_id].add(
                biological_sample_id)

        identifier_mapping = {peptide.pk: peptide.unique_peptide_id
                              for peptide in peptides}

        # Get peptide counts for all, the biological sample and the taxonomy
        self.peptide_counts(identification, unique_peptides, 'All', 0, 'All')
        self.peptide_counts(identification, unique_peptides, 'All',
                            taxonomy_identifier, 'All')
        self.peptide_counts(identification, unique_peptides,
                            biological_sample_id, taxonomy_identifier, 'All')

        # Get peptide counts per allele for all and the biological sample
        if 'mhc_predictor' in data:

            affinities = data['mhc_predictor']['affinity']

            alleles = list(data['mhc_predictor']['affinity'].columns)
            alleles = [x for x in alleles if 'Minimum' not in x]

            for allele in alleles:
                rows = affinities['Minimum allele'] == allele

                peptide_ids = affinities.index[rows].tolist()

                unique_peptides = dict()

                for id_ in peptide_ids:
                    if id_ in identifier_mapping:
                        unique_peptides[identifier_mapping[id_]] = set()

                self.peptide_counts(identification, unique_peptides,
                                    'All', 0, allele)
                self.peptide_counts(identification, unique_peptides,
                                    'All', taxonomy_identifier, allele)
                self.peptide_counts(identification, unique_peptides,
                                    biological_sample_id, taxonomy_identifier,
                                    allele)

    def peptide_counts(self, identification, unique_peptides: dict,
                       biological_sample_id: Union[int, str],
                       taxonomy_identifier: int,
                       allele: str):
        """
        Peptide counts for individual cumulative identification
        :param identification: Identification instance
        :param unique_peptides: Unique peptides set
        :param biological_sample_id: Biological sample primary key
        :param taxonomy_identifier: Taxonomy identifier
        :param allele: Allele name
        :return: None (add entry to rows)
        """

        key = (biological_sample_id, taxonomy_identifier, allele)

        counts = len(unique_peptides)

        if key in self.unique_peptides:

            entry = self.unique_peptides[key]

            for unique_id, file_set in unique_peptides.items():

                if unique_id in entry:
                    entry[unique_id].update(file_set)
                else:
                    entry[unique_id] = file_set
        else:
            self.unique_peptides[key] = unique_peptides.copy()

        counts_after = len(self.unique_peptides[key])

        if biological_sample_id == 'All':
            biological_sample = None
        else:
            biological_sample = identification.biological_sample

        if allele == 'All':
            allele = None
        else:
            allele = MHCAllele.objects.get(name=allele)

        return PeptideCounts.objects.create(
            identification=identification,
            biological_sample=biological_sample,
            taxonomy_identifier=taxonomy_identifier,
            allele=allele,
            individual_counts=counts,
            cumulative_counts=counts_after
        )

    def peptide_file_counts(self) -> None:
        """
        Counts the number of time a peptide is observed in different files
        :return: None, save to database
        """

        PeptideFileCount.objects.all().delete()

        for key in self.unique_peptides.keys():

            biological_sample_id, taxonomy_identifier, allele = key

            if allele == 'All' in key:

                entries = self.unique_peptides[key].items()

                counts = defaultdict(int)

                for unique_peptide_id, file_set in entries:
                    counts[len(file_set)] += 1

                if biological_sample_id == 'All':
                    biological_sample_id = None

                for file_counts in sorted(counts.keys()):
                    PeptideFileCount.objects.create(
                        biological_sample_id=biological_sample_id,
                        taxonomy_identifier=taxonomy_identifier,
                        file_counts=file_counts,
                        peptide_counts=counts[file_counts]
                    )

    def peptide_biological_sample_counts(self) -> None:
        """
        Counts the number of time a peptide is observed in different biological
        samples
        :return: None, save to database
        """

        PeptideBiologicalSampleCount.objects.all().delete()

        taxonomy_identifiers = defaultdict(set)

        for biological_sample in self.biological_samples:

            if biological_sample != 'All':
                biological_sample_id = biological_sample.pk
                taxonomy_identifier = biological_sample.Species
                taxonomy_identifiers[taxonomy_identifier].add(
                    biological_sample_id)
            else:
                biological_sample_id = None
                taxonomy_identifier = 0

            counts = defaultdict(int)

            entries = self.biological_sample_counts.items()

            for unique_peptide_id, biological_sample_set in entries:

                if biological_sample_id is None:

                    counts[len(biological_sample_set)] += 1

                else:
                    if biological_sample_id in biological_sample_set:
                        counts[len(biological_sample_set)] += 1

            for bs_counts in sorted(counts.keys()):
                PeptideBiologicalSampleCount.objects.create(
                    biological_sample_id=biological_sample_id,
                    taxonomy_identifier=taxonomy_identifier,
                    biological_sample_counts=bs_counts,
                    peptide_counts=counts[bs_counts]
                )

        for taxonomy_identifier, tax_biological_sample_set in \
                taxonomy_identifiers.items():

            counts = defaultdict(int)

            entries = self.biological_sample_counts.items()

            for unique_peptide_id, biological_sample_set in entries:

                intersection = tax_biological_sample_set.intersection(
                    biological_sample_set)

                if intersection:
                    counts[len(intersection)] += 1

            for bs_counts in sorted(counts.keys()):
                PeptideBiologicalSampleCount.objects.create(
                    biological_sample_id=None,
                    taxonomy_identifier=taxonomy_identifier,
                    biological_sample_counts=bs_counts,
                    peptide_counts=counts[bs_counts]
                )
