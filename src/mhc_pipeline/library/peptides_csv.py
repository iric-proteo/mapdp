"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import functools
import json
import gzip
import re

# Import Django related libraries
from django.core.exceptions import ObjectDoesNotExist
from django.forms.models import model_to_dict

# Third party libraries

# Import project libraries
from base_site.exceptions import BaseSiteException
from ..library.minio import get_storage
from ..library.mhc_allele import MHC_PREDICTOR_NAMES
from ..library.peptides_filter import PeptidesFilter
from ..library import protein_database
from ..models import (File,
                      Identification,
                      ImmunoPeptidome,
                      MHCAllele)


def column_names(filter_options: dict, data=dict()) -> list:
    """
    Provides column names for peptides table
    :param filter_options: Peptide filter options
    :param data: Dictionary
    :return: List of string
    """

    names = [
        'File',
        'raw_file_location',
        'Scan number',
        'Peak count',
        'Total ion current',
        'Retention time',
        'Exp. m/z',
        'Calc. m/z',
        'Mass error (ppm)',
        'Charge',
        'Rank',
        'Peptide sequence',
        'Modification',
        'Length',
        'Score',
        'q-value',
        'Is decoy',
        'One is decoy',
        'Protein count',
        'Protein accessions / positions',
        'Validated',
        'peptide_id',
        'identification_id',
        'biological_sample_id',
        'biological_sample_identifier',
    ]

    genome_names = [
        'mapped',
        'gene_symbol',
        'gene_identifier',
        'transcript_identifier',
        'protein_identifier',
        'protein_position',
        'chromosome',
        'strand',
        'locus',
        'exon_positions',
        'exon_sequences',
        'sap',
        'peptide_variants',
        'snp_positions',
        'ref_peptide',
        'is_ref_peptide'
    ]

    if filter_options.get('protein_description'):
        names.insert(19, 'Protein description')

    # Add extra headers
    if data.get('mhc_predictor', None) is not None:

        mhc_predictor_name = MHC_PREDICTOR_NAMES[filter_options['mhc_predictor']]

        for allele in list(data['mhc_predictor'].columns.values):
            key = f'{mhc_predictor_name} {allele[0]}: {allele[1]}'
            names.append(key)

    if data.get('deconvolution', None) is not None:

        if filter_options['deconvolution_software'].startswith('MoDec'):

            columns = ['resp_1st', 'motif_1st', 'resp_2nd', 'motif_2nd']
            for column in columns:
                column = f"{filter_options['deconvolution_software']}_" \
                         f"{column}"
                names.append(column)
        else:
            columns = range(1, filter_options['deconvolution_cluster_number'] + 1)
            columns = list(columns) + ['trash', 'max']

            for i in columns:
                column = f"{filter_options['deconvolution_software']}_" \
                         f"responsibility_cluster_{i}"
                names.append(column)

            column = f"{filter_options['deconvolution_software']}_max_cluster"
            names.append(column)

    if data.get('genome'):
        names.extend(genome_names)

    names.append('MS/MS url')  # Must be last column for template

    return names


def csv(filter_options: dict):
    """
    Creates CSV of identification
    :param filter_options: Peptide filter options
    :return: None
    """

    data = dict()
    storage = get_storage()

    try:

        # Retrieve peptides
        peptide_filters = PeptidesFilter(filter_options, data=data)
        peptides = peptide_filters.apply()

        # Prepare csv header
        header_columns = '"'
        header_columns += '","'.join(column_names(filter_options, data=data))
        header_columns += '"'
        db_set = [model_to_dict(db) for db in
                  protein_database.from_all(filter_options)]

        db_str = protein_database.formatted_str(db_set)

        filter_ = json.loads(filter_options['filter_json'])

        if 'mhc_allele' in filter_:

            alleles = list()

            for allele_id in filter_['mhc_allele']:
                allele = MHCAllele.objects.get(pk=allele_id)
                alleles.append(allele.name)

            filter_['mhc_allele'] = alleles

        filter_json = json.dumps(filter_)
        header = f'{filter_json}\n{db_str}\n{header_columns}\n'

        # Write to CSV file
        with storage.open('csv_' + filter_options['identification_key'],
                          'wb') as file:

            with gzip.open(file, 'wb') as gz_file:
                gz_file.write(header.encode())

                formatted_peptides = format_peptides(peptides,
                                                     filter_options,
                                                     data=data)
                peptide_count = 0

                for peptide in formatted_peptides:

                    gz_file.write(peptide.encode())
                    peptide_count += 1

        cache_obj = {
            'columns': column_names(filter_options, data=data),
            'csv_file_name': file_name(filter_options),
            'db': db_set,
            'netmhc_threshold': peptide_filters.values.get('netmhc_threshold'),
            'peptide_count': peptide_count,
            'score_threshold': peptide_filters.values.get('score_threshold'),
            'status': 'finished',
            'validation_timestamp': filter_options['validation_timestamp'],
        }

        with storage.open(filter_options['identification_key'], 'w') as file:

            file.write(json.dumps(cache_obj))

    except BaseSiteException as e:

        cache_obj = {
            'status': 'failed',
            'error': e.message,
            'validation_timestamp': 0
        }

        with storage.open(filter_options['identification_key'], 'w') as file:

            file.write(json.dumps(cache_obj))


def file_name(filter_options: dict) -> str:
        """
        Prepares CSV file name with time and identifiers
        :param filter_options: Peptide filter options
        :return: File name
        """

        name = 'MAPDP_identifications_'
        name += filter_options['timestamp']
        name += '_IRIC_codes_' + '_'.join(
            list(get_biological_sample_identifiers(filter_options)))
        name += '.csv'

        return name


def format_peptides(peptides, filter_options: dict, data: dict=dict()):
    """
    Generates peptide strings for CSV or table view.
    :param peptides: Peptide instances
    :param filter_options: Peptide filter options
    :param data: Dictionary
    :return: Yield peptide line
    """

    string_format = '"%s","%s","%s","%s","%.2E","%.2f","%s","%s","%.2f","%s",' \
                    '"%s","%s","%s","%s","%s","%.1E","%s","%s","%s",' \
                    '"%s","%s","%s","%s","%s","%s"'

    if filter_options.get('protein_description'):
        string_format += ',"%s"'

    url = filter_options['url']
    url_parts = url.split(sep='999')

    url_format = ',"' + url_parts[0] + '%s' + url_parts[1] + '"'

    mhc_pred_df = data.get('mhc_predictor', None)
    mhc_pred_bool = mhc_pred_df is not None
    mhc_pred_empty = list()

    if mhc_pred_bool:

        for i in range(0, mhc_pred_df.shape[1], 1):
            string_format += ',"%s"'
            mhc_pred_empty.append('')

    deconvolution_df = data.get('deconvolution', None)
    deconvolution_bool = deconvolution_df is not None
    deconvolution_empty = list()

    if deconvolution_bool:

        for i in range(0, deconvolution_df.shape[1], 1):
            string_format += ',"%s"'
            deconvolution_empty.append('')

    if data.get('genome'):
        for i in range(0, 16):
            string_format += ',"%s"'

    string_format += url_format
    string_format += '\n'

    for peptide in peptides:

        if peptide.q_value is None:
            peptide.q_value = 1.0

        values = [peptide.scan_file,
                  get_raw_file_path(peptide.identification.biological_sample,
                                    peptide.scan_file),
                  peptide.scan_number,
                  peptide.peak_count,
                  peptide.total_ion_current,
                  peptide.retention_time / 60.0,
                  peptide.experimental_mz,
                  peptide.calculated_mz,
                  peptide.error_ppm,
                  peptide.charge_state,
                  peptide.rank,
                  peptide.peptide_sequence,
                  peptide.modification_formatted,
                  peptide.peptide_length,
                  peptide.peptide_score,
                  peptide.q_value,
                  peptide.is_decoy,
                  peptide.one_is_decoy,
                  peptide.protein_count,
                  peptide.protein_accessions_positions,
                  peptide.validated,
                  peptide.pk,
                  peptide.identification_id,
                  peptide.identification.biological_sample_id,
                  peptide.identification.biological_sample.identifier,
                  ]

        if filter_options.get('protein_description'):
            values.insert(19, get_protein_description(peptide))

        if mhc_pred_bool:
            try:
                values.extend(mhc_pred_df.loc[peptide.pk])

            except KeyError:
                values.extend(mhc_pred_empty)

        if deconvolution_bool:
            try:
                values.extend(deconvolution_df.loc[peptide.peptide_sequence])

            except KeyError:
                values.extend(deconvolution_empty)

        if data.get('genome'):

            for genome in peptide.unique_peptide.genome:
                genome_values = [
                    genome.mapped,
                    genome.gene_symbol,
                    genome.gene_identifier,
                    genome.transcript_identifier,
                    genome.protein_identifier,
                    genome.protein_position,
                    genome.chromosome,
                    genome.strand,
                    genome.locus,
                    genome.exon_positions,
                    genome.exon_sequences,
                    genome.sap,
                    genome.peptide_variants,
                    genome.snp_positions,
                    genome.ref_peptide,
                    genome.is_ref_peptide
                ]

                tmp = values + genome_values + [peptide.pk]

                yield string_format % tuple(tmp)

        else:
            values.append(peptide.pk)
            yield string_format % tuple(values)


def get_biological_sample_identifiers(filter_options: dict) -> set:
        """
        Prepares a list of BiologicalSample identifiers
        :param filter_options: Peptide filter options
        :return: Set of BiologicalSample identifiers
        """

        identifiers = set()

        identifications = Identification.objects.filter(
            pk__in=filter_options['identification'])
        identifications = identifications.select_related('biological_sample')

        immunopeptidomes = ImmunoPeptidome.objects.filter(
            pk__in=filter_options['immunopeptidome'])
        immunopeptidomes = immunopeptidomes.prefetch_related('biological_sample')

        # Associate biological samples
        for identification in identifications:
            identifiers.add(identification.biological_sample.identifier)

        for immunopeptidome in immunopeptidomes:

            for biological_sample in immunopeptidome.biological_sample.all():
                identifiers.add(biological_sample.identifier)

        return identifiers


@functools.lru_cache(maxsize=128)
def get_protein_description(peptide) -> str:
    """
    Get protein descriptions for a peptide
    :param peptide: Peptide instance
    :return: Protein description
    """

    descriptions = list()

    for protein in peptide.protein_matches.all():

        descriptions.append(str(protein.short_description))

    return '; '.join(descriptions)


@functools.lru_cache(maxsize=128)
def get_raw_file_path(biological_sample, scan_file: str):
    """
    Gets raw file path from MGF file
    :param biological_sample: Biological sample instance
    :param scan_file: MGF scan file name
    :return:
    """

    path = ''

    scan_file = scan_file.replace('.mgf', '.raw')

    try:

        file = File.objects.filter(
            file_set__in=biological_sample.biologicalsamplefileset_set.filter(
                trashed=False),
            name=scan_file)[0]

        path = re.sub(r'^.+-=Proteomics_Raw_Data=-[\\/]', '', file.path)
        path = re.sub(r'^/mnt/ms/', '', file.path)

    except IndexError:
        pass

    return path


