"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from collections import defaultdict
from functools import partial

# Import Django related libraries
from django.db.models import Q

# Third party libraries

# Import project libraries
from base_site.exceptions import BaseSiteException
from library.fdr import FDR
from ..models import Peptide, PeptideGenome, UniquePeptide
from . import mhc_deconvolution
from . import mhc_prediction


class PeptidesFilter:
    def __init__(self, filter_options: dict, data: dict = None):
        """
        Filters initialization
        :param filter_options: Peptide filter options
        :param data: Extra data for filter
        """

        self.global_filters = list()
        self.peptide_filters = list()

        self.data = data

        self.filter_options = filter_options

        self.peptides = self.filtered_queryset(filter_options)
        self.retained_peptides = dict()

        self.peptide_key = 'pk'
        self.values = dict()

        self.set(filter_options)

    def apply(self):
        """
        Applies filters on selected Peptide instances.
        :return: Peptides (queryset or list)
        """

        # Add extra data for filter
        if self.filter_options.get('mhc_predictor'):
            self.data['mhc_predictor'] = mhc_prediction.get_predictions(
                self.peptides, self.filter_options)

        if self.filter_options.get('deconvolution_software') and \
                self.filter_options.get('deconvolution_before_fdr'):
            self.data['deconvolution'] = mhc_deconvolution.deconvolution(
                self.peptides, self.filter_options)

        if len(self.peptide_filters) + len(self.global_filters):

            for peptide in self.peptides:

                for filter_ in self.peptide_filters:

                    if filter_(peptide) is False:
                        break

                else:
                    key = getattr(peptide, self.peptide_key)
                    self.retained_peptides[key] = peptide

            for filter_ in self.global_filters:
                filter_()

            self.peptides = list(self.retained_peptides.values())

        if self.filter_options.get('deconvolution_software') and \
                self.filter_options.get('deconvolution_before_fdr') is False:
            self.data['deconvolution'] = mhc_deconvolution.deconvolution(
                self.peptides, self.filter_options)

            self.peptides = [peptide for peptide in self.peptides
                             if self.single_mhc_deconvolution(peptide)]

        self.warning()

        return self.peptides

    @staticmethod
    def filtered_queryset(filter_options: dict):
        """
        Generates Peptide queryset based on filter.
        :param filter_options: Peptide filter options
        :return: Peptide queryset
        """

        peptides = Peptide.objects.none()

        # Select peptides from Identification of Immunopeptidomes instances
        if filter_options.get('identification'):
            peptides = Peptide.objects.filter(
                identification_id__in=filter_options['identification'])

        elif filter_options.get('immunopeptidome'):
            t = Peptide.immunopeptidome_set.through
            tmp = t.objects.filter(
                immunopeptidome_id__in=filter_options['immunopeptidome'])
            peptides = Peptide.objects.filter(
                id__in=tmp.values('peptide_id'))

            if 'identification' in filter_options:
                peptides = Peptide.objects.filter(
                    Q(id__in=tmp.values('peptide_id')) |
                    Q(identification_id__in=filter_options['identification']))

        peptides = peptides.prefetch_related(
            'identification__biological_sample')

        if filter_options.get('protein_description'):
            peptides = peptides.prefetch_related('protein_matches')

        # Restrict Peptide queryset with filter values
        for key, value in filter_options.items():

            if value and '__' in key:
                peptides = peptides.filter(**{key: value})

                if key == 'error_ppm__lte':
                    peptides = peptides.filter(**{'error_ppm__gte': -value})

        if filter_options.get('remove_decoy') is True and bool(
                filter_options.get('score_mhc_fdr')) == False and bool(
            filter_options.get('score_fdr')) == False:
            peptides = peptides.filter(**{'is_decoy__exact': False})

        validated__in = filter_options.pop('validated__in', False)
        if validated__in:

            if 'None' in validated__in:
                validated__in.remove('None')
                peptides = peptides.filter(
                    Q(validated__isnull=True) | Q(
                        validated__in=validated__in))
            else:
                peptides = peptides.filter(validated__in=validated__in)

        if 'files' in filter_options:

            files = filter_options['files'].split(',')

            if filter_options['files_action'] == 'exclude':

                for file in files:
                    peptides = peptides.exclude(scan_file__contains=file)

            elif filter_options['files_action'] == 'filter':

                for file in files:
                    peptides = peptides.filter(scan_file__contains=file)

        if 'pp_id' in filter_options:
            peptide_genome = PeptideGenome.objects.filter(
                pp_id=filter_options['pp_id'])
            unique_peptide = UniquePeptide.objects.filter(
                peptidegenome__in=peptide_genome)
            peptides = peptides.exclude(unique_peptide__in=unique_peptide)

        return peptides

    def global_score_fdr(self, fdr_threshold, remove_decoy):
        """
        Filters peptides to the specified FDR based on peptide score.

        :param fdr_threshold: Float in [0, 1] range.
        :param remove_decoy: Boolean
        :return: None but modifies self.context['score_threshold'] and
        self.retained_peptides
        """

        tp_fp_scores = defaultdict(list)

        for peptide in self.retained_peptides.values():
            tp_fp_scores[peptide.is_decoy].append(peptide.peptide_score)

        nb_target, score_threshold = FDR.fdr_score(tp_fp_scores[False],
                                                   tp_fp_scores[True],
                                                   fdr_threshold)

        self.values['score_threshold'] = score_threshold

        if nb_target is not None:

            for key in list(self.retained_peptides.keys()):

                peptide = self.retained_peptides[key]

                if peptide.peptide_score < score_threshold:
                    del self.retained_peptides[key]
                    continue

                if remove_decoy and peptide.is_decoy:
                    del self.retained_peptides[key]

    def global_score_mhc_fdr(self, allele_name, fdr_threshold, remove_decoy):
        """
        Filters peptides to the specified FDR based on peptide score and MHC
        affinity.
        :param allele_name: String
        :param fdr_threshold: Float in [0, 1] range.
        :param remove_decoy: Boolean
        :return: None but modifies self.context['score_threshold'],
        self.context['mhc_threshold'], self.retained_peptides.
        """

        tp_data = {
            'peptide_score': [],
            'mhc_binding': [],
        }
        fp_data = {
            'peptide_score': [],
            'mhc_binding': [],
        }

        for key in list(self.retained_peptides.keys()):

            peptide = self.retained_peptides[key]

            affinity = mhc_prediction.get_affinity(self.data['mhc_predictor'],
                                                   peptide.pk, allele_name)

            if affinity is not None:

                if peptide.is_decoy:

                    fp_data['peptide_score'].append(
                        peptide.peptide_score)
                    fp_data['mhc_binding'].append(affinity)

                    if remove_decoy:
                        del self.retained_peptides[key]

                else:

                    tp_data['peptide_score'].append(
                        peptide.peptide_score)
                    tp_data['mhc_binding'].append(affinity)

            else:
                del self.retained_peptides[key]

        nb_target, score_threshold, mhc_threshold = FDR.fdr_score_mhc(
            tp_data, fp_data, fdr_threshold)

        self.values['score_threshold'] = score_threshold
        self.values['mhc_threshold'] = mhc_threshold

        if nb_target is not None:

            for key in list(self.retained_peptides.keys()):

                peptide = self.retained_peptides[key]

                if peptide.peptide_score < score_threshold:
                    del self.retained_peptides[key]
                    continue

                affinity = mhc_prediction.get_affinity(
                    self.data['mhc_predictor'],
                    peptide.pk, allele_name)

                if affinity > mhc_threshold:
                    del self.retained_peptides[key]

    def single_mhc_affinity(self, peptide):
        """
        Filters Peptide instance by their MHC affinity.
        :param peptide: Peptide instance
        :return: Boolean
        """

        return peptide.pk in self.data['mhc_predictor'].index

    def single_mhc_deconvolution(self, peptide) -> bool:
        """
        Filters Peptide instance by their MHC deconvolution.
        :param peptide: Peptide instance
        :return: Boolean
        """

        # if peptide.is_decoy:
        #     return True

        return peptide.peptide_sequence in self.data['deconvolution'].index

    def single_unique_sequence(self, peptide):
        """
        Keeps a single Peptide instance for each unique peptide sequence.
        :param peptide: Peptide instance
        :return: Boolean
        """

        previous_peptide = self.retained_peptides.get(peptide.unique_peptide_id,
                                                      False)

        return (not previous_peptide or
                peptide.peptide_score > previous_peptide.peptide_score)

    def set(self, filter_options: dict) -> None:
        """
        Checks which filters should be applied based on user selection.
        :param filter_options: Peptide filter option
        :return: None but changes peptide and global filter lists
        """

        if filter_options.get('unique_sequence'):
            self.peptide_key = 'unique_peptide_id'
            self.peptide_filters.append(self.single_unique_sequence)

        if (filter_options.get('affinity_threshold') or filter_options.get(
                'rank_threshold')) and filter_options.get('mhc_predictor'):
            self.peptide_filters.append(self.single_mhc_affinity)

        if (filter_options.get('deconvolution_remove_trash') or
            filter_options.get('deconvolution_max_trash_responsibility')) and \
                filter_options.get('deconvolution_software') and \
                filter_options.get('deconvolution_before_fdr'):
            self.peptide_filters.append(self.single_mhc_deconvolution)

        if filter_options.get('score_fdr') is not None:
            filter_ = partial(self.global_score_fdr,
                              filter_options['score_fdr'],
                              filter_options.get('remove_decoy', False))

            self.global_filters.append(filter_)

        if filter_options.get('score_mhc_fdr') is not None:
            filter_ = partial(self.global_score_mhc_fdr, 'Minimum',
                              filter_options['score_mhc_fdr'],
                              filter_options.get('remove_decoy', False))

            self.global_filters.append(filter_)

    def warning(self):
        """
        Checks for issues and warns user.
        :return: Raises BaseSiteException
        """

        if 'score_threshold' in self.values:

            msg = None

            if self.values['score_threshold'] is None:
                msg = 'Can`t compute FDR calculation. No decoy hit.'

            if str(self.values['score_threshold']).startswith('Min FDR'):
                msg = 'Computed FDRs are higher than the threshold. '
                msg += self.values['score_threshold']

            if msg:
                raise BaseSiteException('warning', msg)
