"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404

# Third party libraries
from guardian.shortcuts import (assign_perm,
                                get_perms,
                                remove_perm)


# Import project libraries


def check_content_permission(request, action: str, obj) -> None:
    """
    Checks that user has requested permission and annotates obj with
    permissions.

    :param request: HttpRequest
    :param action: Permission name 'view', 'change', ...
    :param obj: Model object instance.
    :return: None
    """

    permissions = get_perms(request.user, obj)
    has_permission = False

    for permission in permissions:

        name, content_type = permission.split('_')
        setattr(obj, name + '_permission', True)

        if name == action:
            has_permission = True

    setattr(obj, 'content_type', obj.__class__.__name__.lower())

    if not has_permission:
        raise PermissionDenied


def check_set_class_permission(request, file_set):
    """
    Check that user is member of certain group to access
    file system folder.
    :param request: HttpRequest
    :param file_set: FileSet model object
    :return: None
    """

    groups = file_set.get_class_attribute('allowed_groups')

    if not request.user.groups.filter(name__in=groups).exists():
        raise PermissionDenied


def get_obj_by_type(request, content_type, pk, permission,
                    app_label='mhc_pipeline'):
    """
    Gets object by content type and checks permission in the process.
    :param request: HttpRequest
    :param content_type: String content type of model object.
    :param pk: Primary key of model object.
    :param permission: String of permission (change, delete, change)
    :param app_label: String of app
    :return: Model object instance.
    """

    content_type = ContentType.objects.get(app_label=app_label,
                                           model=content_type)

    model_class = content_type.model_class()

    if permission is not None:
        obj = get_if_permission(request, model_class, permission, pk=pk)

    else:
        obj = get_object_or_404(model_class, pk=pk)

    return obj


def get_if_permission(request, klass, permission, **kwargs):
    """
    Get instance of object if user has permission.
    :param request: HttpRequest
    :param klass: A Model class, a Manager, or a QuerySet instance from which to
    get the object.
    :param permission: String of permission (change, delete, change)
    :param kwargs: Lookup parameters, which should be in the format accepted by
    get() and filter().
    :return: Object instance
    """

    instance = get_object_or_404(klass, **kwargs)
    check_content_permission(request, permission, instance)

    return instance


def rm_content_permission(instance, user, permissions):
    """
    Removes content permissions for a specific user.
    :param instance: Content instance
    :param user: User instance
    :param permissions: Permissions list ('view', 'change', ...)
    :return: None
    """

    content_type = instance.__class__.__name__.lower()

    for permission in permissions:
        remove_perm('%s_%s' % (permission, content_type), user, instance)


def set_content_permission(instance, user, permissions):
    """
    Sets content permissions for a specific user.
    :param instance: Content instance
    :param user: User instance
    :param permissions: Permissions list ('view', 'change', ...)
    :return: None
    """

    content_type = instance.__class__.__name__.lower()

    for permission in permissions:
        assign_perm('%s_%s' % (permission, content_type), user, instance)
