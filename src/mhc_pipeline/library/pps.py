"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import logging

# Import Django related libraries
from django.conf import settings

# Third party libraries
import requests

# Import project libraries
from base_site.exceptions import BaseSiteException


def alive() -> bool:
    """
    Checks if personalized proteome service is accessible
    :return: Boolean
    """

    try:
        response = pps_request('get', '/alive/')

        return response.status_code == 204

    except BaseSiteException:

        return False


def check_status_code(response):
    """
    Checks status code returned by PPS
    :param response: requests.models.Response
    :return: None or raise BaseSiteException
    """

    status_code = response.status_code

    if status_code in [200, 201, 204]:
        return None

    elif status_code in [403, 502, 504]:
        service_unavailable()

    else:
        if status_code == 400:
            message = 'Personalized proteome service returned the following ' \
                      f'error: {response.json()}'

        elif status_code == 404:
            message = 'Personalized proteome service returned the following ' \
                      'error: resource not found.'

        else:
            message = 'Personalized proteome service returned an unexpected ' \
                      'status code.'

            logger = logging.getLogger('django')
            logger.error(message)

        message += f'\nurl: {response.url}\nstatus_code: {status_code}'

        raise BaseSiteException('error', message)


def get_personalized_proteome_for_form(pp_ids: list) -> list:
    """
    Gets personalized proteome entries
    :param pp_ids: List of personalized proteome primary key
    :return: Personalized proteome
    """

    proteomes = list()

    if len(pp_ids):

        pp_ids = ','.join([str(id_) for id_ in pp_ids])
        response = pps_request('get', f'/personalized_proteomes/?ids={pp_ids}')

        for proteome in response.json():

            proteomes.append(
                (proteome['url'],
                 f'Id: {proteome["id"]} - {proteome["snps_name"]}'))

    return proteomes


def get_reference_genome_for_form() -> list:
    """
    Gets reference genomes
    :return: List of genome
    """

    response = pps_request('get', '/genomes/?trashed=false')

    genomes = [(0, 'Please select')]

    for genome in response.json():
        genomes.append((genome['url'],
                        f'Id: {genome["id"]} - {genome["name"]} ({genome["species"]})'))

    return genomes


def get_snps_for_form(snps_ids: list) -> list:
    """
    Gets snps from PPS
    :param snps_ids: SNPs primary key list
    :return: List of SNPs
    """

    snps_ids = ','.join([str(id_) for id_ in snps_ids])
    pps_snps = pps_request('get', f'/snps/?ids={snps_ids}').json()

    snps_entries = [(0, 'Please select')]

    for snps in pps_snps:

        if snps['import_status'] == 'finished':
            snps_entries.append((snps['url'],
                                 f'Id: {snps["id"]} - {snps["name"]}'
                                 f'[{snps["genome_name"]}]'))

    return snps_entries


def pps_request(method, url_suffix, **kwargs):
    """
    Prepares HTTP request to PPS
    :param method: String (get, post, delete)
    :param url_suffix: String (e.g. /genomes/)
    :param kwargs: Parameters
    :return: requests.models.Response
    """

    methods = {'delete': requests.delete,
               'get': requests.get,
               'patch': requests.patch,
               'post': requests.post}

    try:
        kwargs['headers'] = {'Authorization': f'Token {settings.PPS_TOKEN}'}

        if url_suffix.startswith('http'):
            url = url_suffix
        else:
            url = settings.PPS_HOST + url_suffix

        response = methods[method](url, **kwargs)

        check_status_code(response)

        return response

    except (requests.exceptions.ChunkedEncodingError,
            requests.exceptions.ConnectionError,
            requests.exceptions.ConnectTimeout):

        service_unavailable()


def service_unavailable():
    """
    Logs unavailable service and raises exception to notice user.
    :return: Raise BaseSiteException
    """

    message = 'Personalized proteome service is currently unavailable.' \
              'Please try again later or notice your system administrator.'

    raise BaseSiteException('warning', message)
