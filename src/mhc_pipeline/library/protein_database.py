"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries

# Third party libraries

# Import project libraries
from ..models import (Identification, ImmunoPeptidome)


def from_all(fc):
    """
    Gets protein databases associated to identifications and
    immunopeptidomes.
    :param fc: Form cleaned data
    :return: Set of ProteinDatabase instances
    """

    db_set = set()

    identifications = Identification.objects.filter(pk__in=fc['identification'])
    identifications = identifications.prefetch_related('protein_database')
    immunopeptidomes = ImmunoPeptidome.objects.filter(
        pk__in=fc['immunopeptidome'])

    for identification in identifications:
        for db in identification.protein_database.all():
            db_set.add(db)

    for immunopeptidome in immunopeptidomes:
        dbs = from_immunopeptidome(immunopeptidome)
        db_set = db_set.union(dbs)

    return db_set


def from_immunopeptidome(immunopeptidome):
    """
    Gets protein databases associated to immunopeptidomes.
    :param immunopeptidome: Immunopeptidome instance
    :return: Set of ProteinDatabase instances
    """

    db_set = set()

    for identification in immunopeptidome.identification.all():
        for db in identification.protein_database.all():
            db_set.add(db)

    for immunopeptidome_child in immunopeptidome.immunopeptidome.all():
        dbs = from_immunopeptidome(immunopeptidome_child)
        db_set = db_set.union(dbs)

    return db_set


def formatted_str(db_set):
    """
    Generates string that represent source protein sequence databases
    of peptides.
    :param db_set:  Set of ProteinDatabase instances
    :return: String
    """

    db_str = 'Protein databases: '

    for db in db_set:
        location = db["location"].replace('file:/', '')
        db_str += f'Id:{db["id"]} - {db["name"]} ({location})| '

    return db_str
