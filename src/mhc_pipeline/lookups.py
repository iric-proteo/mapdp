"""
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries
from django.core.exceptions import PermissionDenied
from django.db.models import Q

# Third party libraries
from ajax_select import register, LookupChannel

# Import project libraries
from .models import (Identification,
                     ImmunoPeptidome,
                     MHCAllele)


class LookupMixin:

    def check_auth(self, request) -> None:
        """
        Anyone authenticated can see this resource. get_query is filtering data
        based on the user.
        """
        if not request.user.is_authenticated:
            raise PermissionDenied


@register('identifications')
class IdentificationLookup(LookupMixin, LookupChannel):

    model = Identification

    def get_query(self, q: str, request):
        """
        Gets Identification queryset for a user and lookup keyword.
        :param q: Query string
        :param request: HTTP request object
        :return: Queryset
        """

        identification = Identification.objects.for_user(request.user)

        identification = identification.select_related('file_set',
                                                       'biological_sample')

        q_expression = Q(file_set__name__icontains=q) | Q(pk__icontains=q)
        q_expression = q_expression | Q(biological_sample__identifier__icontains=q)

        return identification.filter(q_expression)[:20]

    def format_item_display(self, item: Identification) -> str:
        """
        Formats label for display
        :param item: Identification instance
        :return: Formatted label
        """
        return item.alternate_label()

    def format_match(self, item: Identification) -> str:
        """
        Formats label for search matches
        :param item: Identification instance
        :return: Formatted label
        """
        return item.alternate_label()


@register('immunopeptidomes')
class ImmunopeptidomeLookup(LookupMixin, LookupChannel):

    model = ImmunoPeptidome

    def get_query(self, q: str, request):
        """
        Gets Immunopeptidome queryset for a user and lookup keyword.
        :param q: Query string
        :param request: HTTP request object
        :return: Queryset
        """

        q_expression = Q(name__icontains=q) | Q(pk__icontains=q)
        immunopeptidomes = ImmunoPeptidome.objects.for_user(request.user,
                                                            filter_=q_expression,
                                                            reverse_sorted=True)

        return immunopeptidomes[:20]


@register('mhcalleles')
class MHCAlleleLookup(LookupMixin, LookupChannel):

    model = MHCAllele

    def get_query(self, q: str, request):
        """
        Gets MHCAllele queryset with a lookup keyword.
        :param q: Query string
        :param request: HTTP request object
        :return: Queryset
        """
        return self.model.objects.filter(name__icontains=q)[:20]


