"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import json

# Import Django related libraries
from django.core.management.base import BaseCommand

# Import third party library

# Import project libraries
from mhc_pipeline.models import Filter, MHCAllele


class Command(BaseCommand):

    help = 'Clean filters'

    def handle(self, *args, **options):

        self.stdout.write('Cleaning filters...')

        filters = Filter.objects.all()

        for filter in filters:

            values = json.loads(filter.json_values)
            print('before', values)

            # Remove fields
            for field in ['ref_url', 'url', 'timestamp']:
                values.pop(field, None)

            values = fix_mhc_allele(values)
            values = fix_netmhc(values)

            print('after', values)
            filter.json_values = json.dumps(values)
            filter.save()

        self.stdout.write('Completed.')


def fix_netmhc(values):

    if 'netmhc' in values:

        names_fix = {
            'NetMHC 3.4': 'NETMHC34',
            'NetMHC 4.0': 'NETMHC4',
            'NetMHCcons 1.1': 'NETMHCCONS11'
        }

        if values['netmhc'] in names_fix:
            values['netmhc'] = names_fix[values['netmhc']]

    return values


def fix_mhc_allele(values):

    if 'mhc_allele' in values and type(values['mhc_allele']) == list:

        mhc_allele = list()

        for value in values['mhc_allele']:

            if type(value) is str and value.startswith('H'):

                allele = MHCAllele.objects.get(name=value)
                value = str(allele.pk)

            mhc_allele.append(str(value))

        values['mhc_allele'] = '|' + '|'.join(mhc_allele) + '|'

    return values
