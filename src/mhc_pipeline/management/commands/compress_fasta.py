"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import os
from pathlib import Path
import zipfile

# Import Django related libraries
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Q

# Import third party library

# Import project libraries
from mhc_pipeline.models import (File,
                                 UploadedFile)


class Command(BaseCommand):

    help = 'Compress uploaded FASTA files'

    def handle(self, *args, **options):

        self.stdout.write('Compressing uploaded FASTA file...')

        files = File.objects.filter(Q(extension='.fa') | Q(extension='.fasta'))

        fasta_root = Path(settings.MEDIA_ROOT) / 'Fasta'

        for file_ in files:

            if (fasta_root / file_.name).exists():

                zip_file_name = file_.name + '.zip'

                with zipfile.ZipFile(fasta_root / zip_file_name, 'w',
                                     zipfile.ZIP_DEFLATED) as zipf:
                    zipf.write(str(fasta_root / file_.name), arcname=file_.name)

                file_stat = os.stat(fasta_root / zip_file_name)
                file_.extension = '.zip'
                file_.name = file_.name + '.zip'
                file_.size = file_stat.st_size
                file_.save()
                print('file', file_.pk, file_.name)

        uploaded_files = UploadedFile.objects.filter(class_name='Fasta')

        for file_ in uploaded_files:

            if not file_.name.endswith('zip'):
                file_.name = file_.name + '.zip'
                file_.file = file_.file.name + '.zip'
                file_.save()
                print('uploadedfile', file_.pk, file_.name)

        self.stdout.write('Task completed')


