""""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand

# Import project libraries
from mhc_pipeline.library.mhc_allele import store_mhc_alleles
from mhc_pipeline.models import Annotation, MHCAllele, Tag


class Command(BaseCommand):
    help = 'Setup for MHC pipeline application'

    def handle(self, *args, **options):
        site = Site.objects.all()[0]

        if site.name != 'MAPDP':
            self.stdout.write('Setting up MHC pipeline application')

            site.domain = settings.ALLOWED_HOSTS[0]
            site.name = 'MAPDP'
            site.save()

            user = User.objects.create_superuser('mapdp-admin',
                                                 'change@me.com',
                                                 'change_me_')

            ms_group = Group.objects.create(name='ms')
            ms_group.user_set.add(user)
            mhc_group = Group.objects.create(name='mhc')
            mhc_group.user_set.add(user)
            mhc_group = Group.objects.create(name='validation')
            mhc_group.user_set.add(user)
            mhc_group = Group.objects.create(name='upload')
            mhc_group.user_set.add(user)

            Tag.objects.create(name='MiHA')
            Tag.objects.create(name='TSA')
            Tag.objects.create(name='Published')

            self.stdout.write('Completed.')

            self.stdout.write('Storing MHC alleles to database...')

            mhc_classes = ['A', 'B', 'C', 'DRA', 'DRB1', 'DRB3', 'DRB4', 'DPA1',
                           'DPB1', 'DQA1', 'DQB1']

            store_mhc_alleles(mhc_classes=mhc_classes)

            # Create mouse alleles
            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='IA',
                                     name='H-2-IAd')

            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='IA',
                                     name='H-2-IAb')

            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='L',
                                     name='H-2-Ld')

            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='K',
                                     name='H-2-Kk')

            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='K',
                                     name='H-2-Kd')

            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='K',
                                     name='H-2-Kb')

            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='D',
                                     name='H-2-Dd')

            MHCAllele.objects.create(mhc_class='H-2',
                                     gene='D',
                                     name='H-2-Db')

            self.stdout.write('Completed.')

            self.stdout.write('Storing annotation to database...')

            Annotation.objects.create(
                type='gnomad',
                sub_path='gnomAD',
                file_name='gnomad.exomes.r2.1.1.sites.vcf.bgz',
                display_name='gnomAD exomes r2.1.1',
                source_url='https://storage.googleapis.com/gnomad-public/release/2.1.1/vcf/exomes/gnomad.exomes.r2.1.1.sites.vcf.bgz',
            )

            Annotation.objects.create(
                type='dbsnp',
                sub_path='dbsnp/dbSNP_human_9606_b151_GRCh37p13',
                file_name='All_20180423.vcf.gz',
                display_name='dbSNP human b151 GRCh37p13',
                source_url='ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/human_9606_b151_GRCh37p13/VCF/All_20180423.vcf.gz',
            )

            Annotation.objects.create(
                type='dbsnp',
                sub_path='dbSNP/dbSNP_human_9606_b151_GRCh38p7',
                file_name='All_20180418.vcf.gz',
                display_name='dbSNP human b151 GRCh38p7',
                source_url='ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/human_9606_b151_GRCh38p7/VCF/All_20180418.vcf.gz',
            )

            Annotation.objects.create(
                type='iedb',
                sub_path='iedb',
                file_name='mhc_ligand_full.zip',
                display_name='IEDB mhc ligand',
                source_url='http://www.iedb.org/downloader.php?file_name=doc/mhc_ligand_full.zip',
            )

            Annotation.objects.create(
                type='hpa',
                sub_path='HPA',
                file_name='mcp.M113.035600-2.csv',
                display_name='Human protein atlas tissue RNA expression',
                source_url='http://www.mcponline.org/content/suppl/2013/12/05/M113.035600.DC1/mcp.M113.035600-2.xlsx',
            )

            self.stdout.write('Completed.')
