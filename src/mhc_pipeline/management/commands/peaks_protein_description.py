"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.core.management.base import BaseCommand

# Import project libraries
from base_site.exceptions import BaseSiteException
from library.peaks_parser import PEAKSMzIdentML
from mhc_pipeline.library.bulk_insert import BulkInsert
from mhc_pipeline.models import Identification, ProteinSequence
from mhc_pipeline.views.peaks import check_files, ProteinDatabaseManager

# TODO-MC add range processsing to speed things

class Command(BaseCommand):

    help = 'Retrieves search parameters for PEAKS search'

    def handle(self, *args, **options):

        self.stdout.write('Retrieving search parameters for PEAKS search...')

        for identification in Identification.objects.order_by('-id'):

            self.stdout.write(str(identification))

            db_proteins = self.get_proteins(identification)

            try:

                self.peaks_protein(identification, db_proteins)
            except BaseSiteException:
                pass

        self.stdout.write('Completed.')

    @staticmethod
    def get_proteins(identification) -> dict:
        """
        Gets protein sequence objects for the identification instance
        :param identification: Identification instance
        :return: Proteins dictionary (key: accession, value: ProteinSequence)
        """

        dbs = identification.protein_database.all()

        proteins = dict()

        for protein in ProteinSequence.objects.filter(protein_database__in=dbs):

            proteins[protein.accession] = protein

        return proteins

    @staticmethod
    def peaks_protein(identification, db_proteins):

        proteins_bulk = BulkInsert(ProteinSequence, ['protein_database_id',
                                                     'accession'])
        db_manager = ProteinDatabaseManager(identification, proteins_bulk)

        files = check_files(identification.file_set, check_mgf=False)

        for scan in PEAKSMzIdentML.read_scans(files['mz_id']):

            proteins = list(db_manager.process_proteins(
                scan.pop('protein_matches')))

            for protein in proteins:

                db_protein = db_proteins.get(protein['accession'])

                if db_protein is not None and \
                        db_protein.short_description is None \
                        and 'short_description' in protein:
                    db_protein.short_description = protein['short_description']
                    db_protein.save()
