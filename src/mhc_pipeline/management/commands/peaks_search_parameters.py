"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json

# Import Django related libraries
from django.core.management.base import BaseCommand

# Import project libraries
from base_site.exceptions import BaseSiteException
from mhc_pipeline.models import Identification
from mhc_pipeline.views.peaks import check_files, get_search_parameters


class Command(BaseCommand):

    help = 'Retrieves search parameters for PEAKS search'

    def handle(self, *args, **options):

        self.stdout.write('Retrieving search parameters for PEAKS search...')

        for identification in Identification.objects.all():

            self.stdout.write(str(identification))

            try:
                files = check_files(identification.file_set, check_mgf=False)
                search_parameters = get_search_parameters(files['mz_id'])

                self.stdout.write('before'),
                if identification.search_parameters is not None:
                    self.stdout.write(identification.search_parameters)
                identification.search_parameters = json.dumps(search_parameters)
                self.stdout.write('after')
                self.stdout.write(identification.search_parameters)
                identification.save()
            except BaseSiteException:
                pass

        self.stdout.write('Completed.')

