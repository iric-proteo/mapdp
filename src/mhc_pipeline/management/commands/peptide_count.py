"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""




# Import standard libraries
from pathlib import Path

# Import Django related libraries
from django.conf import settings
from django.core.management.base import BaseCommand

# Import third party library

# Import project libraries
from mhc_pipeline.models import PeptideCounts
from mhc_pipeline.library.peptide_count import process


class Command(BaseCommand):

    help = 'Retrieves peptide counts for each identification'

    def add_arguments(self, parser):

        parser.add_argument('-f', '--flush', action='store_true',
                            help='Flush database table')

    def handle(self, *args, **options):

        self.stdout.write('Retrieving peptide counts for each '
                          'identification...')

        if options['flush']:
            PeptideCounts.objects.all().delete()

            stats_path = Path(settings.MEDIA_ROOT) / 'stats'
            stats_file = stats_path / 'data.pickle'

            if stats_file.exists():
                stats_file.unlink()

        process(parallel=20)

        self.stdout.write('Task completed')


