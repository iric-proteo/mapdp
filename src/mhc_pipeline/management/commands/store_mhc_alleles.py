"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.core.management.base import BaseCommand

# Import project libraries
from mhc_pipeline.library.mhc_allele import store_mhc_alleles


class Command(BaseCommand):

    help = 'Store MHC alleles to database'

    def add_arguments(self, parser):

        parser.add_argument('alleles', type=str,
                            help='List of allele classes separated by comma')

    def handle(self, *args, **options):

        self.stdout.write('Storing MHC alleles to database...')

        mhc_classes = options['alleles'].split(',')

        store_mhc_alleles(mhc_classes=mhc_classes)

        self.stdout.write('Completed.')

