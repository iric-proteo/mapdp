# Generated by Django 2.2.5 on 2019-10-02 17:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mhc_pipeline', '0002_auto_20190919_1028'),
    ]

    operations = [
        migrations.AlterField(
            model_name='biologicalsample',
            name='identifier',
            field=models.CharField(help_text='Identifier labelled on the biological sample.', max_length=20, unique=True, verbose_name='Identifier'),
        ),
    ]
