"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from collections import namedtuple
from datetime import datetime
import json
import os
import re
from pathlib import Path
from pytz import timezone

# Import Django related libraries
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import caches
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.core.validators import (MaxValueValidator,
                                    MinValueValidator)
from django.db import models
from django.db.models.signals import (post_save,
                                      pre_save)
from django.dispatch import receiver
from django.utils.functional import cached_property

# Third party libraries
from chunked_upload.models import ChunkedUpload
from guardian.shortcuts import get_objects_for_user


# Import project libraries
from .library.permission import get_if_permission


# Helper methods
def model_path(instance, file_name):
    """
    Returns class name of the instance for file upload path
    :param instance: Model instance
    :param file_name: String
    :return: String
    """

    return os.path.join(instance.__class__.__name__, file_name)


STATUSES = (
    ('', ''),
    ('pending', 'Pending'),
    ('started', 'Started'),
    ('success', 'Success'),
    ('failed', 'Failed'),
)


class BaseModel(models.Model):
    """
    This class provides user/time details for object creation, modifications
    and deletion.
    """

    created_at = models.DateTimeField(auto_now_add=True)

    created_by = models.ForeignKey(User, on_delete=models.PROTECT,
                                   related_name='+')

    modified_at = models.DateTimeField(auto_now=True)

    modified_by = models.ForeignKey(User, on_delete=models.PROTECT,
                                    related_name='+')

    trashed = models.BooleanField(default=False)

    class Meta:
        abstract = True


class BiologicalSampleManager(models.Manager):
    """
    This manager class provides selection of instances with the 'view'
    permission.
    """

    def for_user(self, user, related=False):
        """
        Selects instances with the 'view' permission.
        :param user: User instance
        :param related: Query users that created or modified the instance.
        :return: Queryset
        """

        klass = self.get_queryset().filter(trashed=0)

        if related:
            klass = klass.select_related('created_by', 'modified_by')

        return get_objects_for_user(user, 'mhc_pipeline.view_biologicalsample',
                                    klass=klass)


class BiologicalSample(BaseModel):
    """
    This model describes the biological sample analyzed and stored.
    """

    identifier = models.CharField(help_text='Identifier labelled on the '
                                            'biological sample.',
                                  max_length=20,
                                  unique=True,
                                  verbose_name="Identifier")

    short_description = models.CharField(blank=True,
                                         max_length=100,
                                         null=True)

    TAXID_CHOICES = dict()
    TAXID_CHOICES[9606] = 'Homo sapiens'
    TAXID_CHOICES[10090] = 'Mus musculus'

    taxonomy_identifier = models.IntegerField(choices=TAXID_CHOICES.items(),
                                              default=9606,
                                              name='Species')

    cell_type = models.CharField(max_length=100,
                                 blank=True,
                                 null=True)

    tissue = models.CharField(max_length=100,
                              blank=True,
                              null=True)

    organ = models.CharField(max_length=100,
                             blank=True,
                             null=True)

    objects = BiologicalSampleManager()

    def __str__(self):
        """
        Object text representation
        :return: String
        """
        return 'Id: %s' % self.identifier

    def get_absolute_url(self):
        """
        Define a get_absolute_url() method to tell Django how to calculate the
        canonical URL for an object.
        :return: String
        """

        return reverse('biological_sample:view', kwargs={'pk': self.pk})


class FilterManager(models.Manager):
    """
    This manager class provides selection of instances with specific permission.
    """

    def for_user(self, user):
        """
        Selects instances with the 'view' permission.
        :param user: User instance
        :return: Queryset
        """

        return get_objects_for_user(user, 'mhc_pipeline.view_filter',
                                    klass=self.get_queryset().filter(trashed=0),
                                    # with_superuser=False
                                    )

    def visible_for_user(self, user):
        """
        Selects instances with the 'visible' permission.
        :param user: User instance
        :return: Queryset
        """

        return get_objects_for_user(user, 'mhc_pipeline.visible_filter',
                                    klass=self.get_queryset().filter(trashed=0),
                                    with_superuser=False
                                    )


class Filter(BaseModel):
    """
    This model stores a JSON representation of parameters used to filter
    identified peptides.
    """

    name = models.CharField(max_length=1000)

    json_values = models.TextField()

    objects = FilterManager()

    class Meta:
        permissions = (
            ('visible_filter', 'Can show/hide filter'),
        )

    def __str__(self):
        """
        Object text representation
        :return: String
        """

        return self.name

    def get_absolute_url(self):
        """
        Define a get_absolute_url() method to tell Django how to calculate the
        canonical URL for an object.

        This is now a true absolute url but a hack to redirect to the right view.

        :return: String
        """

        return reverse('filter:list')


class LockModel(models.Model):
    """
    This model is used to prevent concurrent writing to models while inserting
    new data sets.
    """

    model = models.CharField(max_length=100, unique=True)

    timestamp = models.DateTimeField(auto_now=True)


class MHCSample(models.Model):
    """
    This model contains specific details related to MHC samples.
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.PROTECT)

    group = models.CharField(blank=True,
                             max_length=50,
                             null=True)

    GENDER_CHOICES = (
        ('F', 'Female'),
        ('M', 'Male')
    )

    gender = models.CharField(blank=True,
                              choices=GENDER_CHOICES,
                              max_length=1,
                              null=True)


class FileSet(BaseModel):
    """
    This model defines file sets of with files specific type.
    """

    name = models.CharField(max_length=150)

    SET_CLASS_CHOICES = dict()
    # SET_CLASS_CHOICES[1] = None
    SET_CLASS_CHOICES['LC-MS'] = 2
    SET_CLASS_CHOICES['Fasta'] = 3
    SET_CLASS_CHOICES['PEAKS - mzIdentML 1.1'] = 4
    SET_CLASS_CHOICES['PEAKS - features'] = 5
    SET_CLASS_CHOICES['MaxQuant'] = 6
    SET_CLASS_CHOICES['Comet'] = 7
    SET_CLASS_CHOICES['BAM'] = 8

    choices = ((v, k) for k, v in SET_CLASS_CHOICES.items())
    set_class = models.IntegerField(choices=choices)

    locked = models.BooleanField(default=False)

    FILE_SET = dict()
    FILE_SET['LC-MS'] = {'dir': settings.MS_DIR,
                         'extensions': ('.raw',),
                         'allowed_groups': ('ms',)}

    comet_note = 'Please select one .mzid (mzIdenML 1.1) file and at least ' \
                 'one .mgf.'

    FILE_SET['Comet'] = {'dir': settings.MS_DIR,
                         'extensions': ('.mzid', '.mgf'),
                         'allowed_groups': ('ms',),
                         'note': comet_note}

    FILE_SET['Fasta'] = {'dir': os.path.join(settings.MEDIA_ROOT, 'Fasta'),
                         'extensions': ('.fa', '.fasta', '.zip'),
                         'allowed_groups': ('ms', 'upload')}
    FILE_SET['BAM'] = {'dir': os.path.join(settings.MEDIA_ROOT, 'BAM'),
                       'extensions': ('.bam',),
                       'allowed_groups': ('upload',)}
    peaks_note = 'Please select one .mzid (mzIdenML 1.1) file and at least ' \
                 'one .mgf generated by PEAKS for Scaffold. When exporting ' \
                 'from PEAKS, set peptides score, proteins score and ' \
                 'unique peptide to 0, check "Show Decoy Hits" in ' \
                 'Windows/Preferences/Display Options and select "Show all"' \
                 ' in Protein section.'

    FILE_SET['PEAKS - mzIdentML 1.1'] = {'dir': settings.MS_DIR,
                                         'extensions': ('.mzid', '.mgf'),
                                         'allowed_groups': ('ms',),
                                         'note': peaks_note}

    FILE_SET['PEAKS - features'] = {'dir': settings.MS_DIR,
                                    'extensions': ('.csv',),
                                    'allowed_groups': ('ms',),
                                    }

    mq_note = 'Please select the following files from the MaxQuant txt ' \
              'folder: evidence.txt, parameters.txt, peptides.txt'

    FILE_SET['MaxQuant'] = {'dir': settings.MS_DIR,
                            'extensions': ('.txt',),
                            'allowed_groups': ('ms',),
                            'note': mq_note,
                            }

    def __str__(self):
        """
        Object text representation
        :return: String
        """

        return f'pk:{self.pk}, name:{self.name}'

    def get_class_attribute(self, attribute):
        """
        Returns attributes for a specific file set class
        :param attribute: String
        :return: Various type
        """

        class_name = self.get_set_class_display()

        return self.FILE_SET[class_name].get(attribute, None)

    @cached_property
    def has_reference(self) -> bool:
        """
        Check if the file set is referenced and update lock
        :return: Boolean
        """

        value = (self.genotyping_set.filter(trashed=False).exists() or
                 self.identification_set.filter(trashed=False). exists())

        if value and self.locked is False:
            self.locked = True
            self.save()

        return value


class BiologicalSampleFileSet(FileSet):
    """
    This model defines file sets associated to a BiologicalSample instance.
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.PROTECT)


class File(models.Model):
    """
    This model describes file attributes.
    """

    file_set = models.ForeignKey(FileSet,
                                 on_delete=models.CASCADE)

    path = models.CharField(max_length=260)

    name = models.CharField(max_length=260)

    extension = models.CharField(max_length=10)

    size = models.BigIntegerField()

    date_time = models.DateTimeField()

    available = models.BooleanField()

    class Meta:
        ordering = ['name']

    @staticmethod
    def add_file(file_set_instance, path, file_name):
        """
        Reads file attributes and creates file instance.
        :param file_set_instance: FileSet instance
        :param path: String
        :param file_name: String
        :return: File instance
        """

        tz = timezone(settings.TIME_ZONE)

        filename, file_extension = os.path.splitext(file_name)

        file_stat = os.stat(os.path.join(path, file_name))

        file_dt = datetime.fromtimestamp(file_stat.st_mtime,
                                         tz=tz)

        return File.objects.create(file_set=file_set_instance,
                                   path=path,
                                   name=file_name,
                                   extension=file_extension,
                                   size=file_stat.st_size,
                                   date_time=file_dt,
                                   available=True,
                                   )

    def full_path(self):
        """
        Returns full path of the file.
        :return: String
        """

        return os.path.join(self.path, self.name)


class ProteinDatabase(models.Model):
    """
    This model stores details of protein sequence database.
    """

    name = models.CharField(max_length=1000)

    file_format = models.CharField(max_length=100)

    location = models.CharField(max_length=256)

    num_sequences = models.IntegerField()

    def __str__(self):
        """
        Object text representation
        :return: String
        """

        return f'Id:{self.pk} - {self.name}'


class Genotyping(BaseModel):
    """
    This model contains specific details related to MHC genotyping.
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.PROTECT)

    file_set = models.ForeignKey(FileSet, on_delete=models.PROTECT)

    update_alleles = models.BooleanField(default=False,
                                         help_text='Update biological sample '
                                                   'MHC alleles with '
                                                   'genotyping results.')

    software = models.CharField(max_length=100)

    state = models.CharField(choices=STATUSES,
                             default='',
                             max_length=10)

    message = models.TextField(blank=True, null=True)


class ProteinSequence(models.Model):
    """
    This model stores protein sequences.
    """

    protein_database = models.ForeignKey(ProteinDatabase,
                                         on_delete=models.CASCADE)

    accession = models.CharField(db_index=True, max_length=10000)

    short_description = models.CharField(blank=True,
                                         max_length=1000,
                                         null=True)

    sequence = models.TextField()

    length = models.IntegerField()

    class Meta:
        unique_together = ('protein_database', 'accession')


class IdentificationManager(models.Manager):
    """
    This manager class provides selection of instances with the 'view'
    permission.
    """

    def for_user(self, user):

        biological_samples = BiologicalSample.objects.for_user(user)

        identification = Identification.objects.filter(
            biological_sample__in=biological_samples, trashed=0)\

        identification = identification.exclude(state='failed')

        return identification


class Identification(BaseModel):
    """
    This model is used to group peptide identification (e.g. 'PEAKS search')
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.PROTECT)

    file_set = models.ForeignKey(FileSet, on_delete=models.PROTECT, null=True)

    software = models.CharField(max_length=100)

    protein_database = models.ManyToManyField(ProteinDatabase)

    search_parameters = models.TextField(blank=True, null=True)

    state = models.CharField(choices=STATUSES,
                             default='',
                             max_length=10)

    message = models.TextField(blank=True, null=True)

    objects = IdentificationManager()

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        """
        Object text representation
        :return: String
        """

        return 'Id: %s' % self.pk

    def alternate_label(self):
        """
        Alternate object text representation
        :return: String
        """

        return "Id: %s - BS Id: %s - %s" % (self.pk,
                                            self.biological_sample.identifier,
                                            self.file_set.name)


class UniquePeptide(models.Model):
    """
    This model stores the list of unique peptide sequences.
    """

    peptide_sequence = models.CharField(max_length=100, unique=True)

    peptide_length = models.PositiveSmallIntegerField()

    def __str__(self):
        """
        Object text representation
        :return: String
        """

        return 'pk:%s, sequence:%s' % (self.pk, self.peptide_sequence)


class Peptide(models.Model):
    """
    This models stores identified peptide details.
    """

    identification = models.ForeignKey(Identification,
                                       on_delete=models.CASCADE)

    scan_number = models.PositiveIntegerField()

    scan_file = models.CharField(max_length=256)

    scan_file_location = models.CharField(max_length=512)

    is_decoy = models.BooleanField()

    one_is_decoy = models.BooleanField()

    q_value = models.FloatField(blank=True, null=True)

    peptide_score = models.FloatField()

    rank = models.PositiveSmallIntegerField()

    rank_total = models.PositiveSmallIntegerField()

    charge_state = models.SmallIntegerField()

    calculated_mz = models.FloatField()

    experimental_mz = models.FloatField()

    error_ppm = models.FloatField()

    peptide_sequence = models.CharField(max_length=100)

    peptide_length = models.PositiveSmallIntegerField()

    modification = models.TextField(blank=True, null=True)

    protein_accessions_positions = models.TextField()

    protein_count = models.PositiveSmallIntegerField()

    protein_matches = models.ManyToManyField(ProteinSequence)

    unique_peptide = models.ForeignKey(UniquePeptide,
                                       models.PROTECT)

    retention_time = models.FloatField()

    scan_file_index = models.BigIntegerField()

    peak_count = models.IntegerField()

    total_ion_current = models.FloatField()

    validated = models.CharField(blank=True,
                                 max_length=1,
                                 null=True)

    validated_at = models.DateTimeField(blank=True, null=True)

    validated_by = models.ForeignKey(User, on_delete=models.PROTECT,
                                     blank=True,
                                     null=True,
                                     related_name='+',
                                     )

    @staticmethod
    def latest_validated_peptide_timestamp():

        validation_timestamp = caches['default'].get('validation_timestamp')

        if validation_timestamp is None:

            try:
                peptide = Peptide.objects.latest('validated_at')

                if peptide.validated_at is None:
                    validation_timestamp = 0

                else:
                    validation_timestamp = peptide.validated_at.timestamp()

            except ObjectDoesNotExist:
                validation_timestamp = 0

            caches['default'].set('validation_timestamp', validation_timestamp,
                                2678400)

        return validation_timestamp

    @property
    def modification_formatted(self):
        """
        Formats modifications JSON string
        :return: String
        """

        if self.modification == '""':
            return ''

        modifications = json.loads(self.modification)

        formatted_mod = list()

        for modification in modifications:
            residues = ''.join(modification.get('residues', ''))

            if 'name' not in modification \
                    and 'unknown modification' in modification:
                modification['name'] = modification['unknown modification']

            formatted_mod.append((f'{modification["name"]}-{residues}'
                                  f'{modification["location"]}'))

        return '; '.join(formatted_mod)

    def save(self, *args, **kwargs):
        """
        Creates a unique peptide instance before saving.
        :param args: Arguments
        :param kwargs: Keyword argument
        :return: None
        """

        obj, created = UniquePeptide.objects.get_or_create(
            peptide_sequence=self.peptide_sequence,
            peptide_length=self.peptide_length
        )
        self.unique_peptide = obj

        super().save(*args, **kwargs)


class New(BaseModel):
    """
    This model stores short message to be display as news on the welcome page.
    """

    short_message = models.CharField(max_length=1000)

    class Meta:
        ordering = ['-created_at']


class MHCAllele(models.Model):
    """
    This model stores MHC allele details.
    """

    mhc_class = models.CharField(max_length=10)

    gene = models.CharField(max_length=10)

    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        """
        Object text representation
        :return: String
        """

        return self.name


class BiologicalSampleMHCAllele(models.Model):
    """
    This model associate MHC alleles to BiologicalSample instance.
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.PROTECT)

    mhc_allele = models.ManyToManyField(MHCAllele, blank=True,
                                        help_text='(e.g. HLA-A*02:01, H-2-Db)')


class ExperimentDetails(models.Model):
    """
    This model stores experimental details.
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.PROTECT)

    name = models.CharField(max_length=500)

    elution_date = models.DateField(blank=True, null=True)

    cell_count = models.IntegerField(blank=True, null=True)

    pct_elution = models.FloatField(blank=True,
                                    null=True,
                                    validators=[MinValueValidator(0),
                                                MaxValueValidator(100)],
                                    verbose_name='% elution')

    pct_viability = models.FloatField(blank=True,
                                      null=True,
                                      validators=[MinValueValidator(0),
                                                  MaxValueValidator(100)],
                                      verbose_name='% viability', )

    facs_file = models.FileField(blank=True,
                                 null=True,
                                 upload_to=model_path)

    amount_injected = models.CharField(max_length=50,
                                       blank=True,
                                       null=True)

    fractionnated = models.NullBooleanField(blank=True, null=True)

    comments = models.TextField(blank=True, null=True)

    def get_absolute_url(self):
        """
        Define a get_absolute_url() method to tell Django how to calculate the
        canonical URL for an object.
        :return: String
        """

        return reverse('biological_sample:experiment_details_edit',
                       kwargs={'pk': self.biological_sample_id,
                               'details_pk': self.pk})


class Tag(models.Model):
    """
    This model stores tag name for immunopeptidome
    """

    name = models.CharField(max_length=50)

    def __str__(self):
        """
        Object string representation
        :return: String
        """

        return self.name

    class Meta:
        ordering = ['name']


class ImmunoPeptidomeManager(models.Manager):
    """
    This manager class provides selection of instances for user based on
    permission.
    """

    def for_user(self, user: User, filter_=None, related: bool = False,
                 reverse_sorted: bool = False) -> list:
        """
        Selects instances with the 'view' permission.
        :param user: User instance
        :param filter_: Extra queryset filter
        :param related: Query related field
        :param reverse_sorted: Reverse sort by primary key
        :return: Immunopeptidomes list
        """

        # Get permission from biological samples
        biological_samples = BiologicalSample.objects.for_user(user)

        biological_samples_ids = {biological_sample.pk for biological_sample in
                                  biological_samples}

        immunopeptidomes = self.get_queryset()\
            .filter(biological_sample__in=biological_samples, trashed=0)\
            .prefetch_related('biological_sample')

        if filter_ is not None:
            immunopeptidomes = immunopeptidomes.filter(filter_)

        if related:
            immunopeptidomes = immunopeptidomes.select_related('created_by',
                                                               'modified_by')

        immunopeptidomes_retained = list()
        immunopeptidomes_retained_keys = set()

        for immunopeptidome in immunopeptidomes:

            # This removes duplicate immunopeptidome caused by multiple
            # biological sample
            if immunopeptidome.pk in immunopeptidomes_retained_keys:
                continue

            keep = True

            # User must have permission for associated biological samples
            for biological_sample in immunopeptidome.biological_sample.all():
                if biological_sample.pk not in biological_samples_ids:
                    keep = False
                    break

            if keep:
                immunopeptidomes_retained.append(immunopeptidome)
                immunopeptidomes_retained_keys.add(immunopeptidome.pk)

        # Get permission from immunopeptidome
        immunopeptidomes = self.get_queryset().filter(trashed=0)

        if filter_ is not None:
            immunopeptidomes = immunopeptidomes.filter(filter_)

        if related:
            immunopeptidomes = immunopeptidomes.select_related('created_by',
                                                               'modified_by')

        permission = 'mhc_pipeline.view_immunopeptidome'
        immunopeptidomes = get_objects_for_user(user,
                                                permission,
                                                klass=immunopeptidomes)

        for immunopeptidome in immunopeptidomes:

            if immunopeptidome.pk not in immunopeptidomes_retained_keys:
                immunopeptidomes_retained.append(immunopeptidome)

        if reverse_sorted:
            immunopeptidomes_retained = sorted(immunopeptidomes_retained,
                                               key=lambda x: x.pk,
                                               reverse=True)

        return immunopeptidomes_retained

    def get_for_user(self, pk: int, user: User, permission: str,
                     related: bool = False,) -> list:
        """
        Selects instances with the 'view' permission.
        :param pk: Immunopeptidome primary key
        :param user: User instance
        :param permission: Permission
        :param related: Query related field
        :return: Immunopeptidome instance
        """

        immunopeptidome = self.get_queryset()\

        if related:
            immunopeptidome = immunopeptidome.select_related('created_by',
                                                             'modified_by')

        immunopeptidome = immunopeptidome.get(pk=pk)

        biological_sample_counts = immunopeptidome.biological_sample.count()

        klass = immunopeptidome.biological_sample.all()

        permission_long = 'mhc_pipeline.' + permission + '_biologicalsample'

        # Get permission from biological samples
        if get_objects_for_user(user, permission_long, klass=klass).count() == \
                biological_sample_counts:
            setattr(immunopeptidome, 'content_type',
                    immunopeptidome.__class__.__name__.lower())
            return immunopeptidome

        Request = namedtuple('Request', ['user'])
        request = Request(user=user)

        # Get permission from immunopeptidome
        immunopeptidome = get_if_permission(request, self.get_queryset(),
                                            permission, pk=pk)

        setattr(immunopeptidome, 'content_type',
                immunopeptidome.__class__.__name__.lower())

        return immunopeptidome


class ImmunoPeptidome(BaseModel):
    """
    This model defines a filtered peptides set.
    """

    name = models.CharField(max_length=500, unique=True)

    comments = models.TextField(blank=True, null=True)

    filter = models.ForeignKey(Filter, on_delete=models.PROTECT,
                               blank=True,
                               null=True)

    biological_sample = models.ManyToManyField(BiologicalSample, blank=True)

    identification = models.ManyToManyField(Identification, blank=True)

    immunopeptidome = models.ManyToManyField('self',
                                             blank=True,
                                             related_name='parent',
                                             symmetrical=False)

    peptides = models.ManyToManyField(Peptide)

    peptide_count = models.IntegerField(blank=True, null=True)

    tags = models.ManyToManyField(Tag,
                                  blank=True)

    objects = ImmunoPeptidomeManager()

    def __str__(self):
        """
        Object string representation
        :return: String
        """

        return 'Id: %s - %s' % (self.pk, self.name)

    def get_absolute_url(self):
        """
        Define a get_absolute_url() method to tell Django how to calculate the
        canonical URL for an object.
        :return: String
        """

        return reverse('immunopeptidome:view', kwargs={'pk': self.pk})


def upload_path_handler(instance, file_name):
    """
    Path handler for file upload.
    :param instance: Model instance
    :param file_name: String
    :return: String
    """

    file_name = re.sub('(\.\w+)$', '_uploaded_file_id_%s\\1' % instance.pk, file_name)

    return os.path.join(instance.class_name, file_name)


class UploadedFile(models.Model):
    """
    This model stores upload file.
    """

    class_name = models.CharField(max_length=255)

    file = models.FileField(max_length=255, upload_to=upload_path_handler)

    name = models.CharField(max_length=255)


# Code for file upload
# Code from http://stackoverflow.com/questions/9968532/django-admin-file-upload-with-current-model-id
_UNSAVED_FILEFIELD = 'unsaved_filefield'


@receiver(pre_save, sender=UploadedFile)
def skip_saving_file(sender, instance, **kwargs):
    """
    Waits that object has been saved before saving files.
    :param sender: Model class
    :param instance: Model instance
    :param kwargs: Keyword arguments
    :return: None
    """

    if not instance.pk and not hasattr(instance, _UNSAVED_FILEFIELD):
        setattr(instance, _UNSAVED_FILEFIELD, instance.file)
        instance.file = None


@receiver(post_save, sender=UploadedFile)
def save_file(sender, instance, created, **kwargs):
    """
    Saves the files now that the object has been saved.
    :param sender: Model class
    :param instance: Model instance
    :param created: Boolean
    :param kwargs: Keyword arguments
    :return: None
    """

    if created and hasattr(instance, _UNSAVED_FILEFIELD):
        instance.file = getattr(instance, _UNSAVED_FILEFIELD)
        instance.save()


class SNPs(models.Model):
    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.PROTECT)

    pps_snps_id = models.IntegerField()

    def __str__(self):
        """
        Object string representation
        :return: String
        """

        return f'Id: {self.pk} - BS {self.biological_sample}'


class PersonalizedProteome(models.Model):
    snps = models.ForeignKey(SNPs,
                             on_delete=models.PROTECT)

    pps_pp_id = models.IntegerField()


class PeptideGenome(models.Model):
    unique_peptide = models.ForeignKey(UniquePeptide,
                                       on_delete=models.PROTECT)

    pp_id = models.PositiveIntegerField()

    mapped = models.BooleanField()

    protein_identifier = models.CharField(blank=True,
                                          max_length=20)

    protein_position = models.PositiveIntegerField(blank=True,
                                                   null=True)

    gene_symbol = models.CharField(blank=True,
                                   max_length=20)

    gene_identifier = models.CharField(blank=True,
                                       max_length=20)

    chromosome = models.CharField(blank=True,
                                  max_length=2)

    strand = models.CharField(blank=True,
                              max_length=1)

    locus = models.CharField(blank=True,
                             max_length=50)

    exon_positions = models.CharField(blank=True,
                                      max_length=100)

    exon_sequences = models.CharField(blank=True,
                                      max_length=300)

    transcript_identifier = models.CharField(blank=True,
                                             max_length=20)

    snp_positions = models.CharField(blank=True,
                                     max_length=150)

    sap = models.CharField(blank=True,
                           max_length=100)

    peptide_variants = models.CharField(blank=True,
                                        max_length=100)

    bed = models.CharField(max_length=250)

    ref_peptide = models.CharField(blank=True,
                                         default='',
                                         max_length=50)

    is_ref_peptide = models.BooleanField(blank=True, default=True)


class Document(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    created_by = models.ForeignKey(User, on_delete=models.PROTECT,
                                   related_name='+')

    path = models.CharField(max_length=260)

    name = models.CharField(max_length=260)

    json_values = models.TextField()

    comment = models.TextField(blank=True, null=True)

    git_sha = models.CharField(max_length=40)

    state = models.CharField(choices=STATUSES,
                             default='',
                             max_length=10)

    trashed = models.BooleanField(default=False)

    @property
    def file_path(self):
        """
        Provides absolute path to file.
        :return: Path object
        """

        return Path(settings.MEDIA_ROOT) / self.path / self.name


class ImmunoPeptidomeDocument(Document):
    immunopeptidome = models.ForeignKey(ImmunoPeptidome,
                                        on_delete=models.CASCADE)


class Annotation(models.Model):
    """
    This model holds file location and type of for annotating
    immunopeptidome peptides.

    Files are expected to be manually added in SHARED_DATA and
    annotation instance created from the admin.
    """

    created_at = models.DateTimeField(auto_now_add=True)

    type = models.CharField(max_length=20)

    sub_path = models.CharField(max_length=300)

    file_name = models.CharField(max_length=100)

    display_name = models.CharField(max_length=50)

    hidden = models.BooleanField(default=False)

    source_url = models.CharField(max_length=300)

    @property
    def file_path(self):
        """
        Provides absolute path to file.
        :return: Path object 
        """

        return Path(settings.SHARED_DATA) / self.sub_path / self.file_name


class PeptideCounts(models.Model):
    """
    Keeps individual and cumulative peptide counts for identification
    """

    identification = models.ForeignKey(Identification,
                                       on_delete=models.CASCADE)

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.CASCADE,
                                          null=True)

    taxonomy_identifier = models.IntegerField(null=True)

    allele = models.ForeignKey(MHCAllele,
                               on_delete=models.CASCADE,
                               null=True)

    individual_counts = models.PositiveIntegerField()

    cumulative_counts = models.PositiveIntegerField()

    class Meta:
        permissions = (
            ('global_view', 'Global view'),
        )


class PeptideBiologicalSampleCount(models.Model):
    """
    Counts the number of time a peptide is observed in different biological
    samples.
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.CASCADE,
                                          null=True)

    taxonomy_identifier = models.IntegerField(null=True)

    biological_sample_counts = models.PositiveIntegerField()

    peptide_counts = models.PositiveIntegerField()


class PeptideFileCount(models.Model):
    """
    Counts the number of time a peptide is observed in different files
    """

    biological_sample = models.ForeignKey(BiologicalSample,
                                          on_delete=models.CASCADE,
                                          null=True)

    taxonomy_identifier = models.IntegerField(null=True)

    file_counts = models.PositiveIntegerField()

    peptide_counts = models.PositiveIntegerField()


ChunkedUploadedFile = ChunkedUpload