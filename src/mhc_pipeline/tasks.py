"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from contextlib import closing
import csv
import gzip
import io
import json
import os
from pathlib import Path
import re
import shutil
import subprocess
from typing import (Dict, List)
import time
import zipfile

# Import Django related libraries
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Prefetch
from django.forms.models import model_to_dict

# Third party libraries
from celery import (shared_task,
                    Task)
import pandas as pd
import pyarrow.parquet as pq
import requests

# Import project libraries
from base_site.exceptions import BaseSiteException
from library import dbsnp
from .library.minio import get_storage
from library.maf import maf_from_frequencies
from .library import mhc_prediction
from .library.mhc_allele import MHC_PREDICTOR_NAMES
from library.parquet import convert_csv_to_parquet
from .library.peptides_filter import PeptidesFilter
from .library import peptides_csv
from .library.lock_model import ModelLocker
from .library.pps import pps_request
from .library import protein_database as db
from .models import (Annotation,
                     File,
                     FileSet,
                     Genotyping,
                     Identification,
                     ImmunoPeptidome,
                     ImmunoPeptidomeDocument,
                     MHCAllele,
                     BiologicalSampleMHCAllele,
                     Peptide,
                     PeptideGenome,
                     UploadedFile)
from .views import (biological_sample_plots,
                    immunopeptidome,
                    peptides_plots,
                    maxquant,
                    peaks,
                    comet)
from .library.peptide_count import process

CACHE_EXPIRATION = 2678400


class DocumentTask(Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        Mark document as failed
        :param exc:
        :param task_id:
        :param args:
        :param kwargs: Requires document_pk keyword
        :param einfo:
        :return: None
        """

        ImmunoPeptidomeDocument.objects.filter(pk=kwargs['document_pk']) \
            .update(state='failed')


def overwrite_document_csv(document: ImmunoPeptidomeDocument, df: pd.DataFrame):
    """
    Overwrite document csv file with the updated data frame
    :param document: ImmunoPeptidomeDocument instance
    :param df: Data frame
    :return: None
    """

    header = ''

    with document.file_path.open(mode='r') as f:
        header += f.readline()
        header += f.readline()

    # Overwrite CSV file
    with document.file_path.open(mode='w') as f:
        f.write(header)
        df.to_csv(f, index=False)


def parse_1000genomes(file_: Path) -> None:
    """
    Parses and extracts entries from 1000 genomes VCF file and stores it into
    Parquet format

    :param file_: VCF file
    :return: None
    """

    csv_file = str(file_) + '.csv'
    parquet_file = str(file_) + '.parquet'

    # Prepare header
    maf_field_columns = ('AF', 'EAS_AF', 'AMR_AF', 'AFR_AF', 'EUR_AF', 'SAS_AF')

    columns = ['rs', 'maf', 'maf_eas', 'maf_amr', 'maf_afr', 'maf_eur',
               'maf_sas']

    with file_.open() as f, open(csv_file, 'w') as out:

        out.write(','.join(columns))
        out.write('\n')

        for line in f:

            if line.startswith('#'):
                continue

            line = line.rstrip()
            chrom, pos, id_, ref, alt, qual, filter_, info = line.split('\t')

            if not id_.startswith('rs'):
                continue

            values = dict()

            for field in info.split(';'):

                if '=' in field:
                    k, v = field.split('=')

                    values[k] = v

            values_list = [id_]

            for field in maf_field_columns:
                frequencies = values[field].split(',')
                maf = maf_from_frequencies(frequencies)

                values_list.append(maf)

            out.write(','.join([str(x) for x in values_list]))
            out.write('\n')

    # Convert CSV to Parquet format
    convert_csv_to_parquet(csv_file, parquet_file)

    Path(csv_file).unlink()


@shared_task(base=DocumentTask)
def annotation_1000genomes(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with 1000 genomes MAF
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: Overwrite document file
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    # Parse and extract entries from VCF file
    annotation_parquet = str(annotation.file_path) + '.parquet'

    if not Path(annotation_parquet).exists():
        parse_1000genomes(annotation.file_path)

    pf = pq.ParquetFile(annotation_parquet)

    chunks = list()

    for i in range(0, pf.num_row_groups):
        chunk = pf.read_row_group(i).to_pandas()

        chunks.append(chunk[chunk['rs'].isin(df['dbsnp_rs'])])

    if len(chunks):
        df_tmp = pd.concat(chunks)

        df_tmp = df_tmp.add_prefix('1000_genomes_')
        merge_df = df.merge(df_tmp, how='left', left_on=['dbsnp_rs'],
                            right_on=['1000_genomes_rs'])
        del (merge_df['1000_genomes_rs'])

        overwrite_document_csv(document, merge_df)


@shared_task(base=DocumentTask)
def annotation_dbsnp(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with dbSNP
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: None
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    retrieve_annotation_file(annotation)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)
    dbsnp.annotate_csv(document.file_path, annotation.file_path)


@shared_task(base=DocumentTask)
def annotation_esp(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with ESP MAF
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: None
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    esp_files = annotation.file_path.parent.glob('*.txt')

    df_list = []

    for file in esp_files:
        df_tmp = pd.read_csv(file, sep=' ', skiprows=7, low_memory=False)
        df_tmp = df_tmp[df_tmp['rsID'].isin(df['dbsnp_rs'])]
        df_tmp = df_tmp[['rsID', 'EuropeanAmericanAlleleCount',
                         'AfricanAmericanAlleleCount',
                         'AllAlleleCount', 'MAFinPercent(EA/AA/All)']]
        df_list.append(df_tmp)

    esp_df = pd.concat(df_list)

    esp_df['maf_ea'], esp_df['maf_aa'], \
    esp_df['maf_all'] = esp_df[
        'MAFinPercent(EA/AA/All)'].str.split('/').str

    for key in ('maf_ea', 'maf_aa', 'maf_all'):
        esp_df[key] = esp_df[key].astype(float) / 100

    esp_df = esp_df[['rsID', 'EuropeanAmericanAlleleCount',
                     'AfricanAmericanAlleleCount', 'AllAlleleCount',
                     'maf_ea', 'maf_aa', 'maf_all'
                     ]]

    columns = {'rsID': 'rs',
               'EuropeanAmericanAlleleCount': 'allele_count_ea',
               'AfricanAmericanAlleleCount': 'allele_count_aa',
               'AllAlleleCount': 'allele_count_all'
               }
    esp_df = esp_df.rename(columns=columns)

    merge_df = df.merge(esp_df.add_prefix('esp_'), how='left',
                        left_on=['dbsnp_rs'], right_on=['esp_rs'])
    del (merge_df['esp_rs'])

    overwrite_document_csv(document, merge_df)


def generate_exac_parquet(exac_file: Path, prefix: str) -> None:
    """
    Generates MAF parquet file from ExAC vcf
    :param exac_file:
    :param prefix: Column prefix
    :return: None
    """

    af_indexes = dict()
    columns = ['rs', 'ref', 'alt']
    maf_indexes = list()

    if prefix == 'gnomad_':

        info_header = ''

        # gnomAD r2.0.1
        # af_list = ['AF_raw', 'AF_POPMAX',
        #            'AF', 'AF_Female', 'AF_Male',
        #            'AF_AFR', 'AF_AFR_Female', 'AF_AFR_Male',
        #            'AF_AMR', 'AF_AMR_Female', 'AF_AMR_Male',
        #            'AF_ASJ', 'AF_ASJ_Female', 'AF_ASJ_Male',
        #            'AF_EAS', 'AF_EAS_Female', 'AF_EAS_Male',
        #            'AF_FIN', 'AF_FIN_Female', 'AF_FIN_Male',
        #            'AF_NFE', 'AF_NFE_Female', 'AF_NFE_Male',
        #            'AF_OTH', 'AF_OTH_Female', 'AF_OTH_Male',
        #            'AF_SAS', 'AF_SAS_Female', 'AF_SAS_Male',
        #            ]

        # gnomAD r2.1.1
        af_list = ['AF_raw', 'AF_popmax',
                   'AF', 'AF_female', 'AF_male',
                   'AF_afr', 'AF_afr_female', 'AF_afr_male',
                   'AF_amr', 'AF_amr_female', 'AF_amr_male',
                   'AF_asj', 'AF_asj_female', 'AF_asj_male',
                   'AF_eas', 'AF_eas_female', 'AF_eas_male', 'AF_eas_kor',
                   'AF_eas_oea', 'AF_eas_jpn',
                   'AF_fin', 'AF_fin_female', 'AF_fin_male',
                   'AF_nfe', 'AF_nfe_female', 'AF_nfe_male', 'AF_nfe_onf',
                   'AF_nfe_nwe', 'AF_nfe_est', 'AF_nfe_seu', 'AF_nfe_bgr',
                   'AF_nfe_swe',
                   'AF_oth', 'AF_oth_female', 'AF_oth_male',
                   'AF_sas', 'AF_sas_female', 'AF_sas_male',
                   'controls_AF_afr',
                   'controls_AF_afr_female',
                   'controls_AF_afr_male',
                   'controls_AF_amr',
                   'controls_AF_amr_female',
                   'controls_AF_amr_male',
                   'controls_AF_asj',
                   'controls_AF_asj_female',
                   'controls_AF_asj_male',
                   'controls_AF_eas',
                   'controls_AF_eas_female',
                   'controls_AF_eas_jpn',
                   'controls_AF_eas_kor',
                   'controls_AF_eas_male',
                   'controls_AF_eas_oea',
                   'controls_AF_female',
                   'controls_AF_fin',
                   'controls_AF_fin_female',
                   'controls_AF_fin_male',
                   'controls_AF_male',
                   'controls_AF_nfe',
                   'controls_AF_nfe_bgr',
                   'controls_AF_nfe_est',
                   'controls_AF_nfe_female',
                   'controls_AF_nfe_male',
                   'controls_AF_nfe_nwe',
                   'controls_AF_nfe_onf',
                   'controls_AF_nfe_seu',
                   'controls_AF_nfe_swe',
                   'controls_AF_oth',
                   'controls_AF_oth_female',
                   'controls_AF_oth_male',
                   'controls_AF_popmax',
                   'controls_AF_raw',
                   'controls_AF_sas',
                   'controls_AF_sas_female',
                   'controls_AF_sas_male',
                   'non_cancer_AF_afr',
                   'non_cancer_AF_afr_female',
                   'non_cancer_AF_afr_male',
                   'non_cancer_AF_amr',
                   'non_cancer_AF_amr_female',
                   'non_cancer_AF_amr_male',
                   'non_cancer_AF_asj',
                   'non_cancer_AF_asj_female',
                   'non_cancer_AF_asj_male',
                   'non_cancer_AF_eas',
                   'non_cancer_AF_eas_female',
                   'non_cancer_AF_eas_jpn',
                   'non_cancer_AF_eas_kor',
                   'non_cancer_AF_eas_male',
                   'non_cancer_AF_eas_oea',
                   'non_cancer_AF_female',
                   'non_cancer_AF_fin',
                   'non_cancer_AF_fin_female',
                   'non_cancer_AF_fin_male',
                   'non_cancer_AF_male',
                   'non_cancer_AF_nfe',
                   'non_cancer_AF_nfe_bgr',
                   'non_cancer_AF_nfe_est',
                   'non_cancer_AF_nfe_female',
                   'non_cancer_AF_nfe_male',
                   'non_cancer_AF_nfe_nwe',
                   'non_cancer_AF_nfe_onf',
                   'non_cancer_AF_nfe_seu',
                   'non_cancer_AF_nfe_swe',
                   'non_cancer_AF_oth',
                   'non_cancer_AF_oth_female',
                   'non_cancer_AF_oth_male',
                   'non_cancer_AF_popmax',
                   'non_cancer_AF_raw',
                   'non_cancer_AF_sas',
                   'non_cancer_AF_sas_female',
                   'non_cancer_AF_sas_male',
                   'non_neuro_AF_afr',
                   'non_neuro_AF_afr_female',
                   'non_neuro_AF_afr_male',
                   'non_neuro_AF_amr',
                   'non_neuro_AF_amr_female',
                   'non_neuro_AF_amr_male',
                   'non_neuro_AF_asj',
                   'non_neuro_AF_asj_female',
                   'non_neuro_AF_asj_male',
                   'non_neuro_AF_eas',
                   'non_neuro_AF_eas_female',
                   'non_neuro_AF_eas_jpn',
                   'non_neuro_AF_eas_kor',
                   'non_neuro_AF_eas_male',
                   'non_neuro_AF_eas_oea',
                   'non_neuro_AF_female',
                   'non_neuro_AF_fin',
                   'non_neuro_AF_fin_female',
                   'non_neuro_AF_fin_male',
                   'non_neuro_AF_male',
                   'non_neuro_AF_nfe',
                   'non_neuro_AF_nfe_bgr',
                   'non_neuro_AF_nfe_est',
                   'non_neuro_AF_nfe_female',
                   'non_neuro_AF_nfe_male',
                   'non_neuro_AF_nfe_nwe',
                   'non_neuro_AF_nfe_onf',
                   'non_neuro_AF_nfe_seu',
                   'non_neuro_AF_nfe_swe',
                   'non_neuro_AF_oth',
                   'non_neuro_AF_oth_female',
                   'non_neuro_AF_oth_male',
                   'non_neuro_AF_popmax',
                   'non_neuro_AF_raw',
                   'non_neuro_AF_sas',
                   'non_neuro_AF_sas_female',
                   'non_neuro_AF_sas_male',
                   'non_topmed_AF_afr',
                   'non_topmed_AF_afr_female',
                   'non_topmed_AF_afr_male',
                   'non_topmed_AF_amr',
                   'non_topmed_AF_amr_female',
                   'non_topmed_AF_amr_male',
                   'non_topmed_AF_asj',
                   'non_topmed_AF_asj_female',
                   'non_topmed_AF_asj_male',
                   'non_topmed_AF_eas',
                   'non_topmed_AF_eas_female',
                   'non_topmed_AF_eas_jpn',
                   'non_topmed_AF_eas_kor',
                   'non_topmed_AF_eas_male',
                   'non_topmed_AF_eas_oea',
                   'non_topmed_AF_female',
                   'non_topmed_AF_fin',
                   'non_topmed_AF_fin_female',
                   'non_topmed_AF_fin_male',
                   'non_topmed_AF_male',
                   'non_topmed_AF_nfe',
                   'non_topmed_AF_nfe_bgr',
                   'non_topmed_AF_nfe_est',
                   'non_topmed_AF_nfe_female',
                   'non_topmed_AF_nfe_male',
                   'non_topmed_AF_nfe_nwe',
                   'non_topmed_AF_nfe_onf',
                   'non_topmed_AF_nfe_seu',
                   'non_topmed_AF_nfe_swe',
                   'non_topmed_AF_oth',
                   'non_topmed_AF_oth_female',
                   'non_topmed_AF_oth_male',
                   'non_topmed_AF_popmax',
                   'non_topmed_AF_raw',
                   'non_topmed_AF_sas',
                   'non_topmed_AF_sas_female',
                   'non_topmed_AF_sas_male',
                   ]

    else:
        info_header = 'Allele|Consequence|IMPACT|SYMBOL|Gene|Feature_type|Feature|BIOTYPE|EXON|INTRON|HGVSc|HGVSp|cDNA_position|CDS_position|Protein_position|Amino_acids|Codons|Existing_variation|ALLELE_NUM|DISTANCE|STRAND|FLAGS|VARIANT_CLASS|MINIMISED|SYMBOL_SOURCE|HGNC_ID|CANONICAL|TSL|APPRIS|CCDS|ENSP|SWISSPROT|TREMBL|UNIPARC|GENE_PHENO|SIFT|PolyPhen|DOMAINS|HGVS_OFFSET|GMAF|AFR_MAF|AMR_MAF|EAS_MAF|EUR_MAF|SAS_MAF|AA_MAF|EA_MAF|ExAC_MAF|ExAC_Adj_MAF|ExAC_AFR_MAF|ExAC_AMR_MAF|ExAC_EAS_MAF|ExAC_FIN_MAF|ExAC_NFE_MAF|ExAC_OTH_MAF|ExAC_SAS_MAF|CLIN_SIG|SOMATIC|PHENO|PUBMED|MOTIF_NAME|MOTIF_POS|HIGH_INF_POS|MOTIF_SCORE_CHANGE|LoF|LoF_filter|LoF_flags|LoF_info|context|ancestral'

        af_list = ['AF',
                   'ESP_AF_POPMAX',
                   'ESP_AF_GLOBAL',
                   'KG_AF_POPMAX',
                   'KG_AF_GLOBAL',
                   ]

    info_header = info_header.replace(' ', '').split('|')

    for index, name in enumerate(af_list):
        af_indexes[name] = index

    columns.extend([x.lower() for x in af_list])

    # Extract MAF indexes
    for i, field in enumerate(info_header):

        if 'MAF' in field:
            maf_indexes.append(i)
            columns.append(field.lower())

    csv_file = str(exac_file) + '.csv'
    parquet_file = str(exac_file) + '.parquet'

    # Extract MAF values and write to CSV file
    with gzip.open(exac_file, 'rt') as in_, \
            open(csv_file, 'w') as out:

        out.write(','.join(columns))
        out.write('\n')

        for line in in_:

            if line.startswith('#'):
                continue

            maf = extract_info_exac(line, maf_indexes, af_indexes)

            if maf is not None:
                out.write(','.join(maf))
                out.write('\n')

    # Convert CSV to Parquet format
    convert_csv_to_parquet(csv_file, parquet_file)

    Path(csv_file).unlink()


def extract_info_exac(line: str, maf_indexes: List[int],
                      af_indexes: Dict) -> List:
    """
    Extracts fields from ExAC info field
    :param line: Line from ExAC vcf file
    :param maf_indexes: Indexes of MAF to extract
    :param af_indexes: Indexes for AF to extract
    :return: List with dbSNP rs id (first element) and MAF values
    """

    maf_values: List[str] = [''] * (len(af_indexes))

    fields = line.split('\t')
    rs_id = fields[2]
    ref = fields[3]
    alt = fields[4]
    info = fields[7]

    if not rs_id.startswith('rs'):
        return None

    if len(alt) != 1:
        return None

    for field in info.split(';'):

        if 'AF_' in field or field.startswith('AF='):

            name, af_values = field.split('=')

            frequencies = af_values.split(',')

            maf = maf_from_frequencies(frequencies)

            maf_values[af_indexes[name]] = str(maf)

        elif field.startswith('CSQ'):

            csq_fields = field.split(',')[0].split('|')

            # Extract MAF
            for index in maf_indexes:
                frequencies = csq_fields[index].split('&')
                frequencies = [x.split(':')[1] for x in frequencies if ':' in x]

                maf = maf_from_frequencies(frequencies)

                maf_values.append(str(maf))

    return [rs_id, ref, alt] + maf_values


def retrieve_annotation_file(annotation: Annotation) -> None:
    """
    Checks if annotation file exits otherwise download it
    :param annotation: Annotation instance
    :return: None
    """

    if not annotation.file_path.exists():
        annotation.file_path.parent.mkdir(exist_ok=True)

        url = annotation.source_url

        with requests.get(url, stream=True) as r:
            r.raise_for_status()

            with annotation.file_path.open('wb') as f:

                for chunk in r.iter_content(chunk_size=8192):

                    if chunk:
                        f.write(chunk)


@shared_task(base=DocumentTask)
def annotation_exac(annotation_pk: int, *, document_pk: int,
                    prefix: str = 'exac_') -> None:
    """
    Annotates peptides csv file with ExAC MAF
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :param prefix: Column prefix
    :return: Overwrite document file
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    # Parse and format ExAC vcf file to parquet
    annotation_parquet = str(annotation.file_path) + '.parquet'

    if not Path(annotation_parquet).exists():
        retrieve_annotation_file(annotation)
        generate_exac_parquet(annotation.file_path, prefix)

    # Read and merge ExAC csv MAF values
    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    pf = pq.ParquetFile(annotation_parquet)

    chunks = list()

    for i in range(0, pf.num_row_groups):
        chunk = pf.read_row_group(i).to_pandas()

        chunks.append(chunk[chunk['rs'].isin(df['dbsnp_rs'])])

    if len(chunks):
        maf_df = pd.concat(chunks)
        maf_df = maf_df.add_prefix(prefix)

        merge_df = df.merge(maf_df, how='left', left_on=['dbsnp_rs'],
                            right_on=[prefix + 'rs'])
        del merge_df[prefix + 'rs']

        overwrite_document_csv(document, merge_df)


@shared_task(base=DocumentTask)
def annotation_gnomad(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with gnomAD MAF
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: Overwrite document file
    """

    annotation_exac(annotation_pk, document_pk=document_pk, prefix='gnomad_')


@shared_task(base=DocumentTask)
def annotation_hpa(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with Human protein atlas tissue RNA expression
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: None
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    retrieve_annotation_file(annotation)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    profiles_df = pd.read_csv(annotation.file_path)

    df_tmp = profiles_df.drop(['Ensembl gene id', 'Category'], axis=1) > 10
    profiles_df = profiles_df.add_prefix('hpa_fpkm_')
    profiles_df['hpa_ubiquitous_expression'] = df_tmp.all(axis=1)
    profiles_df['hpa_bone_marrow/skin_ratio'] = round(
        profiles_df['hpa_fpkm_bone marrow'] / profiles_df['hpa_fpkm_skin'], 2)

    columns = {column: column.replace(' ', '_').lower() for column in
               profiles_df.columns}
    profiles_df = profiles_df.rename(columns=columns)

    merge_df = df.merge(profiles_df, left_on=['gene_identifier'],
                        right_on=['hpa_fpkm_ensembl_gene_id'], how='left')
    del (merge_df['hpa_fpkm_ensembl_gene_id'])

    overwrite_document_csv(document, merge_df)


def generate_iedb_parquet(iedb_file: Path) -> None:
    """
    Generates IEDB mhc ligand parquet file
    :param iedb_file: Zipped CSV file
    :return: None
    """

    identifier_index = 9
    obj_type_index = 10
    sequence_index = 11

    csv_file = str(iedb_file).replace('.zip', '.csv')
    parquet_file = str(iedb_file).replace('.zip', '.parquet')

    identifiers = set()

    with zipfile.ZipFile(iedb_file) as zip_fh:
        with zip_fh.open('mhc_ligand_full.csv', 'r') as fh, \
                open(csv_file, 'w') as out:

            out.write(','.join(('peptide_sequence', 'IEDB_epitope_identifier')))
            out.write('\n')

            # Skip header
            fh.readline()
            fh.readline()

            csv_reader = csv.reader(io.TextIOWrapper(fh, encoding='utf-8'))

            for fields in csv_reader:
                identifier = fields[identifier_index].split('/')[-1]
                sequence = fields[sequence_index]

                linear = fields[obj_type_index] == 'Linear peptide'

                if ' + ' in sequence:
                    sequence = sequence.split(' + ')[0]

                if identifier not in identifiers and linear:
                    out.write(','.join((sequence, identifier)))
                    out.write('\n')
                    identifiers.add(identifier)

    # Convert CSV to Parquet format
    convert_csv_to_parquet(csv_file, parquet_file)

    Path(csv_file).unlink()


@shared_task(base=DocumentTask)
def annotation_iedb(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with IEDB mhc ligand
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: None
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    # Parse and format ExAC vcf file to parquet
    annotation_parquet = str(annotation.file_path).replace('.zip', '.parquet')

    if not Path(annotation_parquet).exists():
        retrieve_annotation_file(annotation)
        generate_iedb_parquet(annotation.file_path)

    iedb_df = pd.read_parquet(annotation_parquet)
    iedb_df['IEDB_epitope_identifier'] = iedb_df[
        'IEDB_epitope_identifier'].astype(str)

    merge_df = df.merge(iedb_df, left_on=['Peptide sequence'],
                        right_on=['peptide_sequence'], how='left')
    del (merge_df['peptide_sequence'])
    overwrite_document_csv(document, merge_df)


@shared_task(base=DocumentTask)
def annotation_miha(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with patented MiHA
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: None
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    patented_miha_df = pd.read_csv(annotation.file_path)
    patented_miha = set(patented_miha_df['Peptide_sequence'])
    patented_miha.update(set(patented_miha_df['Alternative_sequence']))

    df['patented_miha'] = df['Peptide sequence'].isin(patented_miha)

    overwrite_document_csv(document, df)


@shared_task(base=DocumentTask)
def annotation_tcga(annotation_pk: int, *, document_pk: int) -> None:
    """
    Annotates peptides csv file with TCGA AML RNA expression
    :param annotation_pk: Annotation primary key
    :param document_pk: Document primary key
    :return: None
    """

    annotation = Annotation.objects.get(pk=annotation_pk)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    df_tmp = pd.read_csv(annotation.file_path)
    df_tmp['gene_symbol'] = df_tmp['Identifier'].apply(
        lambda x: str(x).split('.')[0])
    df_tmp['Mean'] = df_tmp['Mean'].round(decimals=2)

    df_tmp = df_tmp.rename(columns={'Mean': 'mean_expression'})
    df_tmp = df_tmp[['gene_symbol', 'mean_expression']].add_prefix('tcga_aml_')
    merge_df = df.merge(df_tmp, how='left', left_on=['gene_symbol'],
                        right_on=['tcga_aml_gene_symbol'])
    del (merge_df['tcga_aml_gene_symbol'])

    overwrite_document_csv(document, merge_df)


@shared_task(base=DocumentTask)
def annotate_start(pk: int, pp_url: str, *, document_pk: int) -> int:

    time.sleep(0.2)  # Delay to let the view save the ImmunoPeptidomeDocument
    ImmunoPeptidomeDocument.objects.filter(pk=document_pk).update(
        state='started')


@shared_task(base=DocumentTask)
def peptides_snps_submit(pk: int, pp_url: str, *, document_pk: int) -> int:
    """
    Extracts immunopeptidome peptides and submits to PPS peptides snps
    :param pk: Immunopeptidome primary key
    :param pp_url: Personalized proteome url
    :param document_pk: Document primary key
    :return: Peptides snps id
    """

    pp_id = pp_url.split('/')[-2]
    peptides_snps_id = -1

    # Get and filter peptides
    filter_options = {
        'immunopeptidome': [pk],
        'pp_id': pp_id,
        'remove_decoy': True,
        'unique_sequence': True
    }

    peptide_filters = PeptidesFilter(filter_options)
    peptides = peptide_filters.apply()

    # Format peptides
    pps_input = 'unique_peptide,peptide_sequence,pp_id\n'

    peptide_rows = list()

    if len(peptides):

        for peptide in peptides:
            row = f'{peptide.unique_peptide_id},{peptide.peptide_sequence},{pp_id}\n'
            peptide_rows.append(row)

        pps_input += ''.join(peptide_rows)

        # Make PPS request
        files = {'peptides_file': (
            f'immunopeptidome_{pk}.csv', pps_input, 'text/csv')}
        params = {'files': files,
                  'data': {
                      'identifier_column': 'Protein accessions / positions',
                      'peptide_column': 'peptide_sequence',
                      'header_skip': 0,
                      'personalized_proteome': pp_url
                  }
                  }
        response_json = pps_request('post', '/peptides_snps/', **params).json()
        peptides_snps_id = response_json['id']

    return peptides_snps_id


@shared_task(base=DocumentTask, bind=True, max_retries=500)
def peptides_snps_process(self, pk: int, document_pk: int = None) -> None:
    """
    Retrieves, parses and stores results form PPS peptides snps
    :param self: Task
    :param pk: Peptides snps primary key
    :param document_pk: Document primary key
    :return: None
    """

    # Is the any peptide to process:
    if pk == -1:
        return None

    # First wait for the PPS Peptides snps task to complete
    response = pps_request('get', f'/peptides_snps/{pk}/')
    content = response.json()

    if content['status'] == 'failed':
        ImmunoPeptidomeDocument.objects.filter(pk=document_pk).update(
            state='failed')
        raise RuntimeError('PPS failed to provide peptides_snps')
    elif content['status'] != 'finished':
        raise self.retry(countdown=30)

    # Process CSV file
    response = pps_request('get', content['csv_file'], stream=True)

    mapped_peptides = set()

    entries = None
    objs = list()

    with closing(response), ModelLocker(['peptidegenome'], 18, 10):

        reader = csv.DictReader(response.iter_lines(decode_unicode=True),
                                delimiter=',', quotechar='"')

        for row in reader:

            # Convert values
            row['unique_peptide_id'] = int(row['unique_peptide'])
            mapped_peptides.add(row['unique_peptide_id'])

            row['pp_id'] = int(row['pp_id'])

            # Check that the entry was not stored while processing
            if entries is None:
                entries = set()

                for obj in PeptideGenome.objects.filter(pp_id=row['pp_id']):
                    entries.add(obj.unique_peptide_id)

            if row['unique_peptide_id'] in entries:
                continue

            if row['protein_position'] == '':
                row['protein_position'] = None

            # Removes unnecessary keys
            del (row['peptide_sequence'])
            del (row['unique_peptide'])

            objs.append(PeptideGenome(**row))

        PeptideGenome.objects.bulk_create(objs, batch_size=500)

    # Process un-mapped peptides
    response = pps_request('get', content['peptides_file'], stream=True)

    entries = None
    objs = list()

    with closing(response), ModelLocker(['peptidegenome'], 18, 10):

        reader = csv.DictReader(response.iter_lines(decode_unicode=True),
                                delimiter=',', quotechar='"')

        for row in reader:

            row['unique_peptide_id'] = int(row['unique_peptide'])

            if row['unique_peptide_id'] not in mapped_peptides:

                row['pp_id'] = int(row['pp_id'])
                row['mapped'] = False

                # Check that the entry was not stored while processing
                if entries is None:
                    entries = set()

                    for obj in PeptideGenome.objects.filter(pp_id=row['pp_id']):
                        entries.add(obj.unique_peptide_id)

                if row['unique_peptide_id'] in entries:
                    continue

                # Removes unnecessary keys
                del (row['peptide_sequence'])
                del (row['unique_peptide'])

                objs.append(PeptideGenome(**row))

        PeptideGenome.objects.bulk_create(objs, batch_size=500)


@shared_task(base=DocumentTask)
def immunopeptidome_bed(pk: int, pp_url: str, *, document_pk: int) -> None:
    """
    Generates BED document
    :param pk: Immunopeptidome primary key
    :param pp_url: Personalized proteome url
    :param document_pk: Document primary key
    :return: None
    """

    pp_id = pp_url.split('/')[-2]

    # Get and filter peptides
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    filter_options = {
        'immunopeptidome': [pk],
        'remove_decoy': True,
        'unique_sequence': True
    }

    peptide_filters = PeptidesFilter(filter_options)
    peptides = peptide_filters.apply()

    unique_peptide = [peptide.unique_peptide_id for peptide in peptides]

    peptides_bed = PeptideGenome.objects.filter(pp_id=pp_id,
                                                unique_peptide__in=unique_peptide)

    sub_path = 'BED/'
    path = Path(settings.MEDIA_ROOT) / sub_path
    path.mkdir(exist_ok=True)
    name = f'immunopeptidome_id_{pk}_doc_id_{document_pk}.bed'

    with open(path / name, 'w') as out:
        for peptide in peptides_bed:
            out.write(peptide.bed)
            out.write('\n')

    document.state = 'success'
    document.path = sub_path
    document.name = name
    document.save()


@shared_task(base=DocumentTask)
def immunopeptidome_csv(pk: int, pp_url: str, msms_url: str, *,
                        document_pk: int) -> None:
    """
    Generates CSV document
    :param pk: Immunopeptidome primary key
    :param pp_url: Personalized proteome url
    :param document_pk: Document primary key
    :param msms_url: MSMS url
    :return: None
    """

    data = dict()

    # Get and filter peptides
    immunopeptidome = ImmunoPeptidome.objects.get(pk=pk)
    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    filter_options = {
        'immunopeptidome': [pk],
    }

    peptide_filters = PeptidesFilter(filter_options)
    peptides = peptide_filters.peptides
    peptides = peptides.select_related('unique_peptide')

    if pp_url:
        pp_id = pp_url.split('/')[-2]
        prefetch = Prefetch('unique_peptide__peptidegenome_set',
                            queryset=PeptideGenome.objects.filter(pp_id=pp_id),
                            to_attr='genome')
        peptides = peptides.prefetch_related(prefetch)

        data['genome'] = True

    fc = {
        'url': msms_url
    }

    columns = peptides_csv.column_names(fc, data=data)

    peptide_strings = peptides_csv.format_peptides(peptides, fc,
                                                   data=data)

    sub_path = 'CSV/'
    path = Path(settings.MEDIA_ROOT) / sub_path
    path.mkdir(exist_ok=True)
    name = f'immunopeptidome_id_{pk}_doc_id_{document_pk}.csv'

    filter_json = json.loads(document.json_values)
    filter_json['immunopeptidome'] = [pk]
    filter_json = json.dumps(filter_json)

    db_set = [model_to_dict(entry) for entry in
              db.from_immunopeptidome(immunopeptidome)]
    db_str = db.formatted_str(db_set)

    with open(path / name, 'w') as out:
        out.write(filter_json)
        out.write('\n')
        out.write(db_str)

        out.write('\n')

        out.write('"')
        out.write('","'.join(columns))
        out.write('"\n')

        for peptide in peptide_strings:
            out.write(peptide)

    document.path = sub_path
    document.name = name
    document.save()


@shared_task(base=DocumentTask)
def mhc_predictions(mhc_predictors: list, document_pk: int) -> None:
    """
    Run MHC predictions
    :param mhc_predictors: List of MHC predictors
    :param document_pk: Document primary key
    :return: Overwrite document file
    """

    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    peptides = Peptide.objects.filter(pk__in=df['peptide_id'].drop_duplicates())

    merge_df = df

    for predictor in mhc_predictors:
        df_tmp = mhc_prediction.get_predictions(peptides,
                                                {'mhc_predictor': predictor})

        merge_df = merge_df.merge(df_tmp, how='left', on='peptide_id')

        mhc_predictor_name = MHC_PREDICTOR_NAMES[predictor]

        new_columns = dict()

        for column in merge_df.columns:
            if type(column) == tuple:
                new_column = f'{mhc_predictor_name} {column[0]}: {column[1]}'
                new_columns[column] = new_column

        merge_df = merge_df.rename(columns=new_columns)

    overwrite_document_csv(document, merge_df)


@shared_task(base=DocumentTask)
def peptide_locus_count(*, document_pk: int) -> None:
    """
    Counts the number of locus for each peptide
    :param document_pk: Document primary key
    :return: Overwrite document file
    """

    document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

    df = pd.read_csv(document.file_path, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    df_tmp = df[['Peptide sequence', 'locus']].drop_duplicates()
    df_tmp = df_tmp.groupby(['Peptide sequence']).apply(len)
    df_tmp = df_tmp.rename('peptide_locus_count').to_frame()
    merge_df = df.merge(df_tmp, left_on=['Peptide sequence'], right_index=True)

    overwrite_document_csv(document, merge_df)


@shared_task
def processing_completed(*, document_pk: int) -> None:
    """
    Updates document status when task chain is completed
    :param document_pk: Document primary key
    :return: None
    """

    ImmunoPeptidomeDocument.objects.filter(pk=document_pk) \
        .update(state='success')


annotation_tasks = {
    'dbsnp': annotation_dbsnp,
    'esp': annotation_esp,
    'exac': annotation_exac,
    'gnomad': annotation_gnomad,
    '1000genomes': annotation_1000genomes,
    'hpa': annotation_hpa,
    'tcga': annotation_tcga,
    'miha': annotation_miha,
    'iedb': annotation_iedb,
}


class IdentificationTask(Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        Mark document as failed
        :param exc:
        :param task_id:
        :param args:
        :param kwargs: Requires document_pk keyword
        :param einfo:
        :return: None
        """

        msg = 'Unexpected failure'
        Identification.objects.filter(pk=args[0]).update(message=msg,
                                                         state='failed')


@shared_task(base=IdentificationTask)
def import_identification(pk: int, trial: int, max_trial: int = 3,
                          trial_delay: int = 600, lock_trial: int = 18,
                          lock_trial_delay: int = 10) -> None:
    """
    Import PEAKS or Maxquant or Comet results
    :param pk: Identification primary key
    :param trial: Trial number
    :param max_trial: Maximum number of trial
    :param trial_delay: Seconds before next trial
    :param lock_trial: Number of time to try to acquire db lock
    :param lock_trial_delay: Seconds between db lock trial
    :return: None
    """

    time.sleep(2)  # Delay to let the view save the Identification to database
    identification = Identification.objects.select_related('file_set').get(
        pk=pk)
    identification.state = 'started'
    identification.save()

    file_set = identification.file_set
    file_set.locked = True
    file_set.save()

    try:
        if file_set.set_class == 4:
            peaks.import_peptides(identification, lock_trial, lock_trial_delay)
        elif file_set.set_class == 6:
            maxquant.import_peptides(identification, lock_trial,
                                     lock_trial_delay)
        elif file_set.set_class == 7:
            comet.import_peptides(identification, lock_trial, lock_trial_delay)

    except BaseSiteException as e:
        identification.state = 'failed'
        identification.message = e.message

        if e.level == 'database_locked':

            identification.message += f' (trial {trial}/{max_trial}'
            trial += 1

            if trial <= max_trial:
                identification.message += f', waiting {trial_delay} seconds'
                import_identification.apply_async(args=(identification.pk,
                                                        trial, max_trial,
                                                        lock_trial,
                                                        lock_trial_delay),
                                                  countdown=trial_delay)
            identification.message += ')'
    identification.save()


class IdentificationCSVTask(Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        Mark document as failed
        :param exc:
        :param task_id:
        :param args:
        :param kwargs: Requires document_pk keyword
        :param einfo:
        :return: None
        """

        cache_obj = {
            'status': 'failed',
            'error': 'Unexpected error',
            'validation_timestamp': 0,
        }

        filter_options = args[0]

        storage = get_storage()

        with storage.open(filter_options['identification_key'], 'w') as file:
            file.write(json.dumps(cache_obj))


@shared_task(base=IdentificationCSVTask)
def identification_csv(filter_options: dict) -> None:
    """
    Generates CSV file for identifications
    :param filter_options: Peptide filter options
    :return: None
    """

    peptides_csv.csv(filter_options)


class ImmunopeptidomeTask(Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        Mark document as failed
        :param exc:
        :param task_id:
        :param args:
        :param kwargs:
        :param einfo:
        :return: None
        """

        msg = 'Unexpected failure'
        ImmunoPeptidome.objects.filter(pk=args[0]).update(comments=msg)


@shared_task(base=ImmunopeptidomeTask)
def immunopeptidome_peptides(pk: int, filter_options: dict) -> None:
    """
    Adds peptides to immunopeptidome
    :param pk: Immunopeptidome primary key
    :param filter_options: Peptide filter options
    :return: None
    """

    immunopeptidome.add_peptides(pk, filter_options)


@shared_task
def peptides_generate_plots(cache_key: str, filter_options: dict) -> None:
    """
    Generates and caches immunopeptidome plots
    :param cache_key: Cache key
    :param filter_options: Peptide filter options
    :return: None
    """

    peptides_plots.generate_plots(cache_key, filter_options)


@shared_task
def biological_sample_generate_plots(pk: int, tax_id: int) -> None:
    """
    Generates and caches biological_sample plots
    :param pk: biological_sample primary key
    :param tax_id: Taxonomy identifier
    :return: None
    """

    biological_sample_plots.generate_plots(pk, tax_id)


@shared_task
def peptide_counts():
    process()


@shared_task
def compress_uploaded_file(upload_pk: int, file_pk: int) -> None:
    """
    Compresses uploaded file
    :param upload_pk: UploadedFile instance primary key
    :param file_pk: File instance primary key
    :return: None
    """

    obj = UploadedFile.objects.get(pk=upload_pk)

    file_name = Path(obj.file.path)
    file_name_str = str(file_name)

    zip_file_name = file_name_str + '.zip'

    with zipfile.ZipFile(zip_file_name, 'w', zipfile.ZIP_DEFLATED) as zipf:
        zipf.write(file_name_str, arcname=file_name.name)

    file_name.unlink()

    obj.name = Path(zip_file_name).name
    obj.file = obj.file.name + '.zip'
    obj.save()

    file_stat = os.stat(zip_file_name)

    file_ = File.objects.get(pk=file_pk)
    file_.extension = '.zip'
    file_.name = file_.name + '.zip'
    file_.size = file_stat.st_size
    file_.save()


@shared_task(base=DocumentTask)
def filter_miha(filter_options: dict, document_pk: int = None) -> None:
    """
    Filters MiHA peptides
    :param filter_options: Peptide filter options
    :param document_pk: Document primary key
    :return: None
    """

    old_document = ImmunoPeptidomeDocument.objects.get(
        pk=filter_options['document'])
    new_document = ImmunoPeptidomeDocument.objects.get(
        pk=document_pk)

    new_document.state = 'started'
    new_document.save()

    csv_file = old_document.file_path

    df = pd.read_csv(csv_file, sep=',', low_memory=False, skiprows=2,
                     dtype={'chromosome': 'object'})

    if filter_options.get('maf'):
        df = df[df['gnomad_af'] >= filter_options.get('maf')]

    if filter_options.get('dbsnp_validated'):
        df = df[df['dbsnp_validated'] == True]

    if filter_options.get('no_hla_igg'):
        df = df[~ df['gene_symbol'].str.contains('HLA')]
        df = df[~ df['gene_symbol'].str.contains('FCG')]

    if filter_options.get('unambiguous_genetic_origin'):
        df = df[df['peptide_locus_count'] == 1]

    if filter_options.get('msms_validation'):
        df = df[df['Validated'] == 'G']

    if filter_options.get('expression_not_ubiquitous'):
        df = df[df['hpa_ubiquitous_expression'] == False]

    if filter_options.get('ratio_bone_marrow_skin'):
        df = df[df['hpa_bone_marrow/skin_ratio'] >=
                filter_options.get('ratio_bone_marrow_skin')]

    if filter_options.get('expression_aml'):
        df = df[df['tcga_aml_mean_expression'] >
                filter_options.get('expression_aml')]

    # Write to CSV file

    with csv_file.open(mode='r') as f:
        filter_row = f.readline()
        db_row = f.readline()

    old_values = json.loads(filter_row)
    old_values.update(filter_options)
    del (old_values['immunopeptidome_name'])
    new_filter_row = json.dumps(old_values)

    with new_document.file_path.open(mode='w') as f:
        f.write(new_filter_row)
        f.write(db_row)
        df.to_csv(f, index=False)

    new_document.state = 'success'
    new_document.save()

    # Complete immunopeptidome if required
    if filter_options.get('immunopeptidome_name') != '':
        immunopeptidome.CreateFromFile.process(new_document.immunopeptidome,
                                               df, old_values)


class GenotypingTask(Task):

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """
        Mark document as failed
        :param exc:
        :param task_id:
        :param args:
        :param kwargs:
        :param einfo:
        :return: None
        """

        msg = 'Unexpected failure'
        (Genotyping.objects.filter(pk=args[0])
                           .update(message=msg, state='failed'))


@shared_task(base=GenotypingTask)
def genotyping(pk: int):
    """
    Initialize MHC genotyping Nextflow workflow and results processing
    :param pk: Genotyping primary key
    :return: None
    """

    time.sleep(1)  # Delay to let the view save the Genotyping object
    genotyping = Genotyping.objects.get(pk=pk)
    genotyping.state = 'started'
    genotyping.save()

    work_dir = Path(settings.NEXTFLOW_FS) / 'genotyping' / str(pk)
    work_dir.mkdir(exist_ok=True, parents=True)

    for bam_file in genotyping.file_set.file_set.filter(extension='.bam'):

        shutil.copyfile(bam_file.full_path(), work_dir / bam_file.name)

    command = ['/usr/src/app/bin/nextflow',
               '/usr/src/app/src/nf-workflows/arcashla/main.nf',
               '--bamfiles=*.bam',
               '-profile',
               'k8s']

    cwd = Path.cwd()
    os.chdir(work_dir)

    subprocess.run(command, check=True)

    os.chdir(cwd)

    # Process results
    work_dir = Path(settings.NEXTFLOW_FS) / 'genotyping' / str(pk)
    results_dir = Path(settings.MEDIA_ROOT) / 'genotyping' / str(pk)
    results_dir.mkdir(exist_ok=True, parents=True)

    # Make archive with files
    with zipfile.ZipFile(results_dir / 'results.zip', 'w') as zip_fh:

        for full_path in (work_dir / 'results').glob('**/*'):
            short_path = re.sub('^.+/results/', '', str(full_path))
            zip_fh.write(full_path, short_path)

    # Store alleles
    genotype_dir = work_dir / 'results' / 'genotype'

    if genotyping.update_alleles:

        genotype_file = list(genotype_dir.glob('*.genotype.json'))[0]

        with genotype_file.open() as json_fh:
            data = json.load(json_fh)

            bs_alleles, created = BiologicalSampleMHCAllele.objects.get_or_create(
                biological_sample=genotyping.biological_sample
            )

            for mhc_class, alleles in data.items():

                for allele in alleles:
                    allele = 'HLA-' + re.sub(':\w+$', '', allele)

                    try:
                        allele = MHCAllele.objects.get(name=allele)

                    except ObjectDoesNotExist:
                        genotyping.state = 'failed'
                        genotyping.message = f"{allele} doesn't exists."
                        genotyping.save()
                        return

                    bs_alleles.mhc_allele.add(allele)

    # Parse version
    version_file = genotype_dir / 'version.log'

    with version_file.open() as fh:
        version = fh.readline().rstrip()
        genotyping.software = version

    shutil.rmtree(work_dir)

    genotyping.state = 'success'
    genotyping.save()


@shared_task(base=GenotypingTask)
def genotyping_clean(pk: int):
    """
    Cleans work dir for trashed task
    :param pk: Genotyping primary key
    :return: None
    """

    work_dir = Path(settings.NEXTFLOW_FS) / 'genotyping' / str(pk)
    shutil.rmtree(work_dir, ignore_errors=True)



