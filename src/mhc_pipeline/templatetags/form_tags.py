"""
Copyright 2015-2017 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
"""

# Import standard libraries

# Import Django related libraries
from django import template

# Third party libraries

# Import project libraries
from ..forms import ExperimentDetailsForm

register = template.Library()


@register.filter
def experiment_details_form(obj):
    """
    Prepares ExperimentDetails form with submit option
    :param obj: ExperimentDetails instance
    :return: List
    """

    return [ExperimentDetailsForm(instance=obj, submit=True)]


@register.filter
def experiment_details_form_no_perm(obj):
    """
    Prepares ExperimentDetails form without submit option
    :param obj: ExperimentDetails instance
    :return: List
    """

    return [ExperimentDetailsForm(instance=obj, submit=False)]
