"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json

# Import Django related libraries
from django import template

# Third party libraries

# Import project libraries
from ..models import (Identification,
                      ImmunoPeptidome,
                      MHCAllele)

register = template.Library()


@register.filter
def format_filter_values(obj):
    """
    Formats filter value to HTML
    :param obj: Filter instance
    :return: String
    """

    values = []
    br = 0

    if obj is None or obj == '':
        return ''

    if type(obj) == dict:
        values_tmp = obj
    else:
        values_tmp = json.loads(obj.json_values)

    if 'identification' in values_tmp:

        ids = values_tmp['identification']

        identifications = list()

        for id_ in ids:

            identification = Identification.objects.get(pk=id_)
            identifications.append(identification.alternate_label())
        values_tmp['identification'] = identifications

    if 'immunopeptidome' in values_tmp:

        ids = values_tmp['immunopeptidome']

        immunopeptidomes = list()

        for id_ in ids:

            immunopeptidome = ImmunoPeptidome.objects.get(pk=id_)
            immunopeptidomes.append(str(immunopeptidome))
        values_tmp['immunopeptidome'] = immunopeptidomes

    if 'mhc_allele' in values_tmp:

        allele_ids = values_tmp['mhc_allele'][1:-1].split('|')

        alleles = list()

        for allele_id in allele_ids:

            allele = MHCAllele.objects.get(pk=allele_id)
            alleles.append(allele.name)

        values_tmp['mhc_allele'] = ', '.join(alleles)

    for key, value in sorted(values_tmp.items()):

        if key == 'annotation' or (key == 'mhc_predictor'
                                   and type(value) == list):
            values.append(f'<span class="label label-default">{key}</span>')
            for v in value:
                values.append(f'<span class="label btn-skin">{v}</span> ')

                if (len(values) - br) % 4 == 0:
                    values.append('<br />')
                    br += 1
        else:
            values.append(f'<span class="label label-default">{key}</span>'
                          f'<span class="label btn-skin">{value}</span> '
                          )

        if (len(values) - br) % 4 == 0:
            values.append('<br />')
            br += 1

    return ''.join(values)
