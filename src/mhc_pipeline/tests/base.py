"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import os

# Import Django related libraries
from django.contrib.auth.models import (Group,
                                        User)
from django.http import HttpRequest
from django.test import (Client,
                         TestCase)

# Third party libraries

# Import project libraries
from .helper import TestHelper


class BaseTest(TestCase):
    """
    This class setup environments for test.
    """

    def setUp(self):
        """
        This method setups user, group for test.
        :return: None
        """

        # print(self._testMethodName)

        # Create user
        username = 'test_user'
        password = 'top_secret'

        self.user = User.objects.create_user(username=username,
                                             password=password,
                                             email='user@fake.com')

        # Create group
        self.group = Group.objects.create(name='ms')
        self.group.user_set.add(self.user)

        # Login our test user
        self.client = Client()
        self.client.login(username=username, password=password)

        # Prepare a request object
        self.request = HttpRequest()
        self.request.user = self.user

        # Helper methods
        self.helpers = TestHelper(self.user)

        # print(self._testMethodName)

        os.environ['GIT_SHA'] = 'test'
