"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from datetime import datetime
import os
from pathlib import Path
from pytz import timezone

# Import Django related libraries
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile

# Third party libraries

# Import project libraries
from mhc_pipeline.library.permission import set_content_permission
import mhc_pipeline.models as models
from ..tasks import import_identification


class TestHelper:
    """
    This class defines methods repetitively required by unit tests.
    """

    def __init__(self, user):

        # Some model requires User
        self.user = user

    @staticmethod
    def create_annotation(type_='dbsnp', sub_path='', file_name=''):

        obj = models.Annotation.objects.create(type=type_,
                                               sub_path=sub_path,
                                               file_name=file_name,
                                               display_name='',
                                               hidden=False,
                                               source_url=''
                                               )

        return obj

    def create_biological_sample(self, identifier='test', permissions=list()):
        """
        Creates instance of BiologicalSample
        :param identifier: String
        :param permissions: Permissions list ('view', 'change', ...)
        :return: BiologicalSample instance
        """

        obj = models.BiologicalSample.objects.create(created_by=self.user,
                                                     identifier=identifier,
                                                     modified_by=self.user,
                                                     short_description='Hello')

        set_content_permission(obj, self.user, permissions)
        models.MHCSample.objects.create(biological_sample=obj)

        return obj

    def create_biological_sample_file_set(self, set_class,
                                          biological_sample=None):
        """
        Creates instance of BiologicalSampleFileSet
        :param biological_sample: BiologicalSample instance
        :param set_class: String
        :return: BiologicalSampleFileSet instance
        """

        if biological_sample is None:
            biological_sample = self.create_biological_sample()

        return models.BiologicalSampleFileSet.objects.create(
            biological_sample=biological_sample,
            created_by=self.user,
            modified_by=self.user,
            name='Test',
            set_class=set_class)

    def create_biological_sample_mhc_allele(self, biological_sample=None):
        """
        Creates instance of BiologicalSampleMHCAllele
        :param biological_sample: BiologicalSample instance
        :return: BiologicalSampleMHCAllele instance
        """

        if biological_sample is None:
            biological_sample = self.create_biological_sample()

        obj = models.BiologicalSampleMHCAllele.objects.create(
            biological_sample=biological_sample)

        obj.mhc_allele.add(self.create_mhc_allele())

        return obj

    def create_experiment_details(self, biological_sample=None):
        """
        Creates instance of ExperimentDetails
        :param biological_sample: BiologicalSample instance
        :return: ExperimentDetails instance
        """

        if biological_sample is None:
            biological_sample = self.create_biological_sample()

        return models.ExperimentDetails.objects.create(
            biological_sample=biological_sample,
            name='Test')

    def create_file_set(self):
        """
        Creates instance of FileSet
        :return: FileSet instance
        """

        return models.FileSet.objects.create(created_by=self.user,
                                             modified_by=self.user,
                                             name='Test',
                                             set_class=2)

    def create_file(self, file_set=None, extension='.txt', name='-', path='-'):
        """
        Creates instance of File
        :return: File instance
        """

        if file_set is None:
            file_set = self.create_file_set()

        time_zone = timezone(settings.TIME_ZONE)

        return models.File.objects.create(file_set=file_set,
                                          path=path,
                                          name=name,
                                          extension=extension,
                                          size=1,
                                          date_time=datetime.now(tz=time_zone),
                                          available=True,
                                          )

    def create_filter(self, name='Test', permissions=list()):
        """
        Creates instance of Filter
        :param name: String
        :param permissions: Permissions list ('view', 'change', ...)
        :return: Filter instance
        """

        json_values = '{"peptide_score__gte": 50.0}'

        obj = models.Filter.objects.create(created_by=self.user,
                                           modified_by=self.user,
                                           name=name,
                                           json_values=json_values
                                           )

        set_content_permission(obj, self.user, permissions)

        return obj

    @staticmethod
    def create_lock_model():
        """
        Creates instance of LockModel
        :return: LockModel instance
        """

        return models.LockModel.objects.create(model='Test')

    @staticmethod
    def create_mhc_allele(mhc_class='HLA', gene='A', name='HLA-A*02:01'):
        """
        Creates instance of MHCAllele
        :param mhc_class: String
        :param gene: String
        :param name: String
        :return: MHCAllele instance
        """

        return models.MHCAllele.objects.create(mhc_class=mhc_class,
                                               gene=gene,
                                               name=name)

    @staticmethod
    def create_protein_database():
        """
        Creates instance of ProteinDatabase
        :return: ProteinDatabase instance
        """

        return models.ProteinDatabase.objects.create(name='Uniprot',
                                                     file_format='FASTA',
                                                     location='-',
                                                     num_sequences=5
                                                     )

    def create_identification(self, biological_sample=None):
        """
        Creates instance of Identification
        :return: Identification instance
        """

        if biological_sample is None:
            biological_sample = self.create_biological_sample()

        identification = models.Identification.objects.create(
            created_by=self.user,
            modified_by=self.user,
            biological_sample=biological_sample,
            file_set=self.create_file_set(),
            software='PEAKS',
        )

        identification.protein_database.add(self.create_protein_database())

        return identification

    @staticmethod
    def create_protein_sequence(protein_database):
        """
        Creates of get instance of ProteinSequence
        :param protein_database: ProteinDatabase instance
        :return: ProteinSequence instance
        """

        obj, created = models.ProteinSequence.objects.get_or_create(
            protein_database=protein_database,
            accession='-',
            short_description='-',
            sequence='A',
            length=1)

        return obj

    def create_peptide(self, identification=None,
                       peptide_score=42.0,
                       peptide_sequence='PEPYPEPY',
                       is_decoy=False):
        """
        Creates instance of Peptide
        :param identification: Identification instance
        :param peptide_score: Float
        :param peptide_sequence: String
        :param is_decoy: Boolean
        :return: Peptide instance
        """

        if identification is None:
            identification = self.create_identification()

        peptide = models.Peptide.objects.create(
            identification=identification,
            scan_number=1,
            scan_file='-',
            scan_file_location='home',
            is_decoy=is_decoy,
            one_is_decoy=True,
            q_value=0.001,
            peptide_score=peptide_score,
            rank=1,
            rank_total=1,
            charge_state=2,
            calculated_mz=400.2,
            experimental_mz=400.23,
            error_ppm=10,
            peptide_sequence=peptide_sequence,
            peptide_length=len(
                peptide_sequence),
            modification='[{"location": 4, "monoisotopicMassDelta": ' \
                         '79.96633, "name": "Phospho", "residues": ["Y"]}]',
            protein_accessions_positions='id1[3,5]',
            protein_count=1,
            retention_time=0,
            scan_file_index=0,
            peak_count=10,
            total_ion_current=100.23,
        )

        protein_sequence = self.create_protein_sequence(
            identification.protein_database.all()[0])

        peptide.protein_matches.add(protein_sequence)

        return peptide

    def create_new(self):
        """
        Creates instance of New
        :return: New instance
        """

        return models.New.objects.create(created_by=self.user,
                                         modified_by=self.user,
                                         short_message='Test',
                                         )

    @staticmethod
    def create_uploaded_file():
        """
        Creates instance of UploadedFile
        :return: UploadedFile instance
        """

        file = SimpleUploadedFile('test.fa', b'>prot1')

        instance = models.UploadedFile.objects.create(class_name='Fasta',
                                                      file=file,
                                                      name='test.fa')
        file.close()

        return instance

    @staticmethod
    def create_unique_peptide():
        """
        Creates instance of UniquePeptide
        :return: UniquePeptide instance
        """

        return models.UniquePeptide.objects.create(peptide_sequence='PEP',
                                                   peptide_length=3)

    def create_immunopeptidome(self, name='Immuno subject 1',
                               permissions=list()):
        """
        Creates instance of ImmunoPeptidome
        :return: ImmunoPeptidome instance
        """

        obj = models.ImmunoPeptidome.objects.create(
            created_by=self.user,
            modified_by=self.user,
            name=name,
            comments='Text',
            filter=self.create_filter(),
        )

        peptide = self.create_peptide()
        obj.peptides.add(peptide)
        obj.identification.add(peptide.identification)
        obj.biological_sample.add(
            peptide.identification.biological_sample)

        set_content_permission(obj, self.user, permissions)

        return obj

    def create_immunopeptidome_document(self, immunopeptidome=None,
                                        path='', name=''):

        if immunopeptidome is None:
            immunopeptidome = self.create_immunopeptidome()

        obj = models.ImmunoPeptidomeDocument.objects.create(
            immunopeptidome=immunopeptidome,
            created_by=self.user,
            path=path,
            name=name,
            json_values='{}',
            comment=None,
            git_sha='GIT_SHA',
            state='pending',
            trashed=False
        )

        return obj

    def create_personalized_proteome(self, biological_sample=None):

        if biological_sample is None:
            biological_sample = self.create_biological_sample(
                permissions=['change', 'view'])

        snp = models.SNPs.objects.create(biological_sample=biological_sample,
                                         pps_snps_id=1)

        obj = models.PersonalizedProteome.objects.create(snps=snp,
                                                         pps_pp_id=1)

        return obj

    def import_peptides(self, missing_file=False,
                        wrong_version=False):
        """
        Imports peptides from a PEAKS results file for test.
        :param missing_file: Boolean to fake missing MGF file.
        :param wrong_version: Boolean
        :return: Identification
        """

        # Create required fake objects
        biological_sample = self.create_biological_sample(
            permissions=['change', 'view'])

        file_set = self.create_biological_sample_file_set(
            4, biological_sample=biological_sample)

        path = os.path.join(file_set.get_class_attribute('dir'),
                            'mzid')
        file = 'peptides_1_1_0_scaffold.mzid'

        if wrong_version:
            file = 'peptides_1_0_0_header.mzid'

        models.File.add_file(file_set, path, file)

        # Fake missing MGF
        if missing_file is False:
            file = 'Yeast_2015-05-26_R9.mgf'
            models.File.add_file(file_set, path, file)

        identification = models.Identification.objects.create(
            created_by=self.user,
            modified_by=self.user,
            biological_sample=biological_sample,
            file_set=file_set,
            software='',
            state='pending'
        )

        import_identification(identification.pk, 1, 2, 1, 1, 1)

        identification.refresh_from_db()

        return identification

    def import_peptide_maxquant(self):

        path = Path('.') / 'test_files' / 'MaxQuant' / 'txt'

        biological_sample = self.create_biological_sample(
            permissions=['change', 'view'])

        fs = self.create_biological_sample_file_set(6,
                                                    biological_sample=biological_sample)

        file_names = ['evidence.txt', 'parameters.txt', 'peptides.txt']
        for name in file_names:
            self.create_file(file_set=fs, name=name, path=path)

        identification = models.Identification.objects.create(
            created_by=self.user,
            modified_by=self.user,
            biological_sample=biological_sample,
            file_set=fs,
            software='',
            state='pending'
        )

        import_identification(identification.pk, 1, 1, 1, 1, 1)

        identification.refresh_from_db()

        return identification

    def import_peptide_maxquant_alt(self):

        path = Path('.') / 'test_files' / 'MaxQuant' / 'txt_alt'

        biological_sample = self.create_biological_sample(
            permissions=['change', 'view'])

        fs = self.create_biological_sample_file_set(6,
                                                    biological_sample=biological_sample)

        file_names = ['evidence.txt', 'parameters.txt', 'peptides.txt']
        for name in file_names:
            self.create_file(file_set=fs, name=name, path=path)

        identification = models.Identification.objects.create(
            created_by=self.user,
            modified_by=self.user,
            biological_sample=biological_sample,
            file_set=fs,
            software='',
            state='pending'
        )

        import_identification(identification.pk, 1, 1, 1, 1, 1)

        identification.refresh_from_db()

        return identification
