"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from types import SimpleNamespace
from unittest.mock import patch

# Import Django related libraries

# Third party libraries

# Import project libraries
from ..admin import FasterAdminPaginator
from .base import BaseTest
from ..models import Peptide


class Test(BaseTest):
    def test_paginator_count(self):
        object_list = Peptide.objects.all()

        p = FasterAdminPaginator(object_list, 1)

        self.assertEqual(p.count, 0)

    def test_paginator_count_error(self):
        object_list = Peptide.objects.all()

        def raise_exception():
            raise AttributeError

        object_list.count = raise_exception

        p = FasterAdminPaginator(object_list, 1)

        self.assertEqual(p.count, 0)

    def test_paginator_count_query(self):
        object_list = Peptide.objects.all()

        def mock_execute(x, y):
            return None

        def mock_fetchone():
            return [1]

        def mock_cursor():
            return SimpleNamespace(execute=mock_execute,
                                   fetchone=mock_fetchone)

        p = FasterAdminPaginator(object_list, 1)

        module_name = 'django.db.connection.cursor'
        with patch(module_name, mock_cursor):
            self.assertEqual(p.count, 1)
