"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from unittest.mock import patch

# Import Django related libraries
from django.core.cache import cache
from django.urls import reverse

# Third party libraries
import pandas as pd
# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from ..models import PeptideCounts
from ..views import biological_sample_plots


class Test(BaseTest):

    def raise_value_error(self, x, y):
        raise ValueError

    def raise_BaseSiteException(self, x, y):
        raise BaseSiteException('error', 'error')

    def test_generate_plots(self):
        revision = biological_sample_plots.revision
        task_key = f'biological_sample_1_9606_plot_revision_{revision}_status'
        cache.delete(task_key)

        self.helpers.create_immunopeptidome(permissions=['view'])

        biological_sample_plots.generate_plots(1, 9606)

        cache.delete(task_key)

    def test_generate_plots_exception(self):
        revision = biological_sample_plots.revision
        task_key = f'biological_sample_1_9606_plot_revision_{revision}_status'
        cache.delete(task_key)

        with self.assertRaises(ValueError), \
            patch('mhc_pipeline.views.biological_sample_plots.get_stats',
                  self.raise_value_error):
            biological_sample_plots.generate_plots(1, 9606)

        cache.delete(task_key)

        with patch('mhc_pipeline.views.biological_sample_plots.get_stats',
                   self.raise_BaseSiteException):
            biological_sample_plots.generate_plots(1, 9606)
            value = cache.get(task_key)
            self.assertTrue(value.startswith('failed'))

        cache.delete(task_key)

    def test_get_stats(self):
        self.helpers.create_identification()

        out = biological_sample_plots.get_stats('1', 9606)

        self.assertIsInstance(out, pd.DataFrame)

        out = biological_sample_plots.get_stats('0', 9606)

        self.assertIsInstance(out, pd.DataFrame)

    def test_get_plot(self):
        self.helpers.create_identification()

        url = reverse('biological_sample:plot', args=('0', 9606, 'test'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_get_plot_check_bs_permission(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])
        self.helpers.create_identification(biological_sample=biological_sample)

        url = reverse('biological_sample:plot', args=(1, 9606, 'test'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_get_plot_biological_sample_peptide_counts(self):
        self.helpers.create_identification()

        url = reverse('biological_sample:plot',
                      args=('0', 9606, 'biological_sample_peptide_counts'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_plot_data(self):
        self.helpers.create_identification()

        url = reverse('biological_sample:plot_data', args=('0', 9606, 'test'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def df_test(self):

        identification = self.helpers.create_identification()
        biological_sample = identification.biological_sample
        allele = self.helpers.create_mhc_allele()

        PeptideCounts.objects.create(
            identification=identification,
            biological_sample=biological_sample,
            taxonomy_identifier=biological_sample.Species,
            allele=allele,
            individual_counts=1,
            cumulative_counts=1
        )

        PeptideCounts.objects.create(
            identification=identification,
            biological_sample=None,
            taxonomy_identifier=0,
            allele=None,
            individual_counts=1,
            cumulative_counts=1
        )

        df = biological_sample_plots.get_stats('1', 9606)

        return df

    def test_allele_counts(self):

        df = self.df_test()

        out = biological_sample_plots.allele_counts(df)
        self.assertIsInstance(out, tuple)

    def test_peptide_counts(self):

        df = self.df_test()

        out = biological_sample_plots.peptide_counts(df)
        self.assertIsInstance(out, tuple)

    def test_peptide_counts_cumulative(self):

        df = self.df_test()

        out = biological_sample_plots.peptide_counts_cumulative(df)
        self.assertIsInstance(out, tuple)

    def test_peptide_biological_sample_counts(self):

        df = self.df_test()

        out = biological_sample_plots.peptide_biological_sample_counts(df)
        self.assertIsInstance(out, tuple)

        df = biological_sample_plots.get_stats('0', 0)

        out = biological_sample_plots.peptide_biological_sample_counts(df)
        self.assertIsInstance(out, tuple)

    def test_biological_sample_peptide_counts(self):

        df = self.df_test()

        out = biological_sample_plots.biological_sample_peptide_counts(df)
        self.assertIsInstance(out, tuple)

    def test_peptide_file_counts(self):

        df = self.df_test()

        out = biological_sample_plots.peptide_file_counts(df)
        self.assertIsInstance(out, tuple)

        df = biological_sample_plots.get_stats('0', 0)

        out = biological_sample_plots.peptide_file_counts(df)
        self.assertIsInstance(out, tuple)
