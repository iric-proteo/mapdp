"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries

# Third party libraries

# Import project libraries
from .base import BaseTest
from mhc_pipeline.library.bulk_insert import BulkInsert
from ..models import UniquePeptide


class Test(BaseTest):
    def fields(self):
        return {
            'peptide_sequence': 'A',
            'peptide_length': 1
        }

    def test_entry_unique_key(self):
        bulk_insert = BulkInsert(UniquePeptide,
                                 unique_fields=['peptide_sequence'])

        key = bulk_insert.entry_unique_key(self.fields())

        self.assertEqual(key, ('A',))

    def test_get_create_pk(self):
        bulk_insert = BulkInsert(UniquePeptide)

        pk = bulk_insert.get_create_pk(self.fields())
        self.assertEqual(pk, 1)

    def test_get_create_unique_pk(self):
        self.helpers.create_peptide()

        bulk_insert = BulkInsert(UniquePeptide,
                                 unique_fields=['peptide_sequence'])

        pk = bulk_insert.get_create_unique_pk(self.fields())
        self.assertEqual(pk, 2)

        pk = bulk_insert.get_create_unique_pk(self.fields())
        self.assertEqual(pk, 2)

        fields = self.fields()
        fields['peptide_sequence'] = 'B'
        pk = bulk_insert.get_create_unique_pk(fields)
        self.assertEqual(pk, 3)

    def test_iterate_create(self):
        bulk_insert = BulkInsert(UniquePeptide,
                                 unique_fields=['peptide_sequence'])

        bulk_insert.to_create_list.append(self.fields())

        entries = list(bulk_insert.iterate_create_pk())

        self.assertEqual(len(entries), 1)

    def test_set_unique_entries_dict(self):

        self.helpers.create_unique_peptide()

        bulk_insert = BulkInsert(UniquePeptide,
                                 unique_fields=['peptide_sequence'])

        bulk_insert.set_unique_entries_dict()

        self.assertEqual(len(bulk_insert.unique_entries_dict), 1)

    def test_store(self):
        bulk_insert = BulkInsert(UniquePeptide,
                                 unique_fields=['peptide_sequence'])

        bulk_insert.get_create_unique_pk(self.fields())

        fields = self.fields()
        fields['peptide_sequence'] = 'B'
        bulk_insert.get_create_unique_pk(fields)

        bulk_insert.store()

        self.assertEqual(UniquePeptide.objects.count(), 2)
