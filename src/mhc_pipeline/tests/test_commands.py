"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json

# Import Django related libraries
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.core.management import call_command
from django.utils.six import StringIO

# Third party libraries
import requests_mock

# Import project libraries
from .base import BaseTest
from library.mhc_allele import URL
from mhc_pipeline.views.peaks import get_search_parameters


class Test(BaseTest):

    def test_mhc_pipeline_setup(self):

        with requests_mock.Mocker() as m:

            data = '# file: hla_nom_p.txt\nA*;01:02;'

            m.get(URL, text=data)

            out = StringIO()

            Group.objects.all().delete()
            call_command('mhc_pipeline_setup', stdout=out)

            self.assertEqual(Site.objects.all()[0].name, 'MAPDP')

    def test_store_mhc_alleles(self):

        with requests_mock.Mocker() as m:

            data = '# file: hla_nom_p.txt\nA*;01:02;'

            m.get(URL, text=data)

            out = StringIO()
            call_command('store_mhc_alleles', 'A', stdout=out)

            self.assertTrue('Completed' in out.getvalue())

    def test_peaks_search_parameters(self):

        identification = self.helpers.import_peptides()

        get_search_parameters.revision += 1

        out = StringIO()
        call_command('peaks_search_parameters', stdout=out)

        identification.refresh_from_db()

        revision = json.loads(identification.search_parameters)['revision']

        self.assertEqual(revision, get_search_parameters.revision)

    def test_peaks_search_parameters_no_file(self):

        self.helpers.create_identification()

        out = StringIO()
        call_command('peaks_search_parameters', stdout=out)
