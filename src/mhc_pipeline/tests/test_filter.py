"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries

# Third party libraries

# Import project libraries
from .base import BaseTest
import mhc_pipeline.library.filter as f
from ..models import Filter


class Test(BaseTest):

    def test_create_filter_no_name(self):
        self.assertEqual(f.create_filter(dict(), None), None)

    def test_create_filter(self):
        fc = {
            'filter_name': 'hey'
        }

        self.assertEqual(f.create_filter(fc, None), None)

    def test_create_filter_mhc_allele(self):
        self.helpers.create_mhc_allele()

        fc = {
            'filter_name': 'hey',
            'mhc_allele': '|1|'
        }

        self.assertIsInstance(f.create_filter(fc, self.user), Filter)

    def test_filter_json(self):
        fc = {
            'identification': [],
            'immunopeptidome': [],
            'ref_url': '',
        }

        out = f.filter_json(fc)

        self.assertIsInstance(out, str)
