"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json
from unittest.mock import Mock

# Import Django related libraries
from django.core.exceptions import ValidationError
from django.conf import settings

# Third party libraries
import requests_mock

# Import project libraries
from .base import BaseTest
import mhc_pipeline.forms as forms
from ..models import ExperimentDetails, Identification


class Test(BaseTest):

    def test_AnnotateForm_empty(self):

        self.helpers.create_annotation()

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '',
            }]

            m.get(url + '/personalized_proteomes/?ids=1',
                  json=data)

            form = forms.AnnotateForm(personalized_proteome=[1])
            self.assertFalse(form.is_valid())

    def test_AnnotateForm_esp(self):

        self.helpers.create_annotation(type_='esp')

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '',
            }]

            m.get(url + '/personalized_proteomes/?ids=1',
                  json=data)

            form = forms.AnnotateForm(personalized_proteome=[1])
            self.assertFalse(form.is_valid())

    def test_AnnotateForm_hpa(self):

        self.helpers.create_annotation(type_='hpa')

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '',
            }]

            m.get(url + '/personalized_proteomes/?ids=1',
                  json=data)

            form = forms.AnnotateForm(personalized_proteome=[1])
            self.assertFalse(form.is_valid())

    def test_AnnotateForm_else(self):

        self.helpers.create_annotation(type_='else')

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '',
            }]

            m.get(url + '/personalized_proteomes/?ids=1',
                  json=data)

            form = forms.AnnotateForm(personalized_proteome=[1])
            self.assertFalse(form.is_valid())

    def test_AnnotateForm_clean_dbsnp(self):

        self.helpers.create_annotation(type_='dbsnp')
        self.helpers.create_annotation(type_='dbsnp')

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '',
            }]

            m.get(url + '/personalized_proteomes/?ids=1',
                  json=data)

            values = {
                'annotation_dbsnp_1': True,
                'annotation_dbsnp_2': True,
            }

            form = forms.AnnotateForm(values, personalized_proteome=[1])
            self.assertFalse(form.is_valid())

    def test_AnnotateForm_clean_maf(self):

        self.helpers.create_annotation(type_='esp')

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '',
            }]

            m.get(url + '/personalized_proteomes/?ids=1',
                  json=data)

            values = {
                'annotation_esp_1': True,
            }

            form = forms.AnnotateForm(values, personalized_proteome=[1])
            self.assertFalse(form.is_valid())

    def test_BiologicalSampleFileSetForm_empty(self):
        form = forms.BiologicalSampleFileSetForm()

        self.assertFalse(form.is_valid())

    def test_BiologicalSampleFileSetForm_set_classes(self):
        form = forms.BiologicalSampleFileSetForm(set_classes=['test'])

        self.assertFalse(form.is_valid())

    def test_CommentForm_empty(self):
        form = forms.CommentForm()

        self.assertFalse(form.is_valid())

    def test_ExperimentsDetailsForm_empty(self):
        form = forms.ExperimentDetailsForm()

        self.assertFalse(form.is_valid())

    def test_ExperimentsDetailsForm_save(self):
        biological_sample = self.helpers.create_biological_sample()

        experiment_details = self.helpers.create_experiment_details(
            biological_sample=biological_sample)

        form_data = {'name': 'test', }
        form = forms.ExperimentDetailsForm(data=form_data,
                                           instance=experiment_details,
                                           submit=True)
        obj = form.save()

        self.assertEqual(ExperimentDetails.objects.count(), 1)
        self.assertIsInstance(obj, ExperimentDetails)

    def test_FilterIdentificationForm_clean_filter_name(self):
        name = 'FilterTest'

        values = {
            'filter_name': name
        }

        form = forms.FilterIdentificationForm(values)
        form.is_valid()

        self.assertEqual(form.clean_filter_name(), name)

    def test_FilterIdentificationForm_clean_immunopeptidome_name(self):
        obj = self.helpers.create_immunopeptidome()

        form_mock = Mock(cleaned_data={'immunopeptidome_name': obj.name},
                         extensions=['.fa'])

        with self.assertRaises(ValidationError):
            forms.FilterIdentificationForm.clean_immunopeptidome_name(form_mock)

    def test_FilterIdentificationForm_clean_immunopeptidome_name_empty(self):
        obj = self.helpers.create_immunopeptidome()

        form_mock = Mock(data={'submit': 'Create'},
                         cleaned_data={'immunopeptidome_name': ''},
                         extensions=['.fa'])

        with self.assertRaises(ValidationError):
            forms.FilterIdentificationForm.clean_immunopeptidome_name(form_mock)

    def test_FilterIdentificationForm_clean_filter_name_duplicate(self):
        name = 'FilterTest'
        self.helpers.create_filter(name=name)

        values = {
            'filter_name': name
        }

        form = forms.FilterIdentificationForm(values)

        self.assertFalse(form.is_valid())

    def test_FilterIdentificationForm_clean_score_mhc_fdr_invalid_fdr(self):
        values = {
            'score_mhc_fdr': 10,
        }

        form = forms.FilterIdentificationForm(values)

        self.assertFalse(form.is_valid())

    def test_FilterIdentificationForm_clean_score_mhc_fdr_no_netmhc(self):
        values = {
            'score_mhc_fdr': 0.1,
        }

        form = forms.FilterIdentificationForm(values)

        self.assertFalse(form.is_valid())

    def test_FilterIdentificationForm_clean_score_mhc_fdr_valid(self):
        values = {
            'score_mhc_fdr': 0.1,
            'mhc_predictor': 'NETMHC34',
            'affinity_threshold': '2'
        }

        form = forms.FilterIdentificationForm(values)

        self.assertTrue(form.is_valid())

    def test_FilterIdentificationForm_empty(self):
        form = forms.FilterIdentificationForm()

        self.assertFalse(form.is_valid())

    def test_FilterIdentificationForm_files(self):
        values = {
            'files': 'File'
        }

        form = forms.FilterIdentificationForm(values)

        self.assertFalse(form.is_valid())

    def test_FilterIdentificationForm_mhc_alleles(self):
        self.helpers.create_mhc_allele()
        form = forms.FilterIdentificationForm()

        self.assertTrue(form.fields['mhc_allele'])

        values = {
            'mhc_allele': 'HLA-A*02:01'
        }

        form = forms.FilterIdentificationForm(values)

        self.assertTrue(form.is_valid())

    def test_MHCAlleleForm_empty(self):
        form = forms.MHCAlleleForm()

        self.assertFalse(form.is_valid())

    def test_AlternateLabelModelMultipleChoiceField(self):
        identification = self.helpers.create_identification()
        qs = Identification.objects.all()
        field = forms.AlternateLabelModelMultipleChoiceField(queryset=qs)

        self.assertIsInstance(field.label_from_instance(identification), str)

    def test_PermissionGroupForm_empty(self):
        form = forms.PermissionGroupForm()

        self.assertFalse(form.is_valid())

    def test_PermissionUpdateForm_empty(self):
        form = forms.PermissionUpdateForm()

        self.assertFalse(form.is_valid())

    def test_PermissionUserForm_empty(self):
        form = forms.PermissionUserForm()

        self.assertFalse(form.is_valid())

    def test_RefGenomeUploadForm(self):
        form = forms.RefGenomeUploadForm()

        self.assertFalse(form.is_valid())

    def test_PPUploadForm(self):
        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'name': '',
                'genome': '',
                'genome_name': 'Genome Name',
                'url': '',
                'import_status': 'finished',
                'trashed': False
            }]

            m.get(url + '/snps/?ids=1', json=data)

            form = forms.PPUploadForm(snps_ids=[1])
            self.assertFalse(form.is_valid())

    def test_SNPSUploadForm(self):
        data = [{
            'id': 1,
            'name': '',
            'species': '',
            'url': '',
        }]

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            m.get(url + '/genomes/?trashed=false',
                  json=data)

            form = forms.SNPSUploadForm()
            self.assertFalse(form.is_valid())

    def test_UploadedFileForm_empty(self):
        form = forms.UploadedFileForm(extensions=['.csv'])

        self.assertFalse(form.is_valid())

    def test_UploadedFileForm_clean_file(self):
        file_mock = Mock()
        file_mock.configure_mock(name='test.txt')

        form_mock = Mock(cleaned_data={'file': file_mock},
                         extensions=['.fa'])

        with self.assertRaises(ValidationError):
            forms.UploadedFileForm.clean_file(form_mock)

        form_mock = Mock(cleaned_data={'file': file_mock},
                         extensions=['.txt'])

        self.assertIsInstance(forms.UploadedFileForm.clean_file(form_mock),
                              Mock)

    def test_UserGroupForm(self):
        form = forms.UserGroupForm(user_groups=self.user.groups.all())
        self.assertFalse(form.is_valid())

    def test_MiHAFilterForm(self):

        form = forms.MiHAFilterForm(documents=list())
        self.assertFalse(form.is_valid())

        values = {
            'immunopeptidome_name': 'Test'
        }

        form = forms.MiHAFilterForm(values, documents=list())
        self.assertFalse(form.is_valid())

        self.helpers.create_immunopeptidome(name='Test')
        form = forms.MiHAFilterForm(values, documents=list())
        self.assertFalse(form.is_valid())

    def test_MiHAFilterForm_valid(self):

        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view', 'change'])
        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)
        obj.state = 'success'

        values = {
            'annotation': ['Human protein atlas tissue RNA expression',
                           'TCGA AML expression', 'gnomAD']
        }
        obj.json_values = json.dumps(values)
        obj.save()

        values = {
            'document': 1,
            'maf': 0.05,
        }
        form = forms.MiHAFilterForm(values, documents=[(1, 1)])

        self.assertTrue(form.is_valid())
