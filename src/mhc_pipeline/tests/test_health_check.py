"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import os
from unittest.mock import patch

# Import Django related libraries
from django.test import override_settings

# Third party libraries
import requests_mock

# Import project libraries
from .base import BaseTest
import mhc_pipeline.health_check as hc


class HealthCheckTest(BaseTest):
    def test_mixin_check(self):
        with self.assertRaises(NotImplementedError):
            hc.BackendMixIn.check(None)

    def test_mixin_identifier(self):
        backend = hc.Media()

        self.assertIsInstance(backend.identifier(), str)

    def test_Media(self):
        b = hc.Media()

        self.assertEqual(b.check_status(), None)

    def test_Media_false(self):
        b = hc.Media()

        def return_false(x):
            return False

        with self.assertRaises(hc.ServiceUnavailable), patch('os.listdir',
                                                             return_false):
            b.check_status()

    def test_Media_exception(self):
        b = hc.Media()

        def return_exception(x):
            raise BlockingIOError

        with self.assertRaises(hc.ServiceUnavailable), patch('os.listdir',
                                                             return_exception):
            b.check_status()

    def test_MHCFlurryBackend(self):
        os.environ['MHCFLURRY_URL'] = 'http://localhost'

        b = hc.MHCFlurryBackend()

        module_mock = 'library.mhcflurry.MHCFlurry.alive_check'
        with self.assertRaises(hc.ServiceUnavailable), patch(module_mock,
                                                             lambda x: False):
            b.check_status()

        with patch(module_mock, lambda x: True):
            self.assertEqual(b.check_status(), None)

    @override_settings(AWS_S3_ENDPOINT_URL='http://localhost')
    def test_Minio(self):
        with requests_mock.Mocker() as m:

            m.get('http://localhost/minio/health/ready')

            hc.Minio().check()

    def test_MSData(self):
        b = hc.MSData()

        self.assertEqual(b.check_status(), None)

    def test_NetMHCBackend(self):
        os.environ['NETMHC_URL'] = 'http://localhost'

        b = hc.NetMHCBackend()

        module_mock = 'library.netmhc.NetMHC.alive_check'
        with self.assertRaises(hc.ServiceUnavailable), patch(module_mock,
                                                          lambda x: False):
            b.check_status()

        with patch(module_mock, lambda x: True):
            self.assertEqual(b.check_status(), None)

    def test_PPS(self):
        b = hc.PPS()

        module_mock = 'mhc_pipeline.library.pps.alive'
        with self.assertRaises(hc.ServiceUnavailable), patch(module_mock,
                                                          lambda: False):
            b.check_status()

    def test_SharedData(self):
        b = hc.SharedData()

        self.assertEqual(b.check_status(), None)

    def test_HealthCheckExceptionCustom(self):

        value = str(hc.HealthCheckExceptionCustom('service', 'msg'))
        self.assertIsInstance(value, str)