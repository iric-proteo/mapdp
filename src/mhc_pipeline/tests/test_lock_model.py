"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import time
# Import Django related libraries

# Third party libraries

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from mhc_pipeline.library.lock_model import ModelLocker
from ..models import LockModel


class LockModelTest(BaseTest):
    def test_clean_expired_lock(self):
        self.helpers.create_lock_model()

        self.assertEqual(LockModel.objects.count(), 1)

        time.sleep(1)
        ModelLocker.clean_expired_locks(minutes=0.0001)

        self.assertEqual(LockModel.objects.count(), 0)

    def test_acquire_locks(self):
        ModelLocker(['lockmodel'], 1, 1)

        self.assertEqual(LockModel.objects.count(), 1)

        with self.assertRaises(BaseSiteException):
            ModelLocker(['lockmodel'], 1, 1)

    def test_release_locks(self):
        locker = ModelLocker(['lockmodel'], 1, 1)
        locker.release_locks()

        self.assertEqual(LockModel.objects.count(), 0)

    def test_acquire_locks_context_manager(self):
        with ModelLocker(['lockmodel'], 1, 1) as locker:

            self.assertEqual(LockModel.objects.count(), 1)
        self.assertEqual(LockModel.objects.count(), 0)
