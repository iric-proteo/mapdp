"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from unittest.mock import Mock

# Import Django related libraries
from django.db.models.query import QuerySet

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..lookups import *


class Test(BaseTest):

    def test_identificiation_get_query(self):
        request = Mock()

        value = IdentificationLookup().get_query('', request)

        self.assertIsInstance(value, QuerySet)

    def test_identification_format_item_display(self):

        item = self.helpers.create_identification()

        value = IdentificationLookup().format_item_display(item)

        self.assertIsInstance(value, str)

    def test_identification_format_match(self):

        item = self.helpers.create_identification()

        value = IdentificationLookup().format_match(item)

        self.assertIsInstance(value, str)

    def test_immunopeptidome_get_query(self):
        request = Mock()

        value = ImmunopeptidomeLookup().get_query('', request)

        self.assertIsInstance(value, list)

    def test_mhcallele_check_auth(self):

        user = Mock(is_authenticated=False)
        request = Mock(user=user)

        with self.assertRaises(PermissionDenied):
            MHCAlleleLookup().check_auth(request)

    def test_mhcallele_get_query(self):

        value = MHCAlleleLookup().get_query('', None)

        self.assertIsInstance(value, QuerySet)

    def test_mhcallele_format_item_display(self):

        item = Mock(name='test')

        value = MHCAlleleLookup().format_item_display(item)

        self.assertIsInstance(value, str)
