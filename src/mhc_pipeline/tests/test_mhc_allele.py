"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries

# Third party libraries
import requests_mock

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from library.mhc_allele import URL
import mhc_pipeline.library.mhc_allele as ma
from ..models import MHCAllele


class Test(BaseTest):
    def test_get_biological_sample_mhc_allele(self):
        self.helpers.create_biological_sample_mhc_allele()

        alleles = MHCAllele.objects.all()
        queryset = ma.get_biological_sample_mhc_allele(
            1, selected_alleles=alleles)
        self.assertEqual(len(queryset), 1)

    def test_get_biological_sample_mhc_allele_does_not_exist(self):
        self.helpers.create_biological_sample()

        with self.assertRaises(BaseSiteException):
            ma.get_biological_sample_mhc_allele(1, None)

    def test_get_biological_sample_mhc_allele_names(self):
        self.helpers.create_biological_sample_mhc_allele()

        queryset = ma.get_biological_sample_mhc_allele_names(1)
        self.assertEqual(len(queryset), 1)

    def test_get_biological_sample_mhc_allele_names_paired(self):
        obj = self.helpers.create_biological_sample_mhc_allele()
        obj.mhc_allele.add(self.helpers.create_mhc_allele(gene='DQA1',
                                                          name='DQA1*01:01'))
        obj.mhc_allele.add(self.helpers.create_mhc_allele(gene='DQB1',
                                                          name='DQB1*02:01'))

        alleles = MHCAllele.objects.all()
        queryset = ma.get_biological_sample_mhc_allele_names_paired(
            1, selected_alleles=alleles)
        self.assertEqual(len(queryset), 2)

        self.assertEqual(queryset[1], 'DQA101:01DQB102:01')

    def test_get_used_mhc_alleles(self):
        self.helpers.create_mhc_allele(name='Test')
        self.helpers.create_biological_sample_mhc_allele()
        alleles = ma.get_used_mhc_alleles()

        self.assertEqual(alleles.count(), 1)

    def test_store_mhc_alleles(self):
        alleles = ['HLA-A*26:03', 'HLA-A*68:02']
        ma.store_mhc_alleles(mhc_alleles=alleles)

        self.assertEqual(MHCAllele.objects.count(), len(alleles))

    def test_store_mhc_alleles_classes(self):
        alleles = ['HLA-A*26:03', 'HLA-A*68:02']
        ma.store_mhc_alleles(mhc_alleles=alleles, mhc_classes=['B'])

        self.assertEqual(MHCAllele.objects.count(), 0)

    def test_store_mhc_alleles_classes_none(self):

        with requests_mock.Mocker() as m:

            data = '# file: hla_nom_p.txt\nA*;01:02;'
            m.get(URL, text=data)

            ma.store_mhc_alleles()

        self.assertEqual(MHCAllele.objects.count(), 1)
