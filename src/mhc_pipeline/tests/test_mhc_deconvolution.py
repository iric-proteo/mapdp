"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from pathlib import Path
import shutil
import tempfile
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings

# Third party libraries

# Import project libraries
from .base import BaseTest
import mhc_pipeline.library.mhc_deconvolution as mhc_deconvolution
import mhc_pipeline.models as models


class Test(BaseTest):

    def test_deconvolution_mixmhcp(self):

        peptide = self.helpers.create_peptide()
        peptides = models.Peptide.objects.all()

        filter_ = {
            'deconvolution_cluster_number': 1,
            'deconvolution_software': 'MixMHCp',
            'identification_key': 'test1'
        }

        work_dir = Path(settings.NEXTFLOW_FS) / 'deconvolution'
        file_dir = (work_dir / 'test1' / 'results' /
                    'deconvolution' / 'responsibility')
        shutil.copytree('./test_files/mixmhcp', file_dir)

        with patch('subprocess.run') as p:
            df = mhc_deconvolution.deconvolution(peptides, filter_)
            self.assertEquals(df.shape[0], 2764)
            p.assert_called_once()

        with patch('subprocess.run') as p:
            shutil.copytree('./test_files/mixmhcp', file_dir)
            df = mhc_deconvolution.deconvolution(list(peptides), filter_)
            self.assertEquals(df.shape[0], 2764)
            p.assert_called_once()

    def test_deconvolution_modec(self):

        peptide = self.helpers.create_peptide()
        peptides = models.Peptide.objects.all()

        filter_ = {
            'deconvolution_cluster_number': 1,
            'deconvolution_software': 'MoDec',
            'identification_key': 'test2'
        }

        work_dir = Path(settings.NEXTFLOW_FS) / 'deconvolution'
        file_dir = (work_dir / 'test2' / 'results' /
                    'deconvolution' / 'Responsibilities')
        shutil.copytree('./test_files/modec', file_dir)

        with patch('subprocess.run') as p:
            df = mhc_deconvolution.deconvolution(peptides, filter_)
            self.assertEquals(df.shape[0], 5790)
            p.assert_called_once()

    def test_data_frame_mixmhcp(self):

        with tempfile.TemporaryDirectory() as tmp_dir:

            work_dir = Path(tmp_dir)
            file_dir = work_dir / 'results' / 'deconvolution' / 'responsibility'
            shutil.copytree('./test_files/mixmhcp', file_dir)

            df = mhc_deconvolution.data_frame_mixmhcp(work_dir, 1)
            self.assertEquals(df.shape[0], 2764)

    def test_data_frame_modec(self):

        with tempfile.TemporaryDirectory() as tmp_dir:

            work_dir = Path(tmp_dir)
            file_dir = (work_dir / 'results' / 'deconvolution' /
                        'Responsibilities')
            shutil.copytree('./test_files/modec', file_dir)

            df = mhc_deconvolution.data_frame_modec(work_dir, 1)
            self.assertEquals(df.shape[0], 5790)

    def test_filters_mixmhcp(self):
        with tempfile.TemporaryDirectory() as tmp_dir:
            work_dir = Path(tmp_dir)
            file_dir = work_dir / 'results' / 'deconvolution' / 'responsibility'
            shutil.copytree('./test_files/mixmhcp', file_dir)

            df = mhc_deconvolution.data_frame_mixmhcp(work_dir, 1)

            filter_ = {
                'deconvolution_remove_trash': True,
            }

            df_tmp = mhc_deconvolution.filters_mixmhcp(df, filter_)

            self.assertEquals(df_tmp.shape[0], 2561)

            filter_ = {
                'deconvolution_max_trash_responsibility': 0.1,
            }

            df_tmp = mhc_deconvolution.filters_mixmhcp(df, filter_)

            self.assertEquals(df_tmp.shape[0], 2459)

    def test_filters_modec(self):
        with tempfile.TemporaryDirectory() as tmp_dir:
            work_dir = Path(tmp_dir)
            file_dir = (work_dir / 'results' / 'deconvolution' /
                        'Responsibilities')
            shutil.copytree('./test_files/modec', file_dir)

            df = mhc_deconvolution.data_frame_modec(work_dir, 1)

            filter_ = {
                'deconvolution_remove_trash': True,
            }

            df_tmp = mhc_deconvolution.filters_modec(df, filter_)

            self.assertEquals(df_tmp.shape[0], 5611)

            filter_ = {
                'deconvolution_max_trash_responsibility': 0.1,
            }

            df_tmp = mhc_deconvolution.filters_modec(df, filter_)

            self.assertEquals(df_tmp.shape[0], 5324)

    def test_peptides_queryset(self):

        peptide = self.helpers.create_peptide()
        peptides = models.Peptide.objects.all()

        queryset = mhc_deconvolution.peptides_queryset(peptides, 'MoDec')
        self.assertEquals(queryset.count(), 0)

        queryset = mhc_deconvolution.peptides_queryset(peptides, 'Else')
        self.assertEquals(queryset.count(), 1)

    def test_peptides_selection(self):

        peptide = self.helpers.create_peptide()
        peptides = list(models.Peptide.objects.all())

        selected_peptides = mhc_deconvolution.peptides_selection(peptides,
                                                                 'MoDec')
        self.assertEquals(len(selected_peptides), 0)

        selected_peptides = mhc_deconvolution.peptides_selection(peptides,
                                                                 'Else')
        self.assertEquals(len(selected_peptides), 1)

        peptide.is_decoy = True
        peptide.save()

        peptides = list(models.Peptide.objects.all())
        selected_peptides = mhc_deconvolution.peptides_selection(peptides,
                                                                 'Else')
        self.assertEquals(len(selected_peptides), 0)
