"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os
from unittest.mock import patch

# Import Django related libraries
from django.db.models.query import QuerySet

# Third party libraries
import pandas as pd
from requests.exceptions import ConnectionError
import requests_mock

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
import mhc_pipeline.library.mhc_prediction as mhc_prediction
import mhc_pipeline.models as models


class Test(BaseTest):
    def test_bin_peptides_per_alleles(self):
        peptide = self.helpers.create_peptide()
        bs = peptide.identification.biological_sample
        self.helpers.create_biological_sample_mhc_allele(biological_sample=bs)

        ignore_sample_alleles = True
        peptides = models.Peptide.objects.all()
        peptides = mhc_prediction.peptides_queryset(peptides, 'Other')
        selected_alleles = models.MHCAllele.objects.all()

        allele_peptides, sequences = mhc_prediction.bin_peptides_per_alleles(
            peptides, selected_alleles, ignore_sample_alleles)

        self.assertIsInstance(allele_peptides, dict)
        self.assertIsInstance(sequences, dict)

        ignore_sample_alleles = False
        allele_peptides, sequences = mhc_prediction.bin_peptides_per_alleles(
            peptides, selected_alleles, ignore_sample_alleles)

    def test_data_frame(self):
        pred = {
            'peptide_id': 1,
            'affinity': 1,
            'mhc_allele__name': 'Allele'
        }

        self.assertIsInstance(mhc_prediction.data_frame([pred]), pd.DataFrame)

    def test_data_frame_empty(self):
        with self.assertRaises(BaseSiteException):
            mhc_prediction.data_frame([])

    def test_data_frame_rank(self):
        pred = {
            'peptide_id': 1,
            'affinity': 1,
            'rank': 1,
            'rank_el': 1,
            'mhc_allele__name': 'Allele'
        }

        self.assertIsInstance(mhc_prediction.data_frame([pred]), pd.DataFrame)

    def test_filters(self):
        predictions = {
            'peptide_id': 1,
            'affinity': 1,
            'rank': 1,
            'mhc_allele__name': 'Allele'
        }

        df = mhc_prediction.data_frame([predictions])

        fc = {
            'affinity_threshold': 10,
            'rank_threshold': '',
        }

        df = mhc_prediction.filters(df, fc, 'NETMHC34')

        self.assertEqual(df.shape, (1, 6))

        fc = {
            'affinity_threshold': 0.1,
            'rank_threshold': '',
        }

        df = mhc_prediction.filters(df, fc, 'NETMHC34')

        self.assertEqual(df.shape, (0, 6))

        df = mhc_prediction.data_frame([predictions])

        fc = {
            'affinity_threshold': '',
            'rank_threshold': 1,
        }

        df = mhc_prediction.filters(df, fc, 'NETMHC34')

        self.assertEqual(df.shape, (1, 6))

        fc = {
            'affinity_threshold': '',
            'rank_threshold': 0.1,
        }

        df = mhc_prediction.filters(df, fc, 'NETMHC34')

        self.assertEqual(df.shape, (0, 6))

        predictions = {
            'peptide_id': 1,
            'affinity': 1,
            'mhc_allele__name': 'Allele'
        }

        df = mhc_prediction.data_frame([predictions])

        with self.assertRaises(BaseSiteException):
            mhc_prediction.filters(df, fc, 'NETMHC34')

        fc = {
            'affinity_threshold': '',
            'rank_el_threshold': 0.1,
        }

        df = mhc_prediction.data_frame([predictions])

        with self.assertRaises(BaseSiteException):
            mhc_prediction.filters(df, fc, 'NETMHC34')

        predictions = {
            'peptide_id': 1,
            'affinity': 1,
            'rank_el': 1,
            'mhc_allele__name': 'Allele'
        }

        df = mhc_prediction.data_frame([predictions])
        df = mhc_prediction.filters(df, fc, 'NETMHCpan41')

        self.assertEqual(df.shape[0], 0)

    def test_format_predictions(self):
        obj = self.helpers.create_biological_sample_mhc_allele()
        allele = obj.mhc_allele.all()[0]

        sequences = {
            'PEPTIDE': [[1, 1]]
        }

        predictions = [
            {
                'affinity': 1,
                'peptide': 'PEPTIDE',
                'rank': 1,
                'rank_el': 1,
            }
        ]

        params = {
            'allele': allele,
            'ignore_sample_alleles': True
        }
        alleles = models.MHCAllele.objects.all()

        args = [predictions, params]

        predictions_complete = list()
        mhc_prediction.format_predictions(args, sequences=sequences,
                                          predictions_complete=predictions_complete,
                                          selected_alleles=None)

        self.assertEquals(len(predictions_complete), 1)

    def test_jobs_params(self):
        allele = self.helpers.create_mhc_allele()

        classified_peptides = {
            allele: [''],
        }

        self.assertEqual(
            len(list(mhc_prediction.jobs_params(
                classified_peptides, '', False))), 1)

    def test_jobs_params_mhcflurry(self):
        allele = self.helpers.create_mhc_allele()

        classified_peptides = {
            allele: [''],
        }

        self.assertEqual(
            len(list(mhc_prediction.jobs_params(
                classified_peptides, 'mhcflurry_2.0.1', False))), 1)

    def test_get_affinity(self):
        df = pd.DataFrame()

        self.assertEqual(mhc_prediction.get_affinity(df, 1, ''), None)

    def test_get_predictions(self):
        url = 'http://localhost'
        os.environ['NETMHC_URL'] = url

        data = 'Thursday November  9 2017 14:01\n\n\nNetMHC version 3.4. 9mer predictions using Artificial Neural Networks - Direct. Allele HLA-A01:01. \nStrong binder threshold  50 nM. Weak binder threshold score 500 nM\n\n\n\n----------------------------------------------------------------------------------------------------\n pos    peptide      logscore affinity(nM) Bind Level    Protein Name     Allele\n----------------------------------------------------------------------------------------------------\n   0  CEFREVTVH         0.077        21674                   Peptides HLA-A01:01\n   1  PYSAIFATV         0.075        22256                   Peptides HLA-A01:01\n   2  WPTNYHHVQ         0.079        21331                   Peptides HLA-A01:01\n   3  RWHRKVVKI         0.078        21539                   Peptides HLA-A01:01\n   4  DSWTRDWDF         0.086        19706                   Peptides HLA-A01:01\n--------------------------------------------------------------------------------------------------\n\n'.encode()

        peptide = self.helpers.create_peptide()
        bs = peptide.identification.biological_sample
        identification = peptide.identification
        self.helpers.create_biological_sample_mhc_allele(biological_sample=bs)

        peptides = 'CEFREVTVH PYSAIFATV WPTNYHHVQ RWHRKVVKI DSWTRDWD'.split(' ')
        for peptide in peptides:
            self.helpers.create_peptide(peptide_sequence=peptide,
                                        identification=identification)

        peptides = models.Peptide.objects.all()
        peptides = mhc_prediction.peptides_queryset(peptides, 'Other')

        def get_predictions(x, z):

            z.keywords['predictions_complete'].append({
                'peptide_id': 1,
                'affinity': 1,
                'mhc_allele__name': 'Allele'
            })

        with requests_mock.Mocker() as m, \
                patch('library.netmhc.NetMHC.queue_jobs', get_predictions):
            m.post(url + '/predict', content=data)

            fc = {
                'affinity_threshold': '',
                'ignore_sample_alleles': True,
                'mhc_allele': [1],
                'rank_threshold': '',
                'mhc_predictor': 'NETMHC34'
            }

            out = mhc_prediction.get_predictions(peptides, fc)

            self.assertIsInstance(out, pd.DataFrame)

        def raise_exception(x, y):
            raise ConnectionError

        with patch('library.netmhc.NetMHC.queue_jobs', raise_exception), \
             self.assertRaises(BaseSiteException):
            mhc_prediction.get_predictions(peptides, fc)

    def test_process_predictions(self):
        obj = self.helpers.create_biological_sample_mhc_allele()
        allele = obj.mhc_allele.all()[0]

        sequences = {
            'PEPTIDE': [[1, 1]]
        }

        predictions = [
            {
                'affinity': 1,
                'peptide': 'PEPTIDE',
                'rank': 1,
            }
        ]

        params = {
            'allele': allele,
            'ignore_sample_alleles': False
        }
        alleles = models.MHCAllele.objects.all()

        args = [predictions, params]

        out = []
        mhc_prediction.format_predictions(args, sequences=sequences,
                                          predictions_complete=out,
                                          selected_alleles=alleles)

        self.assertEqual(len(out), 0)

        params = {
            'allele': allele,
            'ignore_sample_alleles': True
        }

        args = [predictions, params]

        mhc_prediction.format_predictions(args, sequences=sequences,
                                          predictions_complete=out,
                                          selected_alleles=alleles)

        self.assertEqual(len(out), 1)

    def test_process_predictions_run_time_error(self):
        obj = self.helpers.create_biological_sample_mhc_allele()
        allele = obj.mhc_allele.all()[0]

        sequences = {
            'PEPTIDE': [[1, 1]]
        }

        predictions = [
            {
                'affinity': 1,
                'peptide': 'PEPTIDE',
                'rank': 1,
            }
        ]

        params = {
            'allele': allele,
            'ignore_sample_alleles': False
        }

        args = [predictions, params]
        out = []

        def raise_exception(x, selected_alleles=None):
            raise RuntimeError

        alleles = models.MHCAllele.objects.all()

        target = 'mhc_pipeline.library.mhc_allele.get_biological_sample_mhc_allele_names'
        with patch(target, raise_exception), self.assertRaises(
                BaseSiteException):
            mhc_prediction.format_predictions(args, sequences=sequences,
                                              predictions_complete=out,
                                              selected_alleles=alleles)

        def raise_exception_msg(x, selected_alleles=None):
            raise RuntimeError('cannot be found in hla_list')

        with patch(target, raise_exception_msg):
            self.assertEqual(mhc_prediction.format_predictions(
                args, sequences=sequences, predictions_complete=out,
                selected_alleles=alleles), None)

    def test_peptides_queryset(self):
        peptides = models.Peptide.objects.all()

        self.assertIsInstance(mhc_prediction.peptides_queryset(peptides,
                                                               'NETMHCIIpan31'),
                              QuerySet)

        self.assertIsInstance(mhc_prediction.peptides_queryset(peptides,
                                                               'Other'),
                              QuerySet)
