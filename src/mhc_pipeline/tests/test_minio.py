"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from unittest import TestCase

# Import Django related libraries
from django.core.files.storage import DefaultStorage
from django.test.utils import override_settings

# Third party libraries

# Import project libraries
from ..library.minio import get_storage, MinioStorage

# Create your tests here.


class Test(TestCase):

    def test_default_storage(self):

        storage = get_storage()

        self.assertEqual(type(storage), DefaultStorage)

    @override_settings(AWS_S3_ENDPOINT_URL='test', AWS_BUCKET_ACL=None,
                       AWS_DEFAULT_ACL=None)
    def test_minio_storage(self):

        storage = get_storage()
        storage._setup()

        self.assertEqual(type(storage), MinioStorage)
