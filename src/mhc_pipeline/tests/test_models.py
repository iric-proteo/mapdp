"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import os
from pathlib import Path

# Import Django related libraries
from django.conf import settings
from django.core.cache import caches
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone

# Third party libraries

# Import project libraries
from .base import BaseTest
import mhc_pipeline.models as models
from ..library.permission import set_content_permission


class Test(BaseTest):
    """
    Test for models.
    """

    def test_Annotation_file_path(self):
        obj = self.helpers.create_annotation()

        self.assertIsInstance(obj.file_path, Path)

    def test_BiologicalSample_creation(self):
        obj = self.helpers.create_biological_sample()

        self.assertEqual(models.BiologicalSample.objects.count(), 1)
        self.assertIsInstance(obj, models.BiologicalSample)

    def test_BiologicalSample_str(self):
        obj = self.helpers.create_biological_sample()

        self.assertIsInstance(str(obj), str)

    def test_BiologicalSample_url(self):
        obj = self.helpers.create_biological_sample()
        url = reverse('biological_sample:view', kwargs={'pk': obj.pk})

        self.assertEqual(obj.get_absolute_url(), url)

    def test_BiologicalSampleManager_for_user(self):
        self.helpers.create_biological_sample()

        self.assertEqual(
            models.BiologicalSample.objects.for_user(self.user).count(), 0)

        self.helpers.create_biological_sample(identifier='test2',
                                              permissions=['view'])

        self.assertEqual(
            models.BiologicalSample.objects.for_user(self.user,
                                                     related=True
                                                     ).count(), 1)

    def test_BiologicalSampleFileSet_creation(self):
        obj = self.helpers.create_biological_sample_file_set(2)

        self.assertEqual(models.BiologicalSampleFileSet.objects.count(), 1)
        self.assertIsInstance(obj, models.BiologicalSampleFileSet)

    def test_BiologicalSampleMHCAllele_creation(self):
        obj = self.helpers.create_biological_sample_mhc_allele()

        self.assertEqual(models.BiologicalSampleMHCAllele.objects.count(), 1)
        self.assertIsInstance(obj, models.BiologicalSampleMHCAllele)

    def test_ExperimentDetails_creation(self):
        obj = self.helpers.create_experiment_details()

        self.assertEqual(models.ExperimentDetails.objects.count(), 1)
        self.assertIsInstance(obj, models.ExperimentDetails)

    def test_ExperimentDetails_get_absolute_url(self):
        obj = self.helpers.create_experiment_details(
            biological_sample=self.helpers.create_biological_sample(
                permissions=['change', 'view'])
        )

        url = obj.get_absolute_url()
        response = self.client.get(url)

        self.assertIsInstance(url, str)
        self.assertEqual(response.status_code, 200)

    def test_File_add_file(self):
        file_set = self.helpers.create_file_set()
        obj = models.File.add_file(file_set,
                                   os.path.join(settings.MS_DIR, 'fileset'),
                                   'test.raw')

        self.assertEqual(models.File.objects.count(), 1)
        self.assertIsInstance(obj, models.File)

    def test_File_creation(self):
        obj = self.helpers.create_file()

        self.assertEqual(models.File.objects.count(), 1)
        self.assertIsInstance(obj, models.File)

    def test_File_creation_full_path(self):
        obj = self.helpers.create_file()

        self.assertEqual(obj.full_path(), '-%s-' % os.sep)

    def test_FileSet_creation(self):
        obj = self.helpers.create_file_set()

        self.assertEqual(models.FileSet.objects.count(), 1)
        self.assertIsInstance(obj, models.FileSet)

    def test_FileSet_get_class_attribute(self):
        obj = self.helpers.create_file_set()

        self.assertEqual(obj.get_class_attribute('dir'), 'test_files')

    def test_FileSet_has_reference(self):
        identification_obj = self.helpers.create_identification()
        obj = identification_obj.file_set

        self.assertTrue(type(obj.has_reference) is bool)
        self.assertEqual(True, obj.has_reference)

    def test_FileSet_str(self):
        obj = self.helpers.create_file_set()

        self.assertEqual(str(obj), 'pk:1, name:Test')

    def test_Filter_creation(self):
        obj = self.helpers.create_filter()

        self.assertEqual(models.Filter.objects.count(), 1)
        self.assertIsInstance(obj, models.Filter)

    def test_Filter_get_absolute_url(self):
        obj = self.helpers.create_filter()

        self.assertTrue(type(obj.get_absolute_url()) is str)

    def test_Filter_str(self):
        obj = self.helpers.create_filter()

        self.assertEqual(str(obj), obj.name)

    def test_FilterManager_for_user(self):
        self.helpers.create_filter()

        self.assertEqual(models.Filter.objects.for_user(self.user).count(), 0)

        self.helpers.create_filter(name='for_user', permissions=['view'])

        self.assertEqual(models.Filter.objects.for_user(self.user).count(), 1)

    def test_FilterManager_visible_for_user(self):
        self.helpers.create_filter()

        self.assertEqual(
            models.Filter.objects.visible_for_user(self.user).count(), 0)

        self.helpers.create_filter(name='visible_for_user',
                                   permissions=['visible'])

        self.assertEqual(
            models.Filter.objects.visible_for_user(self.user).count(), 1)

    def test_Identification_create(self):
        self.helpers.create_identification()

        self.assertEqual(models.Identification.objects.count(), 1)

    def test_Identification_for_user_with_perm(self):
        obj = self.helpers.create_identification()
        set_content_permission(obj.biological_sample, self.user, ['view'])

        queryset = models.Identification.objects.for_user(self.user)
        self.assertEqual(queryset.count(), 1)

    def test_Identification_for_user_no_perm(self):
        self.helpers.create_identification()

        queryset = models.Identification.objects.for_user(self.user)
        self.assertEqual(queryset.count(), 0)

    def test_Identification_str(self):
        obj = self.helpers.create_identification()

        self.assertIsInstance(str(obj), str)

    def test_Identification_alternate_label(self):
        obj = self.helpers.create_identification()

        self.assertIsInstance(obj.alternate_label(), str)

    def test_ImmunoPeptidome_creation(self):
        obj = self.helpers.create_immunopeptidome()

        self.assertEqual(models.ImmunoPeptidome.objects.count(), 1)
        self.assertIsInstance(obj, models.ImmunoPeptidome)

    def test_ImmunoPeptidome_get_absolute_url(self):
        obj = self.helpers.create_immunopeptidome(permissions=['view'])

        url = obj.get_absolute_url()
        response = self.client.get(url)

        self.assertIsInstance(url, str)
        self.assertEqual(response.status_code, 200)

    def test_ImmunoPeptidome_str(self):
        obj = self.helpers.create_immunopeptidome()

        self.assertIsInstance(str(obj), str)

    def test_ImmunoPeptidome_for_user(self):
        self.helpers.create_immunopeptidome()

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user)

        self.assertEqual(len(queryset), 0)

    def test_ImmunoPeptidome_for_user_biological_sample_permission(self):

        obj = self.helpers.create_immunopeptidome()

        biological_sample = obj.biological_sample.all()[0]

        set_content_permission(biological_sample, self.user, ['view'])

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user)

        self.assertEqual(len(queryset), 1)

    def test_ImmunoPeptidome_for_user_two_biological_sample_permission(self):

        obj = self.helpers.create_immunopeptidome()

        biological_sample = obj.biological_sample.all()[0]
        biological_sample2 = self.helpers.create_biological_sample(
            identifier='test2')
        obj.biological_sample.add(biological_sample2)

        set_content_permission(biological_sample, self.user, ['view'])

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user)

        self.assertEqual(len(queryset), 0)

    def test_ImmunoPeptidome_for_user_two_biological_sample_ok_permission(self):

        obj = self.helpers.create_immunopeptidome()

        biological_sample = obj.biological_sample.all()[0]
        biological_sample2 = self.helpers.create_biological_sample(
            identifier='test2')
        obj.biological_sample.add(biological_sample2)

        set_content_permission(biological_sample, self.user, ['view'])
        set_content_permission(biological_sample2, self.user, ['view'])

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user)

        self.assertEqual(len(queryset), 1)

    def test_ImmunoPeptidome_for_user_immunopeptidome_permission(self):

        self.helpers.create_immunopeptidome(permissions=['view'])

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user)

        self.assertEqual(len(queryset), 1)

    def test_ImmunoPeptidome_for_user_both_permissions(self):

        obj = self.helpers.create_immunopeptidome(permissions=['view'])

        biological_sample = obj.biological_sample.all()[0]

        set_content_permission(biological_sample, self.user, ['view'])

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user)

        self.assertEqual(len(queryset), 1)

    def test_ImmunoPeptidome_for_user_extra(self):

        self.helpers.create_immunopeptidome(permissions=['view'])
        obj = models.ImmunoPeptidome.objects.create(
            created_by=self.user,
            modified_by=self.user,
            name='test',
            comments='Text',
            filter=self.helpers.create_filter(),
        )

        set_content_permission(obj, self.user, ['view'])

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user, related=True,
                                  reverse_sorted=True)

        self.assertTrue(queryset[0].pk > queryset[1].pk)

    def test_ImmunoPeptidome_for_user_filter(self):

        self.helpers.create_immunopeptidome(permissions=['view'])
        obj = models.ImmunoPeptidome.objects.create(
            created_by=self.user,
            modified_by=self.user,
            name='test',
            comments='Text',
            filter=self.helpers.create_filter(),
        )

        set_content_permission(obj, self.user, ['view'])

        model = models.ImmunoPeptidome.objects
        queryset = model.for_user(self.user, filter_=Q(pk=1))

        self.assertEqual(len(queryset), 1)

    def test_ImmunoPeptidome_get_for_user_no_permission(self):
        self.helpers.create_immunopeptidome()

        model = models.ImmunoPeptidome.objects

        with self.assertRaises(PermissionDenied):
            model.get_for_user(1, self.user, 'view', related=True)

    def test_ImmunoPeptidome_get_for_user_biological_sample_permission(self):
        obj = self.helpers.create_immunopeptidome()
        set_content_permission(obj.biological_sample.all()[0], self.user,
                               ['view'])

        model = models.ImmunoPeptidome.objects

        obj = model.get_for_user(1, self.user, 'view')

        self.assertIsInstance(obj, models.ImmunoPeptidome)

    def test_ImmunoPeptidome_get_for_user_immunopeptidome_permission(self):
        self.helpers.create_immunopeptidome(permissions=['view'])

        model = models.ImmunoPeptidome.objects

        obj = model.get_for_user(1, self.user, 'view')

        self.assertIsInstance(obj, models.ImmunoPeptidome)




    def test_ImmunopeptidomeDocument_file_path(self):
        obj = self.helpers.create_immunopeptidome_document()
        self.assertIsInstance(obj.file_path, Path)

    def test_LockModel_create(self):
        obj = self.helpers.create_lock_model()

        self.assertEqual(models.LockModel.objects.count(), 1)
        self.assertIsInstance(obj, models.LockModel)

    def test_MHCAllele_creation(self):
        obj = self.helpers.create_mhc_allele()

        self.assertEqual(models.MHCAllele.objects.count(), 1)
        self.assertIsInstance(obj, models.MHCAllele)

    def test_MHCAllele_str(self):
        obj = self.helpers.create_mhc_allele()

        self.assertEqual(str(obj), 'HLA-A*02:01')

    def test_MHCSample_creation(self):
        obj = self.helpers.create_biological_sample()

        self.assertEqual(models.MHCSample.objects.count(), 1)

    def test_model_path(self):
        self.assertEqual(models.model_path('a', 'b'), os.path.join('str', 'b'))

    def test_New_creation(self):
        obj = self.helpers.create_new()

        self.assertEqual(models.New.objects.count(), 1)
        self.assertIsInstance(obj, models.New)

    def test_Peptide_creation(self):
        obj = self.helpers.create_peptide()

        self.assertEqual(models.Peptide.objects.count(), 1)
        self.assertIsInstance(obj, models.Peptide)

    def test_Peptide_latest_validated_peptide_timestamp(self):
        caches['default'].clear()
        self.assertEqual(models.Peptide.latest_validated_peptide_timestamp(), 0)

        caches['default'].clear()
        obj = self.helpers.create_peptide()
        self.assertEqual(obj.latest_validated_peptide_timestamp(), 0)

        caches['default'].clear()

        obj.validated_at = timezone.now()
        obj.save()
        self.assertIsInstance(obj.latest_validated_peptide_timestamp(), float)

    def test_Peptide_modification_formatted(self):
        obj = self.helpers.create_peptide()

        self.assertEqual(obj.modification_formatted, 'Phospho-Y4')

        obj.modification = '""'

        self.assertEqual(obj.modification_formatted, '')

    def test_Peptide_modification_formatted_missing_residues(self):
        obj = self.helpers.create_peptide()
        obj.modification = '[{"name": "TMT6plex", "monoisotopicMassDelta"' \
                           ': 229.16293, "location": 0}]'

        self.assertEqual(obj.modification_formatted, 'TMT6plex-0')

    def test_Peptide_modification_formatted_missing_name(self):
        obj = self.helpers.create_peptide()
        obj.modification = '[{"unknown modification": "TMT6plex", ' \
                           '"monoisotopicMassDelta": 229.16293, "location": 0}]'

        self.assertEqual(obj.modification_formatted, 'TMT6plex-0')

    def test_ProteinDatabase_creation(self):
        obj = self.helpers.create_protein_database()

        self.assertEqual(models.ProteinDatabase.objects.count(), 1)
        self.assertIsInstance(obj, models.ProteinDatabase)

    def test_ProteinDatabase_str(self):
        obj = self.helpers.create_protein_database()

        self.assertEqual(type(obj.__str__()), str)

    def test_SNPs_str(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        obj = models.SNPs.objects.create(biological_sample=biological_sample,
                                         pps_snps_id=1)

        self.assertEqual(type(obj.__str__()), str)

    def test_Tag_str(self):
        obj = models.Tag.objects.create(name='MiHA')

        self.assertIsInstance(str(obj), str)

    def test_UniquePeptide_creation(self):
        obj = self.helpers.create_unique_peptide()

        self.assertEqual(models.UniquePeptide.objects.count(), 1)
        self.assertIsInstance(obj, models.UniquePeptide)

    def test_UniquePeptide_str(self):
        obj = self.helpers.create_unique_peptide()

        self.assertEqual(str(obj), 'pk:1, sequence:PEP')

    def test_UploadedFile_creation(self):
        obj = self.helpers.create_uploaded_file()

        self.assertEqual(models.UploadedFile.objects.count(), 1)
        self.assertIsInstance(obj, models.UploadedFile)
        self.assertTrue(os.path.exists(obj.file.path))

        os.remove(obj.file.path)
