"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import os
from pathlib import Path
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings

# Third party libraries
import pandas as pd

# Import project libraries
from .base import BaseTest
from ..library import peptide_count
from ..models import (PeptideBiologicalSampleCount,
                      PeptideCounts,
                      PeptideFileCount)

devnull = open(os.devnull, 'w')


class Test(BaseTest):

    def test_get_peptides(self):
        identification = self.helpers.create_identification()

        self.helpers.create_peptide(identification=identification,
                                    peptide_score=1,
                                    peptide_sequence='PEPYPEPYY',
                                    is_decoy=True)

        self.helpers.create_peptide(identification=identification,
                                    peptide_score=30,
                                    peptide_sequence='PEPYPEPYYY',
                                    is_decoy=False)

        with patch('sys.stdout', devnull):
            out = peptide_count.get_peptides(identification)

        self.assertIsInstance(out, tuple)

    def test_get_peptides_exception(self):
        identification = self.helpers.create_identification()

        with patch('sys.stdout', devnull):
            out = peptide_count.get_peptides(identification)

        self.assertIsInstance(out, tuple)

    def test_get_peptides_failed(self):
        identification = self.helpers.create_identification()
        identification.state = 'failed'

        with patch('sys.stdout', devnull):
            out = peptide_count.get_peptides(identification)

        self.assertIsInstance(out, tuple)

    def test_get_peptides_state(self):
        identification = self.helpers.create_identification()
        identification.state = 'pending'

        with patch('time.sleep', lambda x: x), patch('sys.stdout', devnull):
            out = peptide_count.get_peptides(identification)

        self.assertIsInstance(out, tuple)

    def test_Stats_init(self):
        stats = peptide_count.Stats()
        self.assertIn('unique_peptides', stats.__dict__)

    def test_peptide_count(self):
        identification = self.helpers.create_identification()
        allele = self.helpers.create_mhc_allele()

        stats = peptide_count.Stats()

        unique_peptides = {
            1: set([1]),
            2: set([1])
        }

        key = ('All', 0, 'All')
        pc = stats.peptide_counts(identification, unique_peptides, 'All', 0,
                                  'All')

        self.assertEqual(len(stats.unique_peptides[key]), 2)
        self.assertEqual(pc.individual_counts, 2)
        self.assertEqual(pc.cumulative_counts, 2)

        pc = stats.peptide_counts(identification, unique_peptides, 'All', 0,
                                  'All')

        self.assertEqual(len(stats.unique_peptides[key]), 2)
        self.assertEqual(pc.individual_counts, 2)
        self.assertEqual(pc.cumulative_counts, 2)

        unique_peptides = {
            2: set([1]),
            3: set([1])
        }

        pc = stats.peptide_counts(identification, unique_peptides, 'All', 0,
                                  'All')

        self.assertEqual(len(stats.unique_peptides[key]), 3)
        self.assertEqual(pc.individual_counts, 2)
        self.assertEqual(pc.cumulative_counts, 3)

        pc = stats.peptide_counts(identification, unique_peptides, 1, 0,
                                  allele.name)

        self.assertEqual(len(stats.unique_peptides[key]), 3)
        self.assertEqual(pc.individual_counts, 2)
        self.assertEqual(pc.cumulative_counts, 2)

        pc = stats.peptide_counts(identification, unique_peptides, 1, 9606,
                                  allele.name)

        self.assertEqual(len(stats.unique_peptides[key]), 3)
        self.assertEqual(pc.individual_counts, 2)
        self.assertEqual(pc.cumulative_counts, 2)

        unique_peptides = {
            2: set([2]),
            3: set([2])
        }

        pc = stats.peptide_counts(identification, unique_peptides, 1, 9606,
                                  'All')

        self.assertEqual(len(stats.unique_peptides[key]), 3)
        self.assertEqual(pc.individual_counts, 2)
        self.assertEqual(pc.cumulative_counts, 2)

        unique_peptides = {
            2: set([1]),
            3: set([2])
        }

        pc = stats.peptide_counts(identification, unique_peptides, 1, 9606,
                                  'All')

        key2 = (1, 9606, 'All')
        self.assertEqual(len(stats.unique_peptides[key]), 3)
        self.assertEqual(len(stats.unique_peptides[key][1]), 1)
        self.assertEqual(len(stats.unique_peptides[key2][2]), 2)
        self.assertEqual(pc.individual_counts, 2)
        self.assertEqual(pc.cumulative_counts, 2)

    def test_process_peptides(self):
        identification = self.helpers.create_identification()
        self.helpers.create_mhc_allele()

        pep1 = self.helpers.create_peptide(identification=identification,
                                           peptide_score=1,
                                           peptide_sequence='PEPYPEPYY',
                                           is_decoy=True)

        pep2 = self.helpers.create_peptide(identification=identification,
                                           peptide_score=30,
                                           peptide_sequence='PEPYPEPYYY',
                                           is_decoy=False)

        peptides = [pep1, pep2]

        data = {
            'mhc_predictor': {
                'affinity': pd.DataFrame([
                    {'HLA-A*02:01': 1, 'Minimum': 1,
                     'Minimum allele': 'HLA-A*02:01'},
                    {'HLA-A*02:01': 1, 'Minimum': 1,
                     'Minimum allele': 'HLA-A*02:01'}
                ])

            }

        }

        stats = peptide_count.Stats()

        stats.process_peptides(identification.pk, peptides, data)

        key = ('All', 0, 'All')

        self.assertEqual(len(stats.unique_peptides[key]), 2)

    def test_peptide_file_count(self):

        identification = self.helpers.create_identification()
        self.helpers.create_mhc_allele()

        stats = peptide_count.Stats()

        unique_peptides = {
            1: set([1]),
            2: set([1, 2])
        }

        stats.peptide_counts(identification, unique_peptides, 'All', 0, 'All')

        stats.peptide_file_counts()

        obj = PeptideFileCount.objects.get(pk=1)

        self.assertEqual(obj.file_counts, 1)
        self.assertEqual(obj.peptide_counts, 1)

    def test_peptide_biological_sample_counts(self):
        identification = self.helpers.create_identification()
        self.helpers.create_mhc_allele()

        pep1 = self.helpers.create_peptide(identification=identification,
                                           peptide_score=1,
                                           peptide_sequence='PEPYPEPYY',
                                           is_decoy=True)

        pep2 = self.helpers.create_peptide(identification=identification,
                                           peptide_score=30,
                                           peptide_sequence='PEPYPEPYYY',
                                           is_decoy=False)

        peptides = [pep1, pep2]

        data = {
            'mhc_predictor': {
                'affinity': pd.DataFrame([
                    {'HLA-A*02:01': 1, 'Minimum': 1,
                     'Minimum allele': 'HLA-A*02:01'},
                    {'HLA-A*02:01': 1, 'Minimum': 1,
                     'Minimum allele': 'HLA-A*02:01'}
                ])

            }

        }

        stats = peptide_count.Stats()

        stats.process_peptides(identification.pk, peptides, data)

        stats.peptide_biological_sample_counts()

        obj = PeptideBiologicalSampleCount.objects.get(pk=1)

        self.assertEqual(obj.biological_sample_counts, 1)

    def test_process(self):

        self.helpers.create_identification()

        with patch('sys.stdout', devnull):
            peptide_count.process()
            peptide_count.process()

        stats_path = Path(settings.MEDIA_ROOT) / 'stats'
        stats_file = stats_path / 'data.pickle'

        stats_file.unlink()
        stats_path.rmdir()

        obj = PeptideCounts.objects.get(pk=1)
        self.assertEqual(obj.individual_counts, 0)

