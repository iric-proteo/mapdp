"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from unittest.mock import patch

# Import Django related libraries
from django.db.models import Prefetch

# Third party libraries

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from mhc_pipeline.library import mhc_prediction
from ..library.minio import get_storage
from ..library import peptides_csv
from ..models import (Peptide,
                      PeptideGenome)


class Test(BaseTest):
    # Helpers
    def prep_data(self, case=1):

        if case == 2:
            data = {
                    'peptide_id': 100,
                    'affinity': 100,
                    'mhc_allele__name': 'Test'
                }
        else:
            data = {
                'peptide_id': 1,
                'affinity': 100,
                'mhc_allele__name': 'Test',
            }

        predictions = [data]

        df = mhc_prediction.data_frame(predictions)

        return {'mhc_predictor': df}

    # Tests
    def test_column_names(self):

        fc = {
            'protein_description': True,
            'mhc_predictor': 'NETMHC34',
        }

        data = self.prep_data()
        data['genome'] = True

        out = peptides_csv.column_names(fc, data=data)

        self.assertIsInstance(out, list)

    def test_csv(self):

        self.helpers.create_peptide()
        self.helpers.create_mhc_allele()

        filter_options = {
            'filter_json': '{"mhc_allele": ["1"]}',
            'identification': [1],
            'identification_key': 'key',
            'immunopeptidome': [1],
            'timestamp': '0',
            'url': '/999/',
            'validation_timestamp': ''
        }

        peptides_csv.csv(filter_options)

        storage = get_storage()

        self.assertTrue(storage.exists('key'))
        storage.delete('key')

        self.assertTrue(storage.exists('csv_key'))
        storage.delete('csv_key')

        def fake(a):
            raise BaseSiteException('test', 'test')

        with patch('mhc_pipeline.library.peptides_filter.PeptidesFilter.apply', fake):

            peptides_csv.csv(filter_options)

            self.assertTrue(storage.exists('key'))
            storage.delete('key')

    def test_file_name(self):

        filter_options = {
            'identification': [1],
            'immunopeptidome': [1],
            'timestamp': '0'
        }

        name = peptides_csv.file_name(filter_options)

        self.assertIsInstance(name , str)

    def test_format_peptides(self):

        peptide = self.helpers.create_peptide()
        peptide.q_value = None
        peptide.save()

        PeptideGenome.objects.create(unique_peptide_id=1,
                                     pp_id=1,
                                     mapped=False)

        peptides = Peptide.objects.all().select_related('unique_peptide')

        prefetch = Prefetch('unique_peptide__peptidegenome_set',
                            queryset=PeptideGenome.objects.filter(pp_id=1),
                            to_attr='genome')
        peptides = peptides.prefetch_related(prefetch)

        url = '/999'

        data = self.prep_data()
        data['genome'] = True
        filter_options = {
            'protein_description': True,
            'url': url
        }

        formatted_peptides = peptides_csv.format_peptides(peptides,
                                                          filter_options,
                                                          data=data)

        self.assertIsInstance(next(formatted_peptides), str)

        # KeyError check
        formatted_peptides = peptides_csv.format_peptides(peptides,
                                                          filter_options,
                                                          data=self.prep_data(2))

        self.assertIsInstance(next(formatted_peptides), str)

    def test_get_biological_sample_identifiers(self):
        self.helpers.create_immunopeptidome()

        fc = {
            'identification': [1],
            'immunopeptidome': [1],
        }

        out = peptides_csv.get_biological_sample_identifiers(fc)

        self.assertIsInstance(out, set)

    def test_get_protein_description(self):

        peptide = self.helpers.create_peptide()

        out = peptides_csv.get_protein_description(peptide)

        self.assertIsInstance(out, str)
