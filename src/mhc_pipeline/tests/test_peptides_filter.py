"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from unittest.mock import MagicMock, patch

# Import Django related libraries
from django.db.models.query import QuerySet

# Third party libraries

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from ..models import BiologicalSampleMHCAllele
from ..library.peptides_filter import PeptidesFilter


class Test(BaseTest):
    # Helpers

    # Tests

    def test_PeptidesFilter_init(self):

        pf = PeptidesFilter({}, {})
        self.assertEqual(pf.peptide_key, 'pk')

    def test_PeptidesFilter_filtered_queryset_identification(self):
        peptide = self.helpers.create_peptide()

        self.helpers.create_peptide(identification=peptide.identification)

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'error_ppm__lte': 10,
            'protein_description': True,
            'remove_decoy': True,
            'unique_sequence': True
        }

        pf = PeptidesFilter(fc, {})
        self.assertIsInstance(pf.peptides, QuerySet)

        pf.apply()
        self.assertEqual(len(pf.peptides), 1)

    def test_PeptidesFilter_filtered_queryset_validation(self):
        self.helpers.create_identification()

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'validated__in': ['None'],
        }

        pf = PeptidesFilter(fc, {})

        self.assertIsInstance(pf.peptides, QuerySet)

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'validated__in': ['G'],
        }

        pf = PeptidesFilter(fc, {})

        self.assertIsInstance(pf.peptides, QuerySet)

    def test_PeptidesFilter_filtered_queryset_file(self):
        self.helpers.create_identification()

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'files': 'None',
            'files_action': 'filter',
        }

        pf = PeptidesFilter(fc, {})

        self.assertIsInstance(pf.peptides, QuerySet)

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'files': 'None',
            'files_action': 'exclude',
        }

        pf = PeptidesFilter(fc, {})

        self.assertIsInstance(pf.peptides, QuerySet)

    def test_PeptidesFilter_filtered_queryset_immunopeptidome(self):
        self.helpers.create_immunopeptidome()

        fc = {
            'identification': [],
            'immunopeptidome': [1],
        }

        pf = PeptidesFilter(fc, {})

        self.assertIsInstance(pf.peptides, QuerySet)

    def test_PeptidesFilter_filtered_queryset_pp_id(self):
        self.helpers.create_immunopeptidome()

        fc = {
            'identification': [],
            'immunopeptidome': [1],
            'pp_id': 1,
        }

        pf = PeptidesFilter(fc, {})

        self.assertIsInstance(pf.peptides, QuerySet)

    def test_PeptidesFilter_filtered_queryset_mhc(self):
        self.helpers.create_identification()

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'error_ppm__lte': 1,
            'remove_decoy': True,
            'score_fdr': '',
            'score_mhc_fdr': '',
            'unique_sequence': True,
            'affinity_threshold': 1,
            'mhc_predictor': 'NETMHC',
        }

        pf = PeptidesFilter(fc, {})

        self.assertIsInstance(pf.peptides, QuerySet)

    def test_PeptidesFilter_warning(self):

        pf = PeptidesFilter({}, {})

        pf.values['score_threshold'] = None

        with self.assertRaises(BaseSiteException):

            pf.warning()

        pf.values['score_threshold'] = 'Min FDR'

        with self.assertRaises(BaseSiteException):
            pf.warning()

    def test_PeptidesFilter_single_mhc(self):

        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])
        identification = self.helpers.create_identification(
            biological_sample=biological_sample)

        allele = self.helpers.create_mhc_allele()

        obj = BiologicalSampleMHCAllele.objects.create(
            biological_sample=biological_sample)

        obj.mhc_allele.add(allele)

        self.helpers.create_peptide(identification=identification)

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'ignore_sample_alleles': False,
            'affinity_threshold': 1,
            'mhc_allele': [],
            'mhc_predictor': 'NETMHC34',
        }

        pf = PeptidesFilter(fc, {})

        def fake(a, b):
            return MagicMock()

        with patch('mhc_pipeline.library.mhc_prediction.get_predictions', fake):

            pf.apply()

        self.assertIsInstance(pf.peptides, list)

    def test_PeptidesFilter_global_score_fdr(self):
        peptide = self.helpers.create_peptide()
        self.helpers.create_peptide(identification=peptide.identification,
                                    peptide_score=2,
                                    is_decoy=True)
        self.helpers.create_peptide(identification=peptide.identification,
                                    is_decoy=True)

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'score_fdr': 100,
            'remove_decoy': True,
        }

        pf = PeptidesFilter(fc, {})
        pf.apply()

        self.assertIsInstance(pf.peptides, list)

    def test_PeptidesFilter_global_score_mhc_fdr(self):
        peptide = self.helpers.create_peptide()
        self.helpers.create_peptide(identification=peptide.identification,
                                    peptide_score=2,
                                    is_decoy=True)
        self.helpers.create_peptide(identification=peptide.identification,
                                    is_decoy=True)

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'score_mhc_fdr': 100,
            'remove_decoy': True,
            'mhc_allele': [],
            'mhc_predictor': 'NETMHC34',
        }

        pf = PeptidesFilter(fc, {})

        def fake(a, b):
            return MagicMock()

        def get_affinity(a, b, c):
            return 1

        with patch('mhc_pipeline.library.mhc_prediction.get_predictions', fake), \
             patch('mhc_pipeline.library.mhc_prediction.get_affinity', get_affinity):
            pf.apply()

        self.assertIsInstance(pf.peptides, list)

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'score_mhc_fdr': 100,
            'mhc_allele': [],
            'mhc_predictor': 'NETMHC34',
        }

        pf = PeptidesFilter(fc, {})

        def get_affinity(a, b, c):

            if b == 2:
                return None
            else:
                return 1

        with patch('mhc_pipeline.library.mhc_prediction.get_predictions', fake), \
             patch('mhc_pipeline.library.mhc_prediction.get_affinity', get_affinity):
            pf.apply()

        fc = {
            'identification': [1],
            'immunopeptidome': [],
            'score_mhc_fdr': 100,
            'mhc_allele': [],
            'mhc_predictor': 'NETMHC34',
        }

        pf = PeptidesFilter(fc, {})

        def fake(a, b):
            return MagicMock()

        def get_affinity(a, b, c):

            if b == 2:
                return 400
            else:
                return 1

        with patch('mhc_pipeline.library.mhc_prediction.get_predictions', fake), \
             patch('mhc_pipeline.library.mhc_prediction.get_affinity', get_affinity):
            pf.apply()

        self.helpers.create_peptide(identification=peptide.identification,
                                    peptide_score=100,
                                    is_decoy=True)

        def get_affinity(a, b, c):

            if b == 3:
                return 400
            else:
                return 1

        with patch('mhc_pipeline.library.mhc_prediction.get_predictions', fake), \
             patch('mhc_pipeline.library.mhc_prediction.get_affinity', get_affinity):
            pf.apply()
