"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from unittest.mock import MagicMock, patch

# Import Django related libraries
from django.core.cache import cache
from django.urls import reverse

# Third party libraries
import pandas as pd
# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from ..views import peptides_plots


class Test(BaseTest):

    def raise_value_error(self, x):

        raise ValueError

    def raise_basesite_error(self, x):

        raise BaseSiteException('x', 'y')

    def test_generate_plots(self):

        revision = peptides_plots.revision
        key = 'test'
        task_key = key + f'_{revision}_plots_status'
        cache.delete(task_key)

        self.helpers.create_immunopeptidome(permissions=['view'])

        filter_options = {
            'immunopeptidome': [1],
            'netmhc': '',
        }

        peptides_plots.generate_plots(key, filter_options)

        cache.delete(task_key)

    def test_generate_plots_exception(self):

        revision = peptides_plots.revision
        key = 'test'
        task_key = key + f'_{revision}_plots_status'
        cache.delete(task_key)

        self.helpers.create_immunopeptidome(permissions=['view'])

        filter_options = {
            'immunopeptidome': [1],
            'netmhc': '',
        }

        with self.assertRaises(ValueError), \
            patch('mhc_pipeline.views.peptides_plots.get_unique_peptides',
                   self.raise_value_error):
            peptides_plots.generate_plots(key, filter_options)

        cache.delete(task_key)

        with patch('mhc_pipeline.views.peptides_plots.get_unique_peptides',
                   self.raise_basesite_error):
            peptides_plots.generate_plots(key, filter_options)

        cache.delete(task_key)

    def test_get_unique_peptides(self):

        self.helpers.create_immunopeptidome()
        filter_options = {
            'immunopeptidome': [1],
            'netmhc': '',
        }

        out = peptides_plots.get_unique_peptides(filter_options)

        self.assertIsInstance(out, tuple)

    def test_get_plot(self):

        self.helpers.create_immunopeptidome(permissions=['view'])

        url = reverse('peptides_plots:plot', args=('2', 'test'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        url = reverse('peptides_plots:plot', args=('1', 'test'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_plot_data(self):

        self.helpers.create_immunopeptidome(permissions=['view'])

        url = reverse('peptides_plots:plot_data', args=('2', 'test'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        url = reverse('peptides_plots:plot_data', args=('1', 'test'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_Plots(self):

        self.helpers.create_immunopeptidome(permissions=['view'])

        url = reverse('peptides_plots:plots', args=('2',))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        url = reverse('peptides_plots:plots', args=('1',))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_state(self):

        self.helpers.create_immunopeptidome(permissions=['view'])

        url = reverse('peptides_plots:plots_state', args=(1, ))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_allele_fraction(self):

        data = {
            'netmhc': {
                'affinity': {
                    'Minimum allele': pd.Series([1, 1, 2])
                }
            }
        }

        out = peptides_plots.allele_fraction(MagicMock(), data)

        self.assertIsInstance(out, tuple)

    def test_allele_binding(self):

        data = {
            'netmhc': {
                'affinity':

                    pd.DataFrame({
                        'A': [10, 100, 1000],
                        'Minimum': [10, 100, 1000],
                        'Minimum allele': ['A', 'A', 'A']
                    })
            }
        }

        out = peptides_plots.allele_binding(MagicMock(), data)

        self.assertIsInstance(out, tuple)

    def test_Plots_clean_filter(self):

        filter_options = {
            'mhc_allele': ['1']
        }

        values = peptides_plots.Plots.clean_filter(filter_options)

        self.assertTrue(values['mhc_allele'] == '|1|')

    def test_Plots_clean_filter_context(self):

        values = {
            'key': '2',
            'filter_options': {
                'mhc_allele': ['1']
            }
        }

        self.request.method = 'get'
        view = peptides_plots.Plots.as_view()(self.request, **values)
        self.assertIn('filter_options', view.context_data)




