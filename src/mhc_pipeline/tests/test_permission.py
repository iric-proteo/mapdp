"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.core.exceptions import PermissionDenied
from django.http.response import Http404

# Third party libraries

# Import project libraries
from .base import BaseTest
import mhc_pipeline.library.permission as p
import mhc_pipeline.models as models


class PermissionTest(BaseTest):
    def test_check_content_permission_has_no_perm(self):
        biological_sample = self.helpers.create_biological_sample()

        with self.assertRaises(PermissionDenied):
            p.check_content_permission(self.request, 'change',
                                       biological_sample)

    def test_get_obj_by_type(self):
        with self.assertRaises(Http404):
            p.get_obj_by_type(self.request, 'biologicalsample', 1, 'change')

        self.helpers.create_biological_sample()

        with self.assertRaises(PermissionDenied):
            p.get_obj_by_type(self.request, 'biologicalsample', 1, 'change')

    def test_check_content_permission_has_perm(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        self.assertEqual(p.check_content_permission(self.request, 'change',
                                                    biological_sample),
                         None)

    def test_check_set_class_no_permission(self):
        self.group.user_set.remove(self.user)
        file_set = self.helpers.create_file_set()

        with self.assertRaises(PermissionDenied):
            p.check_set_class_permission(self.request, file_set)

    def test_check_set_class_permission(self):
        file_set = self.helpers.create_file_set()

        self.assertEqual(p.check_set_class_permission(self.request, file_set),
                         None)

    def test_get_if_permission(self):
        self.helpers.create_biological_sample(permissions=['change'])

        obj = p.get_if_permission(self.request,
                                  models.BiologicalSample,
                                  'change',
                                  pk=1)

        self.assertIsInstance(obj, models.BiologicalSample)

    def test_rm_content_permission(self):
        biological_sample = self.helpers.create_biological_sample()

        p.rm_content_permission(biological_sample, self.user,
                                 permissions=['view'])

        with self.assertRaises(PermissionDenied):
            p.check_content_permission(self.request, 'view', biological_sample)

    def test_set_content_permission(self):
        biological_sample = self.helpers.create_biological_sample()

        p.set_content_permission(biological_sample, self.user,
                                 permissions=['view'])

        p.check_content_permission(self.request, 'view', biological_sample)
