"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from types import SimpleNamespace

# Import Django related libraries
from django.conf import settings

# Third party libraries
import requests_mock

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
import mhc_pipeline.library.pps as pps


class PPSTest(BaseTest):
    def setUp(self):
        self.url = 'http://localhost'
        settings.PPS_HOST = self.url

    def test_check_status_code(self):
        response = SimpleNamespace(status_code=200)

        self.assertIsNone(pps.check_status_code(response))

        response.content = 'Error'
        response.json = lambda: 'Error'
        response.url = ''

        for status_code in [403, 400, 404, 1]:
            response.status_code = status_code

            with self.assertRaises(BaseSiteException):
                pps.check_status_code(response)

    def test_alive(self):
        self.assertFalse(pps.alive())

        with requests_mock.Mocker() as m:
            m.get(self.url + '/alive/', text='resp')

            self.assertFalse(pps.alive())

            m.get(self.url + '/alive/', status_code=204, text='resp')

            self.assertTrue(pps.alive())

    def test_get_personalized_proteome_for_form(self):
        with requests_mock.Mocker() as m:
            data = [
                {
                    'id': 1,
                    'snps_name': '',
                    'url': '',
                },
                {
                    'id': 2,
                    'snps_name': '',
                    'url': '',
                }
            ]

            m.get(self.url + '/personalized_proteomes/?ids=1',
                  json=data)

            self.assertEqual(len(pps.get_personalized_proteome_for_form(pp_ids=[1])), 2)

    def test_get_reference_genome_for_form(self):
        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'name': '',
                'species': '',
                'url': '',
            }]

            m.get(self.url + '/genomes/?trashed=false', json=data)

            self.assertEqual(len(pps.get_reference_genome_for_form()), 2)

    def test_get_snps_for_form(self):
        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'name': '',
                'species': '',
                'url': '',

            }]

            m.get(self.url + '/genomes/?trashed=false',
                  json=data)

            data = [{
                'id': 1,
                'name': '',
                'genome': '',
                'genome_name': 'Genome Name',
                'url': '',
                'import_status': 'finished',
                'trashed': False
            }]

            m.get(self.url + '/snps/?ids=1',
                  json=data)

            self.assertEqual(len(pps.get_snps_for_form([1])), 2)

    def test_pps_request(self):
        with requests_mock.Mocker() as m:
            url = self.url + '/alive/'
            text = 'resp'
            m.get(url, text=text)

            response = pps.pps_request('get', url)

            self.assertEqual(response.text, text)

    def test_service_unavailable(self):
        with self.assertRaises(BaseSiteException):
            pps.service_unavailable()
