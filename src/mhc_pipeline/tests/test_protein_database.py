"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.forms.models import model_to_dict

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..library import protein_database as db
from ..models import (Identification,
                      ImmunoPeptidome)


class Test(BaseTest):
    # Helpers

    # Tests
    def test_from_all(self):
        self.helpers.create_identification()

        fc = {
            'identification': Identification.objects.all(),
            'immunopeptidome': ImmunoPeptidome.objects.all(),
        }

        out = db.from_all(fc)

        self.assertIsInstance(out, set)

    def test_from_immunopeptidome(self):
        obj = self.helpers.create_immunopeptidome()

        obj2 = ImmunoPeptidome.objects.create(
            created_by=self.user,
            modified_by=self.user,
            name='Immuno subject 2',
            comments='Text',
            filter=self.helpers.create_filter(),
        )

        obj.immunopeptidome.add(obj2)

        fc = {
            'identification': Identification.objects.all(),
            'immunopeptidome': ImmunoPeptidome.objects.all(),
        }

        out = db.from_all(fc)

        self.assertIsInstance(out, set)

    def test_formatted_str(self):
        database = self.helpers.create_protein_database()

        db_set = set([database])

        db_set = [model_to_dict(entry) for entry in db_set]

        out = db.formatted_str(db_set)

        self.assertIsInstance(out, str)
