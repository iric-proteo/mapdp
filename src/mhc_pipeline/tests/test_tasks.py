""""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from pathlib import Path
import shutil
from unittest.mock import patch
import time

# Import Django related libraries
from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist

# Third party libraries
import celery
import pandas as pd
import requests_mock

# Import project libraries
from .base import BaseTest
from mhc_pipeline import tasks
from ..library.lock_model import ModelLocker
from ..library.minio import get_storage
from ..models import File, Genotyping, PeptideGenome
from ..views import biological_sample_plots, peptides_plots

doc_length = 6


class Test(BaseTest):
    def setUp(self):
        super().setUp()

        self.url = 'http://localhost'
        settings.PPS_HOST = self.url

    # Helpers
    def store_genome_bed(self):
        pk = 1
        document_pk = 1

        data = {
            'peptides_file': '/peptides_file',
            'csv_file': '/csv',
            'status': 'finished',
        }

        csv_data = 'unique_peptide,peptide_sequence,pp_id,mapped,gene_symbol,gene_identifier,transcript_identifier,protein_identifier,protein_position,chromosome,strand,locus,exon_positions,exon_sequences,sap,peptide_variants,snp_positions\n"1","LPSQAFEYILYNKG","1",True,"CTSH","ENSMUSG00000032359","ENSMUST00000034915","ENSMUSP00000034915","183","9","+","chr9:90067031-90067072","90067031-90067072","CTCCCCAGCCAGGCCTTCGAGTACATCCTATACAACAAGGGC","","",""\n"2","TPLLLRGLTGSA","1",False,"","","","","","","","","","","","",""\n"2","TPLLLRGLTGSA","1",False,"","","","","","","","","","","","",""'
        pep_data = 'unique_peptide,peptide_sequence,pp_id\n"2","LPZZZSQAFEYILYNKG","1"'
        bed_data = 'chr9	90067030	90067072	LPSQAFEYILYNKG	900	+	90067030	90067072	0	1	42	0'

        with requests_mock.Mocker() as m:
            m.get(self.url + f'/peptides_snps/{pk}/', json=data)
            m.get(self.url + f'/csv', text=csv_data)
            m.get(self.url + f'/peptides_file', text=pep_data)

            tasks.peptides_snps_process(pk, document_pk=document_pk)

    # Tests
    def test_annotation_dbsnp(self):
        annotation = self.helpers.create_annotation()
        document = self.helpers.create_immunopeptidome_document()

        with patch('library.dbsnp.annotate_csv') as p:
            tasks.annotation_dbsnp(annotation.pk, document_pk=document.pk)

            p.assert_called_once()

    def test_annotation_1000genomes(self):
        annotation = self.helpers.create_annotation(type_='1000genomes',
                                                    sub_path='annotation',
                                                    file_name='1000genomes.vcf')
        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )

        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        try:
            shutil.copytree('./test_files/annotation',
                            settings.MEDIA_ROOT + '/annotation')
        except FileExistsError:
            pass

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.annotation_1000genomes(annotation.pk, document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            fields = line.rstrip().split(',')

            self.assertEqual(fields[doc_length], '0.42531899999999995')

        document.file_path.unlink()
        Path(str(annotation.file_path) + '.parquet').unlink()

    def test_annotation_esp(self):
        annotation = self.helpers.create_annotation(type_='esp',
                                                    sub_path='annotation',
                                                    file_name='esp.csv')
        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )
        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        try:
            shutil.copytree('./test_files/annotation',
                            settings.MEDIA_ROOT + '/annotation')
        except FileExistsError:
            pass

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.annotation_esp(annotation.pk, document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            fields = line.rstrip().split(',')

            self.assertEqual(fields[doc_length], 'G=313/T=6535')

        document.file_path.unlink()

    def test_annotation_exac(self):

        annotation = self.helpers.create_annotation(type_='exac',
                                                    sub_path='annotation',
                                                    file_name='ExAC.gz')
        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )
        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        try:
            shutil.copytree('./test_files/annotation',
                            settings.MEDIA_ROOT + '/annotation')
        except FileExistsError:
            pass

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.annotation_exac(annotation.pk, document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            fields = line.rstrip().split(',')
            self.assertEqual(fields[-1], '0.027160000000000004')

        document.file_path.unlink()
        Path(str(annotation.file_path) + '.parquet').unlink()

        # shutil.copy(str(csv_file_ori), str(csv_file_tmp))
        # tasks.annotation_gnomad(annotation.pk, document_pk=document.pk)
        # document.file_path.unlink()
        #
        # Path(str(annotation.file_path) + '.parquet').unlink()

    def test_annotation_hpa(self):
        annotation = self.helpers.create_annotation(type_='hpa',
                                                    sub_path='annotation',
                                                    file_name='hpa.csv')
        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )
        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        try:
            shutil.copytree('./test_files/annotation',
                            settings.MEDIA_ROOT + '/annotation')
        except FileExistsError:
            pass

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.annotation_hpa(annotation.pk, document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            fields = line.rstrip().split(',')

            self.assertEqual(fields[doc_length], '4.35')

        document.file_path.unlink()

    def test_annotation_miha(self):
        annotation = self.helpers.create_annotation(type_='miha',
                                                    sub_path='annotation',
                                                    file_name='miha.csv')
        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )
        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        try:
            shutil.copytree('./test_files/annotation',
                            settings.MEDIA_ROOT + '/annotation')
        except FileExistsError:
            pass

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.annotation_miha(annotation.pk, document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            fields = line.rstrip().split(',')

            self.assertEqual(fields[doc_length], 'True')

        document.file_path.unlink()

    def test_annotation_start(self):

        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )

        tasks.annotate_start(1, '', document_pk=1)

        document.refresh_from_db()

        self.assertEqual(document.state, 'started')

    def test_annotation_tcga(self):
        annotation = self.helpers.create_annotation(type_='tcga',
                                                    sub_path='annotation',
                                                    file_name='tcga.csv')
        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )
        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        try:
            shutil.copytree('./test_files/annotation',
                            settings.MEDIA_ROOT + '/annotation')
        except FileExistsError:
            pass

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.annotation_tcga(annotation.pk, document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            fields = line.rstrip().split(',')

            self.assertEqual(fields[doc_length], '0.07')

        document.file_path.unlink()

    def test_DocumentTask_on_failure(self):
        document = self.helpers.create_immunopeptidome_document()

        tasks.DocumentTask.on_failure(1, 2, 3, 4, {'document_pk': document.pk},
                                      6)

        document.refresh_from_db()

        self.assertEqual(document.state, 'failed')

    def test_annotation_iedb(self):

        annotation = self.helpers.create_annotation(
            type_='iedb',
            sub_path='annotation',
            file_name='mhc_ligand_full.zip')

        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )
        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        Path(settings.MEDIA_ROOT + '/annotation').mkdir(exist_ok=True)

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.annotation_iedb(annotation.pk, document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            self.assertIn('33667', line)

        document.file_path.unlink()
        Path(str(annotation.file_path).replace('.zip', '.parquet')).unlink()

    def test_peptide_locus_count(self):
        document = self.helpers.create_immunopeptidome_document(
            path='annotation',
            name='doc_tmp.csv'
        )
        document_ori = self.helpers.create_immunopeptidome_document(
            immunopeptidome=document.immunopeptidome,
            path='annotation',
            name='doc.csv'
        )

        try:
            shutil.copytree('./test_files/annotation',
                            settings.MEDIA_ROOT + '/annotation')
        except FileExistsError:
            pass

        csv_file_ori = document_ori.file_path
        csv_file_tmp = document.file_path
        shutil.copy(str(csv_file_ori), str(csv_file_tmp))

        tasks.peptide_locus_count(document_pk=document.pk)

        with document.file_path.open() as f:
            for i in range(1, 5):
                line = f.readline()

            fields = line.rstrip().split(',')

            self.assertEqual(fields[doc_length], '1')

        document.file_path.unlink()

    def test_peptides_snps_submit(self):
        immunopeptidome = self.helpers.create_immunopeptidome()

        with requests_mock.Mocker() as m:
            m.post(self.url + '/peptides_snps/', json={'id': 1})

            value = tasks.peptides_snps_submit(immunopeptidome.pk,
                                               '/1/',
                                               document_pk=1)

            self.assertIsInstance(value, int)

    def test_peptides_snps_process_failed(self):
        pk = 1
        document_pk = 1

        with requests_mock.Mocker() as m, self.assertRaises(RuntimeError):
            m.get(self.url + f'/peptides_snps/{pk}/', json={'status': 'failed'})

            tasks.peptides_snps_process(pk, document_pk=document_pk)

    def test_peptides_snps_process_no_peptide(self):
        pk = -1
        document_pk = 1

        self.assertIsNone(
            tasks.peptides_snps_process(pk, document_pk=document_pk))

    def test_peptides_snps_process_retry(self):
        pk = 1
        document_pk = 1

        with requests_mock.Mocker() as m, self.assertRaises(
                celery.exceptions.Retry):
            m.get(self.url + f'/peptides_snps/{pk}/', json={'status': 'retry'})

            tasks.peptides_snps_process(pk, document_pk=document_pk)

    def test_peptides_snps_process_finished(self):
        obj = self.helpers.create_biological_sample(identifier='a')
        identification = self.helpers.create_identification(
            biological_sample=obj
        )

        for peptide in [
            'LPSQAFEYILYNKG',
            'TPLLLRGLTGSA',
                   ]:
            obj = self.helpers.create_peptide(
                peptide_sequence=peptide,
                identification=identification)

        self.store_genome_bed()

        self.assertEqual(PeptideGenome.objects.count(), 3)

        self.store_genome_bed()

    def test_immunopeptidome_bed(self):
        self.store_genome_bed()

        pp_url = '/1/'

        obj = self.helpers.create_immunopeptidome_document()

        peptide = self.helpers.create_peptide(
            identification=obj.immunopeptidome.identification.all()[0],
            peptide_sequence='LPSQAFEYILYNKG')
        obj.immunopeptidome.peptides.add(peptide)
        tasks.immunopeptidome_bed(obj.immunopeptidome_id, pp_url,
                                  document_pk=obj.pk)
        obj.refresh_from_db()
        self.assertEqual(obj.state, 'success')
        full_path = Path(settings.MEDIA_ROOT) / obj.path / obj.name
        self.assertTrue(full_path.exists())

        full_path.unlink()

    def test_immunopeptidome_csv(self):
        self.store_genome_bed()

        pp_url = '/1/'

        obj = self.helpers.create_immunopeptidome_document()

        peptide = self.helpers.create_peptide(
            identification=obj.immunopeptidome.identification.all()[0],
            peptide_sequence='LPSQAFEYILYNKG')
        obj.immunopeptidome.peptides.add(peptide)

        tasks.immunopeptidome_csv(obj.immunopeptidome_id, pp_url,
                                  self.url + '/999/', document_pk=obj.pk)

        obj.refresh_from_db()

        full_path = Path(settings.MEDIA_ROOT) / obj.path / obj.name

        self.assertTrue(full_path.exists())

        full_path.unlink()

    def test_processing_completed(self):
        obj = self.helpers.create_immunopeptidome_document()

        tasks.processing_completed(document_pk=obj.pk)

        obj.refresh_from_db()
        self.assertEqual(obj.state, 'success')

    def test_import_identification(self):
        identification = self.helpers.import_peptides()

        self.assertEqual(identification.state, 'success')

    def test_import_identification_locks(self):

        with ModelLocker(['peptide'], 1, 1):
            identification = self.helpers.import_peptides()

            self.assertTrue(identification.message.startswith('Database'))

    def test_import_identification_parse_exceptions(self):
        def raise_exception(*args, **kwargs):
            raise ValueError

        module_name = 'library.mgf_parser.MGFParser'

        with patch(module_name + '.get_scans_dict', raise_exception):
            identification = self.helpers.import_peptides()
            self.assertEqual(identification.state, 'failed')

    def test_IdentificationTask_on_failure(self):
        identification = self.helpers.import_peptides()

        tasks.IdentificationTask.on_failure(1, 2, 3, [1], 5, 6)

        identification.refresh_from_db()

        self.assertEqual(identification.state, 'failed')

    def test_import_identification_maxquant(self):
        identification = self.helpers.import_peptide_maxquant()

        self.assertEqual(identification.state, 'success')

    def test_immunopeptidome_peptides(self):

        # Complete this test
        with self.assertRaises(ObjectDoesNotExist):
            tasks.immunopeptidome_peptides(0, {})

    def test_ImmunopeptidomeTask_on_failure(self):

        obj = self.helpers.create_immunopeptidome()
        comments = obj.comments

        tasks.ImmunopeptidomeTask.on_failure(0, 0, 0, [1], 0, 0)

        obj.refresh_from_db()

        self.assertNotEquals(comments, obj.comments)

    def test_identification_csv(self):

        # Complete this test
        with self.assertRaises(KeyError):
            tasks.identification_csv({})

    def test_IdentificationCSVTask_on_failure(self):

        key = 'a'

        filter_options = {
            'identification_key': key
        }

        tasks.IdentificationCSVTask.on_failure(0, 0, 0, [filter_options], 0, 0)

        storage = get_storage()

        self.assertTrue(storage.exists(key))
        storage.delete(key)

    def test_peptides_generate_plots(self):

        revision = peptides_plots.revision
        key = 'test'
        task_key = key + f'_{revision}_plots_status'
        cache.delete(task_key)

        self.helpers.create_immunopeptidome(permissions=['view'])

        filter_options = {
            'immunopeptidome': [1],
            'netmhc': '',
        }

        tasks.peptides_generate_plots(key, filter_options)

        cache.delete(task_key)

    def test_biological_sample_generate_plots(self):

        revision = biological_sample_plots.revision
        task_key = f'biological_sample_1_9606_plot_revision_{revision}_status'
        cache.delete(task_key)

        tasks.biological_sample_generate_plots('0', 0)

        cache.delete(task_key)

    def test_peptide_counts(self):

        tasks.peptide_counts()

    def test_compress_uploaded_file(self):

        obj = self.helpers.create_uploaded_file()

        path = Path(obj.file.path)

        fs = self.helpers.create_file_set()
        file_ = File.add_file(fs, path.parent, path.name)

        tasks.compress_uploaded_file(obj.pk, file_.pk)

        file_.refresh_from_db()

        file = path.parent / file_.name

        self.assertTrue(file.exists())

    def test_filter_miha(self):

        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view', 'change'])

        obj_a = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome, name='old')

        obj_b = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome, name='new')

        filter_options = {
            'document': 1,
            'immunopeptidome_name': 'Test',
            'maf': 0.05,
            'dbsnp_validated': True,
            'no_hla_igg': True,
            'unambiguous_genetic_origin': True,
            'msms_validation': True,
            'expression_not_ubiquitous': True,
            'ratio_bone_marrow_skin': 1,
            'expression_aml': 1
        }

        old_file = Path(obj_a.file_path)
        new_file = Path(obj_b.file_path)

        with old_file.open('w') as f_out:
            f_out.write('{}\n');
            f_out.write('db');

        df = pd.DataFrame({
            'gnomad_af': [1],
            'dbsnp_validated': [True],
            'gene_symbol': ['HEHE'],
            'peptide_locus_count': [1],
            'Validated': ['G'],
            'hpa_ubiquitous_expression': [True],
            'hpa_bone_marrow/skin_ratio': [0],
            'tcga_aml_mean_expression': [1],
        })

        with patch('pandas.read_csv', return_value=df), \
             patch('mhc_pipeline.views.immunopeptidome.CreateFromFile.'
                   'process') as p:
            tasks.filter_miha(filter_options, 2)

            p.assert_called()

        old_file.unlink()
        self.assertTrue(new_file.exists())
        new_file.unlink()

    def test_genotyping(self):

        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])
        fs = self.helpers.create_biological_sample_file_set(
            8, biological_sample=biological_sample)
        File.add_file(fs, './test_files/bam', 'fake.bam')

        genotype = Genotyping(
            id=3,
            biological_sample=biological_sample,
            file_set=fs,
            software='test',
            state='pending',
            created_by=self.user,
            modified_by=self.user,
            update_alleles=True,
        )
        genotype.save()

        alleles = [
            'HLA-A*01:01',
            'HLA-A*02:01',
            'HLA-B*07:02',
            'HLA-B*44:03',
            'HLA-C*07:02',
            'HLA-C*16:01',
            'HLA-DPA1*01:03',
            'HLA-DPB1*03:01',
            'HLA-DPB1*04:01',
            'HLA-DQA1*02:01',
            'HLA-DQA1*03:02',
            'HLA-DQB1*02:02',
            'HLA-DQB1*03:03',
            'HLA-DRA*01:01',
            'HLA-DRB1*07:01',
            'HLA-DRB1*09:01',
        ]

        for allele in alleles:
            self.helpers.create_mhc_allele(name=allele)

        work_dir = Path(settings.NEXTFLOW_FS) / 'genotyping' / str(genotype.pk)
        #work_dir.mkdir(exist_ok=True, parents=True)
        
        genotype_dir = work_dir / 'results' / 'genotype'
        shutil.copytree('./test_files/arcashla', work_dir / 'results')
        self.assertTrue(genotype_dir.exists())

        with patch('subprocess.run') as p:
            tasks.genotyping(genotype.pk)
            p.assert_called_once()

        genotype.refresh_from_db()

        self.assertEquals(genotype.state, 'success')

        self.assertFalse(work_dir.exists())

        bs_alleles = (biological_sample.biologicalsamplemhcallele_set.all()[0]
                      .mhc_allele)

        self.assertEquals(bs_alleles.count(), 16)

    def test_genotyping_clean(self):

        work_dir = Path(settings.NEXTFLOW_FS) / 'genotyping' / str(2)
        work_dir.mkdir(exist_ok=True, parents=True)
        self.assertTrue(work_dir.exists())

        tasks.genotyping_clean(2)

        self.assertFalse(work_dir.exists())

