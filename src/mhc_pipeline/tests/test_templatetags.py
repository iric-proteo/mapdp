"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries

# Third party libraries

# Import project libraries
from .base import BaseTest
from mhc_pipeline.templatetags import form_tags, misc_tags


class TemplateTagsTest(BaseTest):
    def test_experiment_details_form(self):

        obj = self.helpers.create_experiment_details()

        self.assertIsInstance(form_tags.experiment_details_form(obj), list)

    def test_experiment_details_form_no_perm(self):

        obj = self.helpers.create_experiment_details()

        self.assertIsInstance(form_tags.experiment_details_form_no_perm(obj), list)

    def test_format_filter_values_none(self):
        self.assertEqual(misc_tags.format_filter_values(None), '')

    def test_format_filter_values(self):
        obj = self.helpers.create_filter()

        self.assertIsInstance(misc_tags.format_filter_values(obj), str)

    def test_format_filter_values_3(self):
        obj = self.helpers.create_filter()
        obj.json_values = '{"1": 50.0,"2": 50.0,"3": 50.0}'

        self.assertIsInstance(misc_tags.format_filter_values(obj), str)

    def test_format_filter_values_dict_allele_str(self):

        allele = self.helpers.create_mhc_allele()

        obj = {
            'mhc_allele': '|1|',
        }

        value = misc_tags.format_filter_values(obj)
        self.assertTrue(allele.name in value)

    def test_format_filter_values_five(self):

        obj = {
            '1': '1',
            '2': '1',
            '3': '1',
            '4': '1',
            '5': '1',
        }

        value = misc_tags.format_filter_values(obj)
        self.assertTrue('br /' in value)

    def test_format_filter_values_annotation(self):

        obj = {
            'annotation': ['1', '2', '3', '4', '5'],
        }

        value = misc_tags.format_filter_values(obj)
        self.assertTrue('br /' in value)
