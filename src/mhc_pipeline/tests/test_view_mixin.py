"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.core.exceptions import ImproperlyConfigured
from django.views.generic import TemplateView
from django.urls import reverse

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..models import BiologicalSample, Identification
from ..views.view_mixin import (TemplateMixin,
                                TrashMixin)


class Test(BaseTest):
    """
    Tests for View MixIn
    """

    def test_TemplateMixin(self):
        class DummyView(TemplateMixin, TemplateView):
            template_base = 'test'

            template_name = 'test'

        mixin = DummyView()

        self.assertEqual(['testtest'], mixin.get_template_names())

    def test_TemplateMixin_template_base_None(self):
        class DummyView(TemplateMixin, TemplateView):
            template_base = None

            template_name = 'test'

        mixin = DummyView()

        with self.assertRaises(ImproperlyConfigured):
            mixin.get_template_names()

    def test_TemplateMixin_template_name_None(self):
        class DummyView(TemplateMixin, TemplateView):
            pass

        mixin = DummyView()

        with self.assertRaises(ImproperlyConfigured):
            mixin.get_template_names()

    def test_TrashMixin_model_None(self):
        class DummyView(TrashMixin, TemplateView):
            pass

        mixin = DummyView()

        with self.assertRaises(ImproperlyConfigured):
            mixin.get_context_data()

    def test_TrashMixin_confirmed(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['delete'])

        class DummyView(TrashMixin, TemplateView):
            model = BiologicalSample

            request = self.request

        mixin = DummyView()

        context = mixin.get_context_data(confirmed='true',
                                         pk=biological_sample.pk)

        self.assertEqual(context['obj'], biological_sample)
        self.assertTrue(context['confirmed'])

    def test_TrashMixin_not_confirmed(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['delete'])

        class DummyView(TrashMixin, TemplateView):
            model = BiologicalSample

            request = self.request

        mixin = DummyView()

        context = mixin.get_context_data(pk=biological_sample.pk)

        self.assertEqual(context['obj'], biological_sample)
        self.assertFalse(context['confirmed'])

    def test_Trash_bs_fileset(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        identification = self.helpers.create_identification(
            biological_sample=biological_sample)

        url = reverse('identification:trash', args=(1, 'false'))
        self.client.get(url)

        self.assertFalse(identification.trashed)

        url = reverse('identification:trash', args=(1, 'true'))
        self.client.get(url)

        identification = Identification.objects.get(pk=1)

        self.assertTrue(identification.trashed)