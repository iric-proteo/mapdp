"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from unittest.mock import patch

# Import Django related libraries
from django.core import cache
from django.urls import reverse

# Third party libraries

# Import project libraries
from .base import BaseTest
from mhc_pipeline.views import base_site


class BaseSiteTest(BaseTest):
    """
    Tests for base site views
    """

    def test_home_returns_correct_html(self):
        mem_cache = cache.caches['mem']
        mem_cache.clear()

        with patch.object(base_site, 'cache', mem_cache):

            url = reverse('home')
            response = self.client.get(url)

            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'mhc_pipeline/base_site/home.html')

            # Cache
            response = self.client.get(url)

            self.assertEqual(response.status_code, 200)
