"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.conf import settings
from django.core.cache import cache
from django.urls import reverse

# Third party libraries
from guardian.shortcuts import assign_perm
import requests_mock

# Import project libraries
from .base import BaseTest
from ..forms import (BiologicalSampleForm,
                     ExperimentDetailsForm,
                     MHCSampleForm)
import mhc_pipeline.models as models
from ..views import biological_sample_plots


class BiologicalSampleViewsTest(BaseTest):
    # Helpers
    def set_bs_view_url_and_kwargs(self):
        self.kwargs = {
            'identifier': 'test',
            'short_description': 'Short',
            'Species': 9606,
            'set_class': 2
        }

        self.expected_url = reverse('biological_sample:view',
                                    args=(1,))

    # Tests
    def test_create_form_and_template(self):
        url = reverse('biological_sample:create')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/form.html')
        self.assertIsInstance(response.context['form'],
                              BiologicalSampleForm)

    def test_create_redirect(self):
        self.set_bs_view_url_and_kwargs()
        url = reverse('biological_sample:create')
        response = self.client.post(url, self.kwargs)

        self.assertRedirects(response, self.expected_url)

    def test_edit_template(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        url = reverse('biological_sample:edit', args=(biological_sample.pk,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/form.html')
        self.assertIsInstance(response.context['form'],
                              BiologicalSampleForm)

    def test_edit_POST_empty(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        kwargs = {
            'identifier': '',
        }

        url = reverse('biological_sample:edit', args=(biological_sample.pk,))
        response = self.client.post(url, kwargs)

        self.assertFalse(response.context['form'].is_valid())

    def test_edit_redirect(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        self.set_bs_view_url_and_kwargs()

        url = reverse('biological_sample:edit', args=(biological_sample.pk,))
        response = self.client.post(url, self.kwargs)

        self.assertRedirects(response, self.expected_url)

    def test_experiment_details_edit_form_template(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        url = reverse('biological_sample:experiment_details_create',
                      args=(biological_sample.pk,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/form.html')
        self.assertIsInstance(response.context['form'],
                              ExperimentDetailsForm)

    def test_experiment_details_edit_save_redirect(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        url = reverse('biological_sample:experiment_details_create',
                      args=(biological_sample.pk,))
        self.set_bs_view_url_and_kwargs()

        response = self.client.post(url, {'name': 'Test'})

        self.assertRedirects(response, self.expected_url)
        self.assertEqual(models.ExperimentDetails.objects.count(), 1)

    def test_experiment_details_edit_invalid(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        url = reverse('biological_sample:experiment_details_create',
                      args=(biological_sample.pk,))
        self.set_bs_view_url_and_kwargs()

        response = self.client.post(url, {'id_name': 'Test'})

        self.assertEqual(response.status_code, 200)

    def test_experiment_details_edit_details(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        experiment_details = self.helpers.create_experiment_details(
            biological_sample=biological_sample)
        url = reverse('biological_sample:experiment_details_edit',
                      args=(biological_sample.pk, experiment_details.pk))
        self.set_bs_view_url_and_kwargs()

        response = self.client.post(url, {'name': 'Test2'})

        self.assertRedirects(response, self.expected_url)
        self.assertEqual(models.ExperimentDetails.objects.count(), 1)

    def test_list_bs_view_template(self):
        url = reverse('biological_sample:list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/biological_sample/list.html')

    def test_list_bs_samples_permission(self):
        biological_sample = self.helpers.create_biological_sample()

        url = reverse('biological_sample:list')
        response = self.client.get(url)
        self.assertEqual(len(response.context['samples']), 0)

        assign_perm('view_biologicalsample', self.user, biological_sample)

        response = self.client.get(url)
        self.assertEqual(len(response.context['samples']), 1)

    def test_list_bs_samples_trashed(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])

        url = reverse('biological_sample:list')
        response = self.client.get(url)

        self.assertEqual(len(response.context['samples']), 1)

        biological_sample.trashed = 1
        biological_sample.save()

        response = self.client.get(url)

        self.assertEqual(len(response.context['samples']), 0)

    def test_mhc_allele_POST_edit(self):
        self.helpers.create_mhc_allele(name='test')
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])
        obj = self.helpers.create_biological_sample_mhc_allele(
            biological_sample=biological_sample)

        self.assertEqual(obj.mhc_allele.count(), 1)

        url = reverse('biological_sample:mhc_allele',
                      args=(biological_sample.pk,))

        kwargs = {
            'mhc_allele': '|1|2|',
        }

        self.client.post(url, kwargs)

        self.assertEqual(obj.mhc_allele.count(), 2)

    def test_mhc_allele_POST_redirect(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        self.set_bs_view_url_and_kwargs()
        self.helpers.create_mhc_allele()

        url = reverse('biological_sample:mhc_allele',
                      args=(biological_sample.pk,))

        kwargs = {
            'mhc_allele': 1,
        }

        response = self.client.post(url, kwargs)

        self.assertRedirects(response, self.expected_url)

    def test_mhc_allele_template(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        url = reverse('biological_sample:mhc_allele',
                      args=(biological_sample.pk,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/form.html')

    def test_mhc_details_form_and_template(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        url = reverse('biological_sample:mhc_details',
                      args=(biological_sample.pk,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/form.html')
        self.assertIsInstance(response.context['form'],
                              MHCSampleForm)

    def test_mhc_details_POST_invalid(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        kwargs = {
            'gender': 'X',
        }

        url = reverse('biological_sample:mhc_details',
                      args=(biological_sample.pk,))
        response = self.client.post(url, kwargs)

        self.assertFalse(response.context['form'].is_valid())

    def test_mhc_details_redirect(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        self.set_bs_view_url_and_kwargs()

        url = reverse('biological_sample:mhc_details',
                      args=(biological_sample.pk,))
        response = self.client.post(url, self.kwargs)

        self.assertRedirects(response, self.expected_url)

    def test_pps_unavailable(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        models.SNPs.objects.create(biological_sample=biological_sample,
                                   pps_snps_id=1)

        url = 'http://localhost'
        settings.PPS_HOST = url

        with requests_mock.Mocker() as m:
            m.get(url + '/snps/1/', status_code=404, text='resp')

            url = reverse('biological_sample:view',
                          args=(biological_sample.pk,))
            response = self.client.get(url)

            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response,
                                    'mhc_pipeline/biological_sample/view.html')

    def test_trash_confirmed(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view', 'delete'])

        url = reverse('biological_sample:trash',
                      args=(biological_sample.pk, 'true'))
        response = self.client.get(url)

        self.assertEqual(response.context['obj'].trashed, True)

    def test_trash_no_permission(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        url = reverse('biological_sample:trash',
                      args=(biological_sample.pk, 'false'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_trash_template(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view', 'delete'])

        url = reverse('biological_sample:trash',
                      args=(biological_sample.pk, 'false'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/trash.html')

    def test_view_template(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        url = reverse('biological_sample:view', args=(biological_sample.pk,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/biological_sample/view.html')

    def test_ExperimentDetailsTab(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])

        url = reverse('biological_sample:experiment_details_tabview',
                      args=(biological_sample.pk,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Plots(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])

        revision = biological_sample_plots.revision
        task_key = f'biological_sample_1_9606_plot_revision_{revision}_status'
        cache.delete(task_key)

        url = reverse('biological_sample:plots',
                      args=(biological_sample.pk, 9606))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        url = reverse('biological_sample:plots',
                      args=('0', 0))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        cache.delete(task_key)

    def test_PlotsState(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])

        url = reverse('biological_sample:plots_state',
                      args=(biological_sample.pk, 9606))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
