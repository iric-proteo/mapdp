"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json
import os
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings
from django.core.exceptions import (ImproperlyConfigured,
                                    PermissionDenied)
from django.urls import reverse

# Third party libraries

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from ..forms import (BiologicalSampleFileSetForm,
                     UploadedFileForm)
from ..models import ChunkedUploadedFile
from ..views import fileset


class Test(BaseTest):

    # Helper methods
    def create_fake_objects(self):
        self.bs = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        self.fs = self.helpers.create_biological_sample_file_set(
            2, biological_sample=self.bs)

    def test_ChunkedUpload_context(self):

        self.create_fake_objects()
        self.fs.set_class = 3
        self.fs.save()

        url = reverse('fileset:chunked_form',
                      args=('biologicalsample', self.bs.pk, self.fs.pk,
                            'Fasta'))

        response = self.client.get(url)

        self.assertTrue('identifier' in response.context)

    def test_MyChunkedUploadView(self):

        self.create_fake_objects()
        self.fs.set_class = 3
        self.fs.save()

        url = reverse('fileset:chunked_upload',
                      args=('biologicalsample', self.bs.pk, self.fs.pk,
                            'Fasta'))

        self.assertEquals(ChunkedUploadedFile.objects.count(), 0)

        file = open(os.path.join(settings.MS_DIR, 'fasta', 'test.fa'), 'r')
        response = self.client.post(url, {'the_file': file})
        data = json.loads(response.content)
        file.close()

        self.assertEquals(ChunkedUploadedFile.objects.count(), 1)

        url = reverse('fileset:chunked_upload_complete',
                      args=('biologicalsample', self.bs.pk, self.fs.pk,
                            'Fasta'))

        response = self.client.post(url,
                                    {'md5': '8ad3fd3089dee04067eb4f67f9d6fc7f',
                                     'upload_id': data['upload_id']})
        data = json.loads(response.content)
        self.assertTrue('message' in data)
