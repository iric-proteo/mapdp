"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import os
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings
from django.core.exceptions import (ImproperlyConfigured,
                                    PermissionDenied)
from django.urls import reverse

# Third party libraries

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from ..forms import (BiologicalSampleFileSetForm,
                     UploadedFileForm)
from ..models import (BiologicalSampleFileSet,
                      File,
                      FileSet)
from ..views import fileset


class Test(BaseTest):
    # Helper methods
    def create_fake_objects(self):
        self.bs = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        self.fs = self.helpers.create_biological_sample_file_set(
            2, biological_sample=self.bs)

    def create_fake_objects_with_files(self):
        self.create_fake_objects()
        File.add_file(self.fs, os.path.join(settings.MS_DIR, 'fileset'),
                      'test.raw')
        File.add_file(self.fs, os.path.join(settings.MS_DIR, 'fileset'),
                      'test2.raw')

    def set_browse_url_and_kwargs(self):
        # Test invalid directory
        self.kwargs = {'subdir': '/fileset/',
                       'fs_pk': self.fs.pk,
                       'ct_pk': self.bs.pk,
                       'content_type': 'biologicalsample'}

        self.url = reverse('fileset:browse',
                           kwargs=self.kwargs)

    def set_create_url_and_kwargs(self):
        self.kwargs = {'name': 'Test',
                       'set_class': 2,
                       'ct_pk': self.bs.pk,
                       'content_type': 'biologicalsample'}

        self.url = reverse('fileset:create',
                           args=('biologicalsample', self.bs.pk, '2,3,4,5,6'))

    def set_refresh_url_and_kwargs(self):
        self.kwargs = {'fs_pk': self.fs.pk,
                       'ct_pk': self.bs.pk,
                       'content_type': 'biologicalsample',
                       }

        self.url = reverse('fileset:refresh',
                           args=('biologicalsample', self.bs.pk, self.fs.pk))

    # Tests

    def test_create_view_form_template(self):
        self.bs = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        self.set_create_url_and_kwargs()
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'mhc_pipeline/fileset/form.html')
        self.assertIsInstance(response.context['form'],
                              BiologicalSampleFileSetForm)

    def test_create_view_POST_redirect(self):
        self.create_fake_objects()
        self.set_create_url_and_kwargs()

        expected_url = reverse('fileset:browse',
                               args=('biologicalsample', self.bs.pk, 2, '/'))
        response = self.client.post(self.url, self.kwargs)

        self.assertRedirects(response, expected_url)

    def test_create_view_POST_redirect_fasta(self):
        self.create_fake_objects()
        self.set_create_url_and_kwargs()
        self.kwargs['set_class'] = 3

        response = self.client.post(self.url, self.kwargs)

        expected_url = reverse('fileset:chunked_form',
                               args=(
                                   'biologicalsample', self.bs.pk, 2, 'Fasta'))

        self.assertRedirects(response, expected_url)

    def test_create_view_POST_save(self):
        self.create_fake_objects()
        self.set_create_url_and_kwargs()
        self.client.post(self.url, self.kwargs)

        self.assertEqual(BiologicalSampleFileSet.objects.count(), 2)

    def test_Browse_clean_subdir_str(self):
        clean_subdir, url_subdir = fileset.Browse.clean_subdir_str('/1/<--')

        self.assertEqual(clean_subdir, '')
        self.assertEqual(url_subdir, '/')

        clean_subdir, url_subdir = fileset.Browse.clean_subdir_str('/1/')

        self.assertEqual(clean_subdir, os.sep + '1')
        self.assertEqual(url_subdir, '/1/')

        clean_subdir, url_subdir = fileset.Browse.clean_subdir_str('/1/../')

        self.assertEqual(clean_subdir, '')
        self.assertEqual(url_subdir, '')

        clean_subdir, url_subdir = fileset.Browse.clean_subdir_str('')

        self.assertEqual(clean_subdir, '')
        self.assertEqual(url_subdir, '')

    def test_Browse_ls(self):
        file_set = self.helpers.create_biological_sample_file_set(2)

        path, directories, files = fileset.Browse.ls(file_set, '/fileset/')

        self.assertEqual(len(directories), 0)
        self.assertEqual(len(files), 3)

    def test_Browse_template(self):
        self.create_fake_objects()
        url = reverse('fileset:browse',
                      args=('biologicalsample', self.bs.pk, self.fs.pk, '/'))

        response = self.client.get(url)

        self.assertTemplateUsed(response, 'mhc_pipeline/fileset/browse.html')

    def test_Browse_get(self):
        self.create_fake_objects()
        self.set_browse_url_and_kwargs()
        response = self.client.get(self.url, self.kwargs)

        self.assertEqual(response.status_code, 200)

    def raise_filenotfound(self, x, y):

        raise FileNotFoundError

    def test_Browse_get_filenotfound(self):
        self.create_fake_objects()
        self.set_browse_url_and_kwargs()

        with patch('mhc_pipeline.views.fileset.Browse.ls',
                  self.raise_filenotfound):
            response = self.client.get(self.url, self.kwargs)

        self.assertEqual(response.status_code, 404)

    def test_Browse_POST_add_files(self):
        self.create_fake_objects()
        self.set_browse_url_and_kwargs()

        # Test addition of files
        self.kwargs['id[]'] = [0, 1]
        self.client.post(self.url, self.kwargs)

        self.assertEqual(self.fs.file_set.count(), 2)

        # Test addition of files with one existing
        self.kwargs['id[]'] = [1, 2]
        self.client.post(self.url, self.kwargs)

        self.assertEqual(self.fs.file_set.count(), 3)

    def test_Browse_POST_filenotfound(self):
        self.create_fake_objects()
        self.set_browse_url_and_kwargs()

        # Test addition of files
        self.kwargs['id[]'] = [0, 1]

        with patch('mhc_pipeline.views.fileset.Browse.ls',
                  self.raise_filenotfound):
            response = self.client.post(self.url, self.kwargs)

        self.assertEqual(response.status_code, 404)

    def test_Browse_POST_identification(self):
        self.bs = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        self.fs = self.helpers.create_biological_sample_file_set(
            4, biological_sample=self.bs)

        self.set_browse_url_and_kwargs()

        # Test addition of files
        self.kwargs['id[]'] = [0, 1]

        response = self.client.post(self.url, self.kwargs)

        self.assertEqual(response.status_code, 302)

    def test_Browse_POST_redirect_no_id(self):
        self.create_fake_objects()
        self.set_browse_url_and_kwargs()

        expected_url = reverse('biological_sample:view',
                               args=(self.bs.pk,))

        response = self.client.post(self.url, self.kwargs)

        self.assertRedirects(response, expected_url)

    def test_Refresh_view(self):
        self.create_fake_objects_with_files()
        self.set_refresh_url_and_kwargs()

        # Modify files to see if refresh is doing is job
        f1 = self.fs.file_set.all()[0]
        f1.available = False
        f1.save()

        f2 = self.fs.file_set.all()[1]
        f2.name = 'DoNotExists'
        f2.save()

        self.client.get(self.url, self.kwargs)

        self.assertFalse(self.fs.file_set.get(pk=f2.pk).available)
        self.assertTrue(self.fs.file_set.get(pk=f1.pk).available)

    def test_Remove_view(self):
        self.create_fake_objects_with_files()

        url = reverse('fileset:remove_file',
                      args=('biologicalsample', self.bs.pk, self.fs.pk, 1))
        self.client.get(url)

        self.assertEqual(self.fs.file_set.count(), 1)

    def test_SendFile_view_file_found(self):
        self.create_fake_objects_with_files()

        url = reverse('fileset:send_file',
                      args=('biologicalsample', self.bs.pk, self.fs.pk, 1))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_SendFile_view_file_not_found(self):
        self.create_fake_objects_with_files()

        f = File.objects.get(pk=1)
        f.name = 'test33e342.txt'
        f.save()

        url = reverse('fileset:send_file',
                      args=('biologicalsample', self.bs.pk, self.fs.pk, 1))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_SendFiles_view_file_found(self):
        self.create_fake_objects_with_files()

        url = reverse('fileset:send_files',
                      args=('biologicalsample', self.bs.pk, self.fs.pk))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_SendFiles_view_file_not_found(self):
        self.create_fake_objects_with_files()

        f = File.objects.get(pk=1)
        f.name = 'test33e342.txt'
        f.save()

        url = reverse('fileset:send_files',
                      args=('biologicalsample', self.bs.pk, self.fs.pk))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_SendFiles_view_file_not_available(self):
        self.create_fake_objects_with_files()

        f = File.objects.get(pk=1)
        f.available = False
        f.save()

        url = reverse('fileset:send_files',
                      args=('biologicalsample', self.bs.pk, self.fs.pk))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Trash_view(self):
        self.create_fake_objects()

        # Execute view
        url = reverse('fileset:trash',
                      args=('biologicalsample', self.bs.pk, self.fs.pk))
        self.client.get(url)

        # Verify trashed flag
        fs = FileSet.objects.get(pk=self.fs.pk)

        self.assertTrue(fs.trashed)

    def test_EnvironmentMixin_init(self):
        env = fileset.EnvironmentMixin()

        self.assertIsNone(env.file_set)
        self.assertIsNone(env.instance)

    def test_EnvironmentMixin_set_environment(self):
        self.create_fake_objects()

        env = fileset.EnvironmentMixin()

        env.set_environment(self.request,
                            content_type='biologicalsample',
                            ct_pk=1, fs_pk=1)

        self.assertIsNotNone(env.file_set)
        self.assertIsNotNone(env.instance)

    def test_EnvironmentMixin_check_reference(self):
        bs = self.helpers.create_biological_sample(
            permissions=['change', 'view'])
        self.file_set = self.helpers.create_biological_sample_file_set(
            4, biological_sample=bs)

        identification = self.helpers.create_identification(
            biological_sample=bs
        )
        self.file_set.identification_set.add(identification)

        with self.assertRaises(BaseSiteException):
            fileset.EnvironmentMixin.check_reference(self)

    def test_EnvironmentMixin_check_permission(self):
        self.create_fake_objects()
        self.group.user_set.remove(self.user)

        env = fileset.EnvironmentMixin()
        env.check_set_class_permission = True

        with self.assertRaises(PermissionDenied):
            env.set_environment(self.request,
                                content_type='biologicalsample',
                                ct_pk=1, fs_pk=1)

    def test_FileSetRedirect_get_redirect(self):
        biological_sample = self.helpers.create_biological_sample()

        def fake_method():
            return None

        fileset_view = fileset.FileSetRedirect()
        fileset_view.task = fake_method
        fileset_view.instance = biological_sample
        redirect = fileset_view.get(self.request,
                                    content_type='biologicalsample',
                                    ct_pk=1, fs_pk=1)

        self.assertEqual(redirect.url,
                         fileset_view.instance.get_absolute_url())

    def test_FileSetRedirect_get_task(self):
        def fake_method():
            return True

        fileset_view = fileset.FileSetRedirect()
        fileset_view.task = fake_method

        return_value = fileset_view.get(self.request,
                                        content_type='biologicalsample',
                                        ct_pk=1, fs_pk=1)

        self.assertTrue(return_value)

    def test_FileSetRedirect_task(self):
        fileset_view = fileset.FileSetRedirect()

        with self.assertRaises(ImproperlyConfigured):
            fileset_view.task()

    # def test_Upload_view_template(self):
    #     self.create_fake_objects()
    #
    #     url = reverse('fileset:upload',
    #                   args=('biologicalsample', self.bs.pk, self.fs.pk,
    #                         'Fasta'))
    #     response = self.client.get(url)
    #
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'mhc_pipeline/base_site/form.html')
    #     self.assertIsInstance(response.context['form'],
    #                           UploadedFileForm)

    # def test_Upload_form_valid(self):
    #     self.create_fake_objects()
    #     self.fs.set_class = 3
    #     self.fs.save()
    #
    #     url = reverse('fileset:upload',
    #                   args=('biologicalsample', self.bs.pk, self.fs.pk,
    #                         'Fasta'))
    #
    #     file = open(os.path.join(settings.MS_DIR, 'fasta', 'test.fa'), 'r')
    #     response = self.client.post(url, {'file': file})
    #     file.close()
    #
    #     expected_url = reverse('biological_sample:view',
    #                            args=(self.bs.pk,))
    #
    #     self.assertRedirects(response, expected_url)
    #
    #     file_full_path = os.path.join(settings.MEDIA_ROOT, 'Fasta',
    #                                   'test_uploaded_file_id_1.fa.zip')
    #
    #     self.assertTrue(os.path.exists(file_full_path))
    #
    #     os.remove(file_full_path)

    def test_Tab(self):
        self.create_fake_objects()

        self.kwargs = {'pk': 1,
                       'fileset_classes': 'Fasta'
                       }

        self.url = reverse('fileset:tabview',
                           kwargs=self.kwargs)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

    def test_ViewFiles(self):
        self.create_fake_objects()
        self.fs.set_class = 3
        self.fs.save()

        url = reverse('fileset:view_files',
                      args=('biologicalsample', self.bs.pk, self.fs.pk))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_filter_biological_filesets(self):

        self.create_fake_objects()

        filesets = fileset.filter_biological_filesets(1, [2])
        self.assertEquals(filesets.count(), 1)

        filesets = fileset.filter_biological_filesets(1, [1])
        self.assertEquals(filesets.count(), 0)

        filesets = fileset.filter_biological_filesets(1, [1, 2])
        self.assertEquals(filesets.count(), 1)
