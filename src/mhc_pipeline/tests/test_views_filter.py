"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.contrib.auth.models import User
from django.urls import reverse

# Third party libraries

# Import project libraries
from .base import BaseTest
import mhc_pipeline.views.filter as f
from ..models import Filter


class Test(BaseTest):

    def test_hide(self):
        self.helpers.create_filter(permissions=['view'])

        url = reverse('filter:hide', args=(1, 'true'))
        self.client.get(url)

        url = reverse('filter:hide', args=(1, 'false'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Edit(self):
        self.helpers.create_filter(permissions=['change'])

        url = reverse('filter:edit', args=(1,))
        response = self.client.post(url, data={'name': 'Test'})

        expected_url = reverse('filter:list')

        self.assertRedirects(response, expected_url)

    def test_List(self):
        self.helpers.create_filter(permissions=['view'])

        url = reverse('filter:hide', args=(1, 'true'))
        self.client.get(url)

        url = reverse('filter:list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Share(self):
        self.helpers.create_filter(permissions=['change'])

        url = reverse('filter:share', args=(1,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Share_post(self):
        self.helpers.create_filter(permissions=['change'])

        User.objects.create_user(username='username',
                                 password='password',
                                 email='user@fake.com')

        url = reverse('filter:share', args=(1,))
        response = self.client.post(url, data={'users': [3, ]})

        expected_url = reverse('filter:list')

        self.assertRedirects(response, expected_url)
