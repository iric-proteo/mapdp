"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from pathlib import Path
import shutil
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings
from django.urls import reverse

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..models import Genotyping


class Test(BaseTest):
    # Helpers

    # Tests
    def test_download(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view', 'change'])
        fs = self.helpers.create_biological_sample_file_set(
            8, biological_sample=biological_sample)
        self.assertFalse(fs.locked)
        self.helpers.create_file(fs)

        values = {
            'file_set': 1,
        }

        url = reverse('genotyping:create', args=(1,))

        with patch('subprocess.run') as p:
            response = self.client.post(url, values)
            p.assert_called_once()

        url = reverse('genotyping:download', args=(1,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_View_Create(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])
        fs = self.helpers.create_biological_sample_file_set(
            8, biological_sample=biological_sample)
        self.assertFalse(fs.locked)
        self.helpers.create_file(fs)

        values = {
            'file_set': 1,
        }

        url = reverse('genotyping:create', args=(1,))

        with patch('subprocess.run') as p:
            response = self.client.post(url, values)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(Genotyping.objects.count(), 1)
            p.assert_called_once()

        fs.refresh_from_db()
        self.assertTrue(fs.locked)

    def test_View_State(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view', 'change'])
        fs = self.helpers.create_biological_sample_file_set(
            8, biological_sample=biological_sample)
        self.assertFalse(fs.locked)
        self.helpers.create_file(fs)
        
        values = {
            'file_set': 1,
        }

        with patch('subprocess.run') as p:
            url = reverse('genotyping:create', args=(1,))
            response = self.client.post(url, values)
            p.assert_called_once()

        url = reverse('genotyping:state', args=(1,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_View_Tab_get_context(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])

        url = reverse('genotyping:tabview', args=(1,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_View_Trash(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])
        fs = self.helpers.create_biological_sample_file_set(
            8, biological_sample=biological_sample)
        self.assertFalse(fs.locked)
        self.helpers.create_file(fs)

        values = {
            'file_set': 1,
        }

        work_dir = Path(settings.NEXTFLOW_FS) / 'genotyping' / str(1)
        work_dir.mkdir(exist_ok=True, parents=True)
        self.assertTrue(work_dir.exists())

        with patch('subprocess.run') as p:
            url = reverse('genotyping:create', args=(1,))
            self.client.post(url, values)
            self.assertEqual(Genotyping.objects.count(), 1)
            fs.refresh_from_db()
            self.assertTrue(fs.locked)
            p.assert_called_once()

        url = reverse('genotyping:trash', args=(1, 'true'))
        self.client.get(url)

        self.assertTrue(Genotyping.objects.get(pk=1).trashed)
        fs.refresh_from_db()
        self.assertFalse(fs.locked)

        self.assertFalse(work_dir.exists())

