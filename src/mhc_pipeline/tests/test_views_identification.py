"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from unittest.mock import patch

# Import Django related libraries
from django.core.exceptions import PermissionDenied
from django.http import QueryDict
from django.urls import reverse

# Third party libraries

# Import project libraries
from .base import BaseTest
from ..forms import FilterIdentificationForm
from ..library.minio import get_storage
from ..library.permission import set_content_permission
from ..models import (BiologicalSampleMHCAllele,
                      Identification)
from ..views.identification import (peptide_search_mode,
                                    validate_data_sets,
                                    View)
from ..views.peaks import get_search_parameters


class Test(BaseTest):
    # Helpers

    def create_bs_id(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view'])
        identification = self.helpers.create_identification(
            biological_sample=biological_sample)

        return biological_sample, identification

    def cache_identification(self):

        self.create_bs_id()

        url = reverse('identification:view')
        values = {
            'filter': True,
            'identification': '1',
            'ignore_cache': True,
        }
        self.key = '06eac56abd6ecd75bcf71ac833b84f102'

        response = self.client.get(url, values)

        return response

    def clean_cache(self):

        key = self.key
        storage = get_storage()
        self.assertTrue(storage.exists(key))
        storage.delete(key)
        self.assertFalse(storage.exists(key))

        self.assertTrue(storage.exists('csv_' + key))
        storage.delete('csv_' + key)
        self.assertFalse(storage.exists('csv_' + key))

    # Tests
    def test_csv(self):
        values = {
            'key': '2'
        }

        url = reverse('identification:csv', kwargs=values)

        response = self.client.post(url, values)

        self.assertEqual(response.status_code, 200)

    def test_csv_content(self):

        self.cache_identification()

        values = {
            'key': self.key
        }

        url = reverse('identification:csv', kwargs=values)

        response = self.client.post(url, values)

        self.assertEqual(response.status_code, 200)

        next(response.streaming_content)

        self.clean_cache()

    def test_csv_permission_denied(self):
        values = {
            'key': '1'
        }

        url = reverse('identification:csv', kwargs=values)

        response = self.client.post(url, values)

        self.assertEqual(response.status_code, 403)

    def test_identification_task_cache(self):

        self.cache_identification()

        values = {
            'key': self.key
        }

        url = reverse('identification:task', kwargs=values)

        response = self.client.post(url, values)

        self.assertEqual(response.status_code, 200)

        self.clean_cache()

    def test_identification_task_none(self):

        values = {
            'key': 'test2'
        }

        url = reverse('identification:task', kwargs=values)

        response = self.client.post(url, values)

        self.assertEqual(response.status_code, 200)

    def test_identification_task_permission_denied(self):
            values = {
                'key': '1'
            }

            url = reverse('identification:task', kwargs=values)

            response = self.client.post(url, values)

            self.assertEqual(response.status_code, 403)

    def test_Trash(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        identification = self.helpers.create_identification(
            biological_sample=biological_sample)

        url = reverse('identification:trash', args=(1, 'false'))
        self.client.get(url)

        self.assertFalse(identification.trashed)

        url = reverse('identification:trash', args=(1, 'true'))
        self.client.get(url)

        identification = Identification.objects.get(pk=1)

        self.assertTrue(identification.trashed)

    def test_get_filter_form(self):
        biological_sample, identification = self.create_bs_id()

        allele = self.helpers.create_mhc_allele()

        obj = BiologicalSampleMHCAllele.objects.create(
            biological_sample=biological_sample)

        obj.mhc_allele.add(allele)

        self.helpers.create_filter()

        url = reverse('identification:view_id', args=(1,))
        values = {
            'selected_filter': 1,
            'filter': True,
            'remove_decoy': True,
            'bs_pk': 1
        }

        response = self.client.get(url, values)

        self.assertEqual(response.status_code, 200)

    def test_get_filter_form_no_allele(self):
        self.create_bs_id()

        self.helpers.create_mhc_allele()

        self.helpers.create_filter()

        url = reverse('identification:view_id', args=(1,))
        values = {
            'selected_filter': 1,
            'filter': True,
            'remove_decoy': True,
            'bs_pk': 1
        }

        response = self.client.get(url, values)

        self.assertEqual(response.status_code, 200)

    def test_View_template(self):

        response = self.cache_identification()

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/identification/view.html')
        self.assertIsInstance(response.context['form'],
                              FilterIdentificationForm)

        self.clean_cache()

    def test_View_timestamp(self):
        self.create_bs_id()

        url = reverse('identification:view')
        values = {
            'filter': True,
            'identification': '1',
        }

        key = '4295a9428e2b252f92afea46995fdde12'

        def fake(a):
            content = '{"validation_timestamp": 10}'
            return content

        with patch('mhc_pipeline.views.identification.identification_content',
                   fake):

            response = self.client.get(url, values)

            self.assertEqual(response.status_code, 200)

        self.key = key
        self.clean_cache()

    def test_View_immunopeptidome(self):
        self.create_bs_id()

        url = reverse('identification:view')
        values = {
            'filter': True,
            'identification': '1',
            'immunopeptidome_name': 'Test',
        }

        response = self.client.get(url, values)

        self.assertEqual(response.status_code, 302)

    def test_View_peptides_plots(self):
        self.create_bs_id()

        url = reverse('identification:view')
        values = {
            'filter': True,
            'identification': '1',
            'immunopeptidome_name': 'Test',
            'peptides_plots': 'Exploratory plots'
        }

        response = self.client.get(url, values)

        self.assertEqual(response.status_code, 200)

    def test_View_update_form_filters(self):
        data = QueryDict('filter_name=Hey')

        f = FilterIdentificationForm(data=data)

        View.update_form_filters('', f, self.user)

    def test_Tab(self):
        self.create_bs_id()

        url = reverse('identification:tabview', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_State(self):

        self.create_bs_id()

        url = reverse('identification:state', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Settings_no_file(self):
        self.create_bs_id()

        url = reverse('identification:settings', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Settings(self):
        self.helpers.import_peptides()

        url = reverse('identification:settings', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Settings_revision(self):
        self.helpers.import_peptides()

        url = reverse('identification:settings', args=(1,))

        self.client.get(url)

        get_search_parameters.revision += 1

        response = self.client.get(url)

        self.assertEqual(response.context['revision'], get_search_parameters.revision)

    def test_Settings_no_db(self):
        identification = self.helpers.import_peptides()

        identification.protein_database.all().delete()

        url = reverse('identification:settings', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.context['db'], '')

    def test_validate_data_sets_identification_no_permission(self):

        fc = {
            'identification': [1],
            'immunopeptidome': []
        }

        with self.assertRaises(PermissionDenied):
            validate_data_sets(fc, self.user)

    def test_validate_data_sets_identification_with_permission(self):

        obj = self.helpers.create_identification()
        set_content_permission(obj.biological_sample, self.user, ['view'])

        fc = {
            'identification': [1],
            'immunopeptidome': []
        }

        self.assertIs(validate_data_sets(fc, self.user), None)

    def test_validate_data_sets_immunopeptidome_no_permission(self):

        fc = {
            'identification': [],
            'immunopeptidome': [1]
        }

        with self.assertRaises(PermissionDenied):
            validate_data_sets(fc, self.user)

    def test_validate_data_sets_immunopeptidome_with_permission(self):

        obj = self.helpers.create_immunopeptidome()
        set_content_permission(obj.biological_sample.all()[0], self.user,
                               ['view'])

        fc = {
            'identification': [],
            'immunopeptidome': [1]
        }

        self.assertIs(validate_data_sets(fc, self.user), None)

    def test_peptide_search_mode(self):

        obj = self.helpers.create_identification()
        set_content_permission(obj.biological_sample, self.user, ['view'])

        fc = {
            'identification': [],
            'immunopeptidome': [],
            'peptide_sequence__icontains': 'A'
        }

        peptide_search_mode(fc, self.user)

        self.assertEqual(len(fc['identification']), 1)
