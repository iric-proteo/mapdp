"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import json
from pathlib import Path
from unittest.mock import patch

# Import Django related libraries
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

# Third party libraries
import requests_mock

# Import project libraries
from .base import BaseTest
from ..forms import ImmunoPeptidomeForm
from ..models import (ImmunoPeptidome,
                      Peptide,
                      Tag)
from ..views.immunopeptidome import (add_peptides,
                                     create_immunopeptidome)


class Test(BaseTest):

    def test_comment_edit(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['change'])
        self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)

        url = reverse('immunopeptidome:document_edit_comment', args=(1, 1))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/form.html')

    def test_comment_edit_post(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['change'])
        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)

        url = reverse('immunopeptidome:document_edit_comment', args=(1, 1))
        response = self.client.post(url, data={'comment': 'test'})

        self.assertEqual(response.status_code, 302)
        obj.refresh_from_db()

        self.assertEqual(obj.comment, 'test')

    def test_create_immunopeptidome_identification(self):
        self.helpers.create_peptide()

        fc = {'immunopeptidome_name': 'Test',
              'identification': ['1'],
              'immunopeptidome': [],
              }

        redirect = create_immunopeptidome(self.user,
                                          None,
                                          fc)

        self.assertEqual(ImmunoPeptidome.objects.count(), 1)
        self.assertEqual(redirect.url,
                         ImmunoPeptidome.objects.all()[
                             0].get_absolute_url())

    def test_create_immunopeptidome_immunopeptidome(self):
        self.helpers.create_immunopeptidome()
        filter_obj = self.helpers.create_filter('Test2')

        fc = {'immunopeptidome_name': 'Test',
              'identification': ['1'],
              'immunopeptidome': [],
              }

        create_immunopeptidome(self.user,
                               filter_obj,
                               fc)

        self.assertEqual(ImmunoPeptidome.objects.count(), 2)

    def test_add_peptides(self):
        self.helpers.create_immunopeptidome()

        fc = {
            'immunopeptidome': [1],
        }

        add_peptides(1, fc)

    def test_add_peptides_no_immunopeptidome(self):
        with self.assertRaises(ObjectDoesNotExist):
            add_peptides(0, {})

    def test_list(self):
        url = reverse('immunopeptidome:list')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/list.html')

    def test_trash(self):
        self.helpers.create_immunopeptidome(permissions=['delete'])

        url = reverse('immunopeptidome:trash', args=(1, 'false'))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/trash.html')

    def test_view(self):
        self.helpers.create_immunopeptidome(permissions=['view'])

        url = reverse('immunopeptidome:view', args=(1,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/immunopeptidome/view.html')
        self.assertTrue('obj' in response.context)

    def test_edit_template(self):
        obj = self.helpers.create_immunopeptidome(
            permissions=['change', 'view'])
        url = reverse('immunopeptidome:edit', args=(obj.pk,))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'mhc_pipeline/base_site/form.html')
        self.assertIsInstance(response.context['form'],
                              ImmunoPeptidomeForm)

    def test_edit_redirect(self):
        obj = self.helpers.create_immunopeptidome(
            permissions=['change', 'view'])
        url = reverse('immunopeptidome:edit', args=(obj.pk,))

        kwargs = {
            'name': 'A',
        }

        response = self.client.post(url, kwargs)
        self.assertRedirects(response, obj.get_absolute_url())

    def test_CreateFromFile(self):

        obj = self.helpers.create_biological_sample(identifier='a')
        obj.pk = 5
        obj.identifier = 'b'
        obj.save()

        identification = self.helpers.create_identification(
            biological_sample=obj
        )

        db = identification.protein_database.all()[0]

        identification.pk = 52
        identification.save()
        identification.protein_database.add(db)

        for pk in [649256,
                   649258,
                   649287,
                   651315,
                   651344,
                   651368,
                   651389,
                   651409,
                   651432,
                   651458,
                   651915,
                   654628,
                   654654,
                   654656,
                   654685,
                   654688,
                   655193,
                   655235,
                   655269,
                   655305,
                   655347,
                   657592,
                   659974,
                   660033,
                   660064,
                   661670,
                   661707,
                   663564,
                   ]:
            obj = self.helpers.create_peptide(identification=identification)
            obj.pk = pk
            obj.save()



        file_name = Path(
            '.') / 'test_files' / 'mapdp_identification' / 'small.csv'

        with open(file_name, 'rb') as f:
            test_file = SimpleUploadedFile('test.csv', f.read(),
                                           content_type='text/csv')

            data = {
                'comment': 'Test',
                'file': test_file,
                'name': 'Test'
            }

            url = reverse('immunopeptidome:create_from_file')

            with patch('mhc_pipeline.views.immunopeptidome.CreateFromFile.'
                       'check_peptides_permission', ):
                expected_url = reverse('immunopeptidome:view', args=(1,))
                response = self.client.post(url, data)

                self.assertRedirects(response, expected_url)

    def test_CreateFromFile_permission_denied(self):
        file_name = Path(
            '.') / 'test_files' / 'mapdp_identification' / 'small.csv'

        with open(file_name, 'rb') as f:
            test_file = SimpleUploadedFile('test.csv', f.read(),
                                           content_type='text/csv')

            data = {
                'comment': 'Test',
                'file': test_file,
                'name': 'Test'
            }

            url = reverse('immunopeptidome:create_from_file')
            response = self.client.post(url, data)

            self.assertEqual(response.status_code, 403)

    def test_document_download(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view'])
        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)

        url = reverse('immunopeptidome:document_download', args=(1, 1))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_document_trash(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['change'])
        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)

        url = reverse('immunopeptidome:document_trash', args=(1, 1))
        response = self.client.get(url)

        self.assertEqual(response.status_code, 302)

        obj.refresh_from_db()

        self.assertTrue(obj.trashed)

    def test_annotate(self):
        url = 'http://localhost'
        settings.PPS_HOST = url

        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['change'])

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '',
            }]

            m.get(url + '/personalized_proteomes/?ids=',
                  json=data)

            url = reverse('immunopeptidome:annotate', args=(1,))
            response = self.client.get(url)

            self.assertEqual(response.status_code, 200)

    @patch('celery.canvas.Signature.delay')
    def test_annotate_post(self, mock_api_call):
        url = 'http://localhost'
        settings.PPS_HOST = url

        self.helpers.create_annotation()

        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['change'])

        self.helpers.create_personalized_proteome(
            biological_sample=immunopeptidome.biological_sample.all()[0])

        with requests_mock.Mocker() as m:
            data = [{
                'id': 1,
                'snps_name': '',
                'url': '/1/',
            }]

            m.get(url + '/personalized_proteomes/?ids=1',
                  json=data)

            url = reverse('immunopeptidome:annotate', args=(1,))

            form_data = {
                'annotation_dbsnp_1': True,
                'format': 'csv',
                'personalized_proteome': '/1/'
            }

            response = self.client.post(url, data=form_data)

            self.assertEqual(response.status_code, 302)

            self.assertTrue(mock_api_call.called)

            form_data = {
                'format': 'bed',
                'personalized_proteome': '/1/'
            }

            response = self.client.post(url, data=form_data)

            self.assertEqual(response.status_code, 302)

    def test_DocumentState(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view'])
        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)

        url = reverse('immunopeptidome:document_state', args=(1, 1))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_PeptideCount(self):
        self.helpers.create_immunopeptidome(
            permissions=['view'])

        url = reverse('immunopeptidome:peptide_count', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_PeptideCount_failure(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view'])

        immunopeptidome.comments = 'Unexpected failure'

        immunopeptidome.save()

        url = reverse('immunopeptidome:peptide_count', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_Plots(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view'])

        url = reverse('immunopeptidome:plots', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

        immunopeptidome.peptide_count = -1
        immunopeptidome.save()

        url = reverse('immunopeptidome:plots', args=(1,))

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_Tab(self):
        self.helpers.create_immunopeptidome(permissions=['view'])

        url = reverse('immunopeptidome:tabview', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_MiHAFilter(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view', 'change'])

        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)
        obj.state = 'success'

        values = {
            'annotation': ['Human protein atlas tissue RNA expression',
                           'TCGA AML expression', 'gnomAD']
        }
        obj.json_values = json.dumps(values)
        obj.save()

        url = reverse('immunopeptidome:miha_filter', args=(1,))

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    def test_MiHAFilter_POST(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view', 'change'])

        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)
        obj.state = 'success'

        values = {
            'annotation': ['Human protein atlas tissue RNA expression',
                           'TCGA AML expression', 'gnomAD']
        }
        obj.json_values = json.dumps(values)
        obj.save()

        url = reverse('immunopeptidome:miha_filter', args=(1,))

        data = {
            'document': 1,
            'maf': 0.05
        }

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 302)

    def test_MiHAFilter_POST_create_immunopeptidome(self):
        immunopeptidome = self.helpers.create_immunopeptidome(
            permissions=['view', 'change'])

        Tag.objects.create(name='MiHA')

        obj = self.helpers.create_immunopeptidome_document(
            immunopeptidome=immunopeptidome)
        obj.state = 'success'

        values = {
            'annotation': ['Human protein atlas tissue RNA expression',
                           'TCGA AML expression', 'gnomAD']
        }
        obj.json_values = json.dumps(values)
        obj.save()

        url = reverse('immunopeptidome:miha_filter', args=(1,))

        data = {
            'document': 1,
            'immunopeptidome_name': 'Test',
            'maf': 0.05
        }

        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 302)
