"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from collections import defaultdict
from pathlib import Path
from unittest.mock import patch

# Import Django related libraries
from django.db import IntegrityError

# Third party libraries

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
import mhc_pipeline.views.maxquant as mq


# Create your tests here.


class MaxQuantTest(BaseTest):
    def setUp(self):
        super().setUp()

        self.path = Path('.') / 'test_files' / 'MaxQuant' / 'txt'

    def test_check_files(self):
        fs = self.helpers.create_file_set()

        with self.assertRaises(BaseSiteException):
            mq.check_files(fs)

        self.helpers.create_file(file_set=fs, name='evidence.txt')

        with self.assertRaises(BaseSiteException):
            mq.check_files(fs)

        fs = self.helpers.create_file_set()

        file_names = ['evidence.txt', 'parameters.txt', 'peptides.txt']
        for name in file_names:
            self.helpers.create_file(file_set=fs, name=name, path=self.path)

        self.assertIsInstance(mq.check_files(fs), dict)

    def test_read_apl_scans(self):
        self.assertIsInstance(mq.read_apl_scans(self.path), defaultdict)

    def test_read_modifications(self):
        parameters = {
            'Site tables': 'Oxidation (M)Sites.txt'
        }

        self.assertIsInstance(mq.read_modifications(self.path, parameters),
                              defaultdict)

    def test_read_parameters(self):
        self.assertIsInstance(mq.read_parameters(self.path / 'parameters.txt'),
                              dict)

    def test_read_peptides_proteins(self):
        self.assertIsInstance(mq.read_peptides_proteins(self.path / 'peptides.txt'),
                              dict)

    def test_import_peptides(self):
        identification = self.helpers.import_peptide_maxquant()
        self.assertEqual(identification.state, 'success')

    def test_import_peptides_read_parameters(self):

        def raise_exception(*args, **kwargs):
            raise ValueError

        module_name = 'mhc_pipeline.views.maxquant'

        with patch(module_name + '.read_parameters', raise_exception):
            self.helpers.import_peptide_maxquant()

    def test_import_peptides_read_peptides_proteins(self):

        def raise_exception(*args, **kwargs):
            raise ValueError

        module_name = 'mhc_pipeline.views.maxquant'

        with patch(module_name + '.read_peptides_proteins', raise_exception):
            self.helpers.import_peptide_maxquant()

    def test_import_peptides_read_modifications(self):

        def raise_exception(*args, **kwargs):
            raise ValueError

        module_name = 'mhc_pipeline.views.maxquant'

        with patch(module_name + '.read_modifications', raise_exception):
            self.helpers.import_peptide_maxquant()

    def test_import_peptides_read_apl_scans(self):

        def raise_exception(*args, **kwargs):
            raise ValueError

        module_name = 'mhc_pipeline.views.maxquant'
        with patch(module_name + '.read_apl_scans', raise_exception):
            self.helpers.import_peptide_maxquant()

    def test_import_peptides_store(self):

        def raise_exception2(*args, **kwargs):
            raise IntegrityError

        with patch('mhc_pipeline.library.bulk_insert.BulkInsert.store',
                   raise_exception2):
            self.helpers.import_peptide_maxquant()

    def test_import_peptides_keyerror(self):

        def raise_exception2(*args, **kwargs):
            raise KeyError

        with patch('mhc_pipeline.library.bulk_insert.BulkInsert.store',
                   raise_exception2):
            self.helpers.import_peptide_maxquant()

    def test_import_peptides_alt(self):
        identification = self.helpers.import_peptide_maxquant_alt()
        self.assertEqual(identification.state, 'success')