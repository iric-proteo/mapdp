"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json

# Import Django related libraries
from django.contrib.auth.models import Group
from django.urls import reverse
from django.test import Client

# Third party libraries

# Import project libraries
from .base import BaseTest
from mhc_pipeline.library.excel_login import excel_login_required
from ..models import File

from ..views.msms import format_peptide_seq_with_mod


class Test(BaseTest):
    # Tests
    def test_excel_login_required(self):
        func = excel_login_required()
        self.assertEqual(func.__name__, 'decorator')

    def test_format_peptide_seq_with_mod(self):
        peptide = self.helpers.create_peptide()

        self.assertEqual(format_peptide_seq_with_mod(peptide), 'PEPYphPEPY')

        peptide.modification = '[{"location": 0, "monoisotopicMassDelta": ' \
                               '79.96633, "name": "Phospho", "residues": ["Y"]}]'

        self.assertEqual(format_peptide_seq_with_mod(peptide), 'PphEPYPEPY')

        peptide.modification = '""'
        self.assertEqual(format_peptide_seq_with_mod(peptide),
                         peptide.peptide_sequence)

    def test_format_peptide_seq_with_mod_tmt(self):
        # Test for Issue #149

        peptide = self.helpers.create_peptide()

        peptide.peptide_sequence = 'LLIENVASL'

        modifications = [
            {
                'location': 0,
                'monoisotopicMassDelta': 224.15248,
                'name': 'TMT'
            },
            {
                'location': 8,
                'monoisotopicMassDelta': 224.15248,
                'residues': [
                    'S'
                ],
                'unknown modification': 'TMT STY'
            }
        ]

        peptide.modification = json.dumps(modifications)

        self.assertEqual(format_peptide_seq_with_mod(peptide),
                         'LtmtlLIENVAStmtlL')

    def test_validation_bad_value(self):
        self.helpers.import_peptides()

        url = reverse('msms:validation', args=(1, 'X'))
        response = self.client.get(url)

        self.assertEqual(response.json(), False)

    def test_validation_content_type(self):
        self.helpers.import_peptides()

        url = reverse('msms:validation', args=(1, 'G'))
        response = self.client.get(url)

        self.assertEqual(response['content-type'], 'application/json')

    def test_validation_group(self):
        self.group = Group.objects.create(name='validation')
        self.group.user_set.add(self.user)

        self.helpers.import_peptides()

        url = reverse('msms:validation', args=(1, 'G'))
        response = self.client.get(url)

        self.assertTrue(response.json())

    def test_view_apl(self):
        self.helpers.import_peptide_maxquant()

        url = reverse('msms:view', args=(1,))
        response = self.client.get(url)

        self.assertTemplateUsed(
            response,
            'mhc_pipeline/msms/spectrum_viewer.html')

    def test_view_bad_agent(self):
        url = reverse('msms:view', args=(27,))
        response = Client(HTTP_USER_AGENT='Excel').get(url)

        self.assertEqual(response.status_code, 200)

    def test_view_file_not_found(self):
        self.helpers.import_peptides()

        f = File.objects.get(extension='.mgf')
        f.path = ''
        f.save()

        url = reverse('msms:view', args=(27,))
        response = self.client.get(url)

        self.assertTemplateUsed(response, 'error.html')

    def test_view_not_logged(self):
        url = reverse('msms:view', args=(27,))
        response = Client().get(url)

        self.assertEqual(response.status_code, 302)

    def test_view_validation_group(self):
        self.group = Group.objects.create(name='validation')
        self.group.user_set.add(self.user)

        self.helpers.import_peptides()

        url = reverse('msms:view', args=(27,))
        response = self.client.get(url)

        self.assertTemplateUsed(
            response,
            'mhc_pipeline/msms/spectrum_viewer.html')
