"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from pathlib import Path
import os
from unittest.mock import patch

# Import Django related libraries
from django.db import IntegrityError

# Third party libraries

# Import project libraries
from .base import BaseTest
from base_site.exceptions import BaseSiteException
from ..library.bulk_insert import BulkInsert
from ..models import (Identification,
                      Peptide,
                      ProteinSequence,
                      UniquePeptide)
from ..views import peaks


class Test(BaseTest):
    def test_check_files(self):
        fs = self.helpers.create_file_set()

        with self.assertRaises(BaseSiteException):
            peaks.check_files(fs)

        file_names = ['test.mgf', 'test.mzid']
        for name in file_names:
            self.helpers.create_file(file_set=fs, extension=Path(name).suffix,
                                     name=name, path='')

        with self.assertRaises(BaseSiteException):
            peaks.check_files(fs)

        path = os.path.join(fs.get_class_attribute('dir'), 'mzid')

        fs = self.helpers.create_file_set()

        with self.assertRaises(BaseSiteException):
            peaks.check_files(fs)

        file_names = ['Yeast_2015-05-26_R9.mgf', 'test.mzid']
        for name in file_names:
            self.helpers.create_file(file_set=fs, extension=Path(name).suffix,
                                     name=name, path=path)

        with self.assertRaises(BaseSiteException):
            peaks.check_files(fs)

        fs = self.helpers.create_file_set()

        file_names = ['Yeast_2015-05-26_R9.mgf', 'peptides_1_1_0_scaffold.mzid']
        for name in file_names:
            self.helpers.create_file(file_set=fs, extension=Path(name).suffix,
                                     name=name, path=path)

        peaks.check_files(fs)

        fs = self.helpers.create_file_set()

        file_names = ['Yeast_2015-05-26_R9.mgf', 'peptides_1_0_0_header.mzid']
        for name in file_names:
            self.helpers.create_file(file_set=fs, extension=Path(name).suffix,
                                     name=name, path=path)

        with self.assertRaises(BaseSiteException):
            peaks.check_files(fs)

        fs = self.helpers.create_file_set()

        file_names = ['Yeast_2015-05-26_R9.mgf']
        for name in file_names:
            self.helpers.create_file(file_set=fs, extension=Path(name).suffix,
                                     name=name, path=path)

        with self.assertRaises(BaseSiteException):
            peaks.check_files(fs)

        fs = self.helpers.create_file_set()

        file_names = ['Yeast_2015-05-26_R9.mgf', 'test.mzid', 'test2.mzid']
        for name in file_names:
            self.helpers.create_file(file_set=fs, extension=Path(name).suffix,
                                     name=name, path=path)

        with self.assertRaises(BaseSiteException):
            peaks.check_files(fs)

    def test_read_mgf_files(self):

        fs = self.helpers.create_file_set()
        path = os.path.join(fs.get_class_attribute('dir'), 'mzid')

        file_names = ['Yeast_2015-05-26_R9.mgf', 'peptides_1_1_0_scaffold.mzid']
        for name in file_names:
            self.helpers.create_file(file_set=fs, extension=Path(name).suffix,
                                     name=name, path=path)

        files = peaks.check_files(fs)

        self.assertIsInstance(peaks.read_mgf_files(files['mgf']), dict)

    def test_PDM(self):

        identification = self.helpers.create_identification()
        protein_bulk = BulkInsert(ProteinSequence, ['protein_database_id',
                                                    'accession'])

        pdm = peaks.ProteinDatabaseManager(identification, protein_bulk)

        self.assertEqual(pdm.identification, identification)

    def test_PDM_get_or_create_database(self):

        identification = self.helpers.create_identification()
        protein_bulk = BulkInsert(ProteinSequence, ['protein_database_id',
                                                    'accession'])

        pdm = peaks.ProteinDatabaseManager(identification, protein_bulk)

        search_db_ref = {'name': 'test',
                         'FileFormat': 'fasta',
                         'location': 'space',
                         'numDatabaseSequences': 100,
                         }

        value = pdm.get_or_create_database(search_db_ref)
        self.assertEqual(value, 2)

    def test_PDM_process_proteins(self):
        identification = self.helpers.create_identification()
        protein_bulk = BulkInsert(ProteinSequence, ['protein_database_id',
                                                    'accession'])

        pdm = peaks.ProteinDatabaseManager(identification, protein_bulk)

        proteins = [{'peptideEvidence_ref': [None]}]

        gen = pdm.process_proteins(proteins)

        with self.assertRaises(StopIteration):
            next(gen)

        search_db_ref = {'name': 'test',
                         'FileFormat': 'fasta',
                         'location': 'space',
                         'numDatabaseSequences': 100,
                         }

        proteins = [{'isDecoy': False,
                     'dBSequence_ref': [{'searchDatabase_ref': [search_db_ref],
                                         'accession': 'A',
                                         'Seq': 'A',
                                         'length': 1
                                         }]}]

        gen = pdm.process_proteins(proteins)
        self.assertIsInstance(next(gen), dict)

    def test_import_peptides(self):
        identification = self.helpers.import_peptides()

        self.assertEqual(identification.state, 'success')

        # Check if identification and peptides are stored
        self.assertEqual(Identification.objects.count(), 1)
        self.assertEqual(ProteinSequence.objects.count(), 688)
        self.assertEqual(UniquePeptide.objects.count(), 111)
        self.assertEqual(Peptide.objects.count(), 125)
        through_model = Peptide.protein_matches.through
        self.assertEqual(through_model.objects.count(), 1405)

    def test_import_peptides_parse_exceptions(self):
        def raise_exception(*args, **kwargs):
            raise ValueError

        module_name = 'library.mgf_parser.MGFParser'

        with patch(module_name + '.get_scans_dict', raise_exception):
            identification = self.helpers.import_peptides()
            self.assertEqual(identification.state, 'failed')

    def test_import_peptides_store_exceptions(self):
        def raise_exception(*args, **kwargs):
            raise ValueError

        with patch('mhc_pipeline.library.bulk_insert.BulkInsert.get_create_pk',
                   raise_exception):
            identification = self.helpers.import_peptides()
            self.assertEqual(identification.state, 'failed')

    def test_import_peptides_integrity_exceptions(self):
        def raise_exception2(*args, **kwargs):
            raise IntegrityError

        with patch('mhc_pipeline.library.bulk_insert.BulkInsert.store',
                   raise_exception2):
            identification = self.helpers.import_peptides()
            self.assertEqual(identification.state, 'failed')

    def test_import_peptides_missing_file(self):
        identification = self.helpers.import_peptides(missing_file=True)
        self.assertEqual(identification.state, 'failed')

        # Check if identification and peptides are stored
        self.assertEqual(Identification.objects.count(), 1)
        self.assertEqual(Peptide.objects.count(), 0)

    def test_import_peptides_wrong_version(self):
        identification = self.helpers.import_peptides(wrong_version=True)
        self.assertEqual(identification.state, 'failed')

        # Check if identification and peptides are stored
        self.assertEqual(Identification.objects.count(), 1)
        self.assertEqual(Peptide.objects.count(), 0)

    def test_import_peptides_missing_mgf(self):
        def empty(*args, **kwargs):
            return dict()

        module_name = 'mhc_pipeline.views.peaks'

        with patch(module_name + '.read_mgf_files', empty):
            identification = self.helpers.import_peptides()
            self.assertEqual(identification.state, 'failed')

    def test_get_search_parameters(self):

        file = './test_files/mzid/search_parameters.xml'
        peaks.get_search_parameters(file)

        file = './test_files/mzid/search_parameters_2.xml'
        peaks.get_search_parameters(file)

        file = './test_files/mzid/search_parameters_3.xml'
        search_parameters = peaks.get_search_parameters(file)

        self.assertIsInstance(search_parameters, dict)
