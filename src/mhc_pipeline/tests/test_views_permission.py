"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.contrib.auth.models import Group, User
from django.urls import reverse

# Third party libraries
from guardian.shortcuts import assign_perm

# Import project libraries
from .base import BaseTest
from ..forms import (PermissionUpdateForm,
                     PermissionGroupForm,
                     PermissionUserForm)
from ..library.permission import set_content_permission
from ..views.permission import (DisplayPermissions,
                                permission_context)


# Create your tests here.

class Test(BaseTest):
    # Helper methods
    def create_displayperms(self):
        return DisplayPermissions(name='Test',
                                  pk=1,
                                  permissions=[])

    # Test methods
    def test_displayperms_init(self):
        dp = self.create_displayperms()

        self.assertIsInstance(dp, DisplayPermissions)
        self.assertFalse(dp.view)

    def test_displayperms_get_display_perms(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        users_perms, groups_perms = DisplayPermissions.get(biological_sample)

        self.assertEqual(len(users_perms), 1)
        self.assertEqual(len(groups_perms), 0)
        self.assertIsInstance(users_perms[0], DisplayPermissions)

        assign_perm('view_biologicalsample', self.group, biological_sample)
        users_perms, groups_perms = DisplayPermissions.get(biological_sample)

        self.assertEqual(len(groups_perms), 1)
        self.assertIsInstance(groups_perms[0], DisplayPermissions)

    def test_update_view_edit_form_template(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'delete', 'view'])

        url = reverse('permission_update',
                      args=('user', self.user.pk,
                            'biologicalsample', biological_sample.pk))
        response = self.client.get(url)

        self.assertTemplateUsed(response, 'mhc_pipeline/permission/update.html')
        self.assertIsInstance(response.context['form'], PermissionUpdateForm)

    def test_update_view_edit_no_user_form(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        url = reverse('permission_update',
                      args=('user', 0,
                            'biologicalsample', biological_sample.pk))
        response = self.client.get(url)

        self.assertIsInstance(response.context['form'], PermissionUserForm)

    def test_update_view_edit_no_group_form(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        url = reverse('permission_update',
                      args=('group', 0,
                            'biologicalsample', biological_sample.pk))
        response = self.client.get(url)

        self.assertIsInstance(response.context['form'], PermissionGroupForm)

    def test_update_view_edit_POST_user_form(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        url = reverse('permission_update',
                      args=('user', self.user.pk,
                            'biologicalsample', biological_sample.pk))

        kwargs = {
            'fake': '',
        }
        response = self.client.post(url, kwargs)

        self.assertEqual(response.status_code, 302)

    def test_update_view_edit_POST_checked_user_redirect(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        url = reverse('permission_update',
                      args=('user', self.user.pk,
                            'biologicalsample', biological_sample.pk))

        kwargs = {
            'view': 'on',
            'edit': 'on',
            'delete': 'on',
        }
        response = self.client.post(url, kwargs)
        expected_url = reverse('biological_sample:view', args=(1,))

        self.assertRedirects(response, expected_url)

    def test_update_view_edit_POST_no_user_form(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        url = reverse('permission_update',
                      args=('user', 0,
                            'biologicalsample', biological_sample.pk))

        kwargs = {
            'view': 'on',
            'edit': 'on',
            'delete': 'on',
            'user': self.user.pk,
        }
        response = self.client.post(url, kwargs)
        expected_url = reverse('biological_sample:view', args=(1,))

        self.assertRedirects(response, expected_url)

    def test_update_view_edit_POST_no_group_form(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        url = reverse('permission_update',
                      args=('group', 0,
                            'biologicalsample', biological_sample.pk))

        kwargs = {
            'view': 'on',
            'edit': 'on',
            'delete': 'on',
            'group': self.group.pk,
        }
        response = self.client.post(url, kwargs)

        self.assertEqual(response.status_code, 200)

    def test_update_view_edit_POST_no_group_form_invalid(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change'])

        url = reverse('permission_update',
                      args=('group', 0,
                            'biologicalsample', biological_sample.pk))

        kwargs = {
            'view': 'on',
            'edit': 'on',
            'delete': 'on',
            'group': 'invalid',
        }
        response = self.client.post(url, kwargs)

        self.assertTrue('group' in response.context['form'].errors)

    def test_View(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['view', 'change'])

        url = reverse('permission_view',
                      args=('biologicalsample', biological_sample.pk))

        response = self.client.get(url)

        self.assertTrue('obj' in response.context)

    def test_permission_context(self):

        obj = self.helpers.create_immunopeptidome()
        setattr(obj, 'content_type', 'immunopeptidome')
        set_content_permission(obj, self.user, ['view'])

        context = permission_context(obj, 'immunopeptidome', self.user)

        self.assertTrue(context['users_perms'][0].view)

    def test_permission_context_two_biological_samples_agreed(self):

        obj = self.helpers.create_immunopeptidome()
        setattr(obj, 'content_type', 'immunopeptidome')

        biological_sample1 = obj.biological_sample.all()[0]
        biological_sample2 = self.helpers.create_biological_sample(
            identifier='test2')
        obj.biological_sample.add(biological_sample2)

        set_content_permission(biological_sample1, self.user, ['view'])
        set_content_permission(biological_sample2, self.user, ['view'])

        context = permission_context(obj, 'immunopeptidome', self.user)
        self.assertTrue(context['users_perms'][0].view)

    def test_permission_context_two_biological_samples_diff(self):

        obj = self.helpers.create_immunopeptidome()
        setattr(obj, 'content_type', 'immunopeptidome')

        biological_sample1 = obj.biological_sample.all()[0]
        biological_sample2 = self.helpers.create_biological_sample(
            identifier='test2')
        obj.biological_sample.add(biological_sample2)

        set_content_permission(biological_sample1, self.user, ['view'])

        context = permission_context(obj, 'immunopeptidome', self.user)
        self.assertEqual(len(context['users_perms']), 0)

    def test_permission_context_immunopeptidome_biological_samples(self):

        obj = self.helpers.create_immunopeptidome(permissions=['view'])
        setattr(obj, 'content_type', 'immunopeptidome')

        biological_sample1 = obj.biological_sample.all()[0]

        set_content_permission(biological_sample1, self.user, ['view'])

        context = permission_context(obj, 'immunopeptidome', self.user)
        self.assertTrue(context['users_perms'][0].view)

