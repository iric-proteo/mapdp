"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

# Third party libraries
import requests_mock

# Import project libraries
from .base import BaseTest
import mhc_pipeline.models as models
from ..views import pps


class PPSTest(BaseTest):
    def setUp(self):
        self.url = 'http://localhost'
        settings.PPS_HOST = self.url

        genome = {
            'id': 1,
            'file': '/test',
            'url': self.url + '/genomes/1/',
            'name': 'Test',
            'species': 'Human',
        }

        snp = {
            'id': 1,
            'created_at': '',
            'comment': '',
            'file': '/test',
            'genome': self.url + '/genomes/1/',
            'genome_name': 'Genome',
            'species': 'Human',
            'import_logs': '',
            'import_status': 'finished',
            'name': 'SNP',
            'url': self.url + '/snps/1/',
            'trashed': False,
            'pp_ref': 0

        }

        pp = {
            'id': 1,
            'created_at': '',
            'comment': '',
            'expression_file': None,
            'fasta_file': '/test',
            'fasta_status': '',
            'protein_length_threshold': 1000,
            'quality_threshold': 20,
            'remove_redundant': False,
            'snps': self.url + '/snps/1/',
            'snps_name': '',
            'snps_id_': 1,
            'tpm_threshold': 0,
            'url': self.url + '/personalized_proteomes/1/',
        }

        pp2 = {
            'id': 2,
            'created_at': '',
            'comment': '',
            'expression_file': '/test',
            'fasta_file': '/test',
            'fasta_status': '',
            'protein_length_threshold': 1000,
            'quality_threshold': 20,
            'remove_redundant': True,
            'snps': self.url + '/snps/1/',
            'snps_name': '',
            'snps_id_': 1,
            'tpm_threshold': None,
            'url': self.url + '/personalized_proteomes/2/',
        }

        peptides_snps = {
            'id': 1,
            'csv_file': '/test.csv'
        }

        self.data = {
            'genome': genome,
            'genome_list': [genome],
            'peptides_snps': peptides_snps,
            'peptides_snps_list': [peptides_snps],
            'pp': pp,
            'pp2': pp2,
            'pp_list': [pp],
            'snps': snp,
            'snps_list': [snp]
        }

        super().setUp()

    def test_CommentEdit(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        with requests_mock.Mocker() as m:
            m.get(self.url + '/snps/1/',
                    json=self.data['snps'])
            m.patch(self.url + '/snps/1/',
                    json=self.data['snps'])

            url = reverse('pps:edit_comment', args=('snps', 1, 1))

            response = self.client.post(url, data={'comment': 'hey'})
            expected_url = biological_sample.get_absolute_url()

            self.assertRedirects(response, expected_url)

    def test_GenomeAdd(self):
        test_file = SimpleUploadedFile('test.txt', b'file_content',
                                       content_type='text/plain')

        with requests_mock.Mocker() as m:
            m.post(self.url + '/genomes/',
                   json=self.data['genome'])

            m.get(self.url + '/genomes/?trashed=false',
                  json=self.data['genome_list'])

            url = reverse('pps:genome_add')
            response = self.client.post(url, {'file': test_file})
            expected_url = reverse('pps:genome_list')

        self.assertRedirects(response, expected_url)

    def test_GenomeList(self):
        with requests_mock.Mocker() as m:
            m.get(self.url + '/genomes/?trashed=false',
                  json=self.data['genome_list'])

            url = reverse('pps:genome_list')
            response = self.client.get(url)

            self.assertTrue('genomes' in response.context)

    def test_GenomeRemove(self):
        with requests_mock.Mocker() as m:
            m.patch(self.url + '/genomes/1/',
                    json=self.data['genome'])

            m.get(self.url + '/genomes/1/',
                  json=self.data['genome'])

            m.get(self.url + '/genomes/?trashed=false',
                  json=self.data['genome_list'])

            url = reverse('pps:genome_remove', args=(1,))
            response = self.client.get(url, args=(1,))
            expected_url = reverse('pps:genome_list')

            self.assertRedirects(response, expected_url)

            m.get(self.url + '/genomes/1/',
                  json=self.data['genome'])

            self.client.get(url, args=(1,))

    def test_SNPsAdd_form_valid(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        test_file = SimpleUploadedFile('test.txt', b'file_content',
                                       content_type='text/plain')

        with requests_mock.Mocker() as m:
            m.get(self.url + '/genomes/1/',
                  json=self.data['genome'])

            m.get(self.url + '/genomes/?trashed=false',
                  json=self.data['genome_list'])

            m.post(self.url + '/snps/',
                   json=self.data['snps'])

            m.get(self.url + '/snps/1/',
                  json=self.data['snps'])

            url = reverse('pps:snps_add', args=(1,))
            response = self.client.post(url, {'file': test_file,
                                              'reference_genome': self.url + '/genomes/1/'},
                                        args=(1,))

            expected_url = biological_sample.get_absolute_url()

            self.assertRedirects(response, expected_url)

    def test_SNPsRemove(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        models.SNPs.objects.create(biological_sample=biological_sample,
                                   pps_snps_id=1)

        with requests_mock.Mocker() as m:
            m.patch(self.url + '/snps/1/',
                    json=self.data['snps'])

            url = reverse('pps:snps_remove', args=(1,
                                                   biological_sample.pk))
            response = self.client.get(url, args=(1,
                                                  biological_sample.pk))
            expected_url = biological_sample.get_absolute_url()

            self.assertRedirects(response, expected_url)

    def test_PPAdd(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        models.SNPs.objects.create(biological_sample=biological_sample,
                                   pps_snps_id=1)

        with requests_mock.Mocker() as m:
            m.get(self.url + '/genomes/1/',
                  json=self.data['genome'])

            m.get(self.url + '/genomes/?trashed=false',
                  json=self.data['genome_list'])

            m.get(self.url + '/snps/1/',
                  json=self.data['snps'])

            m.get(self.url + '/snps/?ids=1',
                  json=self.data['snps_list'])

            m.post(self.url + '/personalized_proteomes/',
                   json=self.data['pp'])

            m.get(self.url + '/personalized_proteomes/1/',
                  json=self.data['pp'])

            url = reverse('pps:pp_add', args=(1,))

            data = {
                'protein_length_threshold': 1000,
                'quality_threshold': 20,
                'snps': self.url + '/snps/1/'
            }

            self.client.post(url, data)

            test_file = SimpleUploadedFile('test.txt', b'file_content',
                                           content_type='text/plain')

            m.post(self.url + '/personalized_proteomes/',
                   json=self.data['pp2'])

            m.get(self.url + '/personalized_proteomes/2/',
                  json=self.data['pp2'])

            data['tpm_threshold'] = 1
            data['expression_file'] = test_file

            response = self.client.post(url, data)

            expected_url = biological_sample.get_absolute_url()

            self.assertRedirects(response, expected_url)

    def test_PPTrash(self):

        obj = self.helpers.create_personalized_proteome()

        with requests_mock.Mocker() as m:
            m.get(self.url + '/genomes/1/',
                  json=self.data['genome'])
            m.get(self.url + '/snps/1/',
                  json=self.data['snps'])
            m.patch(self.url + '/personalized_proteomes/1/',
                    json=self.data['pp'])

            url = reverse('pps:pp_trash', args=(1, 1))

            response = self.client.get(url)

            expected_url = obj.snps.biological_sample.get_absolute_url()

            self.assertRedirects(response, expected_url)

    def test_pygeno_package_download(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        models.SNPs.objects.create(biological_sample=biological_sample,
                                   pps_snps_id=1)

        with requests_mock.Mocker() as m:
            m.get(self.url + '/genomes/1/',
                  json=self.data['genome'])
            m.get(self.url + '/snps/1/',
                  json=self.data['snps'])

            m.get(self.url + '/test',
                  json=self.data['snps'])

            url = reverse('pps:genome_package_download', args=(1, 1))

            response = self.client.get(url)

            self.assertEqual(response['Content-Disposition'],
                              'attachment; filename=test')

            url = reverse('pps:snps_package_download', args=(1, 1))

            response = self.client.get(url)

            self.assertEqual(response['Content-Disposition'],
                              'attachment; filename=test')

    def test_pps_pp_download(self):
        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        models.SNPs.objects.create(biological_sample=biological_sample,
                                   pps_snps_id=1)

        with requests_mock.Mocker() as m:
            m.get(self.url + '/personalized_proteomes/1/',
                  json=self.data['pp'])

            m.get(self.url + '/test',
                  json=self.data['snps'])

            url = reverse('pps:pp_download', args=(1, 1))

            response = self.client.get(url)

            content = 'attachment; filename=personalized_proteome_id_1.fasta'
            self.assertEqual(response['Content-Disposition'],
                              content)

    def test_PPState(self):

        with requests_mock.Mocker() as m:
            m.get(self.url + '/personalized_proteomes/1/',
                  json=self.data['pp'])

            url = reverse('pps:pp_state', args=(1,))

            response = self.client.get(url)

            self.assertEqual(response.status_code, 200)

    def test_SNPsState(self):

        with requests_mock.Mocker() as m:
            m.get(self.url + '/snps/1/',
                  json=self.data['snps'])

            url = reverse('pps:snps_state', args=(1,))

            response = self.client.get(url)

            self.assertEqual(response.status_code, 200)

    def test_get_biological_sample_snps(self):

        biological_sample = self.helpers.create_biological_sample(
            permissions=['change', 'view'])

        models.SNPs.objects.create(biological_sample=biological_sample,
                                   pps_snps_id=1)

        with requests_mock.Mocker() as m:

            m.get(self.url + '/snps/?ids=1',
                  json=[self.data['snps']])

            bs_snps = models.SNPs.objects.all()

            snp_entries = pps.get_biological_sample_snps(bs_snps)

            self.assertIsInstance(snp_entries, list)

    def test_get_biological_sample_pp(self):

        self.helpers.create_personalized_proteome()

        with requests_mock.Mocker() as m:

            m.get(self.url + '/personalized_proteomes/?ids=1',
                  json=[self.data['pp']])

            bs_snps = models.SNPs.objects.all()

            pp_entries = pps.get_biological_sample_pp(bs_snps)

            self.assertIsInstance(pp_entries, list)

        with requests_mock.Mocker() as m:

            m.get(self.url + '/personalized_proteomes/?ids=1',
                  json=[self.data['pp2']])

            bs_snps = models.SNPs.objects.all()

            pp_entries = pps.get_biological_sample_pp(bs_snps)

            self.assertIsInstance(pp_entries, list)

    def test_Tab(self):

        self.helpers.create_personalized_proteome()

        with requests_mock.Mocker() as m:
            m.get(self.url + '/snps/?ids=1',
                  json=[self.data['snps']])

            m.get(self.url + '/personalized_proteomes/?ids=1',
                  json=[self.data['pp']])

            url = reverse('pps:tabview', args=(1,))

            response = self.client.get(url)

            self.assertEqual(response.status_code, 200)

        with requests_mock.Mocker() as m:
            m.get(self.url + '/snps/?ids=1', status_code=403)

            url = reverse('pps:tabview', args=(1,))

            response = self.client.get(url)

            self.assertTrue(response.context['pps_unavailable'])
