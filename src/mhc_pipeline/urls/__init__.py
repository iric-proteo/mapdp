"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.conf.urls import include, url

# Import project libraries
from . import (biological_sample,
               fileset,
               filters,
               genotyping,
               identification,
               immunopeptidome,
               msms,
               misc,
               peptides_plots,
               pps)

urlpatterns = [
    url(r'biological_sample/', include(biological_sample)),

    url(r'fileset/', include(fileset)),

    url(r'filters/', include(filters)),

    url(r'genotyping/', include(genotyping)),

    url(r'identification/', include(identification)),

    url(r'immunopeptidome/', include(immunopeptidome)),

    url(r'msms/', include(msms)),

    url(r'peptides_plots/', include(peptides_plots)),

    url(r'pps/', include(pps)),

    url(r'', include(misc)),
]
