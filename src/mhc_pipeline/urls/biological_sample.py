"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.conf.urls import url

# Import project libraries
from ..views import biological_sample, biological_sample_plots

app_name = 'biological_sample'
urlpatterns = [

    url(r'^create/$',
        biological_sample.Create.as_view(),
        name='create'),

    url(r'^(?P<pk>\d+)/edit/$',
        biological_sample.Edit.as_view(),
        name='edit'),

    url(r'(?P<pk>\d+)/experiment_details/create/$',
        biological_sample.ExperimentDetailsCreateUpdate.as_view(),
        name='experiment_details_create'),

    url(r'(?P<pk>\d+)/experiment_details/(?P<details_pk>\d+)/edit/$',
        biological_sample.ExperimentDetailsCreateUpdate.as_view(),
        name='experiment_details_edit'),

    url(r'list/$',
        biological_sample.List.as_view(),
        name='list'),

    url(r'(?P<pk>\d+)/edit/mhc_allele/$',
        biological_sample.MHCAlleleUpdate.as_view(),
        name='mhc_allele'),

    url(r'(?P<pk>\d+)/edit/mhc_details/$',
        biological_sample.MHCSampleDetailsUpdate.as_view(),
        name='mhc_details'),

    url(r'(?P<pk>\d+)/(?P<tax_id>\d+?)/plot/(?P<name>.+)/$',
        biological_sample_plots.get_plot,
        name='plot'),

    url(r'(?P<pk>\d+)/(?P<tax_id>\d+?)/plot_data/(?P<name>.+)/$',
        biological_sample_plots.plot_data,
        name='plot_data'),

    url(r'(?P<pk>\d+?)/(?P<tax_id>\d+?)/plots/$',
        biological_sample.Plots.as_view(),
        name='plots'),

    url(r'(?P<pk>\d+)/(?P<tax_id>\d+?)/plots/state/$',
        biological_sample.PlotsState.as_view(),
        name='plots_state'),

    url(r'(?P<pk>\d+)/trash/(?P<confirmed>\w+)/$',
        biological_sample.Trash.as_view(),
        name='trash'),

    url(r'(?P<pk>\d+)/view/$',
        biological_sample.View.as_view(),
        name='view'),

    url(r'tabview/(?P<pk>\d+?)/',
        biological_sample.ExperimentDetailsTab.as_view(),
        name='experiment_details_tabview'),
]
