"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.conf.urls import include, url

# Import project libraries
from ..views import chunked_upload, fileset

app_name = 'fileset'
urlpatterns = [
    url(r'(?P<content_type>.+?)/(?P<ct_pk>\d+?)/', include([

        url(r'(?P<fs_pk>\d+?)/browse(?P<subdir>.*)$',
            fileset.Browse.as_view(),
            name='browse'),

        url(r'create/(?P<set_classes>.+?)/$',
            fileset.Create.as_view(),
            name='create'),

        url(r'(?P<fs_pk>\d+?)/', include([

            url(r'refresh/$',
                fileset.Refresh.as_view(),
                name='refresh'),

            url(r'(?P<f_pk>\d+?)/remove_file/$',
                fileset.RemoveFile.as_view(),
                name='remove_file'),

            url(r'(?P<f_pk>\d+?)/send_file/$',
                fileset.SendFile.as_view(),
                name='send_file'),

            url(r'send_files/$',
                fileset.SendFiles.as_view(),
                name='send_files'),

            url(r'trash/$',
                fileset.Trash.as_view(),
                name='trash'),

            # url(r'^upload/(?P<class_name>\w+?)/$',
            #     fileset.Upload.as_view(),
            #     name='upload'),
            #
            url(r'view_files/$',
                fileset.ViewFiles.as_view(),
                name='view_files'),

            url(r'chunked_upload_form/(?P<class_name>\w+?)/$',
                chunked_upload.ChunkedUpload.as_view(),
                name='chunked_form'),

            url(r'chunked_upload/(?P<class_name>\w+?)/$',
                chunked_upload.MyChunkedUploadView.as_view(),
                name='chunked_upload'),
            url(r'chunked_upload_complete/(?P<class_name>\w+?)/$',
                chunked_upload.MyChunkedUploadCompleteView.as_view(),
                name='chunked_upload_complete'),
        ])),
    ])),
    url(r'tabview/(?P<pk>\d+?)/(?P<fileset_classes>.+?)/$',
        fileset.Tab.as_view(),
        name='tabview'),
]
