"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.conf.urls import url

# Import project libraries
from ..views import genotyping

app_name = 'genotyping'

urlpatterns = [

    url(r'^create/(?P<pk>\d+?)/$',
        genotyping.Create.as_view(),
        name='create'),

    url(r'(?P<pk>\d+)/download/$',
        genotyping.download,
        name='download'),

    # url(r'webhook/$',
    #     genotyping.NextflowWebhook.as_view(),
    #     name='webhook'),

    url(r'(?P<pk>\d+?)/state/$',
        genotyping.State.as_view(),
        name='state'),

    url(r'tabview/(?P<pk>\d+?)/',
        genotyping.Tab.as_view(),
        name='tabview'),

    url(r'(?P<pk>\d+)/trash/(?P<confirmed>\w+)/$',
        genotyping.Trash.as_view(),
        name='trash'),

]
