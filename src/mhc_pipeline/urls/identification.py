"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.conf.urls import url

# Import project libraries
from ..views import identification

app_name = 'identification'

urlpatterns = [
    url(r'csv/(?P<key>\w+?)/$',
        identification.csv,
        name='csv'),

    url(r'logos/(?P<key>\w+?)/(?P<file>.+?)$',
        identification.logos,
        name='logos'),

    url(r'(?P<pk>\d+?)/settings/$',
        identification.Settings.as_view(),
        name='settings'),

    url(r'(?P<pk>\d+?)/state/$',
        identification.State.as_view(),
        name='state'),

    url(r'task/(?P<key>\w+?)/$',
        identification.identification_task,
        name='task'),

    url(r'(?P<pk>\d+)/trash/(?P<confirmed>\w+)/$',
        identification.Trash.as_view(),
        name='trash'),

    url(r'view/$',
        identification.View.as_view(),
        name='view'),

    url(r'(?P<identification_pk>\d+?)/view_id/$',
        identification.View.as_view(),
        name='view_id'),

    url(r'tabview/(?P<pk>\d+?)/',
        identification.Tab.as_view(),
        name='tabview'),

]
