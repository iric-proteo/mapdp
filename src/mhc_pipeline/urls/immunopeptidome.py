"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.conf.urls import url

# Import project libraries
from ..views import immunopeptidome, peptides_plots

app_name = 'immunopeptidome'

urlpatterns = [

    url(r'create_from_file/$',
        immunopeptidome.CreateFromFile.as_view(),
        name='create_from_file'),

    url(r'(?P<pk>\d+)/annotate/$',
        immunopeptidome.Annotate.as_view(),
        name='annotate'),

    url(r'(?P<pk>\d+)/edit/$',
        immunopeptidome.Edit.as_view(),
        name='edit'),

    url(r'list/$',
        immunopeptidome.List.as_view(),
        name='list'),

    url(r'(?P<pk>\d+)/miha_filter/$',
        immunopeptidome.MiHAFilter.as_view(),
        name='miha_filter'),

    url(r'(?P<pk>\d+)/peptidecount/$',
        immunopeptidome.PeptideCount.as_view(),
        name='peptide_count'),

    url(r'(?P<pk>\d+)/plots/$',
        immunopeptidome.Plots.as_view(),
        name='plots'),

    url(r'tabview/bs/(?P<pk>\d+?)/',
        immunopeptidome.Tab.as_view(),
        name='tabview'),

    url(r'(?P<pk>\d+)/trash/(?P<confirmed>\w+)/$',
        immunopeptidome.Trash.as_view(),
        name='trash'),

    url(r'(?P<pk>\d+)/view/$',
        immunopeptidome.View.as_view(),
        name='view'),

    url(r'(?P<pk>\d+?)/document/(?P<doc_pk>\d+?)/comment/edit/$',
        immunopeptidome.CommentEdit.as_view(),
        name='document_edit_comment'),

    url(r'^(?P<pk>\d+?)/document/(?P<doc_pk>\d+?)/download/$',
        immunopeptidome.document_download,
        name='document_download'),

    url(r'^(?P<pk>\d+?)/document/(?P<doc_pk>\d+?)/state/$',
        immunopeptidome.DocumentState.as_view(),
        name='document_state'),

    url(r'^(?P<pk>\d+?)/document/(?P<doc_pk>\d+?)/trash/$',
        immunopeptidome.document_trash,
        name='document_trash'),


]
