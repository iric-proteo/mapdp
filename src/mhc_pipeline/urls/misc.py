"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.conf.urls import url

# Import project libraries
from ..views import (maxquant,
                     peaks,
                     permission)


urlpatterns = [

    # # Maxquant peptide import
    # url(r'maxquant/(?P<bs_fs_pk>\d+?)/import/$',
    #     maxquant.import_peptides,
    #     name='maxquant_import'),
    #
    # # Peaks peptide import
    # url(r'peaks/(?P<bs_fs_pk>\d+?)/import/$',
    #     peaks.import_peptides,
    #     name='peaks_import'),

    # Permissions management
    url(r'permission/update/(?P<subject_type>\w+)/(?P<subject_pk>\d+)/'
        r'(?P<obj_model>\w+)/(?P<obj_pk>\d+)/$',
        permission.update,
        name='permission_update'),

    url(r'permission/view/(?P<content_type>.+?)/(?P<pk>\d+?)/',
        permission.Tab.as_view(),
        name='permission_view'),
]
