"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.conf.urls import url

# Import project libraries
from ..views import peptides_plots

app_name = 'peptides_plots'

urlpatterns = [

    url(r'(?P<key>\w+)/plot/(?P<name>.+)/$',
        peptides_plots.get_plot,
        name='plot'),

    url(r'(?P<key>\w+)/plot_data/(?P<name>.+)/$',
        peptides_plots.plot_data,
        name='plot_data'),

    url(r'(?P<key>\w+)/plots/$',
        peptides_plots.Plots.as_view(),
        name='plots'),

    url(r'(?P<key>\w+)/plots/state/$',
        peptides_plots.State.as_view(),
        name='plots_state'),

]
