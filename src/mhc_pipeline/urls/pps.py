"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries

# Import Django related libraries
from django.conf.urls import url

# Import project libraries
from ..views import pps

app_name = 'pps'

urlpatterns = [

    url(r'^genomes/add/$',
        pps.GenomeAdd.as_view(),
        name='genome_add'),

    url(r'genomes/(?P<pk>\d+?)/remove/$',
        pps.GenomeRemove.as_view(),
        name='genome_remove'),

    url(r'^genomes/list/$',
        pps.GenomeList.as_view(),
        name='genome_list'),

    url(r'^genomes/(?P<pk>\d+?)/(?P<bs_pk>\d+?)/package/download/$',
        pps.pygeno_package_download,
        name='genome_package_download'),

    url(r'^snps/add/bs/(?P<bs_pk>\d+?)/$',
        pps.SNPsAdd.as_view(),
        name='snps_add'),

    url(r'snps/(?P<pk>\d+?)/remove/bs/(?P<bs_pk>\d+?)/$',
        pps.SNPsRemove.as_view(),
        name='snps_remove'),

    url(r'^snps/(?P<pk>\d+?)/(?P<bs_pk>\d+?)/package/download/$',
        pps.pygeno_package_download,
        name='snps_package_download'),

    url(r'^snps/(?P<pk>\d+?)/state/$',
        pps.SNPsState.as_view(),
        name='snps_state'),

    url(r'^pp/add/bs/(?P<bs_pk>\d+?)/$',
        pps.PPAdd.as_view(),
        name='pp_add'),

    url(r'^pp/(?P<pk>\d+?)/(?P<bs_pk>\d+?)/(?P<file_type>\w+?)/download/$',
        pps.pp_download,
        name='pp_download'),

    url(r'^pp/(?P<pk>\d+?)/state/$',
        pps.PPState.as_view(),
        name='pp_state'),

    url(r'pp/(?P<pk>\d+?)/trash/bs/(?P<bs_pk>\d+?)/$',
        pps.PPTrash.as_view(),
        name='pp_trash'),

    url(r'(?P<obj>\w+)/(?P<pk>\d+?)/comment/edit/bs/(?P<bs_pk>\d+?)/$',
        pps.CommentEdit.as_view(),
        name='edit_comment'),

    url(r'tabview/(?P<pk>\d+?)/',
        pps.Tab.as_view(),
        name='tabview'),
]
