"""
Copyright 2015-2023 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# Import standard libraries
import re

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.shortcuts import render


# Third party libraries
import feedparser

# Import project libraries
from ..models import (BiologicalSample,
                      File,
                      New,
                      Peptide,
                      UniquePeptide)


# Create your views here.


@login_required
def home(request):


    publications = cache.get('publications')

    if publications is None:

        publications = list()

        try: 

            d = feedparser.parse(
                'https://pubmed.ncbi.nlm.nih.gov/rss/search/1hwQrn1DrkWVI_4yWI3eP1F_nsEn5cVJVHSHuZ_52bghV3Agvz/')

            for entry in d['entries'][:2]:

                entry_mod = f'<p><b>{entry.title}</b></p>{entry.content[0]["value"]}'

                publications.append(entry_mod)

                cache.set('publications', publications, 60 * 60 * 24 * 7)
        
        except URLError:
            pass


    # Stats
    biological_sample_count = cache.get('biological_sample_count')

    if biological_sample_count is None:

        biological_sample_count = BiologicalSample.objects.filter(trashed=False).count()
        cache.set('biological_sample_count', biological_sample_count, 60 * 60 * 24 * 7)

    file_count = cache.get('file_count')

    if file_count is None:

        file_count = File.objects.count()
        cache.set('file_count', file_count, 60 * 60 * 24 * 7)

    unique_peptide_count = cache.get('unique_peptide_count')

    if unique_peptide_count is None:
        unique_peptide_count = UniquePeptide.objects.count()
        cache.set('unique_peptide_count', unique_peptide_count, 60 * 60 * 24 * 7)

    msms_count = cache.get('msms_count')

    if msms_count is None:

        msms_count = Peptide.objects.count()
        cache.set('msms_count', msms_count, 60 * 60 * 24 * 7)

    context = {
        'biological_sample_count':  biological_sample_count,
        'file_count': file_count,
        'msms_count': msms_count,
        'unique_peptide_count': unique_peptide_count,

        'news': New.objects.select_related('created_by', 'modified_by').all()[:5],
        'publications': publications,
    }

    return render(request, 'mhc_pipeline/base_site/home.html', context)
