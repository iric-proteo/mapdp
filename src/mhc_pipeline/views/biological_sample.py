"""
Copyright 2015-2021 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import (CreateView,
                                       UpdateView)
from django.views.generic.list import ListView

# Third party libraries
import bokeh

# Import project libraries
from .biological_sample_plots import revision
from ..forms import (BiologicalSampleForm,
                     ExperimentDetailsForm,
                     MHCAlleleForm,
                     MHCSampleForm)
from ..library.permission import (get_if_permission,
                                  set_content_permission)
from ..models import (BiologicalSample,
                      BiologicalSampleMHCAllele,
                      ExperimentDetails,
                      MHCAllele,
                      MHCSample)
from .. import tasks
from .view_mixin import (TemplateMixin,
                         TrashMixin)


# Create your views here.


class BiologicalSampleMixin(TemplateMixin):
    """
    Mixin for BiologicalSample views
    """

    column_width = 6

    icon = 'flask'

    title = 'Biological samples'

    title_suffix = ''

    template_base = 'mhc_pipeline/biological_sample/'

    template_name = 'form.html'

    def bs_get(self, permission: str = 'change') -> BiologicalSample:
        """
        Gets BiologicalSample instance if change permission
        :param permission: Permission
        :return: BiologicalSample instance
        """

        return get_if_permission(self.request, BiologicalSample,
                                 permission, pk=self.kwargs['pk'])


class ExtraMixin(BiologicalSampleMixin):

    def form_valid(self, form):
        """
        If form is valid, save to database, update modified by
        and redirect to instance url.
        :param form: Form instance
        :return: HttpResponse
        """

        obj = form.save()
        obj.biological_sample.modified_by = self.request.user
        obj.biological_sample.save()

        return redirect(obj.biological_sample)


@method_decorator(login_required, name='dispatch')
class Create(BiologicalSampleMixin, CreateView):
    """
    View to create a new BiologicalSample instance.
    """

    action = 'Create'

    form_class = BiologicalSampleForm

    template_base = 'mhc_pipeline/base_site/'

    def form_valid(self, form):
        """
        If form is valid, save to database and redirect to instance url.
        :param form: Form instance
        :return: HttpResponse
        """

        obj = form.save(commit=False)
        obj.created_by = self.request.user
        obj.modified_by = self.request.user
        obj.save()

        MHCSample.objects.create(biological_sample=obj)

        set_content_permission(obj, self.request.user,
                               permissions=['change', 'delete', 'view'])

        return redirect(obj)


@method_decorator(login_required, name='dispatch')
class Edit(BiologicalSampleMixin, UpdateView):
    """
    View to edit a biological sample instance.
    """

    action = 'Edit'

    form_class = BiologicalSampleForm

    template_base = 'mhc_pipeline/base_site/'

    def form_valid(self, form):
        """
        If form is valid, save to database and redirect to instance url.
        :param form: Form instance
        :return: HttpResponse
        """

        obj = form.instance
        obj.modified_by = self.request.user
        obj.save()

        return redirect(obj)

    def get_object(self) -> BiologicalSample:
        """
        Gets the Biological sample to edit
        :return: Biological sample instance
        """

        return self.bs_get()


@method_decorator(login_required, name='dispatch')
class ExperimentDetailsCreateUpdate(ExtraMixin, UpdateView):
    """
    View to create or update Experimental details.
    """

    action = 'Create'

    column_width = 12

    form_class = ExperimentDetailsForm

    title_suffix = ' - Experiment details'

    template_base = 'mhc_pipeline/base_site/'

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(ExperimentDetailsCreateUpdate, self).get_form_kwargs()
        kwargs.update({
            'submit': True
        })

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })

        return kwargs

    def get_object(self) -> ExperimentDetails:
        """
        Gets the ExperimentalDetails instance or creates an empty one.
        :return: ExperimentalDetails instance
        """

        obj = self.bs_get()

        details_pk = self.kwargs.get('details_pk', None)

        if details_pk is None:
            experiment_details = ExperimentDetails(biological_sample=obj)
        else:
            experiment_details = ExperimentDetails.objects.get(pk=details_pk)

        return experiment_details


@method_decorator(login_required, name='dispatch')
class List(BiologicalSampleMixin, ListView):
    """
    View to list BiologicalSample instances available to the user.
    """

    context_object_name = 'samples'

    template_name = 'list.html'

    def get_queryset(self):
        """
        Gets the list of BiologicalSample for the user.
        :return:
        """

        return BiologicalSample.objects.for_user(self.request.user,
                                                 related=True)


@method_decorator(login_required, name='dispatch')
class MHCAlleleUpdate(ExtraMixin, UpdateView):
    """
    View to update MHCAllele associated to BiologicalSample instance.
    """

    action = 'Modify'

    column_width = 6

    form_class = MHCAlleleForm

    title_suffix = '- MHC Allele'

    template_base = 'mhc_pipeline/base_site/'

    def get_object(self) -> BiologicalSampleMHCAllele:
        """
        Gets the BiologicalSampleMHCAllele instance or creates an empty one.
        :return: BiologicalSampleMHCAllele instance
        """

        obj = self.bs_get()

        bs_mhc, created = BiologicalSampleMHCAllele.objects.get_or_create(
            biological_sample=obj)

        return bs_mhc


@method_decorator(login_required, name='dispatch')
class MHCSampleDetailsUpdate(ExtraMixin, UpdateView):
    """
    View to update MHC details associated to the BiologicalSample
    instance.
    """

    action = 'Modify'

    column_width = 6

    form_class = MHCSampleForm

    title_suffix = '- MHC details'

    template_base = 'mhc_pipeline/base_site/'

    def get_object(self) -> MHCSample:
        """
        Gets the MHCSample instance
        :return: MHCSample instance
        """

        obj = self.bs_get()

        mhc_sample = MHCSample.objects.get(biological_sample=obj)

        return mhc_sample


@method_decorator(login_required, name='dispatch')
class Trash(TrashMixin, BiologicalSampleMixin, TemplateView):
    """
    Trashes BiologicalSample instance
    """

    model = BiologicalSample


@method_decorator(login_required, name='dispatch')
class View(BiologicalSampleMixin, TemplateView):
    """
    View BiologicalSample instance
    """

    template_name = 'view.html'

    def get_context_data(self, **kwargs) -> dict:
        manager = BiologicalSample.objects.select_related('created_by',
                                                          'modified_by')

        obj = get_if_permission(self.request, manager, 'view',
                                pk=self.kwargs['pk'])

        # Get MHC sample details
        mhc_details = MHCSample.objects.get(biological_sample=obj)

        # Get MHC alleles
        queryset_filter = {
            'biologicalsamplemhcallele__biological_sample': obj
        }
        mhc_alleles = MHCAllele.objects.filter(**queryset_filter).order_by(
            'name')

        context = {
            'obj': obj,
            'mhc_alleles': mhc_alleles,
            'mhc_details': mhc_details,
            'genotyping': os.getenv('ARCASHLA_CONTAINER')
        }

        context.update(super().get_context_data(**kwargs))

        return context


@method_decorator(login_required, name='dispatch')
class ExperimentDetailsTab(BiologicalSampleMixin, TemplateView):
    """
    View experiment details tab in biological view
    """

    template_name = 'experiment_details_tab.html'

    def get_context_data(self, **kwargs) -> dict:
        # Is user authorized to look at biological sample
        obj = self.bs_get(permission='view')

        # Get Experiment details
        experiment_details = ExperimentDetails.objects.filter(
            biological_sample=obj)

        context = {
            'obj': obj,
            'experiment_details': experiment_details,
        }

        return context


@method_decorator(login_required, name='dispatch')
class Plots(BiologicalSampleMixin, TemplateView):
    """
    Exploratory plots for identifications of biological samples
    """

    icon = 'bar-chart'

    title = 'Exploratory plots'

    template_name = 'plots.html'

    def get_context_data(self, **kwargs) -> dict:
        context = super().get_context_data(**kwargs)
        context['bokeh_version'] = bokeh.__version__

        if kwargs['pk'] == '0':
            context['identifier'] = 'All'
        else:
            # Is user authorized to look at biological sample
            obj = self.bs_get(permission='view')
            context['identifier'] = obj.identifier
            context['tax_id'] = obj.Species

        tasks.biological_sample_generate_plots.delay(kwargs['pk'],
                                                     kwargs['tax_id'])

        return context


@method_decorator(login_required, name='dispatch')
class PlotsState(BiologicalSampleMixin, TemplateView):
    """
    Provides state of the identification instance.
    """

    template_name = 'plots_state.html'

    def get_context_data(self, **kwargs) -> dict:
        task_key = f'biological_sample_{kwargs["pk"]}_{kwargs["tax_id"]}_' \
                   f'plot_revision_{revision}_status'

        state = cache.get(task_key)

        if state is None:
            state = 'pending'

        context = dict()
        context['state'] = state
        context['pk'] = kwargs['pk']
        context['tax_id'] = kwargs['tax_id']
        context['plots'] = ['peptide_counts_cumulative', 'peptide_counts',
                            'peptide_biological_sample_counts',
                            'peptide_file_counts', 'allele_counts',
                            ]

        if kwargs['pk'] != '0' or \
                self.request.user.has_perm('mhc_pipeline.global_view'):

            context['plots'].append('biological_sample_peptide_counts')

        return context
