"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponse

# Third party libraries
from bokeh.embed import json_item
from bokeh.models import HoverTool
from bokeh.models import Legend
from bokeh.models import ColumnDataSource
from bokeh.palettes import viridis
from bokeh.plotting import figure
from django_pandas.io import read_frame
import pandas as pd


# Import project libraries
from base_site.exceptions import BaseSiteException
from ..library.permission import get_if_permission
from ..models import (BiologicalSample,
                      PeptideCounts,
                      PeptideBiologicalSampleCount,
                      PeptideFileCount)

axis_label_text_font_size = '14pt'
major_label_text_font_size = '12pt'
plot_height = 400
plot_width = 500
CACHE_EXPIRATION = 86400  # One day
revision = '1'


def generate_plots(pk: int, tax_id: int) -> None:
    """
    Queries biological sample stats, generates plots, caches json plots and csv
    data
    :param pk: Biological sample primary key
    :param tax_id: Taxonomy identifier
    :return: None, cached json plots and csv data
    """

    task_key = f'biological_sample_{pk}_{tax_id}_plot_revision_{revision}_' \
        f'status'

    try:
        state = cache.get(task_key)

        if state is None or state == 'failed':

            cache.set(task_key, 'started', CACHE_EXPIRATION)

            df = get_stats(pk, tax_id)

            for plot_name, plot_function in plots.items():
                plot, plot_df = plot_function(df)

                plot_json = json.dumps(json_item(plot))
                csv = plot_df.to_csv(index=False)

                key = f'biological_sample_{pk}_{tax_id}_plot_{plot_name}'
                cache.set(key, plot_json, CACHE_EXPIRATION)
                cache.set(key + '.csv', csv, CACHE_EXPIRATION)

            cache.set(task_key, 'success', CACHE_EXPIRATION)

    except BaseSiteException as e:
        cache.set(task_key, 'failed:' + e.message, CACHE_EXPIRATION)

    except Exception:
        cache.set(task_key, 'failed', CACHE_EXPIRATION)
        raise


def get_stats(biological_sample_pk: str, tax_id: int, allele_all: bool = True) \
        -> pd.DataFrame:
    """
    Reads stats file and extracts values for the biological sample
    :param biological_sample_pk: Biological sample primary key
    :param tax_id: Taxonomy identifier
    :param allele_all: Restricts allele to all if all biological samples are
    selected (pk: 0)
    :return: Pandas data frame
    """

    queryset = PeptideCounts.objects.select_related('identification')

    if biological_sample_pk == '0' or biological_sample_pk is None:
        queryset = queryset.filter(biological_sample=None,
                                   taxonomy_identifier=tax_id)

        if allele_all:
            queryset = queryset.filter(allele=None)
    else:
        queryset = queryset.filter(biological_sample_id=biological_sample_pk)

    df = read_frame(queryset,
                    fieldnames=['identification__pk',
                                'identification__created_at',
                                'biological_sample__pk',
                                'taxonomy_identifier',
                                'allele__name',
                                'individual_counts',
                                'cumulative_counts'])

    df = df.rename(columns={'identification__created_at': 'created_at',
                            'biological_sample__pk': 'biological_sample',
                            'allele__name': 'allele'
                            })
    df = df.reset_index(drop=True)
    df['allele'].replace(to_replace=[None], value='All', inplace=True)
    df['created_at'] = pd.to_datetime(df['created_at'])

    return df


def check_plot_permission(request, pk: int, name: str) -> None:
    """
    Check permission for plot
    :param request: HttpRequest
    :param pk: Biological sample primary key
    :param name: Plot name
    :return: Raises permission denied exception
    """

    if pk != '0':
        get_if_permission(request, BiologicalSample, 'view', pk=pk)

    if pk == '0' and name == 'biological_sample_peptide_counts':
        if not request.user.has_perm('mhc_pipeline.global_view'):
            raise PermissionDenied


@login_required
def get_plot(request, pk: int, tax_id: int, name: str) -> HttpResponse:
    """
    Queries Bokeh plot for the biological sample
    :param request: HttpRequest
    :param pk: Biological sample primary key
    :param tax_id: Taxonomy identifier
    :param name: Plot name
    :return: Bokeh plot in JSON format
    """

    check_plot_permission(request, pk, name)

    plot_key = f'biological_sample_{pk}_{tax_id}_plot_{name}'
    content = cache.get(plot_key)

    response = HttpResponse(content, content_type='application/json')

    return response


@login_required
def plot_data(request, pk: int, tax_id: int, name: str) -> HttpResponse:
    """
    Outputs plot data points to CSV
    :param request: HttpRequest
    :param pk: Biological sample primary key
    :param tax_id: Taxonomy identifier
    :param name: Plot name
    :return: CSV file
    """

    check_plot_permission(request, pk, name)

    file_name = f'biological_sample_{pk}_{tax_id}_plot_{name}.csv'
    content = cache.get(file_name)

    response = HttpResponse(content, content_type='text/csv')

    response['Content-Disposition'] = f'attachment; filename={file_name}'

    return response


def base_plot(tools: str = 'reset,save,xpan,xwheel_zoom',
              height: int = plot_height, width: int = plot_width, **kwargs):
    """
    Plots figure with preset parameters
    :param tools: List of tools in toolbar
    :param height: Plot height
    :param width: Plot width in pixel
    :param kwargs: Keyword arguments
    :return: Bokeh plot
    """

    plot = figure(height=height, width=width,
                  output_backend='svg', tools=tools, **kwargs)

    plot.axis.axis_label_text_font_size = axis_label_text_font_size
    plot.axis.major_label_text_font_size = major_label_text_font_size
    plot.grid.grid_line_color = None
    plot.toolbar.logo = None

    return plot


def allele_counts(df: pd.DataFrame) -> tuple:
    """
    Peptide counts for each MHC allele
    :param df: Stats data frame
    :return: Plots and data frame
    """

    if df.shape[0]:
        biological_sample_pk = df['biological_sample'][0]
        tax_id = df['taxonomy_identifier'][0]

        df = get_stats(biological_sample_pk, tax_id, allele_all=False)

    alleles = df['allele'].unique()

    selected_rows = list()

    for allele in alleles:
        rows = df[df['allele'] == allele]
        row = rows.iloc[[-1]]
        selected_rows.append(row.index.item())

    df = df.loc[selected_rows]
    df = df[['allele', 'cumulative_counts']]
    df = df.sort_values(by='cumulative_counts', ascending=True)

    # Bokeh plot

    height_custom = 50 + 25 * len(alleles)

    if height_custom < plot_height:
        height_custom = plot_height

    df2 = df[df['allele'] != 'All']

    plot = base_plot(height=height_custom,
                     tooltips='@cumulative_counts',
                     x_axis_label='Peptide counts',
                     y_axis_label='Allele',
                     y_range=list(df2['allele'].unique()))

    plot.hbar(left=0, right='cumulative_counts', y='allele',
              height=0.5, source=ColumnDataSource(df2))
    plot.xaxis.major_label_orientation = 'vertical'
    plot.xgrid.grid_line_color = 'navy'
    plot.xgrid.grid_line_alpha = 0.1

    return plot, df


def peptide_counts(df: pd.DataFrame) -> tuple:
    """
    Plots cumulative number of peptides over time for each allele
    :param df: Stats data frame
    :return: Plots and data frame
    """

    # Bokeh plot
    plot = base_plot(title='Unique MHC peptide counts for each identification',
                     tools='box_zoom,reset,save,xwheel_zoom',
                     x_axis_label='Date',
                     x_axis_type='datetime',
                     y_axis_label='Counts',
                     )
    plot.xaxis.major_label_orientation = 'vertical'

    plot.add_tools(
        HoverTool(
            tooltips=[
                ('allele', '@allele'),
                ('date', '@created_at{%F}'),
                ('counts', '@individual_counts'),
            ],

            formatters={
                'created_at': 'datetime',
            },

        )
    )

    alleles = sorted(df['allele'].unique())
    colors = viridis(len(alleles))
    legend_it = []

    for i, allele in enumerate(alleles):
        rows = df[df['allele'] == allele]

        c = plot.circle('created_at', 'individual_counts',
                        line_color=colors[i], line_width=2,
                        source=ColumnDataSource(rows))

        legend_it.append((alleles[i].replace('HLA-', ''), [c]))

    if len(alleles) == 0:
        plot.circle([1], [1], alpha=0)

    legend = Legend(items=legend_it)
    legend.click_policy = 'hide'

    plot.add_layout(legend, 'right')

    return plot, df[['created_at', 'individual_counts']]


def peptide_counts_cumulative(df: pd.DataFrame) -> tuple:
    """
    Plots cumulative number of peptides over time for each allele
    :param df: Stats data frame
    :return: Plots and data frame
    """

    # Bokeh plot
    plot = base_plot(title='Cumulative unique MHC peptide counts',
                     tools='box_zoom,reset,save,xwheel_zoom',
                     # Tooltips buggy for step
                     # https://github.com/bokeh/bokeh/issues/7419
                     x_axis_label='Date',
                     x_axis_type='datetime',
                     y_axis_label='Counts',
                     )
    plot.xaxis.major_label_orientation = 'vertical'

    alleles = sorted(df['allele'].unique())
    colors = viridis(len(alleles))
    legend_it = []

    for i, allele in enumerate(alleles):
        rows = df[df['allele'] == allele]

        c = plot.step('created_at', 'cumulative_counts',
                      line_color=colors[i], line_width=2,
                      source=ColumnDataSource(rows))

        legend_it.append((alleles[i].replace('HLA-', ''), [c]))

    if len(alleles) == 0:
        plot.circle([1], [1], alpha=0)

    legend = Legend(items=legend_it)
    legend.click_policy = 'hide'

    plot.add_layout(legend, 'right')

    return plot, df[['created_at', 'cumulative_counts']]


def peptide_biological_sample_counts(df: pd.DataFrame) -> tuple:
    """
    Plots the number of time peptide have been found in n biological sample
    :param df: Stats data frame
    :return: Plots and data frame
    """

    if df.shape[0]:
        biological_sample_pk = df['biological_sample'][0]
        tax_id = df['taxonomy_identifier'][0]
    else:
        biological_sample_pk = -1
        tax_id = -1

    queryset = PeptideBiologicalSampleCount.objects.select_related()

    queryset = queryset.filter(biological_sample_id=biological_sample_pk)

    if biological_sample_pk is None:
        queryset = queryset.filter(taxonomy_identifier=tax_id)

    df = read_frame(queryset,
                    fieldnames=['biological_sample__identifier',
                                'biological_sample_counts',
                                'peptide_counts'])
    df['biological_sample__identifier'].replace(to_replace=[None], value='All',
                                                inplace=True)

    plot = base_plot(tools='save',
                     tooltips='@biological_sample_counts: @peptide_counts',
                     x_axis_label='Peptide found in n biological samples',
                     y_axis_label='Peptide counts',
                     )

    plot.vbar(x='biological_sample_counts', top='peptide_counts',
              width=0.8, bottom=0,
              source=ColumnDataSource(df))

    return plot, df


def biological_sample_peptide_counts(df: pd.DataFrame) -> tuple:
    """
    Plots the number of peptides per biological sample
    :param df: Stats data frame
    :return: Plots and data frame
    """

    if df.shape[0]:
        biological_sample_pk = df['biological_sample'][0]
        tax_id = df['taxonomy_identifier'][0]
    else:
        biological_sample_pk = -1
        tax_id = -1

    queryset = PeptideBiologicalSampleCount.objects.select_related()

    if tax_id:
        queryset = queryset.filter(taxonomy_identifier=tax_id)

    if biological_sample_pk is not None:
        queryset = queryset.filter(biological_sample_id=biological_sample_pk)

    df = read_frame(queryset,
                    fieldnames=['biological_sample__identifier',
                                'biological_sample_counts',
                                'peptide_counts'])
    df['biological_sample__identifier'].replace(to_replace=[None], value='All',
                                                inplace=True)

    df = df.groupby('biological_sample__identifier', as_index=False).agg('sum')
    df = df.sort_values(by='peptide_counts', ascending=True)

    df2 = df[df['biological_sample__identifier'] != 'All']

    # Bokeh plot
    height_custom = 50 + 25 * df2.shape[0]

    if height_custom < plot_height:
        height_custom = plot_height

    plot = base_plot(height=height_custom,
                     tooltips='@peptide_counts',
                     x_axis_label='Peptide counts',
                     y_axis_label='Biological sample',
                     y_range=list(df2['biological_sample__identifier']),
                     )
    plot.xaxis.major_label_orientation = 'vertical'

    plot.hbar(left=0, right='peptide_counts',
              y='biological_sample__identifier',
              height=0.5, source=ColumnDataSource(df2))
    plot.xgrid.grid_line_color = 'navy'
    plot.xgrid.grid_line_alpha = 0.1

    return plot, df


def peptide_file_counts(df: pd.DataFrame) -> tuple:
    """
    Plots the number of time peptide that have been found in n file
    :param df: Stats data frame
    :return: Plots and data frame
    """

    if df.shape[0]:
        biological_sample_pk = df['biological_sample'][0]
    else:
        biological_sample_pk = -1

    queryset = PeptideFileCount.objects.select_related()

    queryset = queryset.filter(biological_sample_id=biological_sample_pk)

    if biological_sample_pk is None:
        queryset = queryset.filter(taxonomy_identifier=0)

    df = read_frame(queryset,
                    fieldnames=['biological_sample__identifier',
                                'file_counts',
                                'peptide_counts'])
    df['biological_sample__identifier'].replace(to_replace=[None], value='All',
                                                inplace=True)

    plot = base_plot(tooltips='@file_counts: @peptide_counts',
                     x_axis_label='Peptide found in n files',
                     y_axis_label='Peptide counts',
                     )

    plot.vbar(x='file_counts', top='peptide_counts', width=0.8, bottom=0,
              source=ColumnDataSource(df))

    return plot, df


plots = {
    'allele_counts': allele_counts,
    'biological_sample_peptide_counts': biological_sample_peptide_counts,
    'peptide_biological_sample_counts': peptide_biological_sample_counts,
    'peptide_counts': peptide_counts,
    'peptide_counts_cumulative': peptide_counts_cumulative,
    'peptide_file_counts': peptide_file_counts
}
