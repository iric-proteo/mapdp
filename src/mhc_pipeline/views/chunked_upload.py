"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import os

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView

# Third party libraries
from chunked_upload.views import ChunkedUploadView, ChunkedUploadCompleteView

# Import project libraries
from .fileset import EnvironmentMixin, FileSetMixin
from ..models import ChunkedUploadedFile, File, UploadedFile
from .. import tasks


@method_decorator(login_required, name='dispatch')
class ChunkedUpload(EnvironmentMixin, FileSetMixin, TemplateView):
    """
    This view provides the form for file upload.
    """

    action = 'Upload file'
    check_set_class_permission = True
    template_name = 'chunked_upload.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['identifier'] = str(self.instance)
        context['extensions'] = ','.join(self.file_set.get_class_attribute('extensions'))

        return context


@method_decorator(login_required, name='dispatch')
class MyChunkedUploadView(ChunkedUploadView):
    """
    This view receives the posted chunk
    """

    model = ChunkedUploadedFile
    field_name = 'the_file'


@method_decorator(login_required, name='dispatch')
class MyChunkedUploadCompleteView(EnvironmentMixin, ChunkedUploadCompleteView):
    """
    This view place the file at the expected location.
    """

    model = ChunkedUploadedFile

    def on_completion(self, uploaded_file, request):

        # Trick Django to move the file instead of copy
        def temporary_file_path():
            return uploaded_file.file.path

        uploaded_file._temporary_file_path = uploaded_file.file.path

        uploaded_file.temporary_file_path = temporary_file_path

        obj = UploadedFile.objects.create(class_name=self.kwargs['class_name'],
                                          file=uploaded_file,
                                          name=uploaded_file.name)

        # Path and file name change after save
        path, file = os.path.split(obj.file.path)
        file_ = File.add_file(self.file_set, path, file)

        if file_.extension not in ['.bam', '.zip']:
            tasks.compress_uploaded_file.apply_async((obj.pk, file_.pk),
                                                     countdown=2)

    def get_response_data(self, chunked_upload, request):

        return {'message': f"You successfully uploaded "
                           f"{chunked_upload.filename}"}
