"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from functools import reduce
from mimetypes import MimeTypes
import re
import operator
import os
import tempfile
from typing import List
import urllib
import zipfile

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ImproperlyConfigured
from django.core.files import File
from django.db.models import Q
from django.http import FileResponse, Http404
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import (TemplateView,
                                  View)
from django.views.generic.edit import CreateView

# Import project libraries
from base_site.exceptions import BaseSiteException
from ..forms import (BiologicalSampleFileSetForm,
                     UploadedFileForm)
from library.file_system import (get_files,
                                 get_subdirectories)
from ..library.permission import get_if_permission

from ..models import (BiologicalSample,
                      BiologicalSampleFileSet,
                      File,
                      FileSet,
                      Identification)

from ..library.permission import (check_set_class_permission,
                                  get_obj_by_type)
from .. import tasks
from .view_mixin import TemplateMixin


# Create your views here.
# Note: More a BiologicalSampleFileSet views, refactor will be needed if FileSet
# are used for something else

class EnvironmentMixin(object):
    """
    Mixin that setups environment for FileSet views.
    """

    check_set_class_permission = False
    obj_permission = 'change'

    def __init__(self, *args, **kwargs):
        """
        Declares and initializes attributes of this view.
        :return: None
        """

        self.file_set = None

        self.instance = None

    def dispatch(self, request, *args, **kwargs):
        self.set_environment(request, *args, **kwargs)

        return super().dispatch(request, *args, **kwargs)

    def set_environment(self, request, *args, **kwargs):
        """
        Retrieves file_set and supplied content instance.
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
            content_type: Content type String of model object.
            ct_pk: Primary key of model object.
            fs_pk: Primary key of fileset object.
            f_pk: Primary key of File object. (optional)
        :return: None
        """

        self.instance = get_obj_by_type(request, kwargs['content_type'],
                                        kwargs['ct_pk'], self.obj_permission)

        self.file_set = get_object_or_404(FileSet, pk=kwargs['fs_pk'])

        if self.check_set_class_permission:
            check_set_class_permission(request, self.file_set)

    def check_reference(self) -> None:
        """
        Checks that no reference is linked to the file set
        :return: None
        """

        if self.file_set.has_reference is True:

            msg = 'File set has a reference and cannot be deleted or modified.'

            raise BaseSiteException('warning', msg)


class FileSetRedirect(EnvironmentMixin, View):
    """
    FileSet Base class based view. It prepares environment, executes a task
    and returns response or redirects to instance.
    """

    def get(self, request, *args, **kwargs):
        """
        Get methods
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        :return: HttpResponse
        """

        return self.task() or redirect(self.instance)

    def task(self):
        """
        Executes task on FileSet. It musts be defined in children class.
        :return: Not specified
        """

        raise ImproperlyConfigured(
            "FileSetRedirect requires definition of task() method")


class FileSetMixin(TemplateMixin):
    """
    Mixin for FileSet views
    """

    column_width = 6

    icon = 'file'

    title = 'FileSet'

    title_suffix = ''

    template_base = 'mhc_pipeline/fileset/'


@method_decorator(login_required, name='dispatch')
class Create(FileSetMixin, CreateView):
    """
    Creates a new BiologicalSampleFileSet instance.
    """

    action = 'Create'

    form_class = BiologicalSampleFileSetForm

    template_name = 'form.html'

    def form_valid(self, form):
        """
        If form is valid, it saves to database and redirects to next Browse
        view.
        :param form: Form instance
        :return: HttpResponse
        """

        content_type = self.kwargs['content_type']
        ct_pk = self.kwargs['ct_pk']

        obj = get_obj_by_type(self.request, content_type, ct_pk, 'change')

        # Create FileSet
        file_set = form.save(commit=False)
        file_set.biological_sample = obj

        # Permission check
        check_set_class_permission(self.request, file_set)

        file_set.created_by = self.request.user
        file_set.modified_by = self.request.user
        file_set.save()

        obj.modified_by = self.request.user
        obj.save()  # Update modified by/at

        # Redirect user to file browser or file upload

        kwargs = {'ct_pk': ct_pk,
                  'fs_pk': file_set.pk,
                  'content_type': content_type,
                  'subdir': '/',
                  }

        class_name = file_set.get_set_class_display()
        if class_name == 'Fasta' or class_name == 'BAM':
            kwargs['class_name'] = class_name
            del kwargs['subdir']

            return redirect('fileset:chunked_form', **kwargs)

        return redirect('fileset:browse', **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        content_type = self.kwargs['content_type']
        ct_pk = self.kwargs['ct_pk']

        obj = get_obj_by_type(self.request, content_type, ct_pk, 'change')

        context['identifier'] = obj.identifier

        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        set_classes = [int(x) for x in self.kwargs['set_classes'].split(',')]
        kwargs['set_classes'] = set_classes

        return kwargs


@method_decorator(login_required, name='dispatch')
class Browse(FileSetMixin, EnvironmentMixin, TemplateView):
    """
    Generates a view to allow user to select files associated to the FileSet
    instance.
    """

    check_set_class_permission = True

    title_suffix = ' - File browser'

    template_name = 'browse.html'

    @staticmethod
    def clean_subdir_str(subdir):
        """
        Cleans sub-directory string
        :param subdir: String
        :return: (clean_subdir String, url_subdir String)
        """

        clean_subdir = re.sub('[^/]+/<--', '', subdir)

        if '..' in clean_subdir:
            clean_subdir = ''  # For security

        url_subdir = clean_subdir
        clean_subdir = re.sub('/$', '', clean_subdir)
        clean_subdir = clean_subdir.replace('/', os.sep)

        return clean_subdir, url_subdir

    def get_context_data(self, **kwargs):
        """
        Provides context data for the view.
        :param kwargs: Keyword arguments
        :return: Context dictionary
        """

        self.check_reference()

        content_type = self.kwargs['content_type']
        ct_pk = self.kwargs['ct_pk']
        fs_pk = self.kwargs['fs_pk']
        subdir = self.kwargs['subdir']

        clean_subdir, url_subdir = self.clean_subdir_str(subdir)

        try:
            path, directories, files = self.ls(self.file_set, clean_subdir)
        except FileNotFoundError:
            raise Http404

        # Prep files and directories for display
        files_display = list()

        for position, file_name in enumerate(files):
            files_display.append(File(name=file_name, pk=position))

        context = {
            'class_name': self.file_set.get_set_class_display(),
            'content_type': content_type,
            'ct_pk': ct_pk,
            'directories': directories,
            'files': files_display,
            'fileset_note': self.file_set.get_class_attribute('note'),
            'fs_pk': fs_pk,
            'subdir': url_subdir,
            'name': self.file_set.name
        }

        context = {**super().get_context_data(**kwargs), **context}

        return context

    @staticmethod
    def ls(file_set, subdir):
        """
        Lists directories and files in the sub-directories.
        :param file_set: FileSet instance
        :param subdir: String
        :return: Path String, directories list, files list
        """

        system_dir = file_set.get_class_attribute('dir')
        extensions = file_set.get_class_attribute('extensions')

        path = system_dir + subdir

        # Get files and directories
        directories = list()
        files = list()

        files.extend(get_files(path, extensions))
        files.sort(key=str.lower)
        directories.extend(get_subdirectories(path))

        return path, directories, files

    def post(self, request, *args, **kwargs):
        """
        POST method
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        :return: HttpResponse
        """

        clean_subdir, url_subdir = self.clean_subdir_str(kwargs['subdir'])

        try:
            path, directories, files = self.ls(self.file_set, clean_subdir)
        except FileNotFoundError:
            raise Http404

        # User selected files, add them to the FileSet instance
        if 'id[]' in request.POST:

            selected_values = request.POST.getlist('id[]')

            # Get files already associated to file set
            files_fs = dict()

            for file in self.file_set.file_set.all():
                files_fs[(file.name, file.path)] = True

            for position, file in enumerate(files):
                # Do not add existing files
                if (file, path) in files_fs:
                    continue

                if str(position) in selected_values:
                    File.add_file(self.file_set, path, file)

            self.instance.modified_by = request.user
            self.instance.save()  # Update modified by/at

            # Trigger identification import
            if self.file_set.set_class in [4, 6]:
                identification = Identification.objects.create(
                    created_by=request.user,
                    modified_by=request.user,
                    biological_sample=self.instance,
                    file_set=self.file_set,
                    software='',
                    state='pending'
                )

                tasks.import_identification.delay(identification.pk, 1)

        # Redirect user to its content
        return redirect(self.instance)


@method_decorator(login_required, name='dispatch')
class Refresh(FileSetRedirect):
    """
    Checks if files are still available on the file system and updates the
    available status flag.
    """

    obj_permission = 'view'

    def task(self):
        for file in self.file_set.file_set.all():
            full_path = os.path.join(file.path, file.name)
            file.available = os.path.isfile(full_path)
            file.save()


@method_decorator(login_required, name='dispatch')
class RemoveFile(FileSetRedirect):
    """
    Removes a single file from a FileSet.
    """

    def task(self):
        self.check_reference()

        file = get_object_or_404(File, pk=self.kwargs['f_pk'])
        file.delete()

        self.instance.modified_by = self.request.user
        self.instance.save()  # Update modified by/at


@method_decorator(login_required, name='dispatch')
class SendFile(FileSetRedirect):
    """
    Sends a single file.
    """

    obj_permission = 'view'

    def task(self):
        file = get_object_or_404(File, pk=self.kwargs['f_pk'])

        try:
            # Adapted from https://djangosnippets.org/snippets/365/

            # Prepare file
            filename = os.path.join(file.path, file.name)
            # wrapper = FileWrapper(open(filename, 'rb'))

            mime = MimeTypes()
            mime.add_type('application/mzid', '.mzid')
            mime.add_type('application/mgf', '.mgf')
            mime.add_type('application/raw', '.raw')
            url = urllib.request.pathname2url(filename)
            mime_type = mime.guess_type(url)

            # Prepare Http response
            response = FileResponse(open(filename, 'rb'),
                                    content_type=mime_type[0])
            response['Content-Length'] = file.size
            response[
                'Content-Disposition'] = 'attachment; filename=' + file.name

            return response

        except IOError:

            raise Http404


@method_decorator(login_required, name='dispatch')
class SendFiles(FileSetRedirect):
    """
    Sends all files associated to a FileSet.
    """

    obj_permission = 'view'

    def task(self):
        try:
            # Adapted from https://djangosnippets.org/snippets/365/

            # Initialize a temp zip file
            temp = tempfile.TemporaryFile(mode='w+b')
            archive = zipfile.ZipFile(temp, 'w', zipfile.ZIP_DEFLATED)

            # Iterate through files and add them to the zip archive
            for file in self.file_set.file_set.all():

                filename = os.path.join(file.path, file.name)

                # Keep files known to be available
                if file.available:
                    archive.write(filename, file.name)

            archive.close()

            # Prepare response
            ct_length = temp.tell()
            temp.seek(0)
            # wrapper = FileWrapper(temp)
            response = FileResponse(temp, content_type='application/zip')
            response['Content-Length'] = ct_length
            response[
                'Content-Disposition'] = 'attachment; filename=FileSet_pk_%s_%s_' \
                                         'pk_%s.zip' % (self.kwargs['fs_pk'],
                                                        self.kwargs[
                                                            'content_type'],
                                                        self.kwargs['ct_pk'])

            return response

        except IOError:

            raise Http404


@method_decorator(login_required, name='dispatch')
class Trash(FileSetRedirect):
    """
    Trashes a FileSet instance.
    """

    def task(self):

        self.check_reference()

        self.file_set.trashed = True
        self.file_set.save()

        self.instance.modified_by = self.request.user
        self.instance.save()  # Update modified by/at


# @method_decorator(login_required, name='dispatch')
# class Upload(FileSetMixin, EnvironmentMixin, CreateView):
#     """
#     Creates a new UploadedFile instance.
#     """
#
#     action = 'Upload file'
#
#     check_set_class_permission = True
#
#     form_class = UploadedFileForm
#
#     template_base = 'mhc_pipeline/base_site/'
#
#     template_name = 'form.html'
#
#     def form_valid(self, form):
#         """
#         If the form is valid, it saves the associated model.
#         :param form: Form instance
#         :return: HttpResponse
#         """
#
#         obj = form.save(commit=False)
#         path, file = os.path.split(obj.file.path)
#
#         obj.class_name = self.kwargs['class_name']
#         obj.name = file
#         obj.save()
#
#         # Path and file name change after save
#         path, file = os.path.split(obj.file.path)
#         file_ = File.add_file(self.file_set, path, file)
#
#         if file_.extension not in ['.bam', '.zip']:
#             tasks.compress_uploaded_file.apply_async((obj.pk, file_.pk),
#                                                      countdown=2)
#
#         return redirect(self.instance)
#
#     def get_form_kwargs(self):
#         """
#         Returns keyword arguments for instantiating the form.
#         """
#
#         kwargs = super().get_form_kwargs()
#
#         kwargs['extensions'] = self.file_set.get_class_attribute('extensions')
#
#         return kwargs


def filter_biological_filesets(biological_sample: int,
                               numeric_classes: List[int]):
    """
    Prepares a filtered biological filesets queryset
    :param biological_sample: Biological sample identifier
    :param numeric_classes: List of fileset numeric classes
    :return: BiologicalSampleFileSet Queryset
    """

    q_values = [Q(**{'set_class': value}) for value in numeric_classes]

    return (BiologicalSampleFileSet.objects
            .filter(biological_sample=biological_sample, trashed=False)
            .filter(reduce(operator.or_, q_values))
            .prefetch_related('file_set')
            .order_by('-created_at'))


@method_decorator(login_required, name='dispatch')
class Tab(FileSetMixin, TemplateView):
    """
    View tab in biological view
    """

    template_name = 'tab.html'

    def get_context_data(self, **kwargs) -> dict:
        pk = kwargs['pk']

        # Is user authorized to look at biological sample
        obj = get_if_permission(self.request, BiologicalSample, 'view', pk=pk)

        label_classes = kwargs['fileset_classes'].split(',')
        numeric_classes = [FileSet.SET_CLASS_CHOICES[label]
                           for label in label_classes]

        groups = list()

        for label in label_classes:
            groups += FileSet.FILE_SET[label]['allowed_groups']

        add_permission = self.request.user.groups.filter(
            name__in=groups).exists()

        context = {
            'obj': obj,
            'add_permission': add_permission,
            'file_sets': filter_biological_filesets(pk, numeric_classes),
            'set_classes': ','.join([str(value) for value in numeric_classes])
        }

        return context


@method_decorator(login_required, name='dispatch')
class ViewFiles(EnvironmentMixin, FileSetMixin, TemplateView):
    """
    View all files associated to a FileSet.
    """

    obj_permission = 'view'

    template_name = 'single.html'

    def get_context_data(self, **kwargs) -> dict:
        context = {
            'obj': self.instance,
            'file_set': self.file_set,
        }

        return context
