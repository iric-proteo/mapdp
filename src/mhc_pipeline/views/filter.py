"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView

# Third party libraries
from guardian.shortcuts import (get_groups_with_perms,
                                get_users_with_perms)

# Import project libraries
from ..forms import (FilterRenameForm,
                     UserGroupForm)
from ..library.permission import (check_content_permission,
                                  get_if_permission,
                                  rm_content_permission,
                                  set_content_permission)
from ..models import Filter
from .view_mixin import (TemplateMixin,
                         TrashMixin)


@login_required
def hide(request, pk, value):
    """
    Updates visibility of filters
    :param request: HttpRequest object
    :param pk: Filter primary key integer
    :param value: String (true=hidden, false=visible)
    :return: HttpResponse object
    """

    obj = Filter.objects.get(pk=pk)

    check_content_permission(request, 'view', obj)

    if value == 'true':
        rm_content_permission(obj, request.user, ['visible'])

    else:
        set_content_permission(obj, request.user, ['visible'])

    return HttpResponse('{"updated": 1}', content_type='application/json')


class FilterMixin(TemplateMixin):
    """
    Mixin for Filter views
    """

    column_width = 12

    icon = 'filter'

    template_base = 'mhc_pipeline/filters/'

    template_name = 'form.html'

    title = 'Filters'

    title_suffix = ''


@method_decorator(login_required, name='dispatch')
class Edit(FilterMixin, UpdateView):
    """
    View to rename Filter instance.
    """

    action = 'Rename'

    column_width = 6

    form_class = FilterRenameForm

    template_base = 'mhc_pipeline/base_site/'

    def form_valid(self, form):
        """
        If form is valid, save to database and redirect to instance url.
        :param form: Form instance
        :return: HttpResponse
        """

        instance = form.instance
        instance.modified_by = self.request.user
        instance.save()

        return redirect('filter:list')

    def get_object(self, queryset=None):
        """
        Gets the Filter instance to edit
        :param queryset: Custom queryset (not used)
        :return: Filter instance
        """

        return get_if_permission(self.request, Filter, 'change',
                                 pk=self.kwargs['pk'])


@method_decorator(login_required, name='dispatch')
class List(FilterMixin, ListView):
    """
    View to list Filter instances available to the user.
    """

    context_object_name = 'filters'

    template_name = 'list.html'

    def get_queryset(self):
        """
        Returns queryset of Filter instances
        :return: Filter queryset
        """

        filters = Filter.objects.for_user(self.request.user).select_related(
            'created_by', 'modified_by')

        # Set visibility for use in template
        visible_filters = Filter.objects.visible_for_user(
            self.request.user).values_list('pk', flat=True)

        for filter_obj in filters:
            filter_obj.hidden = filter_obj.pk not in visible_filters

        return filters


@method_decorator(login_required, name='dispatch')
class Share(FilterMixin, TemplateView):
    """
    View to share Filter instances with groups or users.
    """

    action = 'Share'

    column_width = 12

    icon = 'share'

    template_name = 'share.html'

    def __init__(self, **kwargs):
        """
        Constructor to initialize variables
        :param kwargs: Keyword arguments
        :return: Share view instance
        """

        super().__init__(**kwargs)

        self.filter = None
        self.groups = None
        self.groups_pk = None
        self.redirect = None
        self.users = None
        self.users_pk = None

    def form_valid(self, form):
        """
        If form is valid, update users and groups that can use the Filter
        instance.
        :param form: Form instance
        :return: HttpResponse
        """

        if form.is_valid():

            permissions = ['view', 'visible']

            for model in ['users', 'groups']:

                selected_objects_pk = {model_object.pk for model_object in
                                       form.cleaned_data[model]}

                model_pk = getattr(self, model + '_pk')

                action_objects = (
                    (set_content_permission, selected_objects_pk - model_pk,
                     form.cleaned_data[model]),
                    (rm_content_permission, model_pk - selected_objects_pk,
                     getattr(self, model)),
                )

                for action, selected_objects, model_objects in action_objects:
                    for model_object in model_objects:
                        if model_object.pk in selected_objects:
                            action(self.filter, model_object, permissions)

            self.redirect = redirect('filter:list')

    def get_context_data(self, **kwargs):
        """
        Adds form to context dictionary and stores filter, groups, users in the
        view for later use.
        :param kwargs: Keywords arguments
        :return: Context dictionary
        """

        self.filter = get_if_permission(self.request, Filter, 'change',
                                        pk=self.kwargs['pk'])

        self.users = get_users_with_perms(self.filter)
        self.groups = get_groups_with_perms(self.filter)
        self.users_pk = {user.pk for user in self.users}
        self.groups_pk = {group.pk for group in self.groups}

        data = dict()

        if self.request.POST:
            data = self.request.POST

        else:
            data['users'] = list(self.users_pk)
            data['groups'] = list(self.groups_pk)

        form = UserGroupForm(data, user_groups=self.request.user.groups.all())

        context = super(Share, self).get_context_data(**kwargs)
        context['form'] = form

        return context

    def post(self, request, *args, **kwargs):
        """
        POST method
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        :return: HttpResponse
        """

        context = self.get_context_data(**kwargs)

        self.form_valid(context['form'])

        return self.redirect or self.render_to_response(context)


@method_decorator(login_required, name='dispatch')
class Trash(TrashMixin, FilterMixin, TemplateView):
    """
    Trashes Filter instance
    """

    model = Filter
