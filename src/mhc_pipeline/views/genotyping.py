"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import json

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView, View

# Third party libraries

# Import project libraries
from base_site.views.base_site import server_secure_media
from .fileset import filter_biological_filesets, Tab as FilesetTab
from ..forms import GenotypingForm
from ..library.permission import get_if_permission
from ..models import (BiologicalSample,
                      FileSet,
                      Genotyping)
from .. import tasks
from .view_mixin import (TemplateMixin,
                         TrashMixin)


@login_required
def download(request, pk: int):
    """
    View to download results
    :param request: HTTP request
    :param pk: Genotyping primary key
    :return: HTTP response
    """

    obj = Genotyping.objects.get(pk=pk)
    get_if_permission(request, BiologicalSample, 'view',
                      pk=obj.biological_sample_id)

    url = f'genotyping/{pk}/results.zip'

    return server_secure_media(request, url, staff_only=False)


class GenotypingMixin(TemplateMixin):
    """
    Mixin for Genotyping views
    """

    column_width = 6

    icon = 'barcode'

    title = 'MHC Genotyping'

    title_suffix = ''

    template_base = 'mhc_pipeline/genotyping/'


@method_decorator(login_required, name='dispatch')
class Create(GenotypingMixin, CreateView):
    """
    View to create a new BiologicalSample instance.
    """

    action = 'Create'

    form_class = GenotypingForm

    template_base = 'mhc_pipeline/base_site/'

    template_name = 'form.html'

    def form_valid(self, form):
        """
        If form is valid, save to database and redirect to instance url.
        :param form: Form instance
        :return: HttpResponse
        """

        pk = self.kwargs['pk']

        # Is user authorized to change the biological sample
        bs = get_if_permission(self.request, BiologicalSample, 'change', pk=pk)
        bs.modified_by = self.request.user
        bs.save()

        obj = form.save(commit=False)
        obj.biological_sample_id = pk
        obj.software = ''
        obj.state = 'pending'
        obj.created_by = self.request.user
        obj.modified_by = self.request.user
        obj.save()

        obj.file_set.locked = True
        obj.file_set.save()

        tasks.genotyping.delay(obj.pk)

        return redirect(bs)

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        kwargs['filesets'] = (filter_biological_filesets(self.kwargs['pk'], [8])
                              .annotate(file_count=Count('file'))
                              .filter(file_count__gt=0)
                              )

        return kwargs


# @method_decorator(csrf_exempt, name='dispatch')
# class NextflowWebhook(View):
#     # Not required for the moment, only useful for detailed execution
#     # TODO-MC no login - random id
#
#     def post(self, request):
#
#         values = json.loads(request.body)
#         print(values)
#         print(values['trace']['status'])
#
#         return HttpResponse("OK")


@method_decorator(login_required, name='dispatch')
class State(GenotypingMixin, TemplateView):
    """
    Provides state of the identification instance.
    """

    template_name = 'state.html'

    def get_context_data(self, **kwargs) -> dict:
        genotype = Genotyping.objects.get(pk=kwargs['pk'])

        # Is user authorized to look at biological sample
        get_if_permission(self.request, BiologicalSample, 'view',
                          pk=genotype.biological_sample_id)

        context = dict()
        context['obj'] = genotype

        return context


@method_decorator(login_required, name='dispatch')
class Tab(GenotypingMixin, TemplateView):
    """
    View tab in biological view
    """

    template_name = 'tab.html'

    def get_context_data(self, **kwargs) -> dict:
        pk = kwargs['pk']

        # Is user authorized to look at biological sample
        obj = get_if_permission(self.request, BiologicalSample, 'view', pk=pk)

        genotypes = (Genotyping.objects
                     .filter(biological_sample=pk, trashed=False)
                     .select_related('file_set')
                     .order_by('-pk'))

        t = FilesetTab(request=self.request)
        kwargs['fileset_classes'] = 'BAM'

        groups = FileSet.FILE_SET['BAM']['allowed_groups']
        # add_permission = self.request.user.groups.filter(
        #     name__in=groups).exists()

        context = t.get_context_data(**kwargs)
        context['obj'] = obj
        # context['add_permission'] = add_permission
        context['genotypes'] = genotypes

        return context


@method_decorator(login_required, name='dispatch')
class Trash(TrashMixin, GenotypingMixin, TemplateView):
    """
    Trashes Genotyping instance.
    """

    bs_fileset = True
    model = Genotyping

    def get_context_data(self, **kwargs):

        if kwargs.get('confirmed', False):
            genotype = Genotyping.objects.get(pk=kwargs['pk'])
            get_if_permission(self.request, BiologicalSample, 'change',
                              pk=genotype.biological_sample_id)
            tasks.genotyping_clean.delay(kwargs['pk'])

        return super().get_context_data(**kwargs)






