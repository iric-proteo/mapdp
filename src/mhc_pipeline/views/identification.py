"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from datetime import datetime
from hashlib import md5
import gzip
import json
import os
from pathlib import Path
import time
from typing import Iterable

# Import Django related libraries
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import (ObjectDoesNotExist, PermissionDenied)
from django.db.models import Q
from django.urls import reverse
from django.http.response import HttpResponse, StreamingHttpResponse
from django.utils.decorators import method_decorator
from django.utils.http import http_date
from django.views.generic import TemplateView

# Third party libraries

# Import project libraries
from base_site.exceptions import BaseSiteException
from .fileset import Tab as FilesetTab
from ..library.filter import (create_filter,
                              filter_json)
from ..library.minio import get_storage
from ..library.permission import get_if_permission
from ..forms import FilterIdentificationForm
from .immunopeptidome import create_immunopeptidome
from ..models import (BiologicalSample,
                      BiologicalSampleMHCAllele,
                      FileSet,
                      Filter,
                      Identification,
                      ImmunoPeptidome,
                      Peptide)
from .peaks import check_files, get_search_parameters
from .peptides_plots import Plots as peptide_plots_view
from ..library import protein_database
from ..library.permission import check_content_permission
from .. import tasks
from .view_mixin import (TemplateMixin,
                         TrashMixin)

CACHE_EXPIRATION = 2678400  # One month


def csv_content(key: str) -> Iterable[bytes]:
    """
    Opens Minio storage and decompress the gzip content.
    :param key: Minio storage key
    :return: Yield one line
    """

    storage = get_storage()

    with storage.open('csv_' + key, 'rb') as file:

        with gzip.open(file, 'rb') as gz_file:

            for line in gz_file:

                yield line


@login_required
def csv(request, key: str) -> HttpResponse:
    """
    Retrieves identification CSV file.
    :param request: HttpRequest object
    :param key: Minio storage key
    :return: StreamHttpResponse
    """

    if not key.endswith(str(request.user.pk)):
        raise PermissionDenied

    storage = get_storage()

    if storage.exists('csv_' + key):

        with storage.open(key, 'r') as file:
            json_obj = json.loads(file.read())

        file_name = json_obj['csv_file_name']

        content = csv_content(key)

    else:
        content = 'No result'
        file_name = 'no_result.csv'

    response = StreamingHttpResponse(content, content_type='text/csv')

    response['Content-Disposition'] = f'attachment; filename={file_name}'

    response['Expires'] = http_date(time.time() + CACHE_EXPIRATION)

    return response


@login_required
def logos(request, key: str, file: str) -> HttpResponse:
    """
    Retrieves identification CSV file.
    :param request: HttpRequest object
    :param key: Minio storage key
    :return: HttpResponse
    """

    if not key.endswith(str(request.user.pk)):
        raise PermissionDenied

    storage = get_storage()

    key = key + '_' + file
    key = key.replace('/', '_')

    if storage.exists(key):

        with storage.open(key, 'rb') as fh:
            content = fh.readlines()

    else:
        content = 'Not ready'

    extension = key.split('.')

    content_type = {
        'html': 'text/html',
        'png': 'image/png',
    }

    response = HttpResponse(content, content_type=content_type[extension[1]])
    response['Expires'] = http_date(time.time() + CACHE_EXPIRATION)

    return response


def identification_content(key: str) -> bytes:
    """
    Retrieves identification JSON content
    :param key: Storage key
    :return: JSON content
    """

    storage = get_storage()

    if storage.exists(key):

        with storage.open(key, 'r') as file:
            content = file.read()

    else:
        content = None

    return content


@login_required
def identification_task(request, key: str) -> HttpResponse:
    """
    Generate JSON file the Identification View (AJAX request).
    :param request: HttpRequest object
    :param key: Cache key
    :return: HttpResponse
    """

    if not key.endswith(str(request.user.pk)):
        raise PermissionDenied

    content = identification_content(key)

    if content is None:
        content = json.dumps('None')

    response = HttpResponse(content,
                            content_type='application/json')

    return response


class IdentificationMixin(TemplateMixin):
    """
    Mixin for Identification views
    """

    column_width = 6

    icon = 'list-alt'

    title = 'Identifications'

    title_suffix = ''

    template_base = 'mhc_pipeline/identification/'


@method_decorator(login_required, name='dispatch')
class Trash(TrashMixin, IdentificationMixin, TemplateView):
    """
    Trashes Identification instance.
    """

    bs_fileset = True
    model = Identification


def validate_data_sets(fc, user):
    """
    Validates that the user has permission to view selected identifications
    and immunopeptidomes.
    :param fc: Filter identification form cleaned
    :param user: User
    :return: Raise PermissionDenied or returns None
    """

    if len(fc['identification']):

        identifications = Identification.objects.for_user(user)
        identifications = identifications.filter(pk__in=fc['identification'])

        if len(fc['identification']) != identifications.count():
            raise PermissionDenied

    if len(fc['immunopeptidome']):

        immunopeptidomes = ImmunoPeptidome.objects.for_user(
            user, filter_=Q(pk__in=fc['immunopeptidome']))

        if len(fc['immunopeptidome']) != len(immunopeptidomes):
            raise PermissionDenied


def peptide_search_mode(fc, user):
    """
    Provides identifications for peptide search
    :param fc: Filter identification form cleaned
    :param user: User
    :return: Changes fc['identification']
    """

    if len(fc['identification']) == 0 and len(fc['immunopeptidome']) == 0 and \
            (fc.get('unique_peptide__peptide_sequence__icontains') != '' or
             fc.get('unique_peptide__peptide_sequence__exact') != ''):

        identifications = Identification.objects.for_user(user)
        identifications = identifications.values_list('id', flat=True)
        fc['identification'] = list(identifications)


@method_decorator(login_required, name='dispatch')
class View(IdentificationMixin, TemplateView):
    """
    View to display peptide identifications.
    """

    template_name = 'view.html'

    def __init__(self):

        self.redirect = None

    def get_context_data(self, **kwargs) -> dict:
        """
        This view lists peptides in a table view.
        :return: Context dict
        """

        form = self.get_filter_form(**kwargs)

        context = super(View, self).get_context_data(**kwargs)
        context['form'] = form
        context['download'] = self.request.GET.get('download', '')
        context['ref_url'] = self.request.GET.get('ref_url', '')

        if form.is_valid() and 'filter' in self.request.GET:

            fc = form.cleaned_data
            validate_data_sets(fc, self.request.user)
            peptide_search_mode(fc, self.request.user)

            # Create filter
            user = self.request.user
            filter_json_ = filter_json(fc)
            new_filter = create_filter(fc, user)
            View.update_form_filters(new_filter, form, user)

            key = md5(filter_json_.encode('utf-8')).hexdigest() + str(user.pk)
            context['identification_key'] = key

            # Collect values for task
            fc['mapdp_git'] = os.environ['GIT_SHA']
            fc['timestamp'] = datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
            fc['filter_json'] = filter_json(fc)
            fc['identification_key'] = key
            fc['url'] = self.request.build_absolute_uri(reverse('msms:view',
                                                                args=[999]))
            fc['validation_timestamp'] = Peptide.latest_validated_peptide_timestamp()

            if fc['deconvolution_software'] != '':
                context['deconvolution_motif'] = True

            # Peptide plots
            if self.request.GET.get('peptides_plots'):
                tasks.peptides_generate_plots.delay(key, fc)

                values = {
                    'key': key,
                    'filter_options': fc,
                }

                self.redirect = peptide_plots_view.as_view()(self.request,
                                                             **values)
                return

            # Immunopeptidome creation
            if fc['immunopeptidome_name']:
                self.redirect = create_immunopeptidome(user, new_filter, fc)
                return

            # Clean cache if required
            cache_obj = identification_content(key)
            ignore_cache = fc.pop('ignore_cache', False)

            if cache_obj is not None:

                cache_obj = json.loads(cache_obj)

                if fc['validation_timestamp'] != \
                        cache_obj['validation_timestamp']:
                    ignore_cache = True

            if ignore_cache:
                storage = get_storage()
                storage.delete(key)
                cache_obj = None

            if cache_obj is None:

                tasks.identification_csv.delay(fc)

        return context

    def get_filter_form(self, **kwargs):
        """
        Initialises FilterIdentificationForm instance
        :param kwargs: Keyword arguments
        :return: FilterIdentificationForm instance
        """

        get_values = self.request.GET
        get_values._mutable = True

        filters = Filter.objects.visible_for_user(self.request.user)

        # Add identification specified in url
        identification_pk = kwargs.get('identification_pk', None)

        if 'identification' in get_values:
            get_values['identification'] = '|'.join(get_values.getlist(
                'identification'))

        if identification_pk is not None and get_values is not None \
                and 'identification' not in get_values:
            get_values['identification'] = str(identification_pk)

        if 'bs_pk' in get_values:

            try:
                mhc_alleles = BiologicalSampleMHCAllele.objects.get(
                    biological_sample_id=get_values['bs_pk']).mhc_allele.order_by('name')

                mhc_alleles_pk = mhc_alleles.values_list('pk', flat=True)
                mhc_alleles_pk = [str(v) for v in mhc_alleles_pk]
                mhc_alleles_pk = '|'.join(mhc_alleles_pk)

                get_values['mhc_allele'] = mhc_alleles_pk

            except ObjectDoesNotExist:
                pass

        # Load Filter object if specified
        if 'selected_filter' in get_values \
                and get_values['selected_filter'].isdigit():
            pk = get_values['selected_filter']

            filter_obj = Filter.objects.get(pk=int(pk))
            values_tmp = json.loads(filter_obj.json_values)

            # Transfer some values not in the stored filter
            values_dict = dict(get_values)
            values_tmp['immunopeptidome_name'] = get_values.get(
                'immunopeptidome_name',
                None)
            values_tmp['identification'] = values_dict.get('identification',
                                                           list())
            values_tmp['identification'] = '|'.join(values_tmp['identification'])
            values_tmp['immunopeptidome'] = values_dict.get('immunopeptidome',
                                                            list())
            values_tmp['immunopeptidome'] = '|'.join(values_tmp['immunopeptidome'])

            get_values = values_tmp

        return FilterIdentificationForm(get_values or None,
                                        filters=filters)

    def get(self, request, *args, **kwargs) -> HttpResponse:
        """
        GET method
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        :return: HttpResponse
        """

        context = self.get_context_data(**kwargs)

        return self.redirect or self.render_to_response(context)

    @staticmethod
    def update_form_filters(filter_, form, user):
        """
        Updates form filters when a new one was just created
        :param filter_: Filter instance
        :param form: FilterIdentificationForm instance
        :param user: User
        :return: None
        """

        if filter_ is not None:
            form.data._mutable = True
            form.data['filter_name'] = ''

            field = form.fields['selected_filter']
            field.queryset = Filter.objects.visible_for_user(user)


@method_decorator(login_required, name='dispatch')
class Tab(IdentificationMixin, TemplateView):
    """
    View tab in biological view
    """

    template_name = 'tab.html'

    def get_context_data(self, **kwargs) -> dict:

        pk = kwargs['pk']

        # Is user authorized to look at biological sample
        obj = get_if_permission(self.request, BiologicalSample, 'view', pk=pk)

        # Get identification
        identification_set = Identification.objects.filter(
            biological_sample=pk, trashed=False)
        identification_set = identification_set.select_related('file_set')

        t = FilesetTab(request=self.request)
        kwargs['fileset_classes'] = 'PEAKS - mzIdentML 1.1,MaxQuant'

        groups = FileSet.FILE_SET['PEAKS - mzIdentML 1.1']['allowed_groups']
        add_permission = self.request.user.groups.filter(name__in=groups).exists()

        context = t.get_context_data(**kwargs)
        context['obj'] = obj
        context['add_permission'] = add_permission
        context['identification_set'] = identification_set.order_by('-pk')

        return context


@method_decorator(login_required, name='dispatch')
class State(IdentificationMixin, TemplateView):
    """
    Provides state of the identification instance.
    """

    template_name = 'state.html'

    def get_context_data(self, **kwargs) -> dict:
        identification = Identification.objects.select_related('biological_sample')
        identification = identification.get(pk=kwargs['pk'])

        biological_sample = identification.biological_sample
        check_content_permission(self.request, 'view', biological_sample)

        context = dict()
        context['obj'] = identification

        return context


@method_decorator(login_required, name='dispatch')
class Settings(IdentificationMixin, TemplateView):
    """
    Provides search parameters for the identification instance.
    """

    template_name = 'settings.html'

    def get_context_data(self, **kwargs) -> dict:
        identification = Identification.objects.select_related('biological_sample')
        identification = identification.get(pk=kwargs['pk'])

        biological_sample = identification.biological_sample
        check_content_permission(self.request, 'view', biological_sample)

        get = False

        if identification.search_parameters is None:
            get = True
            search_parameters = dict()

        else:
            search_parameters = json.loads(identification.search_parameters)

            if search_parameters['revision'] != get_search_parameters.revision:
                get = True

        if get:

            try:
                files = check_files(identification.file_set, check_mgf=False)
                search_parameters.update(get_search_parameters(files['mz_id']))
                identification.search_parameters = json.dumps(search_parameters)
                identification.save()
            except BaseSiteException as e:

                if identification.search_parameters is None:
                    return {
                        'obj': identification,
                        'error': e.message,
                    }

        context = search_parameters

        db_set = protein_database.from_all(
            {'identification': [identification.pk],
             'immunopeptidome': []
             }
        )

        if db_set:
            context['db'] = db_set.pop()
            context['db_location'] = context['db'].location.replace('file:/', '')
            context['db_location'] = context['db_location'].replace('%20', ' ')
        else:
            context['db'] = ''
            context['db_location'] = ''
        context['obj'] = identification

        return context

