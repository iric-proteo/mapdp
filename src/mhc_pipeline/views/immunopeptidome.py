"""
Copyright 2015-2021 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from hashlib import md5
import json
import os
import time

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.core.exceptions import (ObjectDoesNotExist,
                                    PermissionDenied)
from django.db.models import Q
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.views.generic.edit import (CreateView,
                                       FormView,
                                       UpdateView)
from django.views.generic.list import ListView

# Third party libraries
import bokeh
from celery import chain
import pandas as pd

# Import project libraries
from base_site.exceptions import BaseSiteException
from base_site.views.base_site import server_secure_media
from ..library.filter import create_filter, filter_json
from ..library.peptides_filter import PeptidesFilter
from ..forms import (AnnotateForm,
                     CommentForm,
                     ImmunoPeptidomeForm,
                     ImmunoPeptidomeFileForm,
                     MiHAFilterForm
                     )
from ..library.permission import set_content_permission
from ..models import (BiologicalSample,
                      Identification,
                      ImmunoPeptidomeDocument,
                      ImmunoPeptidome,
                      Peptide,
                      PersonalizedProteome,
                      Tag)
from .permission import permission_context
from .. import tasks
from .view_mixin import (TemplateMixin,
                         TrashMixin)


def create_immunopeptidome(user, filter_, fc):
    """
    Creates new ImmunoPeptidome instance

    Peptides a
    :param user: User instance
    :param filter_: Filter instance (optional)
    :param fc: Cleaned form
    :return: Redirects to the new ImmunoPeptidome instance
    """

    instance = ImmunoPeptidome.objects.create(
        created_by=user,
        modified_by=user,
        name=fc['immunopeptidome_name'],
        peptide_count=-1
    )

    # Create a new filter if one was not selected
    if filter_ is None:
        fc['filter_name'] = f'Immunopeptidome filter id: {instance.pk}'
        instance.filter = create_filter(fc, user)

    instance.save()

    # Associate identifications
    identifications = Identification.objects.filter(pk__in=fc['identification'])
    identifications = identifications.prefetch_related('biological_sample')
    instance.identification.add(*list(identifications))

    # Associate immunopeptidome
    immunopeptidomes = ImmunoPeptidome.objects.filter(
        pk__in=fc['immunopeptidome'])
    instance.immunopeptidome.add(*list(immunopeptidomes))

    # Associate biological samples
    for identification in identifications:
        instance.biological_sample.add(identification.biological_sample)

    for immunopeptidome in immunopeptidomes:

        for biological_sample in immunopeptidome.biological_sample.all():
            instance.biological_sample.add(biological_sample)

    # Set permissions for the creator
    set_content_permission(instance, user,
                           permissions=['change', 'delete', 'view'])

    tasks.immunopeptidome_peptides.delay(instance.pk, fc)

    # Redirect to view the new immunopeptidome
    return redirect(instance)


def add_peptides(pk: int, filter_options: dict) -> None:
    """
    Adds peptides to immunopeptidome
    :param pk: Immunopeptidome primary key
    :param filter_options: Peptide filter options
    :return: None
    """

    # Wait for view transaction to be completed
    for attempt in range(20):
        time.sleep(0.1)
        try:
            immunopeptidome = ImmunoPeptidome.objects.get(pk=pk)
        except ObjectDoesNotExist:
            pass
        else:
            break
    else:
        raise ObjectDoesNotExist

    data = dict()
    peptide_filters = PeptidesFilter(filter_options, data=data)
    peptides = peptide_filters.apply()
    peptide_list = list(peptides)
    peptide_count = len(peptide_list)

    for chunk in (peptide_list[i:i + 500] for i in
                  range(0, peptide_count, 500)):
        immunopeptidome.peptides.add(*chunk)

    immunopeptidome.peptide_count = peptide_count
    immunopeptidome.save()


class ImmunoPeptidomeMixin(TemplateMixin):
    """
    Mixin for ImmunoPeptidome views
    """

    column_width = 6

    icon = 'barcode'

    title = 'Immunopeptidomes'

    title_suffix = ''

    template_base = 'mhc_pipeline/immunopeptidome/'


@method_decorator(login_required, name='dispatch')
class CommentEdit(ImmunoPeptidomeMixin, FormView):
    """
    View to edit comment of document object.
    """

    action = 'Edit comment'

    form_class = CommentForm

    template_base = 'mhc_pipeline/base_site/'

    template_name = 'form.html'

    def form_valid(self, form):
        """
        Changes comment to document instance
        :param form: Form instance
        :return: HttpResponse
        """

        document_pk = self.kwargs['doc_pk']
        ip_pk = self.kwargs['pk']

        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            ip_pk, self.request.user, 'change')

        document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)
        document.comment = form.cleaned_data['comment']
        document.save()

        return redirect(immunopeptidome)

    def get_initial(self) -> dict:
        """
        Provides initial value for comment form
        :return: Dictionary
        """

        document_pk = self.kwargs['doc_pk']
        ip_pk = self.kwargs['pk']

        ImmunoPeptidome.objects.get_for_user(ip_pk, self.request.user, 'change')

        document = ImmunoPeptidomeDocument.objects.get(pk=document_pk)

        return {'comment': document.comment}


@method_decorator(login_required, name='dispatch')
class CreateFromFile(ImmunoPeptidomeMixin, CreateView):
    """
    View to create a new Immunopeptidome instance from CSV file.
    """

    action = 'Create from file'

    form_class = ImmunoPeptidomeFileForm

    template_base = 'mhc_pipeline/base_site/'

    template_name = 'form.html'

    def check_peptides_permission(self, df: pd.DataFrame, filter_: dict) \
            -> None:
        """
        Verifies that the user has the right to access peptides in the file.

        Peptide identifiers are validated against peptide identifiers from
        immunopeptidome from header or identification data sets
        :param df: File data frame
        :param filter_: Filter from the file header
        :return: Raise permission denied
        """

        user = self.request.user

        # Get peptide identifiers from identifications
        user_biological_samples = BiologicalSample.objects.for_user(user)
        identifications_with_perm = Identification.objects.filter(
            pk__in=df['identification_id'].unique(),
            biological_sample__in=user_biological_samples)

        peptides = Peptide.objects.filter(
            identification__in=identifications_with_perm).values_list('pk',
                                                                      flat=True)
        peptides_with_perm = set(peptides)

        # Get peptide identifiers from immunopeptidomes
        immunopeptidomes = filter_.get('immunopeptidome', [])

        immunopeptidomes = ImmunoPeptidome.objects.for_user(
            user, filter_=Q(pk__in=immunopeptidomes))

        t = Peptide.immunopeptidome_set.through
        tmp = t.objects.filter(immunopeptidome_id__in=immunopeptidomes)
        peptides = Peptide.objects.filter(id__in=tmp).values_list('pk',
                                                                  flat=True)

        peptides_with_perm.update(peptides)

        if len(set(df['peptide_id']) - peptides_with_perm) != 0:
            raise PermissionDenied

    def form_valid(self, form):
        """
        If form is valid, save to database and redirect to instance url.
        :param form: Form instance
        :return: HttpResponse
        """

        file = self.request.FILES['file']
        user = self.request.user

        filter_header = file.readline().decode('utf-8')
        filter_ = json.loads(filter_header.rstrip())

        # Skip database line
        pos = file.tell()
        line = file.readline().decode('utf-8')

        while 'Scan number' not in line:
            pos = file.tell()
            line = file.readline().decode('utf-8')

        file.seek(pos)

        df = pd.read_csv(file)

        self.check_peptides_permission(df, filter_)

        instance = form.save(commit=False)
        instance.created_by = user
        instance.modified_by = user
        instance.comments += '\nImported from file: ' + \
                             self.request.FILES['file'].name
        instance.save()

        filter_['filter_name'] = f'Immunopeptidome id: {instance.pk}'
        instance.filter = create_filter(filter_, user)
        instance.save()

        set_content_permission(instance, user,
                               permissions=['change', 'delete', 'view'])

        self.process(instance, df, filter_)

        return redirect(instance)

    @staticmethod
    def process(immunopeptidome, df, filter_):

        immunopeptidome.biological_sample.add(
            *df['biological_sample_id'].unique())
        immunopeptidome.identification.add(*filter_.get('identification', []))
        immunopeptidome.immunopeptidome.add(*filter_.get('immunopeptidome', []))
        immunopeptidome.peptide_count = df['peptide_id'].size

        # Associate peptides
        for chunk in (df['peptide_id'][i:i + 500] for i in
                      range(0, df['peptide_id'].size, 500)):
            immunopeptidome.peptides.add(*chunk)

        immunopeptidome.save()


@method_decorator(login_required, name='dispatch')
class Edit(ImmunoPeptidomeMixin, UpdateView):
    """
    View to edit an ImmunoPeptidome instance.
    """

    action = 'Edit'

    form_class = ImmunoPeptidomeForm

    template_base = 'mhc_pipeline/base_site/'

    template_name = 'form.html'

    def form_valid(self, form):
        """
        If form is valid, save to database and redirect to instance url.
        :param form: Form instance
        :return: HttpResponse
        """

        instance = form.save(commit=False)
        instance.modified_by = self.request.user
        instance.save()
        form.save_m2m()

        return redirect(instance)

    def get_object(self, queryset=None):
        """
        Gets the ImmunoPeptidome instance to edit
        :param queryset: ImmunoPeptidome queryset
        :return: ImmunoPeptidome instance
        """

        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], self.request.user, 'change')

        return immunopeptidome


@method_decorator(login_required, name='dispatch')
class List(ImmunoPeptidomeMixin, ListView):
    """
    View to list Immunopeptidome instances accessible to the user.
    """

    context_object_name = 'immunopeptidomes'

    template_name = 'list.html'

    def get_queryset(self):
        return ImmunoPeptidome.objects.for_user(self.request.user,
                                                related=True,
                                                reverse_sorted=True)


@method_decorator(login_required, name='dispatch')
class Tab(ImmunoPeptidomeMixin, ListView):
    """
    View to list Immunopeptidome instances accessible to the user for a
    biological sample.
    """

    context_object_name = 'immunopeptidomes'

    template_name = 'tab.html'

    def get_queryset(self):
        q_expression = Q(biological_sample=self.kwargs['pk'])
        return ImmunoPeptidome.objects.for_user(self.request.user,
                                                filter_=q_expression,
                                                related=True,
                                                reverse_sorted=True)


@method_decorator(login_required, name='dispatch')
class Trash(TrashMixin, ImmunoPeptidomeMixin, TemplateView):
    """
    Trashes Immunopeptidome instance
    """

    model = ImmunoPeptidome


@method_decorator(login_required, name='dispatch')
class View(ImmunoPeptidomeMixin, TemplateView):
    """
    View Immunopeptidome instance
    """

    template_name = 'view.html'

    def get_context_data(self, **kwargs):
        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], self.request.user, 'view', related=True)

        documents = ImmunoPeptidomeDocument.objects.filter(
            immunopeptidome=immunopeptidome, trashed=False).order_by('-pk')

        context = permission_context(immunopeptidome, 'immunopeptidome',
                                     self.request.user)
        context['obj'] = immunopeptidome
        context['steps'] = [immunopeptidome]
        context['documents'] = documents
        context['parent'] = ''
        context = {**context, **super().get_context_data(**kwargs)}

        return context


@method_decorator(login_required, name='dispatch')
class Annotate(ImmunoPeptidomeMixin, FormView):
    """
    View to create annotated immunopeptidome documents
    """

    action = 'Annotate immunopeptidome'

    column_width = 12

    form_class = AnnotateForm

    template_name = 'form.html'

    def form_valid(self, form):
        """
        Adds annotated peptides document to immunopeptidome.
        :param form: AnnotateForm instance
        :return: HttpResponse
        """

        pk = self.kwargs['pk']

        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], self.request.user, 'change')

        fc = form.cleaned_data.copy()
        pp_url = form.cleaned_data['personalized_proteome']
        pp = dict(form.fields['personalized_proteome'].choices)[pp_url]
        fc['personalized_proteome'] = pp

        if fc['personalized_proteome'] == '---------':
            del(fc['personalized_proteome'])

        for k, v in list(fc.items()):

            if k.startswith('annotation'):
                del (fc[k])

                if v is True:
                    label = form.fields[k].label

                    if 'annotation' not in fc:
                        fc['annotation'] = list()
                    fc['annotation'].append(label)

            elif k.startswith('mhc_predictor'):
                del (fc[k])

                if v is True:
                    label = form.fields[k].label

                    if 'mhc_predictor' not in fc:
                        fc['mhc_predictor'] = list()
                    fc['mhc_predictor'].append(label)

        document = ImmunoPeptidomeDocument(
            immunopeptidome=immunopeptidome,
            created_by=self.request.user,
            path='',
            name='',
            json_values=json.dumps(fc),
            comment=None,
            git_sha=os.environ['GIT_SHA'],
            state='pending',
            trashed=False)
        document.save()

        tasks_queue = list()
        tasks_queue.append(tasks.annotate_start.s(
            pk, pp_url, document_pk=document.pk))

        if pp != '---------':
            tasks_queue.extend([
                tasks.peptides_snps_submit.si(pk, pp_url,
                                             document_pk=document.pk),
                tasks.peptides_snps_process.s(document_pk=document.pk),
            ])

        if form.cleaned_data['format'] == 'csv':

            msms_url = self.request.build_absolute_uri(reverse('msms:view',
                                                               args=[999]))

            task = tasks.immunopeptidome_csv.si(pk, pp_url, msms_url,
                                                document_pk=document.pk)
            tasks_queue.append(task)

            if pp != '---------':
                task = tasks.peptide_locus_count.si(document_pk=document.pk)
                tasks_queue.append(task)

            annotation_tasks = tasks.annotation_tasks

            # Annotation
            mhc_predictors = list()
            for k, v in form.cleaned_data.items():

                if k.startswith('annotation') and v is True:
                    _, type_, pk = k.split('_')

                    task = annotation_tasks[type_]
                    task = task.si(pk, document_pk=document.pk)
                    tasks_queue.append(task)

                elif k.startswith('mhc_predictor_') and v is True:
                    _, _, predictor = k.split('_', maxsplit=2)
                    mhc_predictors.append(predictor)

            if len(mhc_predictors):
                task = tasks.mhc_predictions.si(mhc_predictors, document_pk=document.pk)
                tasks_queue.append(task)

            task = tasks.processing_completed.si(document_pk=document.pk)
            tasks_queue.append(task)

            # MHC prediction

        elif form.cleaned_data['format'] == 'bed':
            task = tasks.immunopeptidome_bed.si(pk, pp_url,
                                                document_pk=document.pk)
            tasks_queue.append(task)

        chain(*tasks_queue).delay()

        return redirect(immunopeptidome)

    def get_form_kwargs(self, **kwargs):
        """
        Gets keyword arguments for form
        :param kwargs: Keywords dictionary
        :return: Dictionary
        """

        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], self.request.user, 'change')

        bs_id = immunopeptidome.biological_sample.values_list('id', flat=True)
        pp_ids = PersonalizedProteome.objects.filter(
            snps__biological_sample__in=bs_id) \
            .values_list('pps_pp_id', flat=True)

        form_kwargs = super().get_form_kwargs(**kwargs)
        form_kwargs['personalized_proteome'] = list(pp_ids)

        return form_kwargs


@login_required
def document_download(request, pk, doc_pk):
    """
    View to download document
    :param request: HTTP request
    :param pk: Immunopeptidome primary key
    :param doc_pk: ImmunopeptidomeDocument primary key
    :return: HTTP response
    """

    ImmunoPeptidome.objects.get_for_user(pk, request.user, 'view')

    document = ImmunoPeptidomeDocument.objects.get(pk=doc_pk)

    url = document.path + document.name

    return server_secure_media(request, url, staff_only=False)


@method_decorator(login_required, name='dispatch')
class DocumentState(ImmunoPeptidomeMixin, TemplateView):
    """
    Provides state of the immunopeptidome document instance.
    """

    template_name = 'document_state.html'

    def get_context_data(self, **kwargs) -> dict:
        ImmunoPeptidome.objects.get_for_user(self.kwargs['pk'],
                                             self.request.user, 'view')

        document = ImmunoPeptidomeDocument.objects.get(pk=kwargs['doc_pk'])

        context = dict()
        context['obj'] = document

        return context


@method_decorator(login_required, name='dispatch')
class PeptideCount(ImmunoPeptidomeMixin, TemplateView):
    """
    Provides immunopeptidome peptide count
    """

    template_name = 'count.html'

    def get_context_data(self, **kwargs) -> dict:
        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], self.request.user, 'view')

        context = dict()
        context['peptide_count'] = immunopeptidome.peptide_count

        if immunopeptidome.comments == 'Unexpected failure':
            context['peptide_count'] = 'Unexpected failure'

        return context


@method_decorator(login_required, name='dispatch')
class Plots(ImmunoPeptidomeMixin, TemplateView):
    """
    Exploratory plots for immunopeptidome
    """

    icon = 'bar-chart'

    title = 'Exploratory plots'

    template_base = 'mhc_pipeline/peptides_plots/'

    template_name = 'plots.html'

    def get_context_data(self, **kwargs) -> dict:

        user = self.request.user

        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], user, 'view')

        if immunopeptidome.peptide_count == -1:
            raise BaseSiteException('error', 'Immunopeptidome not ready!')

        filter_options = {
            'immunopeptidome': [immunopeptidome.pk],
            'mhc_predictor': '',
        }

        if immunopeptidome.filter is not None:
            immunopeptidome_filter = json.loads(
                immunopeptidome.filter.json_values)
            filter_options['mhc_predictor'] = immunopeptidome_filter.get('mhc_predictor', '')

        filter_json_ = filter_json(filter_options)

        key = md5(filter_json_.encode('utf-8')).hexdigest() + str(user.pk)

        tasks.peptides_generate_plots.delay(key, filter_options)

        context = super().get_context_data(**kwargs)

        name = f'<b>Immunopeptidome id: </b> {immunopeptidome.pk} - ' \
            f'{immunopeptidome.name}<br />'

        context['name'] = name
        context['key'] = key
        context['bokeh_version'] = bokeh.__version__

        return context


@login_required
def document_trash(request, pk, doc_pk):
    """
    View to trash document
    :param request: HTTP request
    :param pk: Immunopeptidome primary key
    :param doc_pk: ImmunopeptidomeDocument primary key
    :return: HTTP response
    """

    immunopeptidome = ImmunoPeptidome.objects.get_for_user(pk, request.user,
                                                           'change')

    ImmunoPeptidomeDocument.objects.filter(pk=doc_pk).update(trashed=True)

    return redirect(immunopeptidome)


@method_decorator(login_required, name='dispatch')
class MiHAFilter(ImmunoPeptidomeMixin, FormView):
    """
    View to filter MiHA from annotated immunopeptidome documents
    """

    action = 'Filter MiHA peptides'

    column_width = 12

    form_class = MiHAFilterForm

    template_name = 'miha_filter.html'

    def get_form_kwargs(self, **kwargs):
        """
        Gets keyword arguments for form
        :param kwargs: Keywords dictionary
        :return: Dictionary
        """

        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], self.request.user, 'change')

        documents = ImmunoPeptidomeDocument.objects.filter(
            immunopeptidome=immunopeptidome, trashed=False, state='success'
        ).order_by('-pk')

        documents_choices = list()

        for document in documents:

            values = json.loads(document.json_values)

            if 'annotation' in values:
                choice = (document.pk, f'Id: {document.pk}')

                documents_choices.append(choice)

        form_kwargs = super().get_form_kwargs(**kwargs)
        form_kwargs['documents'] = list(documents_choices)

        return form_kwargs

    def form_valid(self, form):

        filter_options = form.cleaned_data

        # Check permission

        immunopeptidome = ImmunoPeptidome.objects.get_for_user(
            self.kwargs['pk'], self.request.user, 'change')

        # Create new document
        document = ImmunoPeptidomeDocument.objects.get(
            pk=filter_options['document'])

        old_values = json.loads(document.json_values)
        old_values.update(filter_options)
        del (old_values['immunopeptidome_name'])
        new_values = json.dumps(old_values)

        if filter_options.get('immunopeptidome_name') != '':
            immunopeptidome = ImmunoPeptidome.objects.create(
                created_by=self.request.user,
                modified_by=self.request.user,
                name=filter_options.get('immunopeptidome_name'),
                peptide_count=-1
            )

            filter_options['filter_name'] = f'Immunopeptidome id: ' \
                f'{immunopeptidome.pk}'
            immunopeptidome.filter = create_filter(filter_options,
                                                   self.request.user)
            immunopeptidome.save()
            tag = Tag.objects.get(name='MiHA')
            immunopeptidome.tags.add(tag)

            set_content_permission(immunopeptidome, self.request.user,
                                   permissions=['change', 'delete', 'view'])

        document.pk = None
        document.id = None
        document.immunopeptidome = immunopeptidome
        document.json_values = new_values
        document.state = 'pending'
        document.save()

        document.name = document.name.replace(
            f'doc_id_{filter_options["document"]}',
            f'doc_id_{document.pk}',
        )
        document.save()

        tasks.filter_miha.apply_async(args=(filter_options,),
                                      kwargs={'document_pk': document.pk},
                                      countdown=1)

        return redirect(immunopeptidome)
