"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
from collections import defaultdict
import csv
import json
from pathlib import Path
import re

# Import Django related libraries
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

# Import third party libraries

# Import project libraries
from ..library.bulk_insert import BulkInsert
from ..library import errors
from ..library.lock_model import ModelLocker
from library.apl_parser import APLParser
from library.ms import measurement_error_ppm
from ..models import (Identification,
                      Peptide,
                      ProteinDatabase,
                      UniquePeptide)

MOD_MASSES = {
    'Acetyl': '42.010564',
    'Deamidation': '0.984015',
    'Oxidation': '15.994915',
    'Phospho': '79.966330',
}


def check_files(fs):
    """
    Checks that expected files are provided and exists.
    :param fs: FileSet
    :return: Dictionary with File object
    """

    files = dict()
    file_names = ['evidence.txt', 'parameters.txt', 'peptides.txt']

    for name in file_names:

        try:
            files[name] = fs.file_set.get(name=name)
            file_name = files[name].full_path()

            if Path(file_name).exists() is False:
                errors.process('file_missing', file_name, log=True)

        except ObjectDoesNotExist:
            errors.process('fileset_missing', name, log=True)

    return files


def read_apl_scans(base_path):
    """
    Reads MS/MS scan from MaxQuant APL files.
    :param base_path: String path of txt folder.
    :return: Dictionary
    """

    apl_scans = defaultdict(dict)

    apl_path = Path(base_path) / '..' / 'andromeda'
    apl_path = apl_path.resolve()

    for apl_file in apl_path.glob('*.apl'):
        apl_parser = APLParser(apl_file)
        apl_parser.get_scans_dict(remove_fields=['header', 'peak_list'],
                                  apl_scans=apl_scans)

    return apl_scans


def read_modifications(path, parameters):
    """
    Reads MaxQuant site tables.
    :param path: Path object
    :param parameters: Dictionary with MaxQuant parameters
    :return: Dictionary
    """

    modifications = defaultdict(list)

    for file in parameters['Site tables'].split(';'):

        modification_name = re.sub(' \(.+\)Sites.txt', '', file)

        with open(Path(path) / file) as f:

            reader = csv.DictReader(f, delimiter='\t')

            for row in reader:

                modification = {
                    'residues': [row['Amino acid'], ],
                    'name': modification_name,
                    'location': int(row['Position in peptide']),
                    'monoisotopicMassDelta': MOD_MASSES.get(modification_name,
                                                            '')
                }

                for peptide_id in row['Peptide IDs'].split(';'):
                    modifications[peptide_id].append(modification)

    return modifications


def read_parameters(file_name):
    """
    Reads MaxQuant parameters.txt file
    :param file_name: String
    :return: Dictionary
    """

    parameters = dict()

    with open(file_name, 'r') as f:
        for row in f:
            fields = row.rstrip().split('\t')

            if len(fields) == 2:
                parameters[fields[0]] = fields[1]

    return parameters


def read_peptides_proteins(file_name):
    """
    Reads MaxQuant peptides.txt, extracts proteins and peptide positions.
    :param file_name: Full path string.
    :return:
    """

    peptides_proteins = dict()

    with open(file_name) as f:
        reader = csv.DictReader(f, delimiter='\t')

        for row in reader:

            if row['Proteins'] == '':
                peptides_proteins[row['id']] = ''

            else:
                proteins = row['Proteins'].split(';')
                proteins[0] = '%s[%s,%s]' % (proteins[0], row['Start position'],
                                             row['End position'])

                peptides_proteins[row['id']] = ';'.join(proteins)

    return peptides_proteins


def import_peptides(identification: Identification, lock_trial: int,
                    lock_trial_delay: int) -> None:
    """
    Stores peptides identified by MaxQuant.
    :param identification: Identification instance
    :param lock_trial: Number of time to try to acquire db lock
    :param lock_trial_delay: Seconds between db lock trial
    :return: None
    """

    files = check_files(identification.file_set)
    identification.software = 'Maxquant'

    parameters = None

    try:
        parameters = read_parameters(files['parameters.txt'].full_path())
    except (AttributeError, KeyError, TypeError, ValueError):
        errors.process('parsing', 'parameters.txt', log=True)

    peptides_proteins = None

    try:
        peptides_proteins = read_peptides_proteins(files['peptides.txt'].full_path())
    except (AttributeError, KeyError, TypeError, ValueError):
        errors.process('parsing', 'peptides.txt', log=True)

    modifications = None
    try:
        modifications = read_modifications(files['evidence.txt'].path,
                                           parameters)
    except (AttributeError, KeyError, TypeError, ValueError):
        errors.process('parsing', 'evidence.txt', log=True)

    try:
        apl_scans = read_apl_scans(files['evidence.txt'].path)
    except (AttributeError, KeyError, TypeError, ValueError):
        errors.process('parsing', 'APL files', log=True)

    protein_database, created = ProteinDatabase.objects.get_or_create(
        name=Path(parameters['Fasta file']).name,
        file_format='FASTA format',
        location=parameters['Fasta file'],
        num_sequences=0,
    )

    identification.protein_database.add(protein_database)

    # Prepare bulk insert
    unique_peptides_bulk = BulkInsert(UniquePeptide, ['peptide_sequence'])
    peptides_bulk = BulkInsert(Peptide)

    unique_peptides = set()

    try:

        # Extract peptides
        with open(files['evidence.txt'].full_path()) as f:

            reader = csv.DictReader(f, delimiter='\t')

            # Fix key name
            scan_number_key = 'MS/MS Scan Number'
            ppm_key = 'Mass Error [ppm]'

            if 'MS/MS scan number' in reader.fieldnames:
                scan_number_key = 'MS/MS scan number'

            if 'Mass error [ppm]' in reader.fieldnames:
                ppm_key = 'Mass error [ppm]'

            for row in reader:

                # Skip matched between run entries
                if row['Type'] == 'MULTI-MATCH':
                    continue

                scan = dict()

                scan['identification'] = identification
                scan['scan_number'] = int(row[scan_number_key])

                scan['scan_file'] = row['Raw file']

                if row['Type'] == 'MULTI-SECPEP':
                    apl_scan = apl_scans[scan['scan_file'] + '_secpep'][row[scan_number_key]]
                else:
                    apl_scan = apl_scans[scan['scan_file']][row[scan_number_key]]

                scan['scan_file_location'] = apl_scan['apl_file']

                scan['is_decoy'] = False
                scan['one_is_decoy'] = False

                if row['Reverse'] == '+':
                    scan['is_decoy'] = True
                    scan['one_is_decoy'] = True

                scan['q_value'] = -1
                scan['peptide_score'] = float(row['Score'])
                scan['rank'] = 1
                scan['rank_total'] = 1
                scan['charge_state'] = int(row['Charge'])
                scan['calculated_mz'] = (float(row['Mass']) + scan['charge_state'] * 1.007276) / scan['charge_state']
                scan['experimental_mz'] = float(row['m/z'])

                if row[ppm_key] != 'NaN':
                    scan['error_ppm'] = float(row[ppm_key])
                else:
                    scan['error_ppm'] = measurement_error_ppm(scan['experimental_mz'],
                                                              scan['calculated_mz'])

                scan['peptide_sequence'] = row['Sequence']
                scan['peptide_length'] = row['Length']

                scan['modification'] = json.dumps(modifications.get(row['Peptide ID'], ''))

                scan['protein_accessions_positions'] = peptides_proteins[row['Peptide ID']]

                scan['protein_count'] = row['Proteins'].count(';') + 1

                # scan['protein_matches'] = None # Not implemented

                # Unique peptide
                unique_peptides.add(scan['peptide_sequence'])

                scan['retention_time'] = float(row['Retention time'])

                scan['scan_file_index'] = apl_scan['scan_file_index']

                scan['peak_count'] = apl_scan['peak_count']
                scan['total_ion_current'] = apl_scan['total_ion_current']

                peptides_bulk.to_create_list.append(scan)

        # Process unique peptides
        chunk_size = 999
        unique_peptides_list = list(unique_peptides)
        for i in range(0, len(unique_peptides_list), chunk_size):
            chunk = unique_peptides_list[i:i+chunk_size]
            unique_peptides_bulk.unique_queryset = UniquePeptide.objects.filter(
                peptide_sequence__in=chunk)
            unique_peptides_bulk.set_unique_entries_dict()

        # Add keys
        with ModelLocker(['peptide', 'uniquepeptide', 'proteinSequence'],
                         lock_trial, lock_trial_delay):

            for index, peptide in enumerate(peptides_bulk.iterate_create_pk()):

                fields = {
                    'peptide_sequence': peptide.peptide_sequence,
                    'peptide_length': peptide.peptide_length
                }
                id_ = unique_peptides_bulk.get_create_unique_pk(fields)
                peptide.unique_peptide_id = id_

            # Bulk store
            unique_peptides_bulk.store()
            peptides_bulk.store()

        identification.state = 'success'

    except (AttributeError, KeyError, TypeError, ValueError):

        errors.process('parsing', 'evidence.txt', log=True)

    except IntegrityError:

        Peptide.objects.filter(identification_id=identification.pk).delete()

        errors.process('database_integrity', '', log=True)
