"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json
import os
from urllib.request import unquote

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.core.cache import caches
from django.http.response import HttpResponse
from django.shortcuts import render
from django.utils import timezone

# Third party libraries

# Import project libraries
from library.apl_parser import APLParser
from library.mgf_parser import MGFParser
from ..models import Peptide
from mhc_pipeline.library import errors
from mhc_pipeline.library.excel_login import excel_login_required
from mhc_pipeline.library.permission import check_content_permission

# Dictionary of peptide modifications with associated symbol
MODIFICATION_SYMBOL = {
    'Carbamidomethyl': 'cm',
    'Cysteinyl': 'cy',
    'Deamidated': 'd',
    'Dimethyl': 'dme',
    'Methyl': 'me',
    'Oxidation': 'ox',
    'Phospho': 'ph',
    'TMT': 'tmtl',
    'TMT STY': 'tmtl',
    'TMT6plex': 'tmth',
    'TMT10plex': 'tmth',
}


def format_peptide_seq_with_mod(peptide):
    """
    Formats peptide sequence string with modification for spectrum viewer.
    :param peptide: Peptide object
    :return: Peptide sequence string with modification symbols.
    """

    modifications = json.loads(peptide.modification)

    residues = list(peptide.peptide_sequence)

    for modification in modifications:
        location = modification['location']

        if 'name' in modification:
            name = modification['name']
        else:
            name = modification['unknown modification']

        if location == 0:
            location = 1

        # Append modification symbol to residue
        residues[location - 1] += MODIFICATION_SYMBOL.get(name, 'unknown')

    return ''.join(residues)


def get_peptide(request, pk):
    """
    Retrieves Peptide and checks permission
    :param request: HttpRequest object
    :param pk: Integer
    :return: Peptide instance
    """

    related = 'identification__biological_sample'
    peptide = Peptide.objects.select_related(related).get(pk=pk)

    identification = peptide.identification

    check_content_permission(request, 'view', identification.biological_sample)

    return peptide


@login_required
def validation(request, pk, value):
    """
    This view updates validation status of a peptide spectrum match.
    :param request: HttpRequest object
    :param pk: Peptide primary key integer
    :param value: Validation character
    :return: HttpResponse object
    """

    peptide = get_peptide(request, pk)

    validated = False

    if (value in ['G', 'Y', 'R'] and
            request.user.groups.filter(name='validation').exists()):
        peptide.validated = value
        peptide.validated_by = request.user
        peptide.validated_at = timezone.now()
        peptide.save()

        validated = True

        validation_timestamp = peptide.validated_at.timestamp()
        caches['default'].set('validation_timestamp', validation_timestamp,
                            2678400)

    return HttpResponse(json.dumps(validated), content_type='application/json')


@excel_login_required
def view(request, pk):
    """
    This view displays PSM MS/MS spectrum.
    :param request: HttpRequest object
    :param pk: Peptide primary key integer
    :return: HttpResponse
    """

    peptide = get_peptide(request, pk)
    fs = peptide.identification.file_set

    if 'PEAKS' in peptide.identification.software:

        mgf_file = fs.file_set.get(extension='.mgf',
                                   name=unquote(peptide.scan_file))
        file_name = os.path.join(mgf_file.path, mgf_file.name)
        parser = MGFParser(file_name)

    else:
        file_name = unquote(peptide.scan_file_location)
        parser = APLParser(file_name)

    # Get peak list
    scan = None

    try:
        scan = parser.get_scan_at_index(peptide.scan_file_index)

    except FileNotFoundError:

        errors.process('file_missing', peptide.scan_file, log=True,
                       request=request)

    for i, value in enumerate(scan['peak_list']):
        scan['peak_list'][i] = f'{value[0]} {value[1]}'

    peak_list = '\n'.join(scan['peak_list'])

    # Check that user has validation permission
    validation_disabled = 'disabled'

    if request.user.groups.filter(name='validation').exists():
        validation_disabled = ''

    # Adjust fragment tolerance with search results
    fragment_tolerance = 10

    if peptide.identification.search_parameters is not None:

        search_parameters = json.loads(peptide.identification.search_parameters)
        fragment_tolerance = float(search_parameters['fragment_tolerance']) * 1e3

    context = {
        'fileset_name': fs.name,
        'myTolerance': fragment_tolerance,
        'myPrecursorZ': peptide.charge_state,
        'myPeptide': format_peptide_seq_with_mod(peptide),
        'myPeaklist': peak_list,
        'peptide': peptide,
        'validation_disabled': validation_disabled
    }

    return render(request,
                  'mhc_pipeline/msms/spectrum_viewer.html',
                  context)
