"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
import json
from pathlib import Path
from urllib.request import unquote

# Import Django related libraries
from django.core.exceptions import (MultipleObjectsReturned,
                                    ObjectDoesNotExist)
from django.db import IntegrityError

# Third parties
import xmltodict

# Import project libraries
from base_site.exceptions import BaseSiteException
from ..library.bulk_insert import BulkInsert
from ..library import errors
from ..library.lock_model import ModelLocker
from library.mgf_parser import MGFParser
from library.peaks_parser import PEAKSMzIdentML
from ..models import (BiologicalSampleFileSet,
                      FileSet,
                      Identification,
                      Peptide,
                      ProteinDatabase,
                      ProteinSequence,
                      UniquePeptide)


class ProteinDatabaseManager(object):
    """
    This class manages storage of protein databases and protein sequences.
    It also retrieves protein identifier
    """

    def __init__(self, identification: Identification, proteins_bulk):
        """
        Init
        :param identification: Identification instance
        :param proteins_bulk: Protein bulk storage object
        """

        self.databases = dict()
        self.identification = identification
        self.proteins_bulk = proteins_bulk

    def get_or_create_database(self, search_db_ref: dict) -> int:
        """
        Gets or create protein database entry
        :param search_db_ref: Database details
        :return: Database primary key
        """

        name = search_db_ref['name']

        database = self.databases.get(name, None)

        if database is None:
            database, created = ProteinDatabase.objects.get_or_create(
                name=name,
                file_format=search_db_ref['FileFormat'],
                location=search_db_ref['location'],
                num_sequences=search_db_ref['numDatabaseSequences'],
            )

            self.identification.protein_database.add(database)
            self.databases[name] = database

            # Retrieve existing protein entries
            self.proteins_bulk.unique_queryset = ProteinSequence.objects.filter(
                protein_database__id=database.pk)

        return database.pk

    def process_proteins(self, protein_matches_not_parsed: list) -> dict:
        """
        Generates protein sequence data
        :param protein_matches_not_parsed: PEAKS mzIdentML protein matches
        :return: Generator of ProteinSequence object
        """

        for protein in protein_matches_not_parsed:

            if 'peptideEvidence_ref' in protein:
                protein = protein['peptideEvidence_ref'][0]

            if protein is not None and not protein['isDecoy']:
                search_db_ref = \
                    protein['dBSequence_ref'][0]['searchDatabase_ref'][0]

                database_id = self.get_or_create_database(search_db_ref)

                protein_entry = protein['dBSequence_ref'][0]

                fields = {
                    'protein_database_id': database_id,
                    'accession': protein_entry['accession'],
                    'sequence': protein_entry['Seq'],
                    'length': protein_entry['length'],
                }

                if 'protein description' in protein_entry:
                    fields['short_description'] = \
                        protein_entry['protein description']

                yield fields


def check_files(fs: BiologicalSampleFileSet, check_mgf: bool = True) -> dict:
    """
    Checks that expected files are provided and exists.
    :param fs: FileSet
    :param check_mgf: Check mgf files
    :return: Dictionary with file objects
    """

    files = dict()

    files['mgf'] = fs.file_set.filter(extension='.mgf')

    if check_mgf:
        if len(files['mgf']) < 1:
            msg = 'Your File set should contain at least one MGF file.'

            raise BaseSiteException('error', msg)

        else:

            for mgf in files['mgf']:

                file_name = mgf.full_path()

                if Path(file_name).exists() is False:
                    errors.process('file_missing', file_name, log=True)

    try:

        files['mz_id'] = fs.file_set.get(extension='.mzid').full_path()

        if Path(files['mz_id']).exists() is False:
            errors.process('file_missing', files['mz_id'], log=True)

        # Check that we have mzIdentML 1.1 file generated by PEAKS
        if not PEAKSMzIdentML.check_peaks_mzident_1_1(files['mz_id']):
            errors.process('mzidentml_version', files['mz_id'], log=True)

    except MultipleObjectsReturned:
        errors.process('mzidentml_multi', None)

    except ObjectDoesNotExist:
        errors.process('fileset_missing', '.mzid file')

    return files


def read_mgf_files(mgf_files: FileSet) -> dict:
    """
    Reads scans from MGF file
    :param mgf_files: QuerySet of MGF files
    :return: Scans dictionary for each file
    """

    mgf_dict = dict()

    for mgf_file in mgf_files:
        mgf_parser = MGFParser(mgf_file.full_path())
        mgf_scans = mgf_parser.get_scans_dict(remove_fields=['CHARGE',
                                                             'peak_list',
                                                             'PEPMASS',
                                                             'TITLE'])
        mgf_dict[mgf_file.name] = mgf_scans

    return mgf_dict


def import_peptides(identification: Identification, lock_trial: int,
                    lock_trial_delay: int) -> None:
    """
    Imports PEAKS results
    :param identification: Identification instance
    :param lock_trial: Number of time to try to acquire db lock
    :param lock_trial_delay: Seconds between db lock trial
    :return: None
    """

    files = check_files(identification.file_set)
    version = PEAKSMzIdentML.get_version(files['mz_id'], full=True)
    identification.software = 'PEAKS ' + version

    search_parameters = get_search_parameters(files['mz_id'])
    identification.search_parameters = json.dumps(search_parameters)

    try:
        mgf_dict = read_mgf_files(files['mgf'])

    except (AttributeError, KeyError, TypeError, ValueError):
        errors.process('parsing', 'MGF files', log=True)

    try:
        # Prepare bulk insert
        proteins_bulk = BulkInsert(ProteinSequence, ['protein_database_id',
                                                     'accession'])
        peptides_bulk = BulkInsert(Peptide)
        through_model = Peptide.protein_matches.through
        pep_prot_bulk = BulkInsert(through_model)
        unique_peptides_bulk = BulkInsert(UniquePeptide, ['peptide_sequence'])

        db_manager = ProteinDatabaseManager(identification, proteins_bulk)

        unique_peptides = set()
        score_decoy = list()

        proteins_matches = list()

        for scan in PEAKSMzIdentML.read_scans(files['mz_id']):

            scan = scan.copy()

            scan['identification'] = identification

            try:
                mgf_scans = mgf_dict[unquote(scan['scan_file'])]

            except KeyError:
                errors.process('fileset_missing', scan["scan_file"])

            mgf_scan = mgf_scans[scan['spectrumID']]
            scan['scan_number'] = int(mgf_scan['SCANS'])
            scan['retention_time'] = float(mgf_scan['RTINSECONDS'])
            scan['scan_file_index'] = mgf_scan['scan_file_index']
            scan['peak_count'] = mgf_scan['peak_count']
            scan['total_ion_current'] = mgf_scan['total_ion_current']

            score_decoy.append((scan['peptide_score'], scan['is_decoy']))

            # Clean non necessary fields
            scan.pop('spectrumID')
            scan.pop('id')

            # Unique peptide
            unique_peptides.add(scan['peptide_sequence'])

            peptides_bulk.to_create_list.append(scan)
            proteins_matches.append(list(db_manager.process_proteins(
                scan.pop('protein_matches'))))

        score_q_value = PEAKSMzIdentML.get_q_values_dict(score_decoy)

        # Add keys
        with ModelLocker(['peptide', 'uniquepeptide', 'proteinSequence'],
                         lock_trial, lock_trial_delay):

            # Query unique peptides
            chunk_size = 999
            unique_peptides_list = list(unique_peptides)
            for i in range(0, len(unique_peptides_list), chunk_size):
                chunk = unique_peptides_list[i:i + chunk_size]
                unique_peptides_bulk.unique_queryset = \
                    UniquePeptide.objects.filter(peptide_sequence__in=chunk)
                unique_peptides_bulk.set_unique_entries_dict()

            proteins_bulk.set_unique_entries_dict()

            for index, peptide in enumerate(peptides_bulk.iterate_create_pk()):

                # Proteins
                protein_matches = proteins_matches[index]

                for match in protein_matches:
                    fields = {
                        'peptide_id': peptide.id,
                        'proteinsequence_id':
                            proteins_bulk.get_create_unique_pk(match)
                    }

                    pep_prot_bulk.get_create_pk(fields)

                # Unique peptides
                fields = {
                    'peptide_sequence': peptide.peptide_sequence,
                    'peptide_length': peptide.peptide_length
                }
                id_ = unique_peptides_bulk.get_create_unique_pk(fields)
                peptide.unique_peptide_id = id_

                peptide.q_value = score_q_value.get(peptide.peptide_score, None)

            # Bulk store
            proteins_bulk.store()
            unique_peptides_bulk.store()
            peptides_bulk.store()
            pep_prot_bulk.store()

        identification.state = 'success'

    except (AttributeError, KeyError, TypeError, ValueError):

        errors.process('parsing', 'mzIdentML file', log=True)

    except IntegrityError:

        Peptide.objects.filter(identification_id=identification.pk).delete()

        errors.process('database_integrity', '', log=True)


def get_search_parameters(file: str) -> dict:
    """
    Extracts XML section with parameters
    :param file: mzIdentML file path
    :return: Search parameters
    """

    lines = list()

    # Extract XML
    with Path(file).open() as f:

        line = f.readline()

        while 'SpectrumIdentificationProtocol' not in line:
            line = f.readline()

        lines.append(line)
        line = f.readline()
        lines.append(line)

        while 'SpectrumIdentificationProtocol' not in line:
            line = f.readline()
            lines.append(line)

    xml = ''.join(lines)

    # Convert to dict
    search_parameters = xmltodict.parse(xml)['SpectrumIdentificationProtocol']

    fragment_tolerance = search_parameters['FragmentTolerance']['cvParam'][0][
        '@value']
    fragment_tolerance_unit = \
        search_parameters['FragmentTolerance']['cvParam'][0]['@unitName']
    parent_tolerance = search_parameters['ParentTolerance']['cvParam'][0][
        '@value']
    parent_tolerance_unit = search_parameters['ParentTolerance']['cvParam'][0][
        '@unitName']

    values = {

        'fragment_tolerance': fragment_tolerance,
        'fragment_tolerance_unit': fragment_tolerance_unit,
        'parent_tolerance': parent_tolerance,
        'parent_tolerance_unit': parent_tolerance_unit,
        'revision': get_search_parameters.revision,

    }

    # Modifications
    modifications = list()

    mod_type = {
        'true': 'fixed',
        'false': 'variable'
    }

    try:
        for modification in search_parameters['ModificationParams'][
                'SearchModification']:
            name = modification['cvParam']['@name']
            mass_delta = modification['@massDelta']
            residues = modification['@residues']
            fixed = mod_type[modification['@fixedMod']]

            if 'SpecificityRules' in modification:
                residues = modification['SpecificityRules']['cvParam']['@name']
                residues = residues.replace('modification specificity ', '')

            modifications.append(
                {
                    'name': name,
                    'fixed': fixed,
                    'mass_delta': mass_delta,
                    'residues': residues,
                }
            )

            values['modifications'] = modifications
    except KeyError:
        pass

    # Enzyme
    enzyme = search_parameters['Enzymes']['Enzyme']

    try:
        enzyme_name = enzyme['EnzymeName']['cvParam']['@name']
        values['enzyme_name'] = enzyme_name
    except KeyError:
        pass

    try:
        missed_cleavages = enzyme['@missedCleavages']
        values['missed_cleavages'] = missed_cleavages
    except KeyError:
        pass

    try:
        semi_specific = enzyme['@semiSpecific']
        values['semi_specific'] = semi_specific
    except KeyError:
        pass

    try:
        enzyme_regexp = enzyme['SiteRegexp']
        values['enzyme_regexp'] = enzyme_regexp
    except KeyError:
        pass

    return values


get_search_parameters.revision = 1
