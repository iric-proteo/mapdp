"""
Copyright 2015-2021 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Import standard libraries
import json
from math import floor

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

# Third party libraries
import bokeh
from bokeh.embed import json_item
from bokeh.models import ColumnDataSource, SingleIntervalTicker
from bokeh.palettes import viridis
from bokeh.plotting import figure
import numpy as np
import pandas as pd

# Import project libraries
from base_site.exceptions import BaseSiteException
from ..library.peptides_filter import PeptidesFilter
from .view_mixin import TemplateMixin

axis_label_text_font_size = '14pt'
major_label_text_font_size = '12pt'
height = 400
width = 400
CACHE_EXPIRATION = 2678400  # One month
revision = '1e'


def generate_plots(key: str, filter_options: dict) -> None:
    """
    Queries peptides, generates plots, caches json plots and csv
    data
    :param key: Key
    :param filter_options: Peptide filter options
    :return: None, cached json plots and csv data
    """

    task_key = key + f'_{revision}_plots_status'

    try:
        state = cache.get(task_key)

        if state is None or state == 'failed':

            cache.set(task_key, 'started', CACHE_EXPIRATION)

            unique_peptides, data = get_unique_peptides(filter_options)

            for plot_name, plot_function in plots.items():

                plot, df = plot_function(unique_peptides, data)

                plot_json = json.dumps(json_item(plot))
                csv = df.to_csv(index=False)

                cache_key = key + f'_plot_{plot_name}'
                cache.set(cache_key, plot_json, CACHE_EXPIRATION)
                cache.set(cache_key + '_csv', csv, CACHE_EXPIRATION)

            cache.set(task_key, 'success', CACHE_EXPIRATION)

    except BaseSiteException as e:
        cache.set(task_key, 'failed:' + e.message, CACHE_EXPIRATION)

    except Exception:
        cache.set(task_key, 'failed', CACHE_EXPIRATION)
        raise


def get_unique_peptides(filter_options: dict):
    """
    Queries unique peptides
    :param filter_options: Peptide filter options
    :return: Unique peptides and data
    """

    data = dict()

    peptide_filters = PeptidesFilter(filter_options, data=data)
    unique_peptides = peptide_filters.apply()

    return unique_peptides, data


@login_required
def get_plot(request, key: str, name: str) -> HttpResponse:
    """
    Retrieves Bokeh plot for peptides
    :param request: HttpRequest
    :param key: Key
    :param name: Plot name
    :return: Bokeh plot in JSON format
    """

    if not key.endswith(str(request.user.pk)):
        raise PermissionDenied

    plot_key = key + f'_plot_{name}'
    content = cache.get(plot_key)

    response = HttpResponse(content, content_type='application/json')

    return response


@login_required
def plot_data(request, key: str, name: str) -> HttpResponse:
    """
    Outputs plot data points to CSV
    :param request: HttpRequest
    :param key: Key
    :param name: Plot name
    :return: CSV file
    """

    if not key.endswith(str(request.user.pk)):
        raise PermissionDenied

    csv_key = key + f'_plot_{name}_csv'
    content = cache.get(csv_key)

    file_name = f'peptides_plot_{name}_{key}.csv'
    response = HttpResponse(content, content_type='text/csv')

    response['Content-Disposition'] = f'attachment; filename={file_name}'

    return response


class PeptidesPlotsMixin(TemplateMixin):
    """
    Mixin for Peptides plots views
    """

    column_width = 6

    icon = 'barcode'

    title = 'Peptides exploratory plots'

    title_suffix = ''

    template_base = 'mhc_pipeline/peptides_plots/'


@method_decorator(login_required, name='dispatch')
class Plots(PeptidesPlotsMixin, TemplateView):
    """
    Exploratory plots for peptides
    """

    icon = 'bar-chart'

    title = 'Exploratory plots'

    template_name = 'plots.html'

    @staticmethod
    def clean_filter(fc):
        fc_copy = fc.copy()

        # Remove fields that should not be saved in the filter
        fields = ['mapdp_git', 'filter_json', 'filter_name',
                  'identification_key',
                  'immunopeptidome_name', 'selected_filter', 'url',
                  'timestamp', 'ref_url', 'validation_timestamp'
                  ]

        for field in fields:
            fc_copy.pop(field, None)

        # Remove empty values
        exclude_list = [None, '', False, []]
        fc_copy = {key: value for key, value in fc_copy.items() if
                   value not in exclude_list}

        # Converts MHC allele
        if 'mhc_allele' in fc_copy:
            fc_copy['mhc_allele'] = '|' + '|'.join(fc_copy['mhc_allele']) + '|'

        return fc_copy

    def get_context_data(self, **kwargs) -> dict:

        if not kwargs['key'].endswith(str(self.request.user.pk)):
            raise PermissionDenied

        context = super().get_context_data(**kwargs)
        context['name'] = kwargs.get('name', '')
        context['key'] = kwargs['key']
        context['bokeh_version'] = bokeh.__version__

        if 'filter_options' in kwargs:
            context['filter_options'] = self.clean_filter(
                kwargs['filter_options'])

        return context


@method_decorator(login_required, name='dispatch')
class State(PeptidesPlotsMixin, TemplateView):
    """
    Provides state of the plots.
    """

    template_name = 'plots_state.html'

    def get_context_data(self, **kwargs) -> dict:
        task_key = f'{kwargs["key"]}_{revision}_plots_status'

        state = cache.get(task_key)

        if state is None:
            state = 'pending'

        context = dict()
        context['state'] = state
        context['key'] = kwargs['key']
        context['plots'] = ['allele_fraction', 'allele_binding', 'length',
                            'score', 'error_ppm', 'retention_time', 'mz']

        return context


def base_plot(tools: str = 'reset,save,xpan,xwheel_zoom', **kwargs):
    """
    Plot figure with preset parameters
    :param tools: List of tools in toolbar
    :param kwargs:
    :return:
    """

    plot = figure(height=height, width=width,
                  output_backend='svg', tools=tools, **kwargs)

    plot.axis.axis_label_text_font_size = axis_label_text_font_size
    plot.axis.major_label_text_font_size = major_label_text_font_size
    plot.grid.grid_line_color = None
    plot.toolbar.logo = None

    return plot


def allele_fraction(peptides, data: dict):
    """
    MHC peptide allele distribution
    :param peptides: Peptides
    :param data: Extra peptide data
    :return: Bokeh plot and data frame
    """

    # Data
    try:
        counts = data['mhc_predictor']['affinity']['Minimum allele'].value_counts()
    except KeyError:
        plot = figure(title='MHC predictions not available!',
                      height=height, width=width)
        plot.circle([1], [1], alpha=0)

        return plot, pd.DataFrame()

    counts = counts.reset_index(name='counts').rename(
        columns={'index': 'allele'})
    df = pd.DataFrame(counts)
    df['pct'] = df['counts'] / df['counts'].sum() * 100
    df['pct'] = df['pct'].round(decimals=1)
    df = df.sort_values('counts', ascending=True)

    # Bokeh plot
    plot = base_plot(tools='save',
                     tooltips="@pct{1.1} % (@counts)",
                     x_axis_label='Fraction (%)',
                     x_range=(0, df['pct'].max() * 1.1),
                     y_axis_label='Allele',
                     y_range=list(df['allele'].unique()))

    plot.hbar(left=0, right='pct', y='allele',
              height=0.5, source=ColumnDataSource(df))

    return plot, df


def allele_binding(peptides, data: dict):
    """
    MHC peptide allele distribution
    :param peptides: Peptides
    :param data: Extra peptide data
    :return: Bokeh plot and data frame
    """

    if 'mhc_predictor' not in data:

        plot = figure(title='MHC predictions not available!',
                      height=height, width=width)
        plot.circle([1], [1], alpha=0)

        return plot, pd.DataFrame()

    affinities = data['mhc_predictor']['affinity']
    values = np.log10(affinities['Minimum'])
    binding_max = max(values)

    alleles = list(data['mhc_predictor']['affinity'].columns)
    alleles = [x for x in alleles if 'Minimum allele' not in x]

    hists = list()

    max_counts = 0

    for allele in alleles:

        if allele != 'Minimum':
            rows = affinities[affinities['Minimum allele'] == allele]
        else:
            rows = affinities
        binding_scores = np.log10(rows['Minimum'])
        #binding_scores = np.log10(affinities[allele])
        hist, edges = np.histogram(binding_scores,
                                   bins=np.arange(1,
                                                  binding_max + 0.2, 0.1))
        hists.append(hist)

        max_counts = max(max_counts, max(hist))

    # Data
    columns = ['affinity'] + alleles
    values = [edges[:-1]] + hists

    df = pd.DataFrame({k: v for k, v in zip(columns, values)})
    df['affinity'] = df['affinity'].apply(lambda x: '{0:.1f}'.format(x))

    # Bokeh plot
    plot = base_plot(tools='box_zoom,reset,save,xpan,xwheel_zoom',
                     tooltips="@left{1.1} binding affinity (nM): @top counts",
                     x_axis_label='log10 MHC binding affinity (nM)',
                     x_range=(1, 7),
                     y_axis_label='Counts',
                     y_range=(0, max_counts * 1.1))

    colors = viridis(len(alleles))

    for i, hist in enumerate(hists):
        color = colors[i]
        plot.step(edges[:-1], hist, line_color=color, line_width=2,
                  legend=alleles[i].replace('HLA-', ''))

    plot.legend.location = "top_right"
    plot.legend.click_policy = "hide"

    return plot, df


def error_ppm(peptides, data):
    """
    Peptide error ppm distribution
    :param peptides: Peptides
    :param data: Extra peptide data
    :return: Data frame or Bokeh plot
    """

    # Data
    values = [peptide.error_ppm for peptide in peptides]
    max_values = pd.Series(values).abs().max().astype(int)
    max_values = int(max_values) + 1
    hist, edges = np.histogram(values, bins=np.arange(max_values//-1,
                                                      max_values + 1, 0.5))

    df = pd.DataFrame({'peptide_mass_error_ppm': edges[:-1], 'counts': hist})

    # Bokeh plot
    plot = base_plot(tooltips="@left{1.1} ppm: @top counts",
                     x_axis_label='Mass error (ppm)',
                     y_axis_label='Counts',
                     y_range=(0, max(hist)))

    plot.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:],
              alpha=0.4)

    return plot, df


def length(peptides, data):
    """
    Peptide length distribution
    :param peptides: Peptides
    :param data: Extra peptide data
    :return: Bokeh plot and data frame
    """

    # Data
    lengths = [peptide.peptide_length for peptide in peptides]
    hist, edges = np.histogram(lengths, bins=range(1, max(lengths) + 2, 1))

    df = pd.DataFrame({'peptide_length': edges[:-1], 'counts': hist})

    # Bokeh plot
    plot = base_plot(tooltips="Length @left: @top counts",
                     x_axis_label='Length (residue)',
                     x_range=(min(lengths), max(lengths) + 1),
                     y_axis_label='Counts',
                     y_range=(0, max(hist)))

    plot.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:], alpha=0.4)

    if max(lengths) - min(lengths) < 8:
        plot.xaxis[0].ticker = SingleIntervalTicker(interval=1)

    return plot, df


def mz(peptides, data):
    """
    Peptide mz distribution
    :param peptides: Peptides
    :param data: Extra peptide data
    :return: Bokeh plot and data frame
    """

    # Data
    values = [peptide.experimental_mz for peptide in peptides]
    hist, edges = np.histogram(values, bins=range(int(min(values)),
                                                  int(max(values)) + 50, 25))

    df = pd.DataFrame({'experimental_mz': edges[:-1], 'counts': hist})

    # Bokeh plot
    plot = base_plot(tooltips="@left: @top counts",
                     x_axis_label='m/z',
                     y_axis_label='Counts',
                     y_range=(0, max(hist)))

    plot.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:], alpha=0.4)

    return plot, df


def retention_time(peptides, data):
    """
    Peptide retention time distribution
    :param peptides: Peptides
    :param data: Extra peptide data
    :return: Bokeh plot and data frame
    """

    # Data
    values = [peptide.retention_time / 60 for peptide in peptides]
    hist, edges = np.histogram(values, bins=range(0, int(max(values)) + 2, 1))

    df = pd.DataFrame({'peptide_retention_time_minute': edges[:-1],
                       'counts': hist})

    # Bokeh plot
    plot = base_plot(tooltips="@left minute: @top counts",
                     x_axis_label='Retention time (minute)',
                     y_axis_label='Counts',
                     y_range=(0, max(hist)))

    plot.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:], alpha=0.4)

    return plot, df


def score(peptides, data):
    """
    Peptide score distribution
    :param peptides: Peptides
    :param data: Extra peptide data
    :return: Bokeh plot and data frame
    """

    # Data
    values = [peptide.peptide_score for peptide in peptides]
    hist, edges = np.histogram(values, bins=range(floor(min(values)),
                                                  int(max(values)) + 1, 2))

    df = pd.DataFrame({'peptide_score': edges[:-1], 'counts': hist})

    # Bokeh plot
    plot = base_plot(tooltips="Score @left: @top counts",
                     x_axis_label='Score',
                     y_axis_label='Counts',
                     y_range=(0, max(hist, default=0)))

    plot.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:], alpha=0.4)

    return plot, df


plots = {
    'allele_fraction': allele_fraction,
    'allele_binding': allele_binding,
    'error_ppm': error_ppm,
    'length': length,
    'mz': mz,
    'retention_time': retention_time,
    'score': score,
}
