"""
Copyright 2015-2019 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from typing import Tuple

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from guardian.shortcuts import (assign_perm,
                                get_perms,
                                get_groups_with_perms,
                                get_users_with_perms,
                                remove_perm)

# Import project libraries
from ..forms import (PermissionGroupForm,
                     PermissionUpdateForm,
                     PermissionUserForm)
from ..library.permission import get_obj_by_type
from .view_mixin import TemplateMixin

# Create your views here.

permission_classes = ['view', 'change', 'delete']


class DisplayPermissions(object):
    """
    This class creates and object to display instance permissions.
    """

    def __init__(self, name: str, pk: int, permissions: list) -> None:
        """
        Initializes
        :param name: Name of the user or group
        :param pk: Primary key of the user or group
        :param permissions: Permissions
        """

        self.name = name
        self.pk = pk
        self.set(permissions)

    @staticmethod
    def get(obj) -> Tuple[list, list]:
        """
        Retrieves user and group permissions associated to the instance.
        :return: Two lists with permissions for users and group
        """

        users_perms = list()

        for user, permissions in get_users_with_perms(
                obj, attach_perms=True).items():

            if user.is_active:
                users_perms.append(DisplayPermissions(user.username, user.pk,
                                                      permissions))

        users_perms = sorted(users_perms, key=lambda x: x.name)

        groups_perms = list()

        for group, permissions in get_groups_with_perms(
                obj, attach_perms=True).items():
            groups_perms.append(DisplayPermissions(group.name, group.pk,
                                                   permissions))

        groups_perms = sorted(groups_perms, key=lambda x: x.name)

        return users_perms, groups_perms

    def set(self, permissions: list) -> None:
        """
        Sets display permissions to the object as attributes
        :param permissions: Permissions
        :return: Sets permission attributes
        """

        for x in permission_classes:
            self.__dict__[x] = False

        for permission in permissions:
            action, content_type = permission.split('_')
            self.__dict__[action] = True


def merge_bs_permission(context1, context2):
    """
    Merges permissions from permission context from two biological samples

    Permissions must agreed between samples

    :param context1: Permission context
    :param context2: Permission context
    :return: Merged permission context
    """

    context1['edit_perm'] = context1['edit_perm'] and context2['edit_perm']

    for type_ in ['users', 'groups']:

        perms1 = dict()

        for perm in context1[type_ + '_perms']:

            perms1[perm.pk] = perm

        perms2 = dict()
        for perm in context2[type_ + '_perms']:

            perms2[perm.pk] = perm

        perms = list()

        for key in set(perms1.keys()).intersection(set(perms2.keys())):

            perms1[key].view = perms1[key].view and perms2[key].view
            perms1[key].change = perms1[key].change and perms2[key].change
            perms1[key].delete = False
            perms.append(perms1[key])

        context1[type_ + '_perms'] = perms

    return context1


def merge_im_bs_permission(context1, context2):
    """
    Merges permissions from permission context from an immunopeptidome and
    a biological sample.

    Permissions any True is kept. Delete is not transfered from biological
    samples.

    :param context1: Immunopeptidome permission context
    :param context2: Biological sample permission context
    :return: Merged permission context
    """

    context1['edit_perm'] = context1['edit_perm'] or context2['edit_perm']

    for type_ in ['users', 'groups']:

        perms = dict()

        for perm in context1[type_ + '_perms']:

            perms[perm.pk] = perm

        for perm in context2[type_ + '_perms']:

            if perm.pk in perms:
                perms[perm.pk].view = perms[perm.pk].view or perm.view
                perms[perm.pk].change = perms[perm.pk].change or perm.change
            else:
                perms[perm.pk] = perm
                perms[perm.pk].delete = False

        context1[type_ + '_perms'] = perms.values()

    return context1


def permission_context(obj, content_type, user) -> dict:
    """
    Prepares context dictionary for permissions display
    :param obj: Object with permissions
    :param content_type: Content type
    :param user: User
    :return: Context dictionary
    """

    users_perms, groups_perms = DisplayPermissions.get(obj)

    context = {
        'obj': obj,
        'edit_perm': user.has_perm(f'change_{obj.content_type}', obj),
        'trash_perm': user.has_perm(f'delete_{obj.content_type}', obj),
        'users_perms': users_perms,
        'groups_perms': groups_perms,
    }

    if content_type == 'immunopeptidome':

        # Merge permissions from Biological samples

        merged_bs_context = None

        for biological_sample in obj.biological_sample.all():
            setattr(biological_sample, 'content_type', 'biologicalsample')
            bs_context = permission_context(biological_sample,
                                            'biologicalsample',
                                            user)
            if merged_bs_context is None:
                merged_bs_context = bs_context
            else:
                merged_bs_context = merge_bs_permission(merged_bs_context,
                                                        bs_context)

        if merged_bs_context is not None:
            context = merge_im_bs_permission(context, merged_bs_context)

        for type_ in ['users', 'groups']:
            context[type_ + '_perms'] = sorted(context[type_ + '_perms'],
                                               key=lambda x: x.name)

    return context


@login_required
def update(request, subject_type: str, subject_pk: int, obj_model: str,
           obj_pk: int):
    """
    Updates permission of an object.
    :param request: HttpRequest()
    :param subject_type: Content type (user or group)
    :param subject_pk: Primary key  of user or group object.
    :param obj_model: Content type of an object.
    :param obj_pk: Primary key of the object.
    :return: HttpResponse()
    """

    # Get content instance
    obj = get_obj_by_type(request, obj_model, obj_pk, 'change')

    # Prepare form

    if request.POST:

        # Get the right form
        if subject_pk != '0':
            form = PermissionUpdateForm(request.POST)

        else:
            if subject_type == 'group':
                form = PermissionGroupForm(request.POST)
            else:
                form = PermissionUserForm(request.POST)

            if form.is_valid():
                subject_pk = form.cleaned_data[subject_type].pk

        # Update permissions in database and return to entry view
        if form.is_valid():

            subject = get_obj_by_type(request, subject_type, subject_pk, None,
                                      app_label='auth')

            for action in permission_classes:

                permission = f'{action}_{obj_model}'

                if form.cleaned_data[action]:
                    assign_perm(permission, subject, obj)
                else:
                    remove_perm(permission, subject, obj)

            return redirect(obj)

    else:

        if subject_pk != '0':
            # Pre-fill form with current permissions
            subject = get_obj_by_type(request, subject_type, subject_pk, None,
                                      app_label='auth')
            permissions = get_perms(subject, obj)
            permissions = DisplayPermissions('', 0, permissions).__dict__

            form = PermissionUpdateForm(permissions)

        else:
            # Create empty form for new user or group
            if subject_type == 'group':
                form = PermissionGroupForm()
            else:
                form = PermissionUserForm()

    context = {
        'form': form,
    }

    # Display permission form
    return render(request, 'mhc_pipeline/permission/update.html', context)


@method_decorator(login_required, name='dispatch')
class Tab(TemplateMixin, TemplateView):
    """
    View object instance permission
    """

    template_base = 'mhc_pipeline/permission/'

    template_name = 'panel.html'

    def get_context_data(self, **kwargs) -> dict:
        obj = get_obj_by_type(self.request, self.kwargs['content_type'],
                              self.kwargs['pk'], 'view')

        # Set context with permissions
        context = permission_context(obj, self.kwargs['content_type'],
                                     self.request.user)

        return context
