"""
Copyright 2015-2022 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries
from collections import namedtuple
import json
import os
import time

# Import Django related libraries
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import (TemplateView,
                                  View)
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateResponseMixin

# Third party libraries

# Import project libraries
from base_site.exceptions import BaseSiteException
from .fileset import Tab as FilesetTab
from ..forms import (CommentForm,
                     PPUploadForm,
                     RefGenomeUploadForm,
                     SNPSUploadForm)
from ..library.permission import get_if_permission
from ..library.pps import pps_request
from ..models import (BiologicalSample,
                      FileSet,
                      PersonalizedProteome,
                      SNPs)
from .view_mixin import TemplateMixin


class PPSMixin(TemplateMixin):
    """
    Mixin for PPS views
    """

    column_width = 12

    icon = 'barcode'

    title = 'Personalized proteome'

    title_suffix = ''

    template_base = 'mhc_pipeline/pps/'

    template_name = 'form.html'

    def bs_get(self) -> BiologicalSample:
        """
        Gets BiologicalSample instance if change permission
        :return: BiologicalSample instance
        """

        return get_if_permission(self.request, BiologicalSample,
                                 'change', pk=self.kwargs['bs_pk'])

    def bs_redirect(self, biological_sample: BiologicalSample) -> HttpResponse:
        """
        Updates biological_sample change date and redirect.
        :param biological_sample: BiologicalSample instance
        :return: Redirect to biological sample view
        """

        biological_sample.modified_by = self.request.user
        biological_sample.save()

        return redirect(biological_sample)


@method_decorator(login_required, name='dispatch')
class CommentEdit(PPSMixin, FormView):
    """
    View to edit comment of PPS object.
    """

    action = 'Edit comment'

    form_class = CommentForm

    template_base = 'mhc_pipeline/base_site/'

    def form_valid(self, form) -> HttpResponse:
        """
        Adds comments to PPS instance
        :param form: Form instance
        :return: Redirect to biological sample view
        """

        biological_sample = self.bs_get()

        pps_request('patch',
                    f'/{self.kwargs["obj"]}/{self.kwargs["pk"]}/',
                    data={'comment': form.cleaned_data['comment']})

        return self.bs_redirect(biological_sample)

    def get_initial(self) -> dict:
        """
        Provides initial value for comment form
        :return: Dictionary with comment
        """

        response = pps_request('get',
                               f'/{self.kwargs["obj"]}/{self.kwargs["pk"]}/'
                               )

        return {'comment': response.json()['comment']}


@method_decorator(login_required, name='dispatch')
class GenomeAdd(PPSMixin, FormView):
    """
    View to add a new reference genome into PPS.
    """

    action = 'Add reference genome'

    form_class = RefGenomeUploadForm

    template_base = 'mhc_pipeline/base_site/'

    def form_valid(self, form) -> HttpResponse:
        """
        Uploads file to PPS
        :param form: Form instance
        :return: Redirects to genome list view
        """

        file = form.cleaned_data['file']
        files = {'file': (file.name, file, file.content_type)}

        pps_request('post', '/genomes/', files=files)

        return redirect('pps:genome_list')


@method_decorator(login_required, name='dispatch')
class GenomeList(PPSMixin, TemplateView):
    """
    View to list reference genome in the PPS service.
    """

    template_name = 'list_genome.html'

    title = 'Reference genomes'

    def get_context_data(self, **kwargs) -> dict:
        """
        Retrieves reference genome from PPS.
        :param kwargs: Keywords arguments
        :return: Context dictionary
        """

        response = pps_request('get', '/genomes/?trashed=false')

        context = super().get_context_data(**kwargs)
        context['genomes'] = response.json()

        return context


@method_decorator(login_required, name='dispatch')
class GenomeRemove(PPSMixin, TemplateResponseMixin, View):
    """
    View to remove a reference genome in the PPS service.
    """

    def get(self, request, *args, **kwargs) -> HttpResponse:
        """
        Deletes a reference genome and waits for PPS to do so.
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        :return: Redirects to genome list view
        """

        url = f'/genomes/{kwargs["pk"]}/'
        pps_request('patch', url, data={'trashed': 'true'})

        time.sleep(0.5)

        return redirect('pps:genome_list')


@method_decorator(login_required, name='dispatch')
class SNPsAdd(PPSMixin, FormView):
    """
    View to add a new SNPs to PPS.
    """

    action = 'Add SNPs'

    form_class = SNPSUploadForm

    template_base = 'mhc_pipeline/base_site/'

    def form_valid(self, form) -> HttpResponse:
        """
        Adds SNPs to PPS
        :param form: Form instance
        :return: Redirects to biological sample view
        """

        biological_sample = self.bs_get()

        genome_id = form.cleaned_data['reference_genome']

        file = form.cleaned_data['file']
        files = {'file': (file.name, file, file.content_type)}
        params = {'files': files,
                  'data': {'genome': genome_id}
                  }

        response = pps_request('post', '/snps/', **params)

        snps = response.json()

        SNPs.objects.create(biological_sample=biological_sample,
                            pps_snps_id=snps['id'])

        return self.bs_redirect(biological_sample)


class SNPsRemove(PPSMixin, TemplateResponseMixin, View):
    """
    View to remove a SNPs in the PPS service.
    """

    def get(self, request, *args, **kwargs) -> HttpResponse:
        """
        Deletes a SNPs and waits for PPS to do so.
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        :return: Redirects to biological sample view
        """

        biological_sample = self.bs_get()

        pps_request('patch', f'/snps/{kwargs["pk"]}/',
                    data={'trashed': 'true'})
        time.sleep(1)

        SNPs.objects.get(pps_snps_id=int(kwargs['pk'])).delete()

        return self.bs_redirect(biological_sample)


@method_decorator(login_required, name='dispatch')
class PPAdd(PPSMixin, FormView):
    """
    View to create a personalized proteome to PPS.
    """

    action = 'Create personalized proteome'

    form_class = PPUploadForm

    def form_valid(self, form) -> HttpResponse:
        """
        Adds Personalized proteome to PPS.
        :param form: Form instance
        :return: Redirects to biological sample view
        """

        biological_sample = self.bs_get()

        snps_id = form.cleaned_data['snps']

        data = {
            'protein_length_threshold': form.cleaned_data[
                'protein_length_threshold'],
            'quality_threshold': form.cleaned_data['quality_threshold'],
            'snps': snps_id
        }

        if form.cleaned_data['tpm_threshold'] is not None:
            data['tpm_threshold'] = form.cleaned_data['tpm_threshold']

        params = {'data': data}

        if form.cleaned_data['expression_file'] is not None:
            file = form.cleaned_data['expression_file']
            files = {'expression_file': (file.name, file, file.content_type)}
            params['files'] = files

        response = pps_request('post', '/personalized_proteomes/', **params)

        pp = response.json()

        snps_id = snps_id.split('snps/')[1].replace('/', '')
        snps = SNPs.objects.get(biological_sample=biological_sample,
                                pps_snps_id=int(snps_id))

        PersonalizedProteome.objects.create(snps=snps,
                                            pps_pp_id=pp['id'])

        return self.bs_redirect(biological_sample)

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """

        biological_sample_pk = self.kwargs['bs_pk']
        snps_ids = SNPs.objects.filter(biological_sample=biological_sample_pk)
        snps_ids = snps_ids.values_list('pps_snps_id')
        snps_ids = [x[0] for x in snps_ids]

        kwargs = super(PPAdd, self).get_form_kwargs()
        kwargs['snps_ids'] = snps_ids

        return kwargs


class PPTrash(PPSMixin, TemplateResponseMixin, View):
    """
    View to trash a PP in the PPS service.
    """

    def get(self, request, *args, **kwargs) -> HttpResponse:
        """
        Deletes a SNPs and waits for PPS to do so.
        :param request: HttpRequest
        :param args: Positional arguments
        :param kwargs: Keyword arguments
        :return: Redirects to biological sample view
        """

        biological_sample = self.bs_get()

        pps_request('patch', f'/personalized_proteomes/{kwargs["pk"]}/',
                    data={'trashed': 'true'})
        time.sleep(1)

        PersonalizedProteome.objects.get(pps_pp_id=int(kwargs['pk'])).delete()

        return self.bs_redirect(biological_sample)


@login_required
def pygeno_package_download(request, pk: int, bs_pk: int) -> HttpResponse:
    """
    Retrieves genome pyGeno package
    :param request: Django request object
    :param pk: PPS snps primary key
    :param bs_pk: Biological sample primary key
    :return: HttpResponse
    """

    model = request.path.split('/')[3]

    if model == 'snps':
        snps = SNPs.objects.get(biological_sample_id=bs_pk,
                                pps_snps_id=pk)

        get_if_permission(request, BiologicalSample,
                          'view',
                          pk=snps.biological_sample_id)

    response = pps_request('get', f'/{model}/{pk}/')
    content = response.json()

    pps_response = pps_request('get', content['file'])

    file_name = content['file'].split('/')[-1]

    response = HttpResponse(pps_response.content,
                            content_type='application/gzip')
    response[
        'Content-Disposition'] = f'attachment; filename={file_name}'

    return response


@login_required
def pp_download(request, pk: int, bs_pk: int, file_type: str) -> HttpResponse:
    """
    Retrieves personalize proteome FASTA file
    :param request: Django request object
    :param pk: PPS personalized proteome primary key
    :param bs_pk: Biological sample primary key
    :param file_type: File type  [fasta or expression]
    :return: HttpResponse
    """

    pp = pps_request('get', f'/personalized_proteomes/{pk}/').json()

    snps = SNPs.objects.get(biological_sample_id=bs_pk,
                            pps_snps_id=pp['snps_id_'])

    bs = get_if_permission(request, BiologicalSample,
                      'view',
                      pk=snps.biological_sample_id)

    pp_file = pps_request('get', pp[f'{file_type}_file'])

    if file_type == 'fasta':

        if pp['fasta_file'].endswith('pp.fasta'):
            file_name = f'bs_{bs.identifier}_personalized_proteome_id_{pk}.' \
                        f'fasta'
        elif pp['fasta_file'].endswith('pp.fasta.zip'):
            file_name = f'bs_{bs.identifier}_personalized_proteome_id_{pk}.' \
                        f'fasta.zip'
        else:
            file_name = pp['fasta_file'].split('/')[-1]

    else:
        file_name = f'bs_{bs.identifier}_expression_file_pp_id_{pk}.tsv'

    # TODO-MC content type is not always text/plain
    response = HttpResponse(pp_file.content, content_type='text/plain')
    response[
        'Content-Disposition'] = 'attachment; filename=%s' % file_name

    return response


def get_biological_sample_pp(bs_snps) -> list:
    """
    Gets snps and personalized proteome associated with the biological sample.
    :param bs_snps: Snps queryset
    :return: List of PP entries
    """

    PPLine = namedtuple('SNPLine', ['snp_id', 'id', 'created_at',
                                    'filters', 'status', 'comment'])
    Filter = namedtuple('Filter', 'json_values')

    pp_entries = list()

    pp_ids = list()

    for snp in bs_snps:

        # Get Personalized proteome
        for personalized_proteome in snp.personalizedproteome_set.all():

            pp_ids.append(str(personalized_proteome.pps_pp_id))

    ids = ",".join(pp_ids)

    if ids:
        url = f'/personalized_proteomes/?ids={ids}'
        response = pps_request('get', url)
        personalized_proteomes = response.json()

        for pp in personalized_proteomes:

            pp_filters = dict()

            pp_filters['SNPs quality threshold'] = pp['quality_threshold']

            if pp['expression_file'] is not None:
                pp_filters['Expression file'] = os.path.basename(
                    pp['expression_file'])

            if pp['tpm_threshold'] is not None:
                pp_filters['TPM threshold'] = pp['tpm_threshold']

            if pp['remove_redundant']:
                pp_filters['Remove redundant sequence'] = True

            pp_filters['Maximum protein length'] = pp[
                'protein_length_threshold']

            entry = PPLine(snp_id=pp['snps_id_'],
                           id=pp['id'],
                           created_at=pp['created_at'],
                           filters=Filter(json_values=json.dumps(pp_filters)),
                           status=pp['fasta_status'],
                           comment=pp['comment'],
                           )

            pp_entries.append(entry)

    return pp_entries


def get_biological_sample_snps(bs_snps) -> list:
    """
    Gets snps and personalized proteome associated with the biological sample.
    :param bs_snps: Snps queryset
    :return: List of SNP entries
    """

    SNPLine = namedtuple('SNPLine', ['id', 'created_at', 'comment', 'name',
                                     'status', 'logs', 'genome_name',
                                     'genome_species', 'pp_ref'])

    pp_snps = dict()
    snp_entries = list()

    for snps in bs_snps:

        pp_snps[snps.pps_snps_id] = snps.personalizedproteome_set.exists()

    snps_ids = ','.join([ str(id_) for id_ in pp_snps.keys()])

    if snps_ids:
        pps_snps = pps_request('get', f'/snps/?ids={snps_ids}').json()

        for snps in pps_snps:

            # Get Reference Genome values
            entry = SNPLine(id=snps['id'],
                            created_at=snps['created_at'],
                            name=snps['name'],
                            status=snps['import_status'],
                            logs=snps['import_logs'],
                            comment=snps['comment'],
                            genome_name=snps['genome_name'],
                            genome_species=snps['species'],
                            pp_ref=pp_snps[snps['id']]
                            )

            snp_entries.append(entry)

    return snp_entries


@method_decorator(login_required, name='dispatch')
class Tab(PPSMixin, TemplateView):
    """
    View tab in biological view
    """

    template_name = 'tab.html'

    def get_context_data(self, **kwargs) -> dict:

        pk = kwargs['pk']

        # Is user authorized to look at biological sample
        obj = get_if_permission(self.request, BiologicalSample, 'view', pk=pk)

        t = FilesetTab(request=self.request)
        kwargs['fileset_classes'] = 'Fasta'
        context = t.get_context_data(**kwargs)
        bs_snps = SNPs.objects.filter(biological_sample=pk)
        bs_snps = bs_snps.prefetch_related('personalizedproteome_set')

        groups = FileSet.FILE_SET['Fasta']['allowed_groups']
        add_permission = self.request.user.groups.filter(name__in=groups).exists()

        try:
            context['add_permission'] = add_permission
            context['snp_entries'] = get_biological_sample_snps(bs_snps)
            context['pp_entries'] = get_biological_sample_pp(bs_snps)
            context['obj'] = obj

        except BaseSiteException:
            context['pps_unavailable'] = True

        return context


@method_decorator(login_required, name='dispatch')
class PPState(PPSMixin, TemplateView):
    """
    Provides state of the personalized proteome instance.
    """

    template_name = 'state.html'

    def get_context_data(self, **kwargs) -> dict:
        url = f'/personalized_proteomes/{kwargs["pk"]}/'
        response = pps_request('get', url)
        personalized_proteome = response.json()

        context = dict()
        context['state'] = personalized_proteome['fasta_status']

        return context


@method_decorator(login_required, name='dispatch')
class SNPsState(PPSMixin, TemplateView):
    """
    Provides state of the SNPs instance.
    """

    template_name = 'state.html'

    def get_context_data(self, **kwargs) -> dict:
        url = f'/snps/{kwargs["pk"]}/'
        response = pps_request('get', url)
        snps = response.json()

        context = dict()
        context['state'] = snps['import_status']

        return context
