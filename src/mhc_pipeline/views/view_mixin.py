"""
Copyright 2015-2020 Mathieu Courcelles
CAPCA - Center for Advanced Proteomics and Chemogenomics Analyses
Pierre Thibault's lab
IRIC - Universite de Montreal

AGPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Import standard libraries

# Import Django related libraries
from django.core.exceptions import ImproperlyConfigured

# Third party libraries

# Import project libraries
from mhc_pipeline.library.permission import (check_content_permission,
                                             get_if_permission)


class TemplateMixin(object):
    """
    Mixin for Template
    """

    template_base = None

    def get_template_names(self):
        """
        Returns a list of template names to be used for the request. Must return
        a list. May not be called if render_to_response is overridden.
        """
        if self.template_name is None:
            raise ImproperlyConfigured(
                "TemplateMixin requires either a definition of "
                "'template_name' or an implementation of 'get_template_names()'")
        elif self.template_base is None:
            raise ImproperlyConfigured(
                "TemplateMixin requires either a definition of "
                "'template_base' or an implementation of 'get_template_names()'")
        else:
            return [self.template_base + self.template_name]


class TrashMixin(TemplateMixin):
    """
    Mixin for Trash view
    """

    bs_fileset = False

    icon = 'trash'

    model = None

    template_base = 'mhc_pipeline/base_site/'

    template_name = 'trash.html'

    title_suffix = '- Trash'

    def get_context_data(self, **kwargs):

        if self.model is None:
            raise ImproperlyConfigured(
                "TrashMixin requires either a definition of "
                "'model' or an implementation of 'get_context_data()'")

        elif self.bs_fileset:

            obj = (self.model.objects
                   .select_related('biological_sample')
                   .get(pk=kwargs['pk']))

            biological_sample = obj.biological_sample
            check_content_permission(self.request, 'change', biological_sample)

            confirmed = kwargs.get('confirmed', False)

            # Trash instance but do not delete from db
            if confirmed == 'true':
                obj.trashed = True
                obj.save()

                if obj.file_set.has_reference is False:
                    obj.file_set.locked = False
                    obj.file_set.save()

                biological_sample.modified_by = self.request.user
                biological_sample.save()

            context = super(TemplateMixin, self).get_context_data(**kwargs)
            context['obj'] = obj
            context['confirmed'] = confirmed
            context['return_url'] = biological_sample.get_absolute_url()
            context['return_name'] = f'Return to Biological sample ' \
                                     f'{biological_sample}'

            return context

        else:

            obj = get_if_permission(self.request,
                                    self.model,
                                    'delete',
                                    pk=kwargs['pk'])

            confirmed = kwargs.get('confirmed', False)

            # Trash instance but do not delete from db
            if confirmed == 'true':
                obj.trashed = True
                obj.save()

            context = super().get_context_data(**kwargs)
            context['obj'] = obj
            context['confirmed'] = confirmed

            return context
