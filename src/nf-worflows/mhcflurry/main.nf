#!/usr/bin/env nextflow


/*
========================================================================================
                 MHCflurry Workflow
========================================================================================
 @#### Authors
 Mathieu Courcelles <mathieu.courcelles@umontreal.ca>

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
Run MHCflurry on peptide sequences and alleles

 ----------------------------------------------------------------------------------------

How to run:
* sudo ~/nextflow ~/code/capamhc/src/nf-workflows/mhcflurry/main.nf  -profile docker -resume
----------------------------------------------------------------------------------------

*/


/*
 * Define the default parameters
 */

peptide_files = Channel.fromPath('./peptides_*.csv')



log.info """\
 =====================================
  MHCflurry Workflow
 =====================================
 """


process mhcflurry {

    container "$MHCFLURRY_CONTAINER"

    publishDir "${params.outdir}", mode:'copy', overwrite: true

    input:
    each file(peptide_file) from peptide_files

    output:
    file '*'

    beforeScript 'chmod o+rw .'
    script:
    """
    mhcflurry-predict ${peptide_file} > pred_${peptide_file}
    """
}



workflow.onComplete {
	log.info ( workflow.success ? "\nSuccessful" : "Failed" )
}
