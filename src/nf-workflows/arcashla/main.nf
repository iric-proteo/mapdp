#!/usr/bin/env nextflow


/*
========================================================================================
                 arcasHLA Workflow
========================================================================================
 @#### Authors
 Mathieu Courcelles <mathieu.courcelles@umontreal.ca>

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
 - 1:   Extract reads
 - 2:   HLA genotyping
 ----------------------------------------------------------------------------------------

How to run:
* Set ARCASHLA_CONTAINER variable
 sudo ~/nextflow ~/code/nf-worflows/arcashla-nf/main.nf --bamfiles=*.bam -profile docker -resume

NXF_HOME=/mnt/.nextflow /mnt/arcashla/nextflow /mnt/arcashla/main.nf --bamfiles=*.bam -profile k8s -resume
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Reference workflows:
 - 1:   arcasHLA <https://github.com/RabadanLab/arcasHLA>
 - 2:   docker image gitlab <https://gitlab.com/courcelm/arcashla>
 ----------------------------------------------------------------------------------------

*/


/*
 * Define the default parameters
 */


bamFiles = Channel.fromPath( params.bamfiles )
                  .ifEmpty { exit 1, "Cannot find any bam files matching: ${params.bamfiles}" }


log.info """\
 =====================================
  arcasHLA Workflow
 =====================================
 BAM files  : ${params.bamfiles}
 =====================================
 """


process extractReads {

    container "$ARCASHLA_CONTAINER"

    publishDir "${params.outdir}/extractReads", mode:'copy', overwrite: true, pattern: '*.log'

    input:
    file bamFile from bamFiles

    output:
    tuple "*.1.fq.gz", "*.2.fq.gz" into fastqFilePairs
    file '*.log'

    script:
    """
    arcasHLA extract ${bamFile} --paired -t 4 -v
    """
}


process genotype {

    container "$ARCASHLA_CONTAINER"

    publishDir "${params.outdir}/genotype", mode:'copy', overwrite: true

    input:
    file(reads) from fastqFilePairs

    output:
    file '*.json'
    file '*.log'

    script:
    """
    arcasHLA genotype ${reads} -g A,B,C,DPA1,DPB1,DQA1,DQB1,DRA,DRB1,DRB4 -t 4 -v
    echo \$ARCASHLA_VERSION > version.log
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nSuccessful" : "Failed" )
}
