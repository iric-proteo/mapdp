#!/usr/bin/env nextflow


/*
========================================================================================
                 MixMHCp Workflow
========================================================================================
 @#### Authors
 Mathieu Courcelles <mathieu.courcelles@umontreal.ca>

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
 - 1:   MHC deconvolution
 - 2:   MHC motif logos
 ----------------------------------------------------------------------------------------

How to run:
* Go in data folder
* Set MIXMHCP_CONTAINER variable
* sudo ~/nextflow ~/code/capamhc/src/nf-worflows/mixmhcp/main.nf --pepfiles=*.txt -profile docker -resume

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Reference workflows:
 - 1:   MixMHCp <https://github.com/GfellerLab/MixMHCp>
 - 2:   docker image gitlab <https://gitlab.com/courcelm/mixmhcp>
 ----------------------------------------------------------------------------------------

*/


/*
 * Define the default parameters
 */


pepFiles = Channel.fromPath( params.pepfiles )
                  .ifEmpty { exit 1, "Cannot find any txt files matching: ${params.pepfiles}" }


log.info """\
 =====================================
  MixMHCp Workflow
 =====================================
 Peptide files  : ${params.pepfiles}
 =====================================
 """


process deconvolution {

    container "$MIXMHCP_CONTAINER"

    publishDir "${params.outdir}", mode:'copy', overwrite: true

    input:
    file pepFile from pepFiles

    output:
    file '*'

    script:
    """
    MixMHCp -i ${pepFile} -o deconvolution
    echo \$VERSION > version.log
    """
}



workflow.onComplete {
	log.info ( workflow.success ? "\nSuccessful" : "Failed" )
}
