
params {
    outdir = "results"
    tracedir = "results/pipeline_info"
}

// Capture exit codes from upstream processes when piping
process.shell = ['/bin/bash', '-euo', 'pipefail']

profiles {
    docker { docker.enabled = true }

    k8s {
        process {
            cpus = 1
            memory = '200MB'
            executor = 'k8s'
            scratch = true
        }

        k8s {
           namespace = "$NEXTFLOW_NAMESPACE"
           pod = [[imagePullSecret: "$PULL_SECRET"]]
           runAsUser = 33
           serviceAccount = "$NEXTFLOW_SERVICE_ACCOUNT"
           storageClaimName = "$NEXTFLOW_PVC"
           storageMountPath = '/mnt/nextflow'
        }
    }
}

dag {
  enabled = true
  file = "${params.tracedir}/pipeline_dag.svg"
}

report {
  enabled = true
  file = "${params.tracedir}/execution_report.html"
}

timeline {
  enabled = true
  file = "${params.tracedir}/execution_timeline.html"
}

trace {
  enabled = true
  file = "${params.tracedir}/execution_trace.txt"
}
