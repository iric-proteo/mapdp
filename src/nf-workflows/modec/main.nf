#!/usr/bin/env nextflow


/*
========================================================================================
                 MoDec Workflow
========================================================================================
 @#### Authors
 Mathieu Courcelles <mathieu.courcelles@umontreal.ca>

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Pipeline overview:
 - 1:   MHC deconvolution
 - 2:   MHC motif logos
 ----------------------------------------------------------------------------------------

How to run:
* Go in data folder
* Set MODEC_CONTAINER variable
* sudo ~/nextflow ~/code/capamhc/src/nf-worflows/modec/main.nf --pepfiles=*.txt -profile docker -resume

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
Reference workflows:
 - 1:   MixMHCp <https://github.com/GfellerLab/MoDec>
 - 2:   docker image gitlab <https://gitlab.com/courcelm/modec>
 ----------------------------------------------------------------------------------------

*/


/*
 * Define the default parameters
 */


pepFiles = Channel.fromPath( params.pepfiles )
                  .ifEmpty { exit 1, "Cannot find any txt files matching: ${params.pepfiles}" }


log.info """\
 =====================================
  MoDec Workflow
 =====================================
 Peptide files  : ${params.pepfiles}
 =====================================
 """


process deconvolution {

    container "$MODEC_CONTAINER"

    publishDir "${params.outdir}", mode:'copy', overwrite: true

    input:
    file pepFile from pepFiles

    output:
    file '*'

    script:
    """
    MoDec_unix -i ${pepFile} -o deconvolution --pepLmin 12 --specInits --makeReport
    echo \$VERSION > version.log
    """
}



workflow.onComplete {
	log.info ( workflow.success ? "\nSuccessful" : "Failed" )
}
