/*
Copyright 2015-2019 Mathieu Courcelles
CAPA - Center for Advanced Proteomics Analyses and Thibault's lab
IRIC - Universite de Montreal
*/

var ids = [];
var urls = [];
var timeout = 1000 * 30;


function updateState() {

    // Updates state of fields and list of monitored fields

    var ids_tmp = [];

    for (i = 0; i < ids.length; ++i) {

        item = ids[i]

        url = urls[item.attr("model")];

        item_url = url.replace("/0/", "/" + item.attr("pk") + "/");

        text = item.text();
        if(text.includes('Started') || text.includes('Pending')) {
            ids_tmp.push(item);
            item.load(item_url);
        }

    }

    ids = ids_tmp;

    if(ids.length) {
        setTimeout(updateState, timeout);
    }
}



$( document ).ready(function() {

    // Gets state fields that need to be monitored
    $(".state").each(function(index) {
        if($( this ).text().includes('Started') || $( this ).text().includes('Pending')) {
            ids.push($( this ));
        }

    });

    setTimeout(updateState, 250);

});
