// Customized from https://www.bootply.com/63891
$('#tabs a').click(function (e) {

	e.preventDefault();

	var url = $(this).attr("data-url");
  	var href = this.hash;
  	console.log(href);
  	var pane = $(this);

    if(href != "#tab_1") {
        $(href).html('<div style="text-align:center"><i class="fa fa-spinner fa-spin fa-2x"></i></div>');
    }

    // ajax load from data-url
    $(href).load(url,function(result){
        pane.tab('show');
    });
});