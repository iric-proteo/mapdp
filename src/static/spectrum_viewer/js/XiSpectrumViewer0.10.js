 
// XiSpectrumViewer.js

var ZOOMGIF = ["data:image/gif;base64,",
   "R0lGODlhEAAQAMZzADs7O0VFRUhISE5OTlVVVVdXV1xeYFpndWJwgGJxgGRyhGdzgGZ1hmh2hml6",
   "i2x6iWl7jGx9jnOFl3aGmHaHmXeImoGLlnqOq3uPp42NjYGPnYORn4GSo4mYqIuYpouZp5iYmJub",
   "m5Cfr5Ois5WjsaCipZuntJiowJuptpqqv5arzZWt06KxwaWxvqi0v6O11Ku2wqe616y6yau6zK66",
   "xbS+yrLA07jByrPC2rbD0rfD07zH2LnI37/M3MjV78nb/NDc8NTc6tLc8tXd6tTd7dzi6tni9tXj",
   "/9bj/9nl/Nrm/9vm/dnn/9zo/93o/97p/+Hq/+Dr/+Hr/uHr/+Lr/uLr/+Pr/+Ps/+Ts/+Tt/+Xt",
   "/+jv/+jw/+nw/+vx/+ry/+zy/+3z/u3z//Hz9vD0//D1//T19fH1//H2//P2//P3//X3//b5//f5",
   "//f6/vn7//r7//v8/v7+/////////////////////////////////////////////////////yH5",
   "BAEKAH8ALAAAAAAQABAAAAefgH+Cfy4tKB6DiYM0OTY8OCkcDIqCMD1AV1dRSTEYCZQyRlNZWldV",
   "SysSiiQ7XFJNT1ZYUD8XB4kdRF5iYF1VSk5HKgiJE0FnaGhkYltKSC8NiRA1b25samVgVEwnC4oU",
   "Y3JxbWlhTz4jlBEiZnNwa19CMxqUfw4VN0VDOiwKJgb1/jzY8MGCoBIFBASkFILAgIWKMhAIADER",
   "CAAVKQUCADs="].join("");

function jbSpectrum(d,x,y,w,h){
   this.d = d;
   this.x = x;
   this.y = y;
   this.w = w;
   this.areax = x;
   this.areay = y;
   this.areaw = w;
   this.areah = h;
   this.h = h;
   this.axes = new Array();
}

jbSpectrum.prototype.activeArea = function(){
   var aa = new Object;
   aa.x = this.areax;
   aa.y = this.areay;
   aa.width = this.areaw;
   aa.height = this.areah;
   return aa;
}

function objInfo(o){
   var mesg = "";
   for(i in o){
      mesg += i + " = " + o[i] + "\n";
   }
   return mesg;
}

jbSpectrum.prototype.axis = function(title,type){
   var axis;
   if(type == 0){
      axis = new jbAxis(this, 0, 0, this.w, title, type);
   }
   else if(type == 1){
      axis = new jbAxis(this, 0, 0, this.h, title, type);
   }
   else if(type == 2){
      axis = new jbAxis(this, this.w, 0, this.h, title, type);
   }
   this.axes.push(axis);
   return axis;
}
jbSpectrum.prototype.getBBox = function(){
   var x1 = "";
   var y1 = "";
   var x2 = "";
   var y2 = "";
   for(var i=0; i< this.axes.length; i++){
      var box = this.axes[i].g.getBBox();
      if(x1 == "" || box.x < x1) x1 = box.x;
      if(y1 == "" || box.y < y1) y1 = box.y;
      if(x2 == "" || box.x+box.width > x2) x2 = box.x+box.width;
      if(y2 == "" || box.y+box.height > y2) y2 = box.y+box.height;
   }
   var mybox = new Object();
   mybox.x = x1;
   mybox.y = y1;
   mybox.width = x2 - x1;
   mybox.height = y2 - y1;
   return mybox;
}
jbSpectrum.prototype.fit = function(){
   var left = 0;
   var right = 0;
   var bottom = 0;
   for(var i=0; i< this.axes.length; i++){
      var box = this.axes[i].g.getBBox();
      if(this.axes[i].type == 0) // x
         bottom = box.height;
      else if(this.axes[i].type == 1) // y-left
         left = box.width;
      else if(this.axes[i].type == 2) // y-right
         right = box.width;
   }
   this.areax = this.x + left;
   this.areay = this.y + bottom;
   this.areaw = this.w - left - right;
   this.areah = this.h - bottom;
   for(var i=0; i< this.axes.length; i++){
      if(this.axes[i].type == 0){ // x
         this.axes[i].x = left;
         this.axes[i].y = bottom;
         this.axes[i].l = this.w - left - right;         
         this.axes[i].redraw();
      }
      else if(this.axes[i].type == 1){ // y-left
         this.axes[i].y = bottom;
         this.axes[i].x = left;
         this.axes[i].l = this.h - bottom;
         this.axes[i].redraw();
      }
      else if(this.axes[i].type == 2){ // y-right
         this.axes[i].y = bottom;
         this.axes[i].x = this.w - right;
         this.axes[i].l = this.h - bottom;
         this.axes[i].redraw();
      }
   }
}

function jbAxis(p,x,y,l,title,type){
   this.p = p;
   this.d = p.d;
   this.x = x;
   this.y = y;
   this.X = x + p.x;
   this.Y = y + p.y;
   this.l = l;            // actual length
   this.title = title;
   this.type = type;
   this.majorticks = new Array();
   this.minorticks = new Array();
   this.labels = new Array();
   this.drawn = false;
   
   this.ytitleoffset = 3;
   this.xtitleoffset = 17;
   
   this.lineColour = '#000000';
   this.majorTickColour = '#000000';
   this.minorTickColour = '#000000';
   this.labelColour = '#000000';
   this.titleColour = '#000000';
   
   this.lineWidth = 2;
   this.majorTickWidth = 1;
   this.minorTickWidth = 1;
   
   this.majorTickLength = 10;
   this.minorTickLength = 5;
   
   this.labelTickXOffset = type ? 3 : 0;
   this.labelTickYOffset = type ? 3 : 10;
   this.labelStyle = "font-family: Calibri; font-size: 8pt;";
   this.titleStyle = "font-family: Calibri; font-size: 12pt;";
}
jbAxis.prototype.undraw = function(){
   this.drawn = false;
   this.g.remove();
   this.majorticks = new Array();
   this.minorticks = new Array();
   this.labels = new Array();  
   this.axisline = 0; 
}
jbAxis.prototype.redraw = function(){
   return this.draw(this.lo, this.hi);
}
jbAxis.prototype.draw = function(lo,hi){
   this.X = this.x + this.p.x;
   this.Y = this.y + this.p.y;
   if(this.drawn)
      this.undraw();
   this.drawn = true;
   this.lo = lo;
   this.hi = hi;
   this.vl = hi - lo; // virtual length (in m/z)
   this.m = this.l/this.vl;
   this.ni = new jbNiceIntervals(lo,hi);
   this.g = this.d.group(this.X, this.Y);
   if(this.type == 1){ // Y1 (intensity)
      for(var i = 0; i < this.ni.x.length; i++){
         var X = this.m * (this.ni.x[i] - this.ni.x1);
         this.majorticks.push(this.g.line(newID(), 0, X, -this.majorTickLength, X, this.majorTickColour, this.majorTickWidth));
         var label = this.ni.x[i];
         if(this.vl >= 10000){
            var n = new Number(label);
            label = n.toExponential(1);
         }
         this.labels.push(this.g.text(newID(), -this.majorTickLength-this.labelTickXOffset,X-this.labelTickYOffset, "end",0,this.labelColour, this.labelStyle, label));
      }
      for(var i = 0; i < this.ni.y.length; i++){
         var X = this.m * (this.ni.y[i] - this.ni.x1);
         this.minorticks.push(this.g.line(newID(), 0, X, -this.minorTickLength, X, this.minorTickColour, this.minorTickWidth));
      }
      this.axisline = this.g.line(newID(),0,0,0,this.l,this.lineColour, this.lineWidth);
      var bound = this.g.getBBox();
      this.g.text(newID(), bound.x-this.g.x-this.ytitleoffset , this.l/2, "middle", -90, this.titleColour, this.titleStyle, this.title);
   }
   else if(this.type == 2){ // Y2 (%)
      for(var i = 0; i < this.ni.x.length; i++){
         var X = this.m * (this.ni.x[i] - this.ni.x1);
         this.majorticks.push(this.g.line(newID(), 0, X, this.majorTickLength, X, this.majorTickColour, this.majorTickWidth));
         var label = this.ni.x[i];
         if(this.vl >= 10000){
            var n = new Number(label);
            label = n.toExponential(1);
         }
         this.labels.push(this.g.text(newID(), this.majorTickLength+this.labelTickXOffset,X-this.labelTickYOffset, "start",0,this.labelColour, this.labelStyle, label));
      }
      for(var i = 0; i < this.ni.y.length; i++){
         var X = this.m * (this.ni.y[i] - this.ni.x1);
         this.minorticks.push(this.g.line(newID(), 0, X, this.minorTickLength, X, this.minorTickColour, this.minorTickWidth));
      }
      this.axisline = this.g.line(newID(),0,0,0,this.l,this.lineColour, this.lineWidth);
      var bound = this.g.getBBox();
      this.g.text(newID(), bound.x+bound.width-this.g.x+this.ytitleoffset , this.l/2, "middle", 90, this.titleColour, this.titleStyle, this.title);
   }
   else { // 0=X   /////// X axis
      for(var i = 0; i < this.ni.x.length; i++){
         var X = this.m * (this.ni.x[i] - this.ni.x1);
         this.majorticks.push(this.g.line(newID(), X, 0, X, -this.majorTickLength, this.majorTickColour, this.majorTickWidth));
         var label = this.ni.x[i];
         if(this.vl >= 10000){
            var n = new Number(label);
            label = n.toExponential(1);
         }
         this.labels.push(this.g.text(newID(), X-this.labelTickXOffset, -this.majorTickLength-this.labelTickYOffset,"middle",0,this.labelColour, this.labelStyle, label));
      }
      for(var i = 0; i < this.ni.y.length; i++){
         var X = this.m * (this.ni.y[i] - this.ni.x1);
         this.minorticks.push(this.g.line(newID(), X, 0, X, -this.minorTickLength, this.minorTickColour, this.minorTickWidth));
      }
      this.axisline = this.g.line(newID(),0,0,this.l,0,this.lineColour, this.lineWidth);
      var bound = this.g.getBBox();
      this.g.text(newID(), this.l/2, bound.y-this.g.y-this.xtitleoffset , "middle", 0, this.titleColour, this.titleStyle, this.title);
   }
}

function jbState(){
   this.states = new Object();
}
jbState.prototype.add = function(target){
   this.states[target] = target.mode;
}


function jbMode(colour,visible,defaultStyle){
   this.colour = colour;
   this.visible = visible;
   this.style = defaultStyle;
   if(! this.style.match(/font-family/))
      this.style += " font-family: Calibri;"
   this.callback = "";
   this.hover = new Object();
   this.primaryFlag = 0;
}
jbMode.prototype.addHover = function(target,targetmode){
   this.hover[target] = targetmode;
}
jbMode.prototype.setPrimaryFlag = function(flag){
   this.primaryFlag = flag;
}
jbMode.prototype.getPrimaryFlag = function(){
   return this.primaryFlag;
}
function jbModeCollection(){
   this.modenames = new Array();
}
jbModeCollection.prototype.add = function(mode,colour,visible,defaultStyle){
   this.modenames.push(mode);
   this.set(mode,colour,visible,defaultStyle);
}

jbModeCollection.prototype.set = function(mode,colour,visible,defaultStyle){
   if(defaultStyle == "INHERIT"){
      defaultStyle = this["_default_"].style;
   }
   this[mode] = new jbMode(colour,visible,defaultStyle);
}
jbModeCollection.prototype.setPrimaryFlag = function(mode,flag){
   this[mode].setPrimaryFlag(flag);
}
jbModeCollection.prototype.getPrimaryFlag = function(mode){
   return this[mode].getPrimaryFlag();
}

// SBItem
function jbSBItem(parent,itemtype,defaultColour,defaultVisible,defaultStyle){
   this.id = newID();
   this.tags = "";
   this.modes = new jbModeCollection();
   this.modes.add("_default_",defaultColour,defaultVisible,defaultStyle);
   this.currentmode = "_default_";
   this.previousmode = "_default_";
   this.parent = parent;
   this.visible = defaultVisible;
}
jbSBItem.prototype.addMode = function(mode,colour,visible,defaultStyle){
   this.modes.add(mode,colour,visible,defaultStyle);
}
jbSBItem.prototype.setMode = function(mode,colour,visible,defaultStyle){
   this.modes.set(mode,colour,visible,defaultStyle);
}
jbSBItem.prototype.setModePrimaryFlag = function(mode,flag){
   this.modes.setPrimaryFlag(mode,flag);
}
jbSBItem.prototype.getModePrimaryFlag = function(mode,flag){
   return this.modes.getPrimaryFlag(mode,flag);
}
jbSBItem.prototype.reApplyMode = function(){
   this.changeMode(this.currentmode);
}

jbSBItem.prototype.setText = function(text,x,y,angle,anchor){
   this.text = text;
   this.x = x;
   this.y = y;
   this.angle = angle;
   var style = this.modes[this.currentmode].style;
   if(! style.match(/font-family/))
      style += " font-family: Calibri;";
   this.drawnObject = this.parent.g.text(
      this.id, x, y, anchor, angle, 
      this.modes[this.currentmode].colour, style, 
      text);
   this.changeMode(this.currentmode);
}
jbSBItem.prototype.changeText = function(text){
   this.parent.d.changeText(this.drawnObject, text);
}
jbSBItem.prototype.setLine = function(x1,y1,x2,y2,strokewidth){
   this.x1 = x1;
   this.y1 = y1;
   this.x2 = x2;
   this.y2 = y2;
   this.strokewidth = strokewidth;
   this.drawnObject = this.parent.g.line(
      this.id, x1, y1, x2, y2,  
      this.modes[this.currentmode].colour, strokewidth);
   this.changeMode(this.currentmode);
}
jbSBItem.prototype.setRect = function(x,y,w,h){
   this.x = x;
   this.y = y;
   this.w = w;
   this.h = h;
   this.drawnObject = this.parent.g.rect(
      this.id, x, y, w, h,  
      this.modes[this.currentmode].colour, '#ffffff',0);
   this.changeMode(this.currentmode);
}
jbSBItem.prototype.getHover = function(){
   return this.modes[this.currentmode].hover;
}

jbSBItem.prototype.hasHoverActions = function(){
   var h = this.getHover();
   var i = 0;
   for(a in h){
      i++;
   }
   return i;
}

jbSBItem.prototype.setHover = function(mode,target,targetmode){
   this.modes[mode].hover[target] = targetmode;
}
jbSBItem.prototype.setDefaultHover = function(target,targetmode){
   this.setHover("_default_", target, targetmode);
}
jbSBItem.prototype.getCallback = function(){
   return this.modes[this.currentmode].callback;
}
jbSBItem.prototype.doHover = function(){
   var hover = this.getHover();
   for(target in hover){
      this.parent.viewer.itemlist[target].changeMode(hover[target]);
   }
}
jbSBItem.prototype.unHover = function(){
   var hover = this.getHover();
   for(target in hover){
      this.parent.viewer.itemlist[target].revertMode();
   }
}
jbSBItem.prototype.revertMode = function(mode){
   this.changeMode(this.previousmode);
}
jbSBItem.prototype.changeMode = function(mode){
   if(this.modes[mode] == undefined)
      return;
   this.previousmode = this.currentmode;
   this.currentmode = mode;
   this.parent.d.recolour(this.drawnObject, this.modes[mode].colour);
   this.parent.d.setStyle(this.drawnObject, this.modes[mode].style);
   if(this.modes[mode].visible){
      this.parent.d.show(this.drawnObject);
   }
   else {
      this.parent.d.hide(this.drawnObject);  
   }
   if(this.isFText)
      this.peak.activateModeText(mode);
}
jbSBItem.prototype.modeScan = function(modenamesarray){
   var maxflag = -1;
   for(var i = 0; i < modenamesarray.length; i++){
      if(this.modes[modenamesarray[i]] != null){
         var thisflag = this.getModePrimaryFlag(modenamesarray[i]);
         if(thisflag > maxflag){
            this.changeMode(modenamesarray[i]);
            maxflag = thisflag;
         }
      }
   }
   if(maxflag < 0)
      this.changeMode("_default_");
}
jbSBItem.prototype.isVisible = function (){
   return this.parent.d.isVisible(this.drawnObject);
}
jbSBItem.prototype.getBBox = function (){
   return this.parent.d.getBBox(this.drawnObject);
}
jbSBItem.prototype.setTags = function(tags){
   this.tags = tags;
}
jbSBItem.prototype.addTags = function(tags){
   this.tags += " "+tags;
}
jbSBItem.prototype.hasElement = function(){
   return this.parent.d.hasElement(this.drawnObject);
}

		
function jbPeak(viewer, mz, ic, fixedtext, hovertext, userdata){
   this.X = 0;
   this.Y = 0;
   this.viewer = viewer;
   this.d = this.viewer.d;
   this.mz = mz;
   this.ic = ic;
   this.fixedtext = fixedtext;
   this.hovertext = hovertext;
   this.userData = userdata;
   this.modeFTexts = new Object;
   this.modeFTexts["_default_"] = fixedtext;
   this.modeHTexts = new Object;
   this.modeHTexts["_default_"] = hovertext;
   
   this.g = this.d.group(this.X, this.Y);
   
	
   // add an item 
   // then add it to this.viewer.itemlist...
   this.ftext = new jbSBItem(this,'text','#000000',1,"font-family: Calibri;");
   this.ftext.isFText = 1;
   this.ftext.peak = this;
   this.htext = new jbSBItem(this,'text','#000000',0,"font-family: Calibri;");
   this.htext.isHText = 1;
   this.htext.peak = this;
   this.fline = new jbSBItem(this,'line','#000000',1,"");
   this.ftext.setTags("PEAK FIXEDTEXT");
   this.htext.setTags("PEAK HOVERTEXT");
   this.fline.setTags("PEAK FIXEDLINE");
   
   
   this.ftext.setText(fixedtext,mz,0,-90,"left");
   this.htext.setText(hovertext,mz,0,-90,"left");
   this.fline.setLine(mz,0,mz,10,0.5)
   this.viewer.itemlist[this.ftext.id] = this.ftext;
   this.viewer.itemlist[this.htext.id] = this.htext;
   this.viewer.itemlist[this.fline.id] = this.fline;
   
   this.items = new Array(this.ftext, this.htext, this.fline);
   
   // now find all the parts of the peptide we want to light up when we hover on
   // this peak...
   this.ftext.changeMode("_default_");
   this.htext.changeMode("_default_");
   this.fline.changeMode("_default_");
   
   this.assoctagmodes = new Object();
}

jbPeak.prototype.addModeFText = function(mode,ftext){
   this.modeFTexts[mode] = ftext;
}

jbPeak.prototype.assocTagMode = function(tags, modename){
   this.assoctagmodes[tags] = modename;
}

jbPeak.prototype.addModeHText = function(mode,htext){
   this.modeHTexts[mode] = htext;
}

jbPeak.prototype.activateModeText = function(mode){
   if(this.fixedtext != ""){
      // try the mode for the fixed text
      if(this.modeFTexts[mode] == null)
         this.ftext.changeText(this.fixedtext);
      else 
         this.ftext.changeText(this.modeFTexts[mode]);
      // try the mode for the hover text
   }
   if(this.htext.hasElement()){
      if(this.modeHTexts[mode] == null)
         this.htext.changeText(this.hovertext);
      else 
         this.htext.changeText(this.modeHTexts[mode]);
   }
}

jbPeak.prototype.setUserData = function(data){
   this.userData = data;
}

jbPeak.prototype.getUserData = function(){
   return this.userData;
}

jbPeak.prototype.hasAnnotation = function(){
   return this.fixedtext.length > 0;
}

jbPeak.prototype.hasHoverActions = function(){
   return this.fline.hasHoverActions();
}

jbPeak.prototype.setDefaultHover = function(targetmode){
   this.fline.setDefaultHover(this.ftext.id,targetmode);
   this.fline.setDefaultHover(this.htext.id,targetmode);
   this.fline.setDefaultHover(this.fline.id,targetmode);
}
jbPeak.prototype.setAssocPeakHover = function(mode, targetpeak, targetmode){
   this.fline.setHover(mode,targetpeak.ftext.id,targetmode);
   this.fline.setHover(mode,targetpeak.htext.id,targetmode);
   this.fline.setHover(mode,targetpeak.fline.id,targetmode);
}
jbPeak.prototype.setAssocItemsHover = function(mode, items, targetmode){
   for(var i=0; i<items.itemlist.length; i++){
      this.fline.setHover(mode,items.itemlist[i].id,targetmode);
   }
}
jbPeak.prototype.setHover = function(mode,target,targetmode){
   this.ftext.setHover(mode,target,targetmode);
   this.htext.setHover(mode,target,targetmode);
   this.fline.setHover(mode,target,targetmode);
}
jbPeak.prototype.setDefaultMode = function(pcolour,tcolour,hcolour,defaultStyle){
   this.ftext.setMode("_default_",tcolour,1,defaultStyle);
   this.htext.setMode("_default_",hcolour,0,defaultStyle);
   this.fline.setMode("_default_",pcolour,1,defaultStyle);
}
jbPeak.prototype.reapplyMode = function(){
   this.ftext.changeMode(this.ftext.currentmode);
   this.htext.changeMode(this.ftext.currentmode);
   this.fline.changeMode(this.ftext.currentmode);
}
jbPeak.prototype.addMode = function(mode,colour,visible,defaultStyle){
   this.ftext.addMode(mode,colour,visible,defaultStyle);
   this.htext.addMode(mode,colour,visible,defaultStyle);
   this.fline.addMode(mode,colour,visible,defaultStyle);
}

jbPeak.prototype.setMode = function(mode,colour,visible,defaultStyle){	
   this.ftext.setMode(mode,colour,visible,defaultStyle);
   this.htext.setMode(mode,colour,visible,defaultStyle);
   this.fline.setMode(mode,colour,visible,defaultStyle);
}

jbPeak.prototype.setModePrimaryFlag = function(mode,flag){
   this.ftext.setModePrimaryFlag(mode,flag);
   this.htext.setModePrimaryFlag(mode,flag);
   this.fline.setModePrimaryFlag(mode,flag);
}

jbPeak.prototype.setFTextMode = function(mode,colour,visible,defaultStyle){
   this.ftext.setMode(mode,colour,visible,defaultStyle);
}

jbPeak.prototype.setLineMode = function(mode,colour,visible,defaultStyle){
   this.fline.setMode(mode,colour,visible,defaultStyle);
}

jbPeak.prototype.setHTextMode = function(mode,colour,visible,defaultStyle){
   this.htext.setMode(mode,colour,visible,defaultStyle);
}

jbPeak.prototype.addTags = function(tags){
   this.ftext.addTags(" | PEAK FIXEDTEXT "+tags);
   this.htext.addTags(" | PEAK HOVERTEXT "+tags);
   this.fline.addTags(" | PEAK FIXEDLINE "+tags);
}
jbPeak.prototype.reposition = function(x,y,h){
   // these are absolute coords
   this.d.reposition(this.ftext.drawnObject, x+2, y+h+5);
   this.d.reposition(this.htext.drawnObject, x+17, y+h+5);
   this.d.reposition(this.fline.drawnObject, x, y);
   this.d.height(this.fline.drawnObject, h);
}
jbPeak.prototype.show = function(){
   this.g.show();
}
jbPeak.prototype.hide = function(){
   this.g.hide();
}
jbPeak.prototype.doHover = function(){
   for(var i = 0; i < this.items.length; i++){
      this.items[i].doHover();
   }
}
jbPeak.prototype.unHover = function(){
   for(var i = 0; i < this.items.length; i++){
      this.items[i].unHover();
   }
}






function jbZoombox(zoomer){
   this.zoomer = zoomer;
   this.spec = zoomer.spec;
   this.d = zoomer.spec.d;
   this.g = this.d.group(0, 0);
   this.lines = new Array();
   for(var i=0; i<4; i++){
      this.lines.push(new jbSBItem(this,'line','#cc6633',0,""));
      this.lines[i].setLine(0,0,0,1,2);
   }
   this.text = new jbSBItem(this,'text','#993300',0,"font-family: Calibri;");
   this.text.setText("Zoom...", 0, "", 0,"middle");
}

jbZoombox.prototype.show = function(){
   this.g.show();
}

jbZoombox.prototype.hide = function(){
   this.g.hide();
}

jbZoombox.prototype.posn = function(x1,x2){
   if(x1 > x2){
      var tmp = x1;
      x1 = x2;
      x2 = tmp;
   }
   var w = x2 - x1;
   var aa = this.spec.activeArea();
   var y1 = aa.y;
   var y2 = aa.y + aa.height;
   var h = aa.height;
   
   var posns = new Array(
             // x, y, w, h
      new Array(x1,y1,0, h), // left
      new Array(x1,y1,w, 0), // bottom
      new Array(x1,y2,w, 0), // top
      new Array(x2,y1,0, h)  // right
   );
   
   for(var i=0; i<4; i++){
      var posn = posns[i];
      this.d.reposition(this.lines[i].drawnObject, posn[0], posn[1]);
      this.d.width(this.lines[i].drawnObject, posn[2]);
      this.d.height(this.lines[i].drawnObject, posn[3]);
   }
   var mz1 = this.zoomer.xToMz(x1);
   var mz2 = this.zoomer.xToMz(x2);
   mz1 = mz1.toFixed(this.zoomer.viewer.xaxis.ni.maxxl+1);
   mz2 = mz2.toFixed(this.zoomer.viewer.xaxis.ni.maxxl+1);
   this.d.changeText(this.text.drawnObject, "Zoom: " + mz1 + " to " + mz2);
   this.d.reposition(this.text.drawnObject, (x1+x2)/2, y2-20);
}




function jbMeasurebox(zoomer){
   this.zoomer = zoomer;
   this.spec = zoomer.spec;
   this.d = zoomer.spec.d;
   this.g = this.d.group(0, 0);
   this.lines = new Array();
   for(var i=0; i<2; i++){
      this.lines.push(new jbSBItem(this,'line','#cc99ff',0,""));
      this.lines[i].setLine(0,0,0,1,1);
   }
   this.lines.push(new jbSBItem(this,'line','#cc99ff',0,""));
   this.lines[i].setLine(0,0,0,1,1);
   this.text = new jbSBItem(this,'text','#330099',0,"font-family: Calibri;");
   this.text.setText("", 0, "", 0,"middle");
   
   this.textfrom = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textfrom.setText("", 0, "", 0,"middle");
   
   this.textto = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textto.setText("", 0, "", 0,"middle");
   
   this.textmass1 = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textmass1.setText("", 0, "", 0,"middle");
   this.textmass2 = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textmass2.setText("", 0, "", 0,"middle");
   this.textmass3 = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textmass3.setText("", 0, "", 0,"middle");
   this.textmass4 = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textmass4.setText("", 0, "", 0,"middle");
   this.textmass5 = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textmass5.setText("", 0, "", 0,"middle");
   this.textmass6 = new jbSBItem(this,'text','#330099',0,"font-size: 8pt;");
   this.textmass6.setText("", 0, "", 0,"middle");
   /*
   var mesg = "";
   for(thing in d.elements[this.text.drawnObject].children){
      mesg += thing + ": " + d.elements[this.text.drawnObject].children[thing] + "\n";
   }
   
   alert(mesg);*/
}

jbMeasurebox.prototype.show = function(){
   this.g.show();
}

jbMeasurebox.prototype.hide = function(){
   this.g.hide();
}

jbMeasurebox.prototype.posn = function(x1,x2,y){
   if(x1 > x2){
      var tmp = x1;
      x1 = x2;
      x2 = tmp;
   }
   var peak1text = "no peak";
   var peak2text = "no peak";
   var mz1 = this.zoomer.xToMz(x1);
   var mz2 = this.zoomer.xToMz(x2);
   mz1 = mz1.toFixed(this.zoomer.viewer.xaxis.ni.maxxl+2);
   mz2 = mz2.toFixed(this.zoomer.viewer.xaxis.ni.maxxl+2);
   
   var n1 = this.zoomer.findNearest(x1,y,1,1);
   var n2 = this.zoomer.findNearest(x2,y,1,1);
   
   if(n1.length > 0){
      var peak1;
      for(var i =0 ; i<n1.length; i++){
         if(n1[i].isHText){
            peak1 = n1[i].peak;
            mz1 = peak1.mz;
            if(peak1.fixedtext == "")
               peak1text = "unexplained peak";
            else
               peak1text = peak1.fixedtext;
            break;
         }
      }
   }
   if(n2.length > 0){
      var peak2;
      for(var i =0 ; i<n2.length; i++){
         if(n2[i].isHText){
            peak2 = n2[i].peak;
            mz2 = peak2.mz;
            if(peak2.fixedtext == "")
               peak2text = "unexplained peak";
            else
               peak2text = peak2.fixedtext;
            break;
         }
      }
   }
   
   var dmz = mz2 - mz1;
   dmz = dmz.toFixed(3);
   
   this.d.changeText(this.text.drawnObject, "Measure: "+dmz+" Th");
   this.d.changeText(this.textfrom.drawnObject, "From: "+mz1+" ("+peak1text+")");
   this.d.changeText(this.textto.drawnObject, "  To: "+mz2+" ("+peak2text+")");
   
   var mass1 = dmz;
   var mass2 = mass1 * 2;
   var mass3 = mass1 * 3;
   var mass4 = mass1 * 4;
   var mass5 = mass1 * 5;
   var mass6 = mass1 * 6;
   mass1 = parseFloat(mass1).toFixed(3);
   mass2 = parseFloat(mass2).toFixed(3);
   mass3 = parseFloat(mass3).toFixed(3);
   mass4 = parseFloat(mass4).toFixed(3);
   mass5 = parseFloat(mass5).toFixed(3);
   mass6 = parseFloat(mass6).toFixed(3); 
   
   this.d.changeText(this.textmass1.drawnObject, "z=1: "+mass1+" Da");
   this.d.changeText(this.textmass2.drawnObject, "z=2: "+mass2+" Da");
   this.d.changeText(this.textmass3.drawnObject, "z=3: "+mass3+" Da");
   this.d.changeText(this.textmass4.drawnObject, "z=4: "+mass4+" Da");
   this.d.changeText(this.textmass5.drawnObject, "z=5: "+mass5+" Da");
   this.d.changeText(this.textmass6.drawnObject, "z=6: "+mass6+" Da");
   
   x1 = this.zoomer.mzToX(mz1);
   x2 = this.zoomer.mzToX(mz2);
   
   var w = x2 - x1;
   var aa = this.spec.activeArea();
   var y1 = aa.y;
   var y2 = aa.y + aa.height;
   var h = aa.height;
   
   var posns = new Array(
             // x, y, w, h
      new Array(x1,y1,0, h), // left
      new Array(x2,y1,0, h),  // right
      new Array(x1,y2-40,w, 0)  // middle
   );
   
   for(var i=0; i<3; i++){
      var posn = posns[i];
      this.d.reposition(this.lines[i].drawnObject, posn[0], posn[1]);
      this.d.width(this.lines[i].drawnObject, posn[2]);
      this.d.height(this.lines[i].drawnObject, posn[3]);
   }
   var spacing = 10;
   var spaces = 20;
   
   this.d.reposition(this.text.drawnObject, (x1+x2)/2, y2-(spaces+=spacing));
   this.d.reposition(this.textfrom.drawnObject, (x1+x2)/2, y2-(spaces+=30));
   this.d.reposition(this.textto.drawnObject, (x1+x2)/2, y2-(spaces+=spacing));
   
   this.d.reposition(this.textmass1.drawnObject, (x1+x2)/2, y2-(spaces+=30));
   this.d.reposition(this.textmass2.drawnObject, (x1+x2)/2, y2-(spaces+=spacing));
   this.d.reposition(this.textmass3.drawnObject, (x1+x2)/2, y2-(spaces+=spacing));
   this.d.reposition(this.textmass4.drawnObject, (x1+x2)/2, y2-(spaces+=spacing));
   this.d.reposition(this.textmass5.drawnObject, (x1+x2)/2, y2-(spaces+=spacing));
   this.d.reposition(this.textmass6.drawnObject, (x1+x2)/2, y2-(spaces+=spacing));
}




function jbZoomer(viewer){
   this.viewer = viewer;
   this.list = new Array();
   this.stack = new Array();
   this.spec = viewer.spec;
   this.zoombox = new jbZoombox(this);
   this.measurebox = new jbMeasurebox(this);
	
   this.afterZoomCallback = function(){};
}


jbZoomer.prototype.zoomUndo = function(){
   if(this.stack.length > 1){
      this.stack.pop();
      var o = this.stack[this.stack.length - 1];
      this.doZoom(o.b,o.e);
   }
}

jbZoomer.prototype.totalUnZoom = function(){
   while(this.stack.length > 1){
      this.stack.pop();
   }
   var o = this.stack[0];
   this.doZoom(o.b,o.e);
}


jbZoomer.prototype.zoomIn = function(){   
   var dmz = this.e - this.b;
   var e = this.e - dmz/4;
   var b = this.b + dmz/4;
   this.zoom(b,e);
}

jbZoomer.prototype.panLeft = function(){   
   var dmz = this.e - this.b;
   var e = this.e - dmz/10;
   var b = this.b - dmz/10;
   this.zoom(b,e);
}

jbZoomer.prototype.panRight = function(){   
   var dmz = this.e - this.b;
   var e = this.e + dmz/10;
   var b = this.b + dmz/10;
   this.zoom(b,e);
}

jbZoomer.prototype.zoomOut = function(){   
   var dmz = this.e - this.b;
   var e = this.e + dmz/2;
   var b = this.b - dmz/2;
   this.zoom(b,e);
}


jbZoomer.prototype.zoom = function(b,e){   
   if(this.checkZoomRange(b,e)) return;
   
   var o = new Object();
   o.b = b;
   o.e = e;
   
   this.stack.push(o);
   this.doZoom(b,e);
	
}


jbZoomer.prototype.autoZoom = function(){   
   this.viewer.minmz = this.viewer.peaks[0].mz;
   this.viewer.maxmz = 0;
   for ( var i = 0 ; i < this.viewer.peaks.length ; i ++ ){
      if(this.viewer.peaks[i].mz < this.viewer.minmz){ this.viewer.minmz = this.viewer.peaks[i].mz; }
      if(this.viewer.peaks[i].mz > this.viewer.maxmz){ this.viewer.maxmz = this.viewer.peaks[i].mz; }
   }
   this.zoom(this.viewer.minmz - 100, this.viewer.maxmz + 100);
}

jbZoomer.prototype.checkZoomRange = function(b,e){
   var dx = e-b;
   var mag = log10(dx);
   if(mag < -6){
      alert("Sorry, you cannot zoom any further.");
      return 1;
   }
   return 0;
}

jbZoomer.prototype.doZoom = function(b,e){ // without affecting history!
   if(this.checkZoomRange(b,e)) return;
   
   this.viewer.xaxis.draw(b,e);
   
   var maxic = 0;
   var maxallic = 0;
   
   
   var mesg = "";
   
   for ( var i = 0 ; i < this.viewer.peaks.length ; i ++ ){
      if(
             this.viewer.peaks[i].mz > b 
            && this.viewer.peaks[i].mz < e
      ){
         this.viewer.peaks[i].show();
         this.viewer.peaks[i].reapplyMode();
         if(this.viewer.peaks[i].ic > maxic)
            maxic = this.viewer.peaks[i].ic;
         if(this.viewer.peaks[i].ic > maxallic)
            maxallic = this.viewer.peaks[i].ic;
      }
      else {
         this.viewer.peaks[i].hide();
         if(this.viewer.peaks[i].ic > maxallic)
            maxallic = this.viewer.peaks[i].ic;
      }
   }
   
   
   if(maxic == 0)
      return;
   var percent = 100 * maxic / maxallic;
   this.viewer.yaxis.draw(0,percent);
   this.viewer.yaxis2.draw(0, 1.2 * maxic); 
   
   
   
   this.viewer.spec.fit();
   var box = this.viewer.spec.activeArea();
   var mx = box.width / (e-b);
   var cx = box.x;
   var cy = box.y;
   var my = box.height / maxic;
   
   this.b = b;
   this.e = e;
   this.mx = mx;
   this.cx = cx;
   this.my = my;
   this.cy = cy;
   
   for ( var i = 0 ; i < this.viewer.peaks.length ; i ++ ){
      if(this.viewer.peaks[i].mz > b && this.viewer.peaks[i].mz < e){
         this.viewer.peaks[i].reposition(
            (this.viewer.peaks[i].mz - b) * mx   + cx, 
            cy, 
            my * this.viewer.peaks[i].ic
         );
      }
      else {
         this.viewer.peaks[i].reposition(-100, -100, 1);
      }
   }
   this.afterZoomCallback();
}

jbZoomer.prototype.xToMz = function(x){
   var aa = this.viewer.spec.activeArea();
   var dx = x - aa.x;
   var dmz = dx / this.mx;
   var mz = dmz + this.b;
/*   var mesg = new Array(
     "x", x, 
     "aa.x", aa.x, 
     "mx", this.mx, 
     "aa.w", aa.width, 
     "b", this.b, 
     "dx", dx, 
     "dmz", dmz, 
     "mz", mz);
      alert(mesg); */
   return  mz;
}

jbZoomer.prototype.mzToX = function(mz){
   var aa = this.viewer.spec.activeArea();
   var dmz = mz - this.b;
   var dx = dmz * this.mx;
   var x = dx + aa.x;
   return x;
}

jbZoomer.prototype.findNearest = function(x,y,disregardAnnotation,disregardY){   
   var mindistance = 10;
   var minpeak = null;
   
   var aa = this.viewer.spec.activeArea();
// alert(aa.x +" < "+ x +" && "+ x +" < "+ x2
//         +"\n&& "+ aa.y +" < "+ y +" && "+ y +" < "+ y2);
         
   var x2 = aa.x + aa.width;
   var y2 = aa.y + aa.height;
   if(aa.x < x && x < x2
         && aa.y < y && y < y2){
      for ( var i = 0 ; i < this.viewer.peaks.length ; i ++ ){
         if(this.viewer.peaks[i].fline.isVisible() 
            && this.viewer.peaks[i].mz > this.b 
            && this.viewer.peaks[i].mz < this.e
            && (this.viewer.peaks[i].hasAnnotation() || disregardAnnotation != null)
         ){         
            var px = (this.viewer.peaks[i].mz - this.b) * this.mx   + this.cx;         
            var py = this.viewer.peaks[i].ic * this.my   + this.cy;
            if(py > y || disregardY != null){
               var dx = px > x ? px - x : x - px;
               if(dx < mindistance){
                  mindistance = dx;
                  minpeak = this.viewer.peaks[i];
               }
            }
         }
      }
   }
   var sb = new Array();
   if(minpeak != null){
      sb.push(minpeak.ftext);
      sb.push(minpeak.fline);
      sb.push(minpeak.htext);
   }
   return sb;
}






function jbPeptidePair(parent, sequence1, sequence2, posn1, posn2, x ,y){ 
      //peptide sequences, linked positions in each peptide, position in parent
      // each residue matches: /([A-Z])(\d*)([a-z]*)/  where $1 is amino acid, $2 is label, $3 is modification
      // and this is taken care of by the jbPeptide object...
   this.parent = parent;
   this.llabel = parent.llabel;
   this.rlabel = parent.rlabel;
   this.viewer = this.parent;
   this.d = this.parent.d;
   this.sequence1 = sequence1;
   this.sequence2 = sequence2;
   this.posn1 = posn1;
   this.posn2 = posn2;
   this.y = y;
   this.x = x;

   this.hspacing = parent.pphspacing;
   this.vspacing = parent.ppvspacing;
   this.maxposn = posn1 > posn2 ? posn1 : posn2;
   if(posn1 > posn2){
      this.seq1posn = 0;
      this.seq2posn = posn1 - posn2;
   }
   else {
      this.seq2posn = 0;
      this.seq1posn = posn2 - posn1;
   }
   var array = new Array(this, sequence1, this.seq1posn, this.y, "PEPTIDE1");
   this.pep1y = this.y;
   this.pep2y = this.y - this.vspacing;
   this.viewer.pep1y = this.pep1y;
   this.viewer.pep2y = this.pep2y;
   this.pep1 = new jbPeptide(this, sequence1, this.seq1posn, this.pep1y, "PEPTIDE1");
   this.pep2 = new jbPeptide(this, sequence2, this.seq2posn, this.pep2y, "PEPTIDE2");
      
   this.pep1res0x = -100;
   this.pep2res0x = -100;
   
   if(this.pep1.fragmentlines.length > 0)
	this.pep1res0x = this.pep1.fragmentlines[0].absx;
   if(this.pep2.fragmentlines.length > 0)
	this.pep2res0x = this.pep2.fragmentlines[0].absx;
   
   this.viewer.pep1res0x = this.pep1res0x;
   this.viewer.pep2res0x = this.pep2res0x;
   
   var box1 = this.pep1.getBBox();
   var box2 = this.pep2.getBBox();
   var ly = box2.y + box2.height;
   var lh = box1.y - ly;
   if(posn1 > 0 && posn2 > 0)
	this.linker = new jbLinker(this, this.maxposn, ly, lh);
   else
	this.linker = new jbLinker(this, -10, ly, lh);
}

jbPeptidePair.prototype.getBBox = function(){
	var bbox = new Object();
	var box1 = this.pep1.getBBox();
	var box2 = this.pep2.getBBox();
	bbox.x = box1.x < box2.x ? box1.x : box2.x;
	bbox.y = box1.y < box2.y ? box1.y : box2.y;
	bbox.x2 = box1.x + box1.width > box2.x + box2.width ? box1.x + box1.width : box2.x + box2.width;
	bbox.width = bbox.x2 - bbox.x;
	bbox.y2 = box1.y + box1.height > box2.y + box2.height ? box1.y + box1.height : box2.y + box2.height;
	bbox.height = bbox.y2 - bbox.y;
	this.bbox = bbox;
	return bbox;
}

jbPeptidePair.prototype.move = function(dx,dy){
	dx = parseInt(dx);
	dy = parseInt(dy);
	this.x += dx;
	this.y += dy;
	this.pep1.move(dx,dy);	
	this.pep2.move(dx,dy);	
	this.linker.move(dx,dy);	
	
   this.pep1res0x = this.pep1.fragmentlines[0].absx;
   this.pep2res0x = this.pep2.fragmentlines[0].absx;
   
   this.viewer.pep1res0x = this.pep1res0x;
   this.viewer.pep2res0x = this.pep2res0x;
   
}


function jbPeptide(parent, sequence, posn, y, tags){ // peptide sequence... 
      // each residue matches: /([A-Z<>\[\]])(\d*)([^A-Z,]*)/  where $1 is amino acid, $2 is label, $3 is modification
      // modification can be any characters except whitespace, uppercase and comma
      // but different modification are split by whitespace
      // modifications of length greater than 3 (e.g. linker modification) will be painted differently
   this.parent = parent;
   this.tags = tags;
   this.viewer = this.parent.viewer;
   this.d = this.parent.d;
   sequence = sequence.replace( /[\(\)]/g, "" );
   var residues = sequence.split( /(?=[A-Z<>\[\]])/ );
   this.residues = new Array();
   this.fragmentlines = new Array();
   this.y = y;
   this.x = this.parent.x;
   this.hspacing = this.parent.hspacing;
   
   this.prN = new jbResidue(this, "", "", "", 0, "NTERMPROTEIN");
   this.pepN = new jbResidue(this, "", "", "", 0, "CTERMPROTEIN");
   this.prC = new jbResidue(this, "", "", "", 0, "NTERMPEPTIDE");
   this.pepC = new jbResidue(this, "", "", "", 0, "CTERMPEPTIDE");
   var minx = "";
   var maxx = "";
   var miny = "";
   var maxy = "";
   this.termy = -15;
   
   var p = 1;
   var yions = new Array();
   var bions = new Array();
   for ( var i = 0 ; i < residues.length+1 ; i ++ ){
      yions.push("R"+i);
      bions.push("L"+i);
   }
   var allyions = yions.join(" ");
   var allbions = bions.join(" ");
   
   if(sequence != ""){
	   for ( var i = 0 ; i < residues.length ; i ++ ){
	      var rlm = residues[i].replace( /([A-Z<>\[\]])(\d*)(.*)/ , "$1,$2,$3").split(/,/);
	      var cur;
	      var rbp = p;
	      var ryp = residues.length - rbp - 1;
	      
	      var mybions = bions.slice(rbp,  residues.length+1).join(" ");
	      var myyions = yions.slice(ryp,  residues.length+1).join(" ");

	// MESSAGE:      
	//  document.getElementById("messages").innerHTML += "i:" + i + " p:" + p + " rbp:" + rbp + " ryp" + ryp + " - " + mybions + " - " + myyions + "<br/>";
		   
	      
	      if(rlm[0] == "<")
		 cur = this.prN = new jbResidue(this, "", rlm[1], rlm[2], posn, this.termy, tags+" NTERMPROTEIN " + allbions); 
	      else if(rlm[0] == "[")
		 cur = this.pepN = new jbResidue(this, "", rlm[1], rlm[2], posn, this.termy, tags+" CTERMPROTEIN " + allyions); 
	      else if(rlm[0] == ">") 
		 cur = this.prC = new jbResidue(this, "", rlm[1], rlm[2], posn+residues.length, this.termy, tags+" NTERMPEPTIDE " + allbions); 
	      else if(rlm[0] == "]")
		 cur = this.pepC = new jbResidue(this, "", rlm[1], rlm[2], posn+residues.length, this.termy, tags+" CTERMPEPTIDE " + allyions); 
	      else {
		 cur = new jbResidue(this, rlm[0], rlm[1], rlm[2], p+posn, 0, tags+" AA" + p + " " + myyions + " " + mybions);
		 this.residues.push(cur); 
		 if(p>1)
		    this.fragmentlines.push(new jbFragLines(this, p-0.5+posn,  rbp-1, ryp, -7,  this.viewer.fragheight, 10, 10, tags + " F" + rbp, this.parent.llabel, this.parent.rlabel));
		 p++; 
	      }
	   }
   }
   this.posn = posn; // how many residue spaces to skip in order to line up with other peptide
}

jbPeptide.prototype.getBBox = function(){
   var bbox = new Object();
   if(this.residues.length > 0){
	    var box = this.residues[0].getAABBox();
	      bbox.x = box.x;
	      bbox.y = box.y;
	      bbox.x2 = box.x + box.width;
	      bbox.y2 = box.y + box.height;
	   for ( var i = 0 ; i <  this.residues.length ; i ++ ) {
	      var box = this.residues[i].getAABBox();
	      if(box.x < bbox.x) bbox.x = box.x;
	      if(box.y < bbox.y) bbox.y = box.y;
	      if(box.width + box.x > bbox.x2) bbox.x2 = box.width + box.x;
	      if(box.height + box.y > bbox.y2) bbox.y2 = box.height + box.y;
	   }
	   bbox.width = bbox.x2 - bbox.x;
	   bbox.height = bbox.y2 - bbox.y;
   }
   else {
	
   }
   return bbox;
}


jbPeptide.prototype.move = function(dx,dy){
	dx = parseInt(dx);
	dy = parseInt(dy);
	this.x += dx;
	this.y += dy;
   for ( var i = 0 ; i <  this.residues.length ; i ++ ) {
	   this.residues[i].move(dx,dy);
   }
   for ( var i = 0 ; i <  this.fragmentlines.length ; i ++ ) {
	   this.fragmentlines[i].move(dx,dy);
	   this.fragmentlines[i].absx = this.x + this.fragmentlines[i].x;
   }
   this.prN.move(dx,dy);
   this.pepN.move(dx,dy);
   this.prC.move(dx,dy);
   this.pepC.move(dx,dy);
}
/*
jbPeptide.prototype.reposition = function(){
   this.x = this.parent.x;
   for ( var i = 0 ; i <  this.residues.length ; i ++ ) {
	   this.residues[i].reposition();
   }
   for ( var i = 0 ; i <  this.fragmentlines.length ; i ++ ) {
	   this.fragmentlines[i].reposition();
   }
   this.prN.reposition();
   this.pepN.reposition();
   this.prC.reposition();
   this.pepC.reposition();
} */

jbPeptide.prototype.asString = function(){
   var arr = new Array();
   if(this.prN.aa)
      arr.push(this.prN.asString());
   if(this.pepN.aa)
      arr.push(this.pepN.asString());
   for ( var i = 0 ; i < this.residues.length ; i ++ ){
      arr.push(this.residues[i].asString());
   }
   if(this.pepC.aa)
      arr.push(this.pepC.asString()); 
   if(this.prC.aa)
      arr.push(this.prC.asString());
   return arr.join("\n");
}

function jbLinker(parent, posn, y, h){ // linked positions in each peptide
   this.parent = parent;
   this.viewer = this.parent.viewer;
   this.d = this.viewer.d;
   var w = this.parent.hspacing;
   this.hspacing = w;
   this.x = w * posn + this.parent.x;
   this.g = this.parent.d.group(this.x, y);
   this.fline = new jbSBItem(this,'line','#000000',1,"");
   this.fline.setLine(0,0,0,h,1);
   this.fline.setTags("PEPTIDE LINKER");
   this.parent.viewer.itemlist[this.fline.id] = this.fline;
}

jbLinker.prototype.move = function(dx,dy){
	this.g.move(dx,dy);	
}


/*

Residue should make a bunch of sbItems, and residue should then be added to the
viewer itemlist so it can be modified in a similar way to peaks.
So I suppose it should have modes too.  
Modes should be added as peaks are added.

*/

function jbResidue(parent, aa,label,mod, i, termy, tags){ // amino acid, modification, label
   this.parent = parent;
   this.viewer = this.parent.viewer;
   this.d = this.viewer.d;
   this.tags = tags;
   this.aa = aa;
   this.mod = mod.toUpperCase();
   this.label = label;
   this.termy = termy;
   this.i = i;
   this.mods = new Array();
   this.longmods = new Array();
   var tmods = mod.split(/\s+/);
   for ( var i = 0 ; i < tmods.length ; i ++ ){
      if(tmods[i].length > 3){
         this.longmods.push(tmods[i].toUpperCase());
      }
      else if(tmods[i].length > 0){
         this.mods.push(tmods[i].toUpperCase());
      }
   }
   var w = this.parent.hspacing;
   this.d = this.parent.d;
   this.x = this.i * w;
   this.y = this.termy;
//   var da = new Array(this.i,w,this.parent.y,this.x, this.y);
//   alert(da.join(","));
//   Fail();
   
   this.g = this.d.group(this.parent.x + this.x, this.parent.y + this.y);
   //alert(this.x);
   
   this.modtexts = new Array();
   // add an item 
   // then add it to this.viewer.itemlist...
   var moddist = 19;
   for ( var i = 0 ; i < this.mods.length ; i ++ ){
      var sbMod = new jbSBItem(this,'text','#999999',1,"font-family: Calibri; font-size:7pt;");
      this.modtexts.push(sbMod);
      this.viewer.itemlist[sbMod.id] = sbMod;
      sbMod.setText(this.mods[i], 0, moddist, 0,"middle");
      sbMod.setTags("PEPTIDE MOD "+tags);
      moddist += 8;
   }
   for ( var i = 0 ; i < this.longmods.length ; i ++ ){
      var sbMod = new jbSBItem(this,'text','#999999',1,"font-family: Calibri; font-size:7pt;");
      this.modtexts.push(sbMod);
      this.viewer.itemlist[sbMod.id] = sbMod;
      sbMod.setText(this.longmods[i], 0, moddist, -45,"start");
      sbMod.setTags("PEPTIDE MOD "+tags);
      moddist += 12;
   }
   if(aa.length > 0){
      this.aatext = new jbSBItem(this,'text','#000000',1,"font-family: Calibri; font-size:16pt;");
      this.aatext.setTags("PEPTIDE AA "+tags);
      this.aatext.setText(aa, 0, 0, 0,"middle");
      this.viewer.itemlist[this.aatext.id] = this.aatext;
   }
   if(label.length > 0){
      this.labeltext = new jbSBItem(this,'text','#000000',1,"font-family: Calibri; font-size:7pt;");
      this.labeltext.setTags("PEPTIDE LABEL "+tags);
      this.labeltext.setText(label, 6, -8, 0,"middle");
      this.viewer.itemlist[this.labeltext.id] = this.labeltext;
   }
   
}

jbResidue.prototype.move =  function(dx,dy){
	this.g.move(dx,dy);
}


jbResidue.prototype.reposition =  function(){
	this.g.reposition(this.parent.x + this.x, this.parent.y + this.y);
	alert(this.parent.x + " + " + this.x);
}


jbResidue.prototype.getBBox = function(){
   return this.g.getBBox();
}
jbResidue.prototype.getAABBox = function(){
   return this.aatext.getBBox();
}
jbResidue.prototype.join = function(j){
   return this.aa + j + this.label + j + this.mods.join("+") + j + this.longmods.join("+");
}
jbResidue.prototype.asString = function(){
   var str = this.aa;
   if(this.label.length){
      str += "\n  label:" + this.label;
   }
   if(this.mods.length){
      str += "\n  mods:" + this.mods.join(", ");
   }
   if(this.longmods.length){
      str += "\n  longmods:" + this.longmods.join(", ");
   }
   return str;
}

function jbFragLines(parent, p, rbp, ryp, yp, h, dx, dy, tags, llabel, rlabel){ 
   this.parent = parent;
   this.viewer = this.parent.viewer;
   this.tags = tags;
   this.d = this.viewer.d;
   this.p = p;
   this.rbp = rbp;
   this.ryp = ryp;
   this.h = h;
   this.yp = yp;
   this.dx = dx;
   this.dy = dy;
   var w = this.parent.hspacing;
   this.d = this.parent.d;
   this.x = this.p * w;
   this.absx = this.parent.x + this.x;
   this.y = 0;
   
   this.g = this.d.group(this.absx, this.parent.y + this.y);
   
   
   this.vert = new jbSBItem(this,'line','#999999',0,"");
   this.viewer.itemlist[this.vert.id] = this.vert;
   this.vert.setLine(0,this.yp,0,this.h+this.yp,1);
   this.vert.setTags(tags + " FRAG VERT" + " L" + rbp + " R" + ryp);
   
   this.right = new jbSBItem(this,'line','#999999',0,"");
   this.viewer.itemlist[this.right.id] = this.right;
   this.right.setLine(0,this.h+this.yp,this.dx,this.h+this.dy+this.yp,1);
   this.right.setTags(tags + " FRAG RIGHT" + " R" + ryp);
   
   this.left = new jbSBItem(this,'line','#999999',0,"");
   this.viewer.itemlist[this.left.id] = this.left;
   this.left.setLine(0,this.yp,0-this.dx,this.yp-this.dy,1);
   this.left.setTags(tags + " FRAG LEFT" + " L" + rbp);
   
   // this is where to add the text on the fragment lines!
   this.ylabel = new jbSBItem(this,'text','#999999',0,'');
   this.viewer.itemlist[this.ylabel.id] = this.ylabel;
   this.ylabel.setText(rlabel+ryp,0,this.h,-45,'start');
   this.ylabel.setTags(tags + " FRAGLABEL FRAG RIGHT" + " R" + ryp);
   
   this.blabel = new jbSBItem(this,'text','#999999',0,'');
   this.viewer.itemlist[this.blabel.id] = this.blabel;
   this.blabel.setText(llabel+rbp,0,-this.h,-45,'start');
   this.blabel.setTags(tags + " FRAGLABEL FRAG LEFT" + " L" + rbp);
}

jbFragLines.prototype.reposition = function(){
   this.absx = this.parent.x + this.x;
   this.vert.setLine(0,this.yp,0,this.h+this.yp,1);
   this.right.setLine(0,this.h+this.yp,this.dx,this.h+this.dy+this.yp,1);
   this.left.setLine(0,this.yp,0-this.dx,this.yp-this.dy,1);
	
}

jbFragLines.prototype.move = function(dx,dy){
	this.g.move(dx,dy);
}



function jbItemCollection(){
   this.itemlist = new Array();
}
jbItemCollection.prototype.addItem = function(item){
   this.itemlist.push(item);
}
jbItemCollection.prototype.addMode = function(mode,colour,visible,defaultStyle){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].addMode(mode,colour,visible,defaultStyle);
   }
}

jbItemCollection.prototype.setHover = function(mode,target,targetmode){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].setHover(mode,target,targetmode);
   }
}
jbItemCollection.prototype.setDefaultHover = function(target,targetmode){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].setDefaultHover(target,targetmode);
   }
}


jbItemCollection.prototype.setMode = function(mode,colour,visible,defaultStyle){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].setMode(mode,colour,visible,defaultStyle);
   }
}
jbItemCollection.prototype.changeMode = function(mode){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].changeMode(mode);
   }
}
jbItemCollection.prototype.reApplyMode = function(){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].reApplyMode();
   }
}
jbItemCollection.prototype.jbItemRememberModes = function(){
   var memory = new Array();
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      memory.push(this.itemlist[i].currentmode);
   }
   return memory;
}
jbItemCollection.prototype.jbItemRestoreModes = function(memory){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].changeMode(memory[i]);
   }
}

jbItemCollection.prototype.modeScan = function(modenamesarray){
   for ( var i = 0 ; i < this.itemlist.length ; i++ ){
      this.itemlist[i].modeScan(modenamesarray);
   }
}


var JBGLOBALEVENTMANAGER;

function JBGLOBALTIMEREVENT(){
	var timeOut = 100;
	if(JBGLOBALEVENTMANAGER){
		JBGLOBALEVENTMANAGER.viewer.timedEvents();
		timeOut = JBGLOBALEVENTMANAGER.viewer.timeOut;
	}
	JBGLOBALTIMEOUT = setTimeout("JBGLOBALTIMEREVENT();",timeOut);
}

var JBGLOBALTIMEOUT = setTimeout("JBGLOBALTIMEREVENT();",100);

function jbEventManager(v){
   this.viewer = v;
   this.sensors = new Array();
   this.moused = new Object();
//   this.leavemode = new Object();
   this.assocleavemode = new Object();
   this.mouseisdown = 0;
   this.isdragging = 0;
}
jbEventManager.prototype.setGlobal = function(){
   JBGLOBALEVENTMANAGER = this;
}
jbEventManager.prototype.mouseGlobalToLocal = function(x,y){
   return JBGLOBALEVENTMANAGER.viewer.d.convertXYPoint(x,y);
}

jbEventManager.prototype.dlbclick = function(e){
   for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
      if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
         JBGLOBALEVENTMANAGER.sensors[i].dblClickCallback(p);
      }
   }
}

// MOUSE DOWN
jbEventManager.prototype.mousedown = function(e){
   var p = JBGLOBALEVENTMANAGER.mouseGlobalToLocal(e.clientX, e.clientY);
   JBGLOBALEVENTMANAGER.lastDownPoint = p;
   JBGLOBALEVENTMANAGER.mouseisdown = 1;
   JBGLOBALEVENTMANAGER.lastEvent = "down";

   for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
      if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
         JBGLOBALEVENTMANAGER.sensors[i].mousedownCallback(p);
      }
   }
}


// MOUSE SCROLL
jbEventManager.prototype.mousescroll = function(e){
   // onmousewheel
   
   // normalize the delta
   var delta;
    if (e.wheelDelta) {
 
        // IE and Opera
        delta = e.wheelDelta / 60;
 
    } else if (e.detail) {
 
        // W3C
        delta = -e.detail / 2;
    }
    else {
       delta = 1;
    }
   
   var p = JBGLOBALEVENTMANAGER.mouseGlobalToLocal(e.clientX, e.clientY);
   JBGLOBALEVENTMANAGER.lastEvent = "scroll";
    
   for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
      if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
         JBGLOBALEVENTMANAGER.sensors[i].mousescrollCallback(p, delta);
      }
   }
}


var MULTICLICKTIMER;
var MULTICLICKFLAG;

// MOUSE UP
jbEventManager.prototype.mouseup = function(e){   
   var p = JBGLOBALEVENTMANAGER.mouseGlobalToLocal(e.clientX, e.clientY);
   JBGLOBALEVENTMANAGER.lastUpPoint = p;
   JBGLOBALEVENTMANAGER.mouseisdown = 0;
   JBGLOBALEVENTMANAGER.isdragging = 0;
   if(JBGLOBALEVENTMANAGER.lastEvent == "down"){
      if(MULTICLICKTIMER != null)
         clearTimeout(MULTICLICKTIMER);

      if(MULTICLICKFLAG){
         for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
            if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
               JBGLOBALEVENTMANAGER.sensors[i].dblClickCallback(p);
            }
         }
         MULTICLICKFLAG = 0;
//         document.getElementById("messages").innerHTML = "2 FLAG:" + MULTICLICKFLAG;
      }
      else {
         MULTICLICKFLAG = 1;
 //        document.getElementById("messages").innerHTML = "1 FLAG:" + MULTICLICKFLAG;
         setTimeout("MULTICLICKFLAG = 0;"
 //           + "document.getElementById(\"messages\").innerHTML = \"0 FLAG:\" + MULTICLICKFLAG;"
            ,500);
      }
      for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
         if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
            JBGLOBALEVENTMANAGER.sensors[i].clickCallback(p);
         }
      }
   }
   else if(JBGLOBALEVENTMANAGER.lastEvent == "move") {
      for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
         if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
            JBGLOBALEVENTMANAGER.sensors[i].dropCallback(
               p,
               JBGLOBALEVENTMANAGER.lastMovePoint,
               JBGLOBALEVENTMANAGER.lastDownPoint
            );
         }
      }
   }
   JBGLOBALEVENTMANAGER.lastEvent = "up";
}

// MOUSE MOVE
jbEventManager.prototype.mousemover = function(e){
   var p = JBGLOBALEVENTMANAGER.mouseGlobalToLocal(e.clientX, e.clientY);
   var matched = 0;
   if(JBGLOBALEVENTMANAGER.mouseisdown){
      for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
         if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
            matched = 1;
            if(JBGLOBALEVENTMANAGER.isdragging){
               JBGLOBALEVENTMANAGER.sensors[i].activeDragCallback(p,JBGLOBALEVENTMANAGER.lastMovePoint,JBGLOBALEVENTMANAGER.lastDownPoint);
            }
            else {
               JBGLOBALEVENTMANAGER.isdragging = 1;
               JBGLOBALEVENTMANAGER.sensors[i].firstDragCallback(p,JBGLOBALEVENTMANAGER.lastDownPoint);
            }
         }
      }
   }
   else {
      for(var i=0; i<JBGLOBALEVENTMANAGER.sensors.length; i++){
         if(JBGLOBALEVENTMANAGER.sensors[i].contains(p.x,p.y)){
            matched = 1;
            JBGLOBALEVENTMANAGER.sensors[i].motionCallback(p,JBGLOBALEVENTMANAGER.lastMovePoint);
         }
      }
   }
   if(matched == 0)   
      JBGLOBALEVENTMANAGER.viewer.d.cursor("default");
   JBGLOBALEVENTMANAGER.lastMovePoint = p;
   JBGLOBALEVENTMANAGER.lastEvent = "move";
}


jbEventManager.prototype.addEventSensor = function(x1,y1,x2,y2,sensor){
   // add a responsive area on the drawing... 
   // sensor must have the method findNearest(x,y)
   var sensor = new jbEventSensor(x1,y1,x2,y2,sensor);
   this.sensors.push(sensor);
   return sensor;
}

function jbEventSensor(x1,y1,x2,y2,sensor){
   // make sure 1 is less than 2!
   if(x1 < x2){
      this.x1 = x1;
      this.x2 = x2;
   }
   else {
      this.x1 = x2;
      this.x2 = x1;
   }
   if(y1 < y2){
      this.y1 = y1;
      this.y2 = y2;
   }
   else {
      this.y1 = y2;
      this.y2 = y1;
   }
   this.sensor = sensor;
   

/*
findNearest(x,y);
motionCallback(p, lastMovePoint);
mousedownCallback(p);
clickCallback(p);
firstDragCallback(p, lastDownPoint);
activeDragCallback(p, lastMovePoint, lastDownPoint);
dropCallback(p, lastMovePoint, lastDownPoint);
*/
   this.findNearest = function(x,y){return new jbItemCollection(); }

   this.mousedownCallback = function(p){}
   this.clickCallback = function(p){}
   this.dblClickCallback = function(p){}
   this.firstDragCallback = function(p, lastDownPoint){}
   this.activeDragCallback = function(p, lastMovePoint, lastDownPoint){}
   this.dropCallback = function(p, lastMovePoint, lastDownPoint){}
   this.mousescrollCallback = function(p, d){}
}


jbEventSensor.prototype.move = function(dx,dy){ 
	this.x1 += dx;
	this.x2 += dx;
	this.y1 += dy;
	this.y2 += dy;
}

// STOCK MOTION CALLBACK FOR MOST (ALL) SENSORS   
jbEventSensor.prototype.motionCallback = function(p, lastMovePoint){ 
   // collect info about stuff
   var res = this.findNearest(p.x,p.y);
   // gives a list of SBItems
   for(var j = 0; j < res.length; j++){
      if(JBGLOBALEVENTMANAGER.moused[res[j].id] == undefined)
         JBGLOBALEVENTMANAGER.moused[res[j].id] = "not hovered"; // previously not hovered

      if(JBGLOBALEVENTMANAGER.moused[res[j].id] == "not hovered"){   // previously not hovered
         // Entered!
         JBGLOBALEVENTMANAGER.moused[res[j].id] = "newly hovered"; // enter event
      }
      else {
         JBGLOBALEVENTMANAGER.moused[res[j].id] = "previously hovered"; // was hovered and still is!
      }
   }
   // unhover stuff
   for(id in JBGLOBALEVENTMANAGER.moused){
      if(JBGLOBALEVENTMANAGER.moused[id] == "hovered"){
         for(associd in JBGLOBALEVENTMANAGER.assocleavemode[id]){
            var lmode = JBGLOBALEVENTMANAGER.assocleavemode[id][associd];
            JBGLOBALEVENTMANAGER.viewer.itemlist[associd].changeMode(lmode);            
         }
         JBGLOBALEVENTMANAGER.assocleavemode[id] = undefined;
         JBGLOBALEVENTMANAGER.moused[id] = "not hovered"; // no longer
      }
   }
   // gives a list of SBItems
   for(var j = 0; j < res.length; j++){
      if(JBGLOBALEVENTMANAGER.moused[res[j].id] == "newly hovered"){
         JBGLOBALEVENTMANAGER.moused[res[j].id] = "hovered";
         
         var cmode = res[j].currentmode;
         var alm = new Object;
         for(associd in res[j].modes[cmode].hover){
            if(alm[associd] == undefined){
               alm[associd] =
                  JBGLOBALEVENTMANAGER.viewer.itemlist[associd].currentmode;
            }
         }
         JBGLOBALEVENTMANAGER.assocleavemode[res[j].id] = alm;
         res[j].doHover();
      }
      else if(JBGLOBALEVENTMANAGER.moused[res[j].id] == "previously hovered"){
         JBGLOBALEVENTMANAGER.moused[res[j].id] = "hovered";
      }
   }
}





jbEventSensor.prototype.contains = function(x,y){
//   alert(this.x1 +" < "+ x +" && "+ x +" < "+ this.x2
//         +"\n&& "+ this.y1 +" < "+ y +" && "+ y +" < "+ this.y2 +" ?");
   if(this.x1 < x && x < this.x2
         && this.y1 < y && y < this.y2)
      return true;
   return false;
}





function jbViewer(d,w,h){
   this.w = w;
   this.h = h;
   this.d = d;
   this.timeOut = 100;
   this.ppspeed = 0;
   this.itemlist = new Object();
   this.peaks = new Array();
   this.nonpeaks = new Array();
   this.spec = new jbSpectrum(d,5,5, w-10, h-180); // 150 for the peptides... might need to change this!
   this.zoomer = new jbZoomer(this);
	
   this.boxheight = 50;
   
   this.pphspacing = 30;
   this.ppvspacing = 90;
   this.fragheight = 30;
      
   this.xaxis = this.spec.axis("m/z",0);
   this.yaxis = this.spec.axis("% of base peak",1);
   this.yaxis2 = this.spec.axis("ion current",2);
   
   this.llabel = "L";
   this.rlabel = "R";
   
   this.nonpeaks.push(this.xaxis);
   this.nonpeaks.push(this.yaxis);
   this.nonpeaks.push(this.yaxis2);
   
   this.eventmanager = new jbEventManager(this);
   this.eventmanager.setGlobal();
   
   this.modetag = new Array();
   
   this.specsensor = this.eventmanager.addEventSensor(
      this.spec.x, this.spec.y, 
      this.spec.x + this.spec.w, this.spec.y + this.spec.h + 10
   );
   var zoomer = this.zoomer;
   this.specsensor.findNearest = function(x,y){
      return zoomer.findNearest(x,y);
   }
   
   var spec = this.spec;
   
   this.specsensor.mousescrollCallback = function(p,d){
      var aa = spec.activeArea();
      if(p.y < aa.y){  // zoomer
         if(d < 0)
            zoomer.zoomOut();
         else 
            zoomer.zoomIn();
      }
      else {// panner
         if(d > 0)
            zoomer.panLeft();
         else 
            zoomer.panRight();
      }
   }
   this.specsensor.firstDragCallback = function(p,o){
      var aa = spec.activeArea();
      if(p.y < aa.y && o.y < aa.y){  // zoomer
         zoomer.zoombox.posn(o.x,p.x);
         zoomer.zoombox.show();
         JBGLOBALEVENTMANAGER.viewer.d.cursor("col-resize");
      }
      else {// measurer
         zoomer.measurebox.posn(o.x,p.x,p.y);
         zoomer.measurebox.show();
         JBGLOBALEVENTMANAGER.viewer.d.cursor("e-resize");
      }
   }
   this.specsensor.activeDragCallback = function(p,l,o){
      var aa = spec.activeArea();
      if(p.y < aa.y && o.y < aa.y){ // zoomer
         zoomer.zoombox.posn(o.x,p.x);
         JBGLOBALEVENTMANAGER.viewer.d.cursor("col-resize");
      }
      else { // measurer
         zoomer.measurebox.posn(o.x,p.x,p.y);
         JBGLOBALEVENTMANAGER.viewer.d.cursor("e-resize");
      }
   }
   this.specsensor.dropCallback = function(p,l,o){
      JBGLOBALEVENTMANAGER.viewer.d.cursor("default");
      zoomer.zoombox.hide();
      var oldb = zoomer.b;
      var olde = zoomer.e;
      var b = zoomer.xToMz(p.x);
      var e = zoomer.xToMz(o.x);
      if(b > e){
         var t = e;
         e = b;
         b = t;
      }
      var aa = spec.activeArea();
      if(p.y < aa.y && o.y < aa.y){ // zoomer
         if(Math.abs(p.x - o.x) > 5)
            zoomer.zoom(b,e);
         else
            JBGLOBALEVENTMANAGER.viewer.specsensor.clickCallback(p);
      }
      else { // measurer
/*         zoomer.measurebox.hide();
         var meas = e - b;
         meas = meas.toFixed(zoomer.viewer.xaxis.ni.maxxl+2);
         alert(meas); */
      }
   }
   this.specsensor.motion2 = jbEventSensor.prototype.motionCallback;
   this.specsensor.motionCallback = function(p, lastMovePoint){ 
      zoomer.measurebox.hide();
      var aa = spec.activeArea();
      if(p.y < aa.y)
         JBGLOBALEVENTMANAGER.viewer.d.cursor("url("+ZOOMGIF+") 6 4, crosshair");
//         JBGLOBALEVENTMANAGER.viewer.d.cursor("url(zoom.gif) 6 4, crosshair");
      else
         JBGLOBALEVENTMANAGER.viewer.d.cursor("default");
      
      JBGLOBALEVENTMANAGER.viewer.specsensor.motion2( p, lastMovePoint);
   }
   
   
      
   this.fragsensor = this.eventmanager.addEventSensor(
      0, 0, 
      this.w, this.h
   );
   
   
   this.fragsensor.contains = function(px,py){
      var viewer = JBGLOBALEVENTMANAGER.viewer;
         
      var fsx = viewer.ppx;
      var fsw = viewer.pphspacing * viewer.ppl;
      var fsy = viewer.ppy - viewer.ppvspacing - viewer.fragheight;
      var fsh = viewer.ppvspacing + viewer.fragheight * 2;

      if(fsx < px && px < fsx + fsw &&
            fsy < py && py < fsy + fsh)   
         return 1;
      return 0;
   }
   
   this.fragsensor.clickCallback = function(){
      if(JBGLOBALEVENTMANAGER.fragsensormouseover){
         if(JBGLOBALEVENTMANAGER.fragsensorclicktoggle == null)
            JBGLOBALEVENTMANAGER.fragsensorclicktoggle = 1;
         else
            JBGLOBALEVENTMANAGER.fragsensorclicktoggle = null;
      }
   }
   
   
   this.fragsensor.dblClickCallback = function(p){
      var pep1 = JBGLOBALEVENTMANAGER.viewer.peptidepair.pep1;
      var pep2 = JBGLOBALEVENTMANAGER.viewer.peptidepair.pep2;
      var pep = "";
      var R = "";
      for(var i = 0; i < pep1.residues.length; i++){
         if(pep1.residues[i].aatext != null){
            var bb = pep1.residues[i].aatext.getBBox();
            if(bb.x < p.x && p.x < bb.x + bb.width
            && bb.y < p.y && p.y < bb.y + bb.height){
               pep = "PEPTIDE1";
               R = pep1.residues[i].tags.replace(/^.*?\b(R\d+)\b.*?$/,"$1");
               break;
            }
         }
      }
      if(pep == ""){
         for(var i = 0; i < pep2.residues.length; i++){
            if(pep2.residues[i].aatext != null){
               var bb = pep2.residues[i].aatext.getBBox();
               if(bb.x < p.x && p.x < bb.x + bb.width
               && bb.y < p.y && p.y < bb.y + bb.height){
                  pep = "PEPTIDE2";
                  R = pep2.residues[i].tags.replace(/^.*?\b(R\d+)\b.*?$/,"$1");
                  break;
               }
            }
         }
      }
      if(pep != "")
         JBGLOBALEVENTMANAGER.viewer.handleResidueDblClick(pep,R);
   }
   
   
   this.handleResidueDblClick = function(pep,R){
   }
   
   this.fragsensor.motionCallback = function(p,lp){
      JBGLOBALEVENTMANAGER.viewer.d.cursor("pointer");
      var viewer = JBGLOBALEVENTMANAGER.viewer;
      var ab = null;
      var pep = null;
      var ii = null;
      var pepl = null;
      var peprex0x = null;
      var ppmargin = JBGLOBALEVENTMANAGER.viewer.ppmargin;
      if(p.x < ppmargin){ // move left (move it right)
	      JBGLOBALEVENTMANAGER.viewer.setPPSpeed((ppmargin-p.x)/3);
      }
      else if(p.x > viewer.w - ppmargin){ // move right (move it left)
	      JBGLOBALEVENTMANAGER.viewer.setPPSpeed((viewer.w - ppmargin - p.x)/3);
      }
      else { // stop moving
	      JBGLOBALEVENTMANAGER.viewer.setPPSpeed(0);	      
      }
      if(viewer.pep1y < p.y && p.y < viewer.pep1y + viewer.fragheight){
         ab = "alpha";
         pep = "PEPTIDE1";
         peprex0x = viewer.pep1res0x;
         pepl = viewer.pep1l;

      }
      else if(viewer.pep2y < p.y && p.y < viewer.pep2y + viewer.fragheight){
         ab = "beta";
         pep = "PEPTIDE2";
         peprex0x = viewer.pep2res0x;
         pepl = viewer.pep2l;
      }
      if(pepl != null){

         var relative_position = (p.x - peprex0x) / viewer.pphspacing;
         var fragment_index = Math.round(relative_position);
         var residue_index = Math.floor(relative_position+1);

         var position_from_fragment = relative_position - fragment_index;
         var position_from_residue = relative_position + 0.5 - residue_index;

         var fragment_abs_dev = Math.abs(position_from_fragment);
         var residue_abs_dev = Math.abs(position_from_residue);

         if(fragment_index >= 0 && fragment_index < pepl - 2
               && fragment_abs_dev < 0.2                          ){
            ii = fragment_index;
         }
         else if(residue_index >= 1 && residue_index < pepl - 1
               && residue_abs_dev < 0.2                           ){
            ii = [residue_index-1, residue_index];
         }


      }
      if(ii != null && ab != null){
         if(JBGLOBALEVENTMANAGER.fragsensormouseover != null
                && JBGLOBALEVENTMANAGER.fragsensormouseover != ab+" "+ii
                     && JBGLOBALEVENTMANAGER.fragsensorclicktoggle == null){
            viewer.modeRestore();
            JBGLOBALEVENTMANAGER.fragsensormouseover = null;
            viewer.fragsensor.mouseout(viewer);
         }
         if(JBGLOBALEVENTMANAGER.fragsensormouseover == null){
            var oPep;
            if(ab == "alpha")
               oPep = viewer.peptidepair.pep1;
            else
               oPep = viewer.peptidepair.pep2;


            JBGLOBALEVENTMANAGER.fragsensormouseover = 1;
            viewer.modeMemorize();

            if(typeof(ii) === "object"){
               frag = new Array(
                  oPep.fragmentlines[ii[0]],
                  oPep.fragmentlines[ii[1]]
               );
            }
            else {
               frag = new Array(oPep.fragmentlines[ii]);
            }
            viewer.fragsensor.mouseover(viewer, frag, ab, pep, ii);
            

         }
      }
      else if(JBGLOBALEVENTMANAGER.fragsensormouseover
                     && JBGLOBALEVENTMANAGER.fragsensorclicktoggle == null){
         JBGLOBALEVENTMANAGER.fragsensormouseover = null;
         viewer.modeRestore();
         viewer.fragsensor.mouseout(viewer);
      }
   }
   
   this.fragsensor.mouseout = function(viewer){
   }
   
   this.fragsensor.mouseover = function(viewer, frag, ab, pep, ii){

   }
   
}

jbViewer.prototype.getBBox = function(){
   
}

jbViewer.prototype.getWidth = function(){
   return this.w;
}
jbViewer.prototype.getHeight = function(){
   return this.h;
}

jbViewer.prototype.registerModeTag = function(mode, pep, ion){
   this.modetag.push(new Array(pep, ion, mode));
}

jbViewer.prototype.modesFromTags = function(pep, ion){
   var res = new Array();
   for(var i = 0; i < this.modetag.length; i++){
      if(pep == this.modetag[i][0] && ion == this.modetag[i][1])
         res.push(this.modetag[i][2]);
   }
   return res;
}

jbViewer.prototype.modeScanAll = function(modenamesarray){
   this.modeScan(this.itemlist, modenamesarray);
}

jbViewer.prototype.modeScan = function(itemlist, modenamesarray){
   for ( itemname in itemlist ){
      itemlist[itemname].modeScan(modenamesarray);
   }
}

jbViewer.prototype.modeMemorize = function(){
   this.modememory = new Object;
   for ( itemname in this.itemlist ){
      this.modememory[itemname] = this.itemlist[itemname].currentmode;
   }
}

jbViewer.prototype.modeRestore = function(){
   for ( itemname in this.itemlist ){
      this.itemlist[itemname].changeMode(this.modememory[itemname]);
   }
}

jbViewer.prototype.searchTags = function(tags){
//   var searchreport = "tags:\n";
   var itemcoll = new jbItemCollection();
   var groups = tags.split(/\|/); // enable to roll multiple searches together...
   for(var k=0; k<groups.length; k++){
//      searchreport += "groups " + k + ": " + groups[k] +"\n";
      var searchitems = groups[k].split(/\s+/);
      var selects = new Array();
      var filters = new Array();
      for(var j=0; j<searchitems.length; j++){
         if(searchitems[j].match(/^!/)){
            filters.push("\\b" + searchitems[j].replace(/^!/,"") + "\\b");
         }
         else if(searchitems[j].match(/\S/)) { // must contain something!
            selects.push("\\b" + searchitems[j] + "\\b");
         }
      }
      
      for ( itemname in this.itemlist ){ // FOR EACH ITEM
//         searchreport += "item: " + itemname +"\n";
         var taggroups = this.itemlist[itemname].tags.split(/\|/);
         for(var i=0; i<taggroups.length; i++){ // FOR EACH TAGSET IN THAT ITEM
            var selectmatch = 1;
            var filtermatch = 0;
            var taggroup = taggroups[i];
//            searchreport += "taggroup " + i + ": " + taggroup +"\n";
            for(var j=0; j<filters.length; j++){
               if(taggroup.match(filters[j])){
//                  searchreport += "!!filtermatch " + j + ": " + taggroup + " =~ "+filters[j] +"\n";
                  filtermatch = 1;
                  break;
               }
            }
            
            if(filtermatch) continue; // if this tagset matches a !tag
            // no !tags so see if any tags match
            
            for(var j=0; j<selects.length; j++){
               if(! taggroup.match(selects[j])){ // if it doesn't match
//                  searchreport += "!!selects!match " + j + ": " + taggroup + " !~ "+selects[j] +"\n";
                  selectmatch = 0; // don't bother
                  break;
               }
               else {
//                  searchreport += "selectsmatch " + j + ": " + taggroup + " !~ "+selects[j] +"\n";
               }
            }
            
            if(selectmatch){ 
               /* if we got this far and the selectmatch flag is still true (1)
                * then there must be all the selection tags and none of the 
                * filter !tags
                */
//               searchreport += "Matches :-)\n";
               itemcoll.addItem(this.itemlist[itemname]);
            }
            
         }
      }
   }
//   itemcoll.searchreport = searchreport;
   return itemcoll;
}
jbViewer.prototype.addPeak = function(mz, ic, fixedtext, hovertext){
   var peak = new jbPeak(this, mz, ic, fixedtext, hovertext);
   peak.index = this.peaks.length;
   // add a peak...
   this.peaks.push(peak);
   // but also add it to this.zoomer.list ...
   this.zoomer.list.push(peak);
   return peak;
}
jbViewer.prototype.zoom = function(b,e){
   this.zoomer.zoom(b, e);
}
jbViewer.prototype.autoZoom = function(){
   this.zoomer.autoZoom();
}

jbViewer.prototype.setPeptides = function(sequence1, sequence2, posn1, posn2, x, y){
   this.peptidepair = new jbPeptidePair(this, sequence1, sequence2, posn1, posn2, x, y);
   this.ppx = x;
	this.ppmargin = x;
   this.ppy = y;
   this.pep1l = sequence1.length;
   this.pep2l = sequence2.length;
   var maxpl = sequence1.length > sequence2.length ? sequence1.length : sequence2.length;
   var maxpp = posn1 > posn2 ? posn1 - posn2 : posn2 - posn1;
   this.ppl = maxpl + maxpp;
   this.peptidepair.getBBox();
   return this.peptidepair;
}
jbViewer.prototype.movePeptides = function(dx,dy){
	this.peptidepair.move(dx,dy);
	this.fragsensor.move(dx,dy);
	this.ppx += dx;
	this.ppy += dy;
}

jbViewer.prototype.setPPSpeed = function(speed){
	this.ppspeed = speed;
}
jbViewer.prototype.ppSpeed = function(speed){
	this.ppspeed = speed;
	if(! this.peptidepair ) return;
	if(speed == 0)
		return;
	if(speed > 0 && this.ppx + speed > this.ppmargin){ // its already or will be too far to the right... move it to max posn and stop
		this.movePeptides(this.ppmargin - this.ppx, 0);
		this.ppspeed = 0;
		return;
	}
	if(speed < 0 && this.ppx + this.peptidepair.bbox.width + speed < this.w - this.ppmargin){ // its already or will be too far to the left... move it to max posn and stop
		this.movePeptides( this.w - this.ppmargin - this.peptidepair.bbox.width - this.ppx, 0);
		this.ppspeed = 0;
		return;
	}
	// all ok?  let's go:
	this.movePeptides(speed,0);
}

jbViewer.prototype.timedEvents = function(){
	this.ppSpeed(this.ppspeed);
}



function ObjectDumper(obj){
   var mesg = "";
   for(thing in obj){
      mesg += thing+"="+obj[thing]+"\n";
   }
   return mesg;
}




