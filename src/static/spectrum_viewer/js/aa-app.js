
var aa;
var app;
var tfb;
var svgdiv;

var SVGW;
var SVGH;





/*
pd0titleP1 pd0titleP2 pd0titleX pd0titleA    |    pd0A0  |   pd0X0
  pd0p1a            |         pd0p1b                    |    pd0A1  |   pd0X1
  pd0p2a            |         pd0p2b                    |    pd0A2  |   pd0X2
  pd0links                                                     |    pd0A3  |   pd0X4
*/

var pdShows = {
	"pd0titleP1" : 	[1, 0, 0, 0],
	"pd0titleP2" : 	[0, 1, 0, 0],
	"pd0titleX" : 	[0, 0, 1, 0],
	"pd0titleA" : 	[0, 0, 0, 1],
	"pd0A0" : 	[0, 0, 0, 1],
	"pd0X0" : 	[0, 0, 1, 1],
	"pd0p1a" : 	[1, 1, 1, 1],
	"pd0p1b" : 	[1, 1, 1, 1],
	"pd0A1" : 	[0, 0, 0, 1],
	"pd0X1" : 	[0, 0, 1, 1],
	"pd0p2a" : 	[0, 1, 1, 1],
	"pd0p2b" : 	[0, 1, 1, 1],
	"pd0A2" : 	[0, 0, 0, 1],
	"pd0X2" : 	[0, 0, 1, 1],
	"pd0links" : 	[1, 1, 1, 1],
	"pd0A3" : 	[0, 0, 0, 1],
	"pd0X4" : 	[0, 0, 1, 1],
};
var pdChecks = {
	"ispep2" : [0, 1, 1, -1],
	"isxlink" : [0, 0, 1, -1],
}

function psShowCheck(i){
	for(id in pdShows){
		document.getElementById(id).style.display = pdShows[id][i] == 1 ? 'block' : 'none';
	}
	for(id in pdChecks){
		if(pdChecks[id][i] >= 0)
			document.getElementById(id).checked = pdChecks[id][i] == 1 ? true : false;
	}
}

var matchtablewin;
function showMatches(){
	matchtablewin.show();
	var et = matchtablewin.EditTable("matches...");
	et.setData(aa.mtab);
	//et.setHeaderRow(tfb.headerrow);
	/*
	var buttons = fr.e.addElement("div",{"class" : "jbAppTableFrameButtonButtons"},null);
	var ok = buttons.addElement("a",{"class" : "jbAppTableFrameButtonOK"},"OK");
	var cancel = buttons.addElement("a",{"class" : "jbAppTableFrameButtonCancel"},"Cancel");
	
	ok.onclick = jbAppTableFrameButtonOK;
	ok.jbAppTableFrameButton = tfb;
	ok.jbAppTableFrameButtonFrame = fr;
	ok.jbAppEditTable = et;
	
	cancel.onclick = jbAppTableFrameButtonCancel;
	cancel.jbAppTableFrameButton = tfb;
	cancel.jbAppTableFrameButtonFrame = fr;*/
}


var tabframes;
var togglebuttons;


function run(){
      
   var myPepE = document.getElementById("myPeptide");
   var myPLE = document.getElementById("myPeaklist");
   
   var alertSetup = 0;
   if(myPepE && myPLE){
      document.getElementById("pepent1").value = myPepE.value;
      document.getElementById("specent").value = myPLE.value;
      document.getElementById("ispep2").checked = false;
      document.getElementById("isxlink").checked = false;
   }  
   else if(window.location.toString().match(/xipost/)) {
      document.getElementById("ispep2").checked = true;
      document.getElementById("isxlink").checked = true;
   }
   else if(! window.location.toString().match(/posted/)) {
      document.getElementById("pepent1").value = "";
      document.getElementById("specent").value = "";
      document.getElementById("ispep2").checked = false;
      document.getElementById("isxlink").checked = false;
      
      alertSetup = 1;
   }
   
   
   var myTolerance = document.getElementById("myTolerance");
   var myPrecursorZ = document.getElementById("myPrecursorZ");
   if(myTolerance && myPrecursorZ){
      document.getElementById("ppm").value = myTolerance.value;
      document.getElementById("maxz").value = myPrecursorZ.value;
   }

/* Attempting to allow saving in browser... current doesn't work :-(
	// has this page already been saved and therefore already has all the stuff we're adding below?
	var deletesaved = ["svgdrawing","jbAppElement_0"];
	for(i in deletesaved){
		var todel = document.getElementById(deletesaved[i]);
		if(todel != null)
			todel.parentNode.removeChild(todel);
	}
*/	
   
   var taTabIs = [
      "specent", "formulae", "e", "iontable", "losslist", "outputs"
   ];
   for(var i=0; i<taTabIs.length; i++){
      document.getElementById(taTabIs[i]).onkeypress = checkTab;
   }
   
	
	app = document.getElementById("appContainer").jbApp();	
	
	// menu...  specent, formulae, outputs, e, o
	var mtes = {
		"peptidetable": "Sequence",
		"specentcontainer":"Spectrum", 
		"formulaecontainer":"Formulae", 
		"econtainer":"Elemental Isotopic Distribution", 
      "iontablecontainer":"Ion Types",
		"losslistcontainer":"Losses",
      "colourtablecontainer":"Peak Colours",
      
		"checkform" : "Peaks Shown",
		"outputscontainer":"Debug Info", 
		//"toolbar":"toolbar",
      "mzDP":"mzDP",
      "zoomexact":"zoomexact",
      
		"helpAbout":"About",
		"helpGettingStarted":"Getting Started",
      
		"ocontainer":"o2",
	};
   
   tabframes = {
      "Data Setup": ["peptidetable", "specentcontainer", "formulaecontainer", "econtainer", "iontablecontainer", "losslistcontainer"],
      "Visual Settings": ["checkform", "colourtablecontainer", "mzDP",  "zoomexact", "outputscontainer"],
      "Help": ["helpAbout", "helpGettingStarted"]
   };
   
   togglebuttons = {
      "Data Setup" : "databutton",
      "Visual Settings": "visbutton",
      "Help": "helpbutton"
   };
   
	for(tf in tabframes){
		var aw = new jbAppWindow(app, tf, null);
      var togglefunction = "ToggleWindow('"+tf+"'); ";
      document.getElementById(togglebuttons[tf]).setAttribute("onclick", togglefunction);
      aw.setCloser(togglefunction);
            
      var tfr = aw.TabFrame("","Horizontal");
      tfr.tfName = tf;
      for(var i=0; i<tabframes[tf].length; i++){
         var te = tabframes[tf][i];
         var t = document.getElementById(te);
         t.parentNode.removeChild(t);
         var tab = tfr.addTab(mtes[te], t);
         mtes[te] = t;
      }
      jbAppTabEvent(tfr.e.id, tabframes[tf][0]);
      jbAppTabEvent(tfr.e.id, tabframes[tf][0]);
      tabframes[tf] = aw;
	}
	
	//document.getElementById("pd0linksSpeclink").onclick =	function () {mtes["specent"].show();}
   
	// no more menu
		
	addSVG();
	di = new jbSvgDrawingInterface(document, svg);
	d = new jbDrawing(di);
	
   
   // open the correct box depending on the data...
   if(document.getElementById("ispep2").checked){
      if(document.getElementById("isxlink").checked){
         psShowCheck(2);  peptidetableTabs(2);
      }
      else {
         psShowCheck(1);  peptidetableTabs(1);
      }
   }
   else {
      psShowCheck(0);  peptidetableTabs(0);
   }
   
   tabframes["Data Setup"].at(300,100);
   tabframes["Visual Settings"].at(300,100);
   tabframes["Help"].at(300,100);
   
   if(window.location.toString().match(/http.*export-html.php/)){
      alert("OK... now you're on the export page.\nChoose save-as from your browser's file menu,\nand select 'Web Page Only' before saving.");
      doParseSpec();
   }
   else if(alertSetup){
      ToggleWindow("Data Setup");
      alert("Please enter spectrum and peptide sequence in the data setup window");
   }
   else if(myPepE && myPLE){
      doParseSpec();
   }
   else if(document.getElementById("pepent1").value.length > 0
            && document.getElementById("specent").value.length > 0) {
      doParseSpec();
   }
}


// global cheating function...
function showWindowedTab(tabcontentid){
   var tc = document.getElementById(tabcontentid);
   var tf = tc.parentNode.parentNode.jbAppTabFrame;
   var w = tc.parentNode.parentNode.parentNode.parentNode.jbAppWindow;
   var tfn = tf.tfName;
   if(w.w.style.display == "none")
      ToggleWindow(tfn);
   jbAppTabEvent(tf.e.id, tabcontentid);
}
function ToggleWindow(tf){
   var w = tabframes[tf];
   var button = document.getElementById(togglebuttons[tf]);
   var buttonstyle = "toolbutton";
   if(w.w.style.display == "none"){
      w.show();
   var buttonstyle = "activetoolbutton";
   }
   else {
      w.hide();
   }
   button.setAttribute("class", buttonstyle);
}

// SALMAN, L00K:
// YOU NEED TO MODIFY THIS SO IT ADDS THE SVG IN THE CORRECT ELEMENT:
function addSVG(){
	
   var marginx = 10;
   var marginy = 22;	
	
   var w = SVGW = $('#id_content').width() - marginx;
   var h = SVGH = window.innerHeight * 0.6 - marginy;
	
   svg = document.createElementNS("http://www.w3.org/2000/svg","svg");
   svg.setAttribute("width", w);
   svg.setAttribute("height", h);
   svg.setAttribute("id", "svgdrawing");
   svg.setAttribute("style", "width:"+w+"px;height:"+h+"px;background:white;border:solid 1px black;");
   document.getElementById("container").appendChild(svg);
   return svg;
}

var namedColours = {
   "green" : "#00FF00",
   "blue" : "#0000FF",
   "orange" : "#FF9900",
   "brown" : "#995500",
   "red" : "#FF0000",
   "purple" : "#990099",
   "black" : "#000000",
};

function doParseSpec(){	
	aa = new AA(); // defined below
	

   
	aa.formulae = parseTable(document.getElementById("formulae").value);
   if(aa.formulae.length == 0){ return alert("ERROR: Formula table empty!!"); }
   
	aa.iso = new Isotopes(parseTable(document.getElementById("e").value));
   if(aa.iso.length == 0){ return alert("ERROR: Isotope table empty!!"); }
   
   aa.mzDP = parseInt(document.getElementById('mzDPin').value);
	
	var rawfilename = "test";
	var scannumber = "1";
	var crosslinker = "testing";
	var proteinname1 = "protein 1";
	var proteinname2 = "protein 2";
	var peptide1 = "[" + document.getElementById("pepent1").value + "]";
	var peptide2 = document.getElementById("ispep2").checked ? "[" + document.getElementById("pepent2").value + "]" : '';
	var linkerpos1 = parseInt(document.getElementById("resent1").value);
	var linkerpos2 = parseInt(document.getElementById("resent2").value);
	var xcheck = document.getElementById("isxlink").checked;
	var lossestab = document.getElementById("losslist").value;
   var iontable = document.getElementById("iontable").value;
   
   
   // figure out whether we want b/y or c/z
   resetSVG(); // need this before we can do anything with v
   var iontablerows = parseTable(iontable);
   if(iontablerows.length == 0){ return alert("ERROR: Ion table empty!!"); }
   
   v.llabel = "b";
   v.rlabel = "y";
   var ionsY = {};
   for(var i=0; i<iontablerows.length; i++){
      // if it contains c/z then change labels to c/z!
      if(! iontablerows[i][0] ) continue; // should be the type
      if(! iontablerows[i][1].match(/y/i) ) continue; // should say yes
      ionsY[iontablerows[i][0]] = 1;
   }
   if(ionsY['c'] && ionsY['z']){
      v.llabel = 'c';
      v.rlabel = 'z';
   }
	
	var spectrum = aa.parse_spec(document.getElementById("specent").value);
   if(spectrum.length == 0){ return alert("ERROR: Spectrum empty!!"); }
   
	var linkermass = 138.06807;
	var z = 4;
	var maxzdev = 2;
	var ppm = parseFloat(document.getElementById("ppm").value);
   
   var tolu = "ppm";
   if(ppm < 0){
      tolu = "Da";
      ppm *= -1;
   }
	
	if(! xcheck){ // checkbox says don't xlink...
		linkerpos1 = linkerpos2 = linkermass = 0;
	}
   
   var params = {
      "maxz" : parseInt(document.getElementById("maxz").value), "minz" : 1,
      "fragrounds" : 1,
      "maxlosses" : 2
   };
   
   // generate fragments:
   var fr = aa.vfragmentxlinkpeptide2(peptide1,peptide2,linkerpos1,linkerpos2, lossestab, iontable, params);
   
   // match fragments and spectrum:
   var matched_2 = aa.match2(fr, spectrum, ppm, tolu);
   
   
   // figure out colours:
   parseColourTable(); // put it into a global hash, colours
   // descriptive writing
   if(!xiSPEC_Title) xiSPEC_Title = 'xiSPEC Standalone';
   if(!xiSPEC_SubTitle) xiSPEC_SubTitle = 'Xi Spectrum Viewer';
   if(!xiSPEC_TitleNote) xiSPEC_TitleNote = 'jimi-carlo@ed.ac.uk';
   var g = d.group(SVGW-20,SVGH-15);
   g.text("customtext1",0,0,"end",0,"grey",
      "font-family:Arial;font-size:10pt;",
      xiSPEC_Title);
   
   g.text("customtext2",0,-13,"end",0,"grey",
      "font-family:Arial;font-size:8pt;",
      xiSPEC_SubTitle);
   
   g.text("customtext3",0,-26,"end",0,"grey",
      "font-family:Arial;font-size:8pt;",
      xiSPEC_TitleNote);
   
   
   // double check:
   if(linkerpos1 == null)
      linkerpos1 = "0";
   if(linkerpos2 == null)
      linkerpos2 = "0";
   if(peptide1 == null)
      peptide1 = "";
   if(peptide2 == null)
      peptide2 = "";
   
   // set up name subs... e.g. symbols for losses, remove alpha for linear peptides
   var ALPHA = String.fromCharCode(0x2090);
   var namesubs = {};
   if(! peptide2)
      namesubs[ALPHA] = "";
   var losses = parseTable(lossestab);
   for(var i=1; i<losses.length; i++){
      if(! losses[i][0]) continue;
      namesubs[losses[i][0]] = losses[i][1];
   }
   
   // set up peptides.
   setPeps(peptide1,peptide2,linkerpos1,linkerpos2);
   var matchedlosses = aa.makePeaks2(matched_2,peptide1,peptide2,colours,namesubs);
   
   // key for matched losses
   var ty = -40;
   var lossestitledone = 0;
   for(var p in matchedlosses){
      if(! lossestitledone){      
         g.text("losses",-80,ty-=13,"start",0,"grey",
            "font-family:Arial;font-size:8pt;",
            "Losses:");
         lossestitledone = 1;
      }
      g.text("loss"+p,-80,ty-=13,"start",0,"grey",
         "font-family:Arial;font-size:8pt;",
         "\t"+matchedlosses[p]+"\t"+p);
   }
   
   // hover residues
   hoverConnect("PEPTIDE1 L3", "PEPTIDE1 R2", "SEL");

   hookEvents(svg,v);
   v.autoZoom();
   resetSonsor();
   
   //// need to reset and handle checks!
   resetChecks();
   handleChecks2(); // look up the checkboxes' values and act on them!
   v.zoomer.afterZoomCallback = handleChecks2;
   
   logtext("===SVG===", 
      document.getElementById("container").innerHTML);

   return;
   
   // sin e!
}


var hoverConnect = function(sourceTags, targetTags, targetMode){
   var sources = v.searchTags(sourceTags);
   var targets = v.searchTags(targetTags);
   for ( var i = 0 ; i < targets.itemlist.length ; i++ ){
      var id = targets.itemlist[i].id;
      sources.setDefaultHover(id, targetMode);
   }
};


var colours = {};
function parseColourTable(){
   // figure out colours:
   var colourtable = document.getElementById("colourtable").value;
   colours = {"unmatched":"#000000"};
   var colourtablerows = parseTable(colourtable);
   for(var i=1; i<colourtablerows.length; i++){
      if(! colourtablerows[i][0] ) continue;
      // Type	Use	Handedness	Colour	Formula
      if( namedColours[colourtablerows[i][1]] == null ){ // global hash of named colours
         if(colourtablerows[i][1].match(/^#[0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f]$/))
            colours[colourtablerows[i][0]] = colourtablerows[i][1];
         else 
            alert("Bad colour: " + colourtablerows[i][1]);
      }
      else {
         colours[colourtablerows[i][0]] = namedColours[colourtablerows[i][1]];
      }
   }
}

function resetChecks(){
   // refreshes from table...
   // we need to look up colours, AND whether or not ions are in use!
   var iontable = document.getElementById("iontable").value;
   var iontablerows = parseTable(iontable);
   
   parseColourTable(); // colours now in colours{}
   
   // swap the orientation!
   
   
   var table = [["Ion","Alpha","Beta","Alpha lossy", "Beta lossy"]];
   var subtypes = ["_alpha","_beta","_alpha_lossy","_beta_lossy"];
   
   for(var i=1; i<iontablerows.length; i++){
      if(! iontablerows[i][0] ) continue;
      if(! iontablerows[i][1].match(/y/i) ) continue;
      // new row:
      var type = iontablerows[i][0];
      var row = [type];
      for(var j=0; j<subtypes.length; j++){
         var subtype = type + subtypes[j];
         var colour = colours[subtype];
         row.push([colour,subtype]);
      }
      table.push(row);
   }
   table = transposeTable(table);
   var htmltable = "<table>";
   for(var i=0; i<table.length; i++){
      var row = table[i];
      htmltable += "<tr>";
      for(var j=0; j<row.length; j++){
         var cell = row[j];
         if(typeof(cell) == "object"){
            var colour = cell[0];
            var subtype = cell[1];
            htmltable += "<td style=\"background: "
               +colour
               +";\"><"+"input type=\"checkbox\" id=\"check"
               +subtype+"\" onclick=\"handleChecks2();\" checked=\"checked\" /> </td>";   
         }
         else { // header
            htmltable += "<th>"+cell+"</th>";
         }
      }
      htmltable += "</tr>";
   }
   htmltable += "</table>";
   
   var e = document.getElementById("checkform"); // the place where the checkboxes live!
   e.innerHTML = htmltable;
   
}



function transposeTable(t){
   var tt = [];
   for(var j=0; j<t[0].length; j++){
      tt.push([]);
   }
   // tt is now as tall as t is wide...
   for(var i=0; i<t.length; i++){
      for(var j=0; j<t[0].length; j++){
         tt[j].push(t[i][j]);
      }
   }
   return tt;
}

function handleChecks2(){
   // parseColourTable() has almost definitely been called here, so rely on colours{}
   var modenames = new Array(); // WE'LL COLLECT THE INDICES OF THE NAMES FOR WHICH BOXES ARE CHECKED...
   for(var type in colours){
      var check = document.getElementById("check"+type);
      if(check != null && check.checked == true)
         modenames.push("mode"+type); // e.g. mode0 is for "other" peaks, mode1 is for "beta b-ion" peaks, etc
   }
   // EACH INDEX CORRESPONDS TO A MODE, SO NOW WE ACTIVATE THOSE MODES...
   v.modeScanAll(modenames); // THIS LIGHTS UP THE RELEVANT PEAKS IN THE APPROPRIATE COLOURS
   
		hideOverlappingSubordinates();
   
   logtext("===SVG===\n(after handlechecks2)", 
      document.getElementById("container").innerHTML);
}


function rebuildSpectrum(){
	logtable("mtab",aa.mtab);
	spectra[0] = aa.mtab;
	makespectrum(0);
}










/// from http://blog.vishalon.net/index.php/javascript-getting-and-setting-caret-position-in-textarea/

function doGetCaretPosition (ctrl) {
	var CaretPos = 0;	// IE Support
	if (document.selection) {
	ctrl.focus ();
		var Sel = document.selection.createRange ();
		Sel.moveStart ('character', -ctrl.value.length);
		CaretPos = Sel.text.length;
	}
	// Firefox support
	else if (ctrl.selectionStart || ctrl.selectionStart == '0')
		CaretPos = ctrl.selectionStart;
	return (CaretPos);
}
function setCaretPosition(ctrl, pos){
	if(ctrl.setSelectionRange)
	{
		ctrl.focus();
		ctrl.setSelectionRange(pos,pos);
	}
	else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
}


/// from http://ajaxian.com/archives/handling-tabs-in-textareas

function checkTab(evt) {
    var tab = "\t";
    var t = evt.target;
    var ss = t.selectionStart;
    var se = t.selectionEnd;
 
    // Tab key - insert tab expansion
    if (evt.keyCode == 9) {
        evt.preventDefault();
               
        // Special case of multi line selection
        if (ss != se && t.value.slice(ss,se).indexOf("n") != -1) {
            // In case selection was not of entire lines (e.g. selection begins in the middle of a line)
            // we ought to tab at the beginning as well as at the start of every following line.
            var pre = t.value.slice(0,ss);
            var sel = t.value.slice(ss,se).replace(/n/g,"n"+tab);
            var post = t.value.slice(se,t.value.length);
            t.value = pre.concat(tab).concat(sel).concat(post);
                   
            t.selectionStart = ss + tab.length;
            t.selectionEnd = se + tab.length;
        }
               
        // "Normal" case (no selection or selection on one line only)
        else {
            t.value = t.value.slice(0,ss).concat(tab).concat(t.value.slice(ss,t.value.length));
            if (ss == se) {
                t.selectionStart = t.selectionEnd = ss + tab.length;
            }
            else {
                t.selectionStart = ss + tab.length;
                t.selectionEnd = se + tab.length;
            }
        }
    }
           
    /* DON'T NEED THIS BIT....
    
    // Backspace key - delete preceding tab expansion, if exists
   else if (evt.keyCode==8 && t.value.slice(ss - 4,ss) == tab) {
        evt.preventDefault();
               
        t.value = t.value.slice(0,ss - 4).concat(t.value.slice(ss,t.value.length));
        t.selectionStart = t.selectionEnd = ss - tab.length;
    }
           
    // Delete key - delete following tab expansion, if exists
    else if (evt.keyCode==46 && t.value.slice(se,se + 4) == tab) {
        evt.preventDefault();
             
        t.value = t.value.slice(0,ss).concat(t.value.slice(ss + 4,t.value.length));
        t.selectionStart = t.selectionEnd = ss;
    }
    // Left/right arrow keys - move across the tab in one go
    else if (evt.keyCode == 37 && t.value.slice(ss - 4,ss) == tab) {
        evt.preventDefault();
        t.selectionStart = t.selectionEnd = ss - 4;
    }
    else if (evt.keyCode == 39 && t.value.slice(ss,ss + 4) == tab) {
        evt.preventDefault();
        t.selectionStart = t.selectionEnd = ss + 4;
    }  */
}
