


// GENERIC DRAWING INTERFACE

var count = 0;

function newID(){
   return "id" + (++count);
}

function jbGroup(drawing,x,y){ 
   this.x = x;
   this.y = y;
   this.drawing = drawing;
   this.children = new Array();
}
jbGroup.prototype.line = function(id,x1,y1,x2,y2,colour,strokewidth){
   var l = this.drawing.line(id,this.x+x1,this.y+y1,this.x+x2,this.y+y2,colour,strokewidth);
   this.children.push(l);
   return l;
}
jbGroup.prototype.text = function(id,x1,y1,anchor,angle,colour,style,text){
   var t = this.drawing.text(id,x1+this.x,y1+this.y,anchor,angle,colour,style,text);
   this.children.push(t);
   return t;
}
jbGroup.prototype.rect = function(id,x1,y1,w,h,colour1,colour2,width){
   var r = this.drawing.rect(id,x1+this.x,y1+this.y,w,h,colour1,colour2,width);
   this.children.push(r);
   return r;
}
jbGroup.prototype.move = function(dx,dy){
   for ( var i = 0 ; i < this.children.length ; i ++ ) {
      this.drawing.move(this.children[i],dx,dy);
   }
}
jbGroup.prototype.reposition = function(x,y){
   var dx = x - this.x;
   var dy = y - this.y;
   this.move(dx,dy);
}
jbGroup.prototype.remove = function(){
   for ( var i = 0 ; i < this.children.length ; i ++ ) {
      this.drawing.remove(this.children[i]);
   }
}
jbGroup.prototype.hide = function(){
   for ( var i = 0 ; i < this.children.length ; i ++ ) {
      this.drawing.hide(this.children[i]);
   }
}
jbGroup.prototype.show = function(){
   for ( var i = 0 ; i < this.children.length ; i ++ ) {
      this.drawing.show(this.children[i]);
   }
}
jbGroup.prototype.getBBox = function(){
   var Box = this.drawing.getBBox(this.children[0]);
   var X1 = Box.x;
   var Y1 = Box.y;
   var X2 = X1 + Box.width;
   var Y2 = Y1 + Box.height;
   var mesg = "";
      mesg += "!! " + Box.x + " " + Box.y + " " + Box.width + " " + Box.height + "\n";
      mesg += "!! " + X1 + " " + Y1 + " " + X2 + " " + Y2 + "\n";
   for ( var i = 0 ; i < this.children.length ; i ++ ) {
      var box = this.drawing.getBBox(this.children[i]);
      var x1 = box.x;
      var y1 = box.y;
      var x2 = x1 + box.width;
      var y2 = y1 + box.height;
      if(x1 < X1) X1 = x1;
      if(y1 < Y1) Y1 = y1;
      if(x2 > X2) X2 = x2;
      if(y2 > Y2) Y2 = y2;
    //  mesg += " : " + x1 + " " + y1 + " " + x2 + " " + y2 + "\n";
      mesg += " - " + X1 + " " + Y1 + " " + X2 + " " + Y2 + "\n";
   }
   var myBox = new Object();
   myBox.x = X1;
   myBox.y = Y1;
   myBox.width = X2 - X1;
   myBox.height = Y2 - Y1;
//   alert(mesg + X1 + " " + Y1 + " " + X2 + " " + Y2);
   return myBox;
}


function jbDrawing(drawinginterface){
   this.drawinginterface = drawinginterface;
   this.elements = new Array();
   this.visible = new Array();
}
jbDrawing.prototype.cursor = function(command){
   this.drawinginterface.cursor(command);
}
jbDrawing.prototype.convertXYPoint = function(x,y){
   return this.drawinginterface.convertXYPoint(x,y);
}
jbDrawing.prototype.group = function(x,y){
   var g = new jbGroup(this,x,y);
   return g;
}
jbDrawing.prototype.line = function(id,x1,y1,x2,y2,colour,strokewidth){
   this.elements.push(this.drawinginterface.line(id,x1,y1,x2,y2,colour,strokewidth));
   this.visible.push(1);
   return this.elements.length-1;
}
jbDrawing.prototype.rect = function(id,x1,y1,w,h,colour1,colour2,width){
   this.elements.push(this.drawinginterface.rect(id,x1,y1,w,h,colour1,colour2,width));
   this.visible.push(1);
   return this.elements.length-1;
}
jbDrawing.prototype.text = function(id,x1,y1,anchor,angle,colour,style,text){
   this.elements.push(this.drawinginterface.text(id,x1,y1,anchor,angle,colour,style,text));
   this.visible.push(1);
   return this.elements.length-1;
}
jbDrawing.prototype.move = function(i, dx, dy){
   this.drawinginterface.move(this.elements[i], dx, dy);
}
jbDrawing.prototype.reposition = function(i,x,y){
   var p = this.drawinginterface.getXY(this.elements[i]);
   var dx = x - p.x;
   var dy = y - p.y;
   this.drawinginterface.move(this.elements[i], dx, dy);
}
jbDrawing.prototype.height = function(i, h){
   this.drawinginterface.height(this.elements[i], h);
}
jbDrawing.prototype.width = function(i, h){
   this.drawinginterface.width(this.elements[i], h);
}
jbDrawing.prototype.remove = function(i){
   this.drawinginterface.remove(this.elements[i]);
}
jbDrawing.prototype.removeAll = function(){
   this.drawinginterface.removeAll();
   this.elements = new Array();
   this.visible = new Array();
}
jbDrawing.prototype.hide = function(i){
   this.drawinginterface.hide(this.elements[i]);
   this.visible[i] = 0;
}
jbDrawing.prototype.show = function(i){
   this.drawinginterface.show(this.elements[i]);
   this.visible[i] = 1;
}
jbDrawing.prototype.isVisible = function(i){
   return this.visible[i];
}

jbDrawing.prototype.changeText = function(i,text){
   this.drawinginterface.changeText(this.elements[i],text);
}


jbDrawing.prototype.hasElement = function(i){
   if(this.elements[i] == null)
      return 0;
   return 1;
}

jbDrawing.prototype.getBBox = function(i){
   return this.drawinginterface.getBBox(this.elements[i]);
}
jbDrawing.prototype.recolour = function(i,colour){
   return this.drawinginterface.recolour(this.elements[i],colour);
}
jbDrawing.prototype.setStyle = function(i,style){
   return this.drawinginterface.setStyle(this.elements[i],style);
}
jbDrawing.prototype.setStrokeWidth = function(i,sw){
   return this.drawinginterface.setStrokeWidth(this.elements[i],sw);
}




/* EXAMPLE:

var svg = addS
var svg = addSVG();
var di = new jbSvgDrawingInterface(document, svg);
var d = new jbDrawing(di);

var g = d.group(10,10);
var g2 = d.group(10,10);

var l = g.line("id1",20,22,100,22,"green",1);
g.text("id2",20,20,"start","blue","Some text");

g2.rect("id3",60,60,50,20,"yellow","black",5);
g2.line("id4",60,60,110,80,"red",3);

// uncomment lines to see what happens...

//g.move(100,0);
//g2.move(0,20);
//d.move(l,20,20);
//g2.remove();
//d.hide(l);
//d.show(l);
//g2.hide(l);
//g2.show(l);


*/

