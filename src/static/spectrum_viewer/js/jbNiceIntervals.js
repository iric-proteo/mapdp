 

function niExample(){
   var html = "";
   var maxn = 0;
   for(var j = 0; j < 100; j++){
      var x1 = 2000*Math.random();
      var dx = Math.pow(10, 8*Math.random()-4);
      var x2 = x1 + dx;
      var ni = new jbNiceIntervals(x1,x2);
            
      html += "<table width=\"100%\"><tr><td><strong><em>"+ni.x1.toFixed(6) + "</em></strong></td>";
      for(var i = 0; i < ni.x.length; i ++){
         html += "<td>" + ni.x[i] + "</td>";
      }
      html += "<td align=\"right\"><strong><em>"+ni.x2.toFixed(6) + "</em></strong></td></tr></table>";
      if(ni.n > maxn)
         maxn = ni.n;
   }
   var div = document.createElement("div");
   div.innerHTML = "Maximum number of major ticks: " + maxn + html;
   document.getElementById("container").appendChild(div);
}

function jbNiceIntervals(x1,x2){
   this.x1 = x1;
   this.x2 = x2;
   var dx = x2 - x1;
   var mag = log10(dx);
   var imag = Math.floor(mag); // luckily, flooring negative number still makes them decrease.
   var rem = mag - imag; // so this will stil be correct, e.g. -0.7 -> -1, 0.3 ... hooray!
   var multiplier = 2;
   var subdivider = 4;
   if(rem < log10(1.5)){       // 10..15
      multiplier = 0.2;      // in 2s
      subdivider = 4;        // with 0.5s
   }
   else if (rem < log10(4)){ // 15..40
      multiplier = 0.5       // in 5s
      subdivider = 5;        // with 1s
   }
   else if (rem < log10(8)){ // 40..80
      multiplier = 1;        // in 10s
      subdivider = 5;        // with 2s
   }
   else {                    // 80..100
      multiplier = 2;        // in 20s
      subdivider = 4;        // with 5s
   }
   var interval = correctRoundingError(Math.pow(10, imag) * multiplier);
//   alert(interval);
   
   this.interval = interval;
   this.subdivider = subdivider;
   subinterval = correctRoundingError(interval/subdivider);
   this.subinterval = subinterval;
   this.X1 = (Math.ceil(correctRoundingError(x1/interval)) * interval);
   this.Xn = (Math.floor(correctRoundingError(x2/interval)) * interval);
   this.Y1 = (Math.ceil(correctRoundingError(x1/subinterval)) * subinterval);
   this.Yn = (Math.floor(correctRoundingError(x2/subinterval)) * subinterval);
      
   this.x = new Array();
   this.y = new Array();
   var v = this.X1;
   var i = 0;
   var maxxl = 0;
   while(1){
      v = correctRoundingError(this.X1 + i * interval);
      if(v > x2) break;
      var s = v.toString().replace(/0+$/,"").replace(/^\d+\./,"");
      if(s.length > maxxl)
         maxxl = s.length;
      this.x.push(v);
      i++;
   }
   this.maxxl = maxxl;
   for(var i = 0; i < this.x.length; i++){
      this.x[i] = parseFloat(this.x[i]).toFixed(maxxl);
   }
   
   
   v = this.Y1;
   i = 0;
   while(1){
      v = correctRoundingError(this.Y1 + i * subinterval);
      if(v > x2) break;
      if(! searchArray(this.x, v))
         this.y.push(v);
      i++;
   }
  
//   var mesg = this.X1 + " " + this.Xn + " " + this.Y1 + " " + this.Yn + "\n"
//      + this.x.join(" ") + "\n" + this.y.join(" ")
    
//   alert(mesg);
      
   this.n = this.x.length;
   this.ny = this.y.length;
}

function correctRoundingError(f){
   return parseFloat(f).toFixed(10);
}

function searchArray(array, f){
 //  var mesg = "";
   for(var i = 0; i < array.length; i++){
      c = correctRoundingError(array[i]);
      f = correctRoundingError(f);
  //    mesg += "cmp " + c + ", " + f + "\n";
      if(c == f)
         return 1;
   }
  // alert(mesg);
   return 0;
}

function log10(x){
   return Math.log(x)/Math.log(10);
}

