 

// DRAWING INTERFACE....

function jbSvgDrawingInterface(document,svgElement){
   this.svgElement = svgElement;
   this.doc = document;
   this.namespace = "http://www.w3.org/2000/svg";
 //  this.line = jbSvgDrawingLine;
}

function Point(x,y){
   this.x = x;
   this.y = y;
}

function degrotate(point, about, by){
   var rad = by * Math.PI / 180;
   var vp = new Point(point.x - about.x, point.y - about.y);
   var result = new Point(point.x,point.y);
   if(vp.x == 0 && vp.y == 0) return result;
   var ph = Math.sqrt(vp.x*vp.x + vp.y*vp.y);
   var angle = Math.acos(vp.x/ph);
   if(vp.y < 0) angle = 2* Math.PI - angle;
   angle += rad; // ROTATE!
   result.x = ph * Math.cos(angle)  + about.x;
   result.y = ph * Math.sin(angle)  + about.y;
   return result;
}



jbSvgDrawingInterface.prototype.cursor = function(command){
   this.svgElement.style.cursor = command;
}

jbSvgDrawingInterface.prototype.convertXYPoint = function(x,y){
   var m = this.svgElement.getScreenCTM();
   var p = this.svgElement.createSVGPoint();
   p.x = x;
   p.y = y;
   p = p.matrixTransform(m.inverse());
   p.y = this.svgElement.getAttribute("height") - p.y;
   return p;
}


jbSvgDrawingInterface.prototype.getBBox = function(e){
   var H = this.svgElement.getAttribute("height");
   var box = e.getBBox();
   var x1 = box.x;
   var y1 = box.y;
   var x2 = box.x + box.width;
   var y2 = box.y + box.height;
      
   if(e == "[object SVGTextElement]"){
      var x = e.getAttribute("x");
      var y = e.getAttribute("y");
      var tr = e.getAttribute("transform");
      var trp = tr.split(/[()]/);
      var tra = trp[1].split(/[,\s]+/);
      var rotangle = parseFloat(tra[0]);
      var rotx =  parseFloat(tra[1]);
      var roty =  parseFloat(tra[2]);
 //     alert(x + " " + y + " " + rotangle + " " + rotx + " " + roty);
      
      var maxx = "";
      var maxy = ""; 
      var minx = "";
      var miny = "";
      
      var other = new Array(x,y,rotx,roty);
      var points = new Array(x1,y1,x2,y1,x1,y2,x2,y2);
//      alert(points.join(" ") + "\n" + other.join(" "));
      
      var about = new Point(rotx,roty);
      
      for(var i = 0; i < points.length; i += 2){
         var point = new Point(points[i], points[i+1]);
         var result = degrotate(point,about,rotangle);
         if(maxx == "" || result.x > maxx) maxx = result.x;
         if(maxy == "" || result.y > maxy) maxy = result.y;
         if(minx == "" || result.x < minx) minx = result.x;
         if(miny == "" || result.y < miny) miny = result.y;
      }
      x1 = minx;
      y1 = miny;
      x2 = maxx;
      y2 = maxy;
  /*    
      this.line(newID(), x1,H-y1,x1,H-y2,'#ff0000',1);
      this.line(newID(), x2,H-y1,x2,H-y2,'#ff0000',1);
      this.line(newID(), x1,H-y1,x2,H-y1,'#ff0000',1);
      this.line(newID(), x1,H-y2,x2,H-y2,'#ff0000',1);
    */  
   }
   var myBox = new Object();
   myBox.x = x1;
   myBox.y = H - y2;
   myBox.width = x2-x1;
   myBox.height = y2-y1;
   return myBox;
}

jbSvgDrawingInterface.prototype.line = function(id,x1,y1,x2,y2,colour,width){
   var H = this.svgElement.getAttribute("height");
   var p = this.doc.createElementNS(this.namespace, "line");
   p.setAttribute("x1", x1);
   p.setAttribute("y1", H-y1);
   p.setAttribute("x2", x2);
   p.setAttribute("y2", H-y2);
   p.setAttribute("id", id);
   p.setAttribute("stroke", colour);
   p.setAttribute("stroke-width", width);
   this.svgElement.appendChild(p);
   return p;
}

jbSvgDrawingInterface.prototype.text = function(id,x1,y1,anchor,angle,colour,style,text){
   var H = this.svgElement.getAttribute("height");
   var l = this.doc.createElementNS("http://www.w3.org/2000/svg", "text");
   var y = H-y1;
   if(anchor.match(/right|end/)) anchor = 'end';
   else if(anchor.match(/middle/)) anchor = 'middle';
   else anchor = 'start';
   l.setAttribute("text-anchor", anchor);
   l.setAttribute("x", x1);
   l.setAttribute("y", y);
   l.setAttribute("transform", "rotate("+angle+" "+x1+","+y+")");
   l.setAttribute("id", id);
   l.setAttribute("fill", colour);
   l.setAttribute("style", style);
   l.appendChild(document.createTextNode(text));
   this.svgElement.appendChild(l);
   return l;
}

jbSvgDrawingInterface.prototype.changeText = function(e,text){
   if(e == "[object SVGTextElement]"){
	while(e.hasChildNodes()){
		e.removeChild(e.firstChild);
	}
	e.appendChild(document.createTextNode(text));
   }
   else {
      alert("unhandled object: "+e);
   }
}


jbSvgDrawingInterface.prototype.rect = function(id,x1,y1,w,h,colour1,colour2,width){
   var H = this.svgElement.getAttribute("height");
   var p = this.doc.createElementNS(this.namespace, "rect");
   p.setAttribute("x", x1);
   p.setAttribute("y", H-y1-h);
   p.setAttribute("width", w );
   p.setAttribute("height", h);
   p.setAttribute("id", id);
   p.setAttribute("fill", colour1);
   p.setAttribute("stroke", colour2);
   p.setAttribute("stroke-width", width);
   this.svgElement.appendChild(p);
   return p;
}
jbSvgDrawingInterface.prototype.move = function(e, dx, dy){
   var H = this.svgElement.getAttribute("height");
   if(e == "[object SVGLineElement]"){
      e.setAttribute("x1", parseInt(e.getAttribute("x1"))+dx);
      e.setAttribute("y1", parseInt(e.getAttribute("y1"))-dy);
      e.setAttribute("x2", parseInt(e.getAttribute("x2"))+dx);
      e.setAttribute("y2", parseInt(e.getAttribute("y2"))-dy);
   }
	else if(e == "[object SVGTextElement]"){
		var x = parseInt(e.getAttribute("x"))+dx;
		var y = parseInt(e.getAttribute("y"))-dy;
		e.setAttribute("x", x);
		e.setAttribute("y", y);
		var transform = e.getAttribute("transform");
		if(transform.match(/rotate\(/)){
			transform = transform.replace(/.*rotate\(\s*/,"");
			transform = transform.replace(/[^\d-].*/,"");
			e.setAttribute("transform", "rotate("+transform+" "+x+","+y+")");
		}
	}
   else if(e == "[object SVGRectElement]"){
      e.setAttribute("x", parseInt(e.getAttribute("x"))+dx);
      e.setAttribute("y", parseInt(e.getAttribute("y"))-dy);
   }
   else {
      alert("unhandled object: "+e);
   }
}

jbSvgDrawingInterface.prototype.getXY = function(e){
   var H = this.svgElement.getAttribute("height");
   
   if(e == "[object SVGLineElement]"){
      return new Point(
         parseFloat(e.getAttribute("x1")),
         H - parseFloat(e.getAttribute("y1"))
      );
   }
   else if(e == "[object SVGTextElement]"){
      return new Point(
         parseFloat(e.getAttribute("x")),
         H - parseFloat(e.getAttribute("y"))
      );
   }
   else if(e == "[object SVGRectElement]"){
      return new Point(
         parseFloat(e.getAttribute("x")),
         H - parseFloat(e.getAttribute("y"))
      );
   }
   else {
      alert("unhandled object: "+e);
   }
}

jbSvgDrawingInterface.prototype.height = function(e, h){
   var H = this.svgElement.getAttribute("height");
   if(e == "[object SVGLineElement]"){
      e.setAttribute("y2", parseInt(e.getAttribute("y1"))-h);
   }
   else if(e == "[object SVGRectElement]"){
      var dh = h - e.getAttribute("height");
      e.setAttribute("height", h);
      e.setAttribute("y", e.getAttribute("y")-dh);
   }
   else {
      alert("unhandled object: "+e);
   }
}

jbSvgDrawingInterface.prototype.width = function(e, w){
   var W = this.svgElement.getAttribute("width");
   if(e == "[object SVGLineElement]"){
      e.setAttribute("x2", parseInt(e.getAttribute("x1"))+w);
   }
   else if(e == "[object SVGRectElement]"){
      var dw = w - e.getAttribute("width");
      e.setAttribute("height", w);
      e.setAttribute("x", e.getAttribute("x")+dw);
   }
   else {
      alert("unhandled object: "+e);
   }
}

jbSvgDrawingInterface.prototype.removeAll = function(){
   while(this.svgElement.firstChild != null){
      this.svgElement.removeChild(this.svgElement.firstChild);
   }
}

jbSvgDrawingInterface.prototype.remove = function(e){
   this.svgElement.removeChild(e);
}
jbSvgDrawingInterface.prototype.hide = function(e){
   e.style.visibility = "hidden";
}
jbSvgDrawingInterface.prototype.show = function(e){
   e.style.visibility = "inherit";
}
jbSvgDrawingInterface.prototype.recolour = function(e,colour){
   if(e == "[object SVGLineElement]"){
      e.setAttribute("stroke",colour);
   }
   else if(e == "[object SVGTextElement]"){
      e.setAttribute("fill",colour);
   }
   else if(e == "[object SVGRectElement]"){
      e.setAttribute("fill",colour);
   }
   else {
      alert("unhandled object: "+e);
   }
}

jbSvgDrawingInterface.prototype.setStyle = function(e,style){
   e.setAttribute("style",style);
}

jbSvgDrawingInterface.prototype.setStrokeWidth = function(e,sw){
   e.setAttribute("stroke-width",sw);
}



/*
example:

var svg = addSVG();

var d = new jbSvgDrawingInterface(document, svg);
d.line("id1",5,10,40,40,"green",3);
d.line("id1",60,30,40,40,"red",2);
d.text("id2",20,20,"start","blue","Some text");
d.rect("id1",110,130,50,20,"yellow","black",1);



*/
