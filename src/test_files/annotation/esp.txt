##QueryTarget=1:1-249250621
##GenesInThisRegion=
##KeggPathwayIDs=
##Allele changes listed in the "Alleles" column are expressed in the format of "refAllele>AlternateAllele", for multi-allelic variants, they are expressed as "refAllele>AlternateAllele1;refAllele>AlternateAllele2;..." etc.
##Allele and genotype aliases for INDELs: A1, A2, or An refers to the N-th alternate allele while R refers to the reference allele as listed in the "Alleles" column.
##"~" signs in front of some dbSNP rsIDs indicating the INDELs are approximately mapped to the dbSNP rsIDs by SeattleSeqAnnotation137. They should be considered as suggestions rather than accurate mappings.
##A value of -1 in the GRCh38Position column means the GRCh37 position can not be mapped to the GRCh38 build.
#base(NCBI.37) rsID dbSNPVersion Alleles EuropeanAmericanAlleleCount AfricanAmericanAlleleCount AllAlleleCount MAFinPercent(EA/AA/All) EuropeanAmericanGenotypeCount AfricanAmericanGenotypeCount AllGenotypeCount AvgSampleReadDepth Genes GeneAccession FunctionGVS hgvsProteinVariant hgvsCdnaVariant codingDnaSize ConservationScorePhastCons ConservationScoreGERP GranthamScore Polyphen2(Class:Score) RefBaseNCBI37 ChimpAllele ClinicalInfo FilterStatus OnIlluminaHumanExomeChip GwasPubMedInfo EA-EstimatedAge(kyrs) AA-EstimatedAge(kyrs) GRCh38Position
1:69428 rs140739101 dbSNP_134 T>G G=313/T=6535 G=14/T=3808 G=327/T=10343 4.5707/0.3663/3.0647 GG=92/GT=129/TT=3203 GG=1/GT=12/TT=1898 GG=93/GT=141/TT=5101 110 OR4F5 NM_001005484.1 missense p.(F113C) c.338T>G 918 1.0 0.9 205 probably-damaging:0.999 T T unknown PASS no unknown NA NA 1:69428
